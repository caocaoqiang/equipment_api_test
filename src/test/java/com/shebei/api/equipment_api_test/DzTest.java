package com.shebei.api.equipment_api_test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import redis.clients.jedis.Jedis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes =EquipmentApiTestApplication.class)
public class DzTest extends AbstractTestNGSpringContextTests {

  @Test
    public void  test(){
        Jedis jedis = new Jedis("10.1.20.57", 6379);
        jedis.auth("bFhfIGv5VzPIDEbz");
        //清空数据库

      String aa=jedis.get("[MERCHANT_WITHDRAW_AND_CANCEL]");
      System.out.println("aaa:"+aa);
    }
    @Test
    public void proxyTest(){
        try {
            URL url= new URL("http://www.baidu.com");
            SocketAddress address = new InetSocketAddress("localhost",8090);
            Proxy proxy = new Proxy(Proxy.Type.HTTP,address);
            URLConnection connection= url.openConnection(proxy);
            connection.connect();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            String result= "";
            while ((line=br.readLine())!= null){
                result +=line;
            }
            System.out.println(result);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
