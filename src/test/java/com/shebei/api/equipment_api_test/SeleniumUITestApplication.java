//package com.shebei.api.equipment_api_test;
//
//
//import com.shebei.api.equipment_api_test.selenium.ClientDriver;
//import com.shebei.api.equipment_api_test.selenium.ParkCloud;
//import com.shebei.api.equipment_api_test.selenium.WoApplication;
//import com.shebei.api.equipment_api_test.service.UserCaseExcute;
//import com.shebei.api.equipment_api_test.testNgQuery.UserTestCase;
//import com.shebei.api.equipment_api_test.utils.GetConfig;
//import com.shebei.api.equipment_api_test.webContoller.GetUserCaseController;
//import net.sourceforge.tess4j.ITesseract;
//import net.sourceforge.tess4j.Tesseract;
//import net.sourceforge.tess4j.TesseractException;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
//
//import java.io.File;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes =EquipmentApiTestApplication.class)
//public class SeleniumUITestApplication extends AbstractTestNGSpringContextTests {
//
//    @Autowired
//    ClientDriver clientDriver;
//
//    @Autowired
//    WoApplication woApplication;
//
//    @Autowired
//    ParkCloud parkCloud;
//
//    @Test
//    public void  test01(){
//        woApplication.test();
//      //  parkCloud.test();
//      //  String ss="{  \"config\": {    \"admitId\": \"noPerson\",    \"sceneId\": \"3369000200000192\"  },  \"inputKey\": \"faceUrl,bodyUrl\",  \"jobs\": [    {      \"delay\": 1000,      \"input\": {        \"sceneId\": \"${config.sceneId}\",        \"imageUrl\": \"${task.33690002000004.input.faceUrl}\"      },      \"jobId\": \"3369000200000007\",      \"output\": {        \"faceId\": \"${job.3369000200000007.output.faceId}\",        \"personId\": \"${job.3369000200000007.output.personId}\",        \"score\": \"${job.3369000200000007.output.score}\"      }    },    {      \"delay\": 1000,      \"condition\": [        {          \"paramKey\": \"${job.3369000200000007.output.score}\",          \"max\": \"0.6\",          \"dataType\": \"float\"        }      ],      \"input\": {        \"admitId\": \"${config.admitId}\",        \"imageUrl\": \"${task.33690002000004.input.faceUrl}\"      },      \"jobId\": \"3369000200000004\",      \"output\": {        \"faceId\": \"${job.3369000200000004.output.faceId}\"      }    },    {      \"delay\": 1000,      \"input\": {        \"sceneId\": \"${config.sceneId}\",        \"faceId\": \"${job.3369000200000004.output.faceId}\"      },      \"jobId\": \"3369000200000005\"    },{\"delay\":1000,\"input\":{\"sceneId\":\"${config.sceneId}\",\"imageUrl\":\"${task.33690002000004.input.faceUrl}\"},\"jobId\":\"3369000200000008\"}  ],  \"taskId\": 33690002000004,  \"version\": 1}";
//
//       // System.out.println(ss.replaceAll("\\n",""));
//    }
//
//    @Test
//    public  void  test02(){
//         File imageFile=new File("/Users/weiqiang/IdeaProjects/equipment_api_test/2.png");
//        ITesseract instance= new Tesseract();
//        instance.setDatapath("/usr/local/Cellar/tesseract/4.1.1/share/tessdata");
//        instance.setLanguage("chi_sim");///usr/local/Cellar/tesseract/4.1.1/share/tessdata
//        try{
//            String result= instance.doOCR(imageFile);
//            System.out.println(result);
//        } catch (TesseractException e) {
//            e.printStackTrace();
//        }
//    }
//
//
//
//}
