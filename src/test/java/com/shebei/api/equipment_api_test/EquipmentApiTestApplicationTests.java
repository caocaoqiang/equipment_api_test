package com.shebei.api.equipment_api_test;


import com.shebei.api.equipment_api_test.dao.ApiNameQueryDAO;
import com.shebei.api.equipment_api_test.dao.CaseExcuteResultDAO;
import com.shebei.api.equipment_api_test.dao.ServerJacocoManagerDAO;
import com.shebei.api.equipment_api_test.dao.UserCaseWoDAO;
import com.shebei.api.equipment_api_test.http.HttpClientFormUrlencoded;
import com.shebei.api.equipment_api_test.http.HttpClientGetImage;
import com.shebei.api.equipment_api_test.job.CaseExcuteJob;
import com.shebei.api.equipment_api_test.model.*;
import com.shebei.api.equipment_api_test.service.UserCaseExcute;

import com.shebei.api.equipment_api_test.service.jacoco.JacocoManagerServer;
import com.shebei.api.equipment_api_test.service.vueservice.NavigateNameService;
import com.shebei.api.equipment_api_test.testNgQuery.UserTestCase;
import com.shebei.api.equipment_api_test.utils.GetLinuxTime;
import com.shebei.api.equipment_api_test.webContoller.GetUserCaseController;
import org.jacoco.core.tools.ExecFileLoader;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import com.shebei.api.equipment_api_test.utils.GetConfig;
import org.testng.annotations.Test;

import java.io.*;
import java.net.Socket;
import java.sql.*;
import java.util.List;
import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@SpringBootTest(classes =EquipmentApiTestApplication.class)
public class EquipmentApiTestApplicationTests  extends AbstractTestNGSpringContextTests {

   @Autowired
  public UserCaseExcute userCaseExcute;
    @Autowired
  public    UserTestCase userTestCase;
    @Autowired
    public GetUserCaseController getUserCaseController;

    @Autowired
    public UserCaseWoDAO userCaseWoDAO;

    @Autowired
    public ApiNameQueryDAO apiNameQueryDAO;



    @Autowired
    CaseExcuteResultDAO caseExcuteResultDAO;
    @Autowired
    CaseExcuteJob caseExcuteJob;
    @Autowired
    NavigateNameService navigateNameService;
    @Autowired
    GetLinuxTime getLinuxTime;
    @Autowired
    HttpClientGetImage httpClientGet;

    @Autowired
    HttpClientFormUrlencoded httpClientFormUrlencoded;
    @Autowired
    JacocoManagerServer jacocoManagerServer;
    @Autowired
    ServerJacocoManagerDAO serverJacocoManagerDAO;
   @BeforeMethod
   public  void setCase() {
        String scene=GetConfig.getAddress("scene").trim();
        String adptiveVersion=GetConfig.getAddress("adaptiveVersion").trim();
        userTestCase.setALLUserCases(scene,adptiveVersion);

    }

//查询库的人员_1596524606598
//人脸集合添加脸传null_1596435005506
    @org.testng.annotations.Test(threadPoolSize = 5)
    public void testOneCasea(){

           userCaseExcute.vueExcuteOneCase(34,"test");//39  personGuid


    }

    @Test
    public  void groupAddPersons(){
//        for(int i=0;i<10000;i++){
            userCaseExcute.excute("人体检测_1600681563264_1600682994829");
//        }

    }
//人员比对faceBase64_1_1596445713980
    @org.testng.annotations.Test(threadPoolSize = 1)
    public  void TestSceneCases(){
        userCaseExcute.excute(userTestCase.getPerTestCase());
    }

    @org.testng.annotations.Test
    public void  woCase(){
        UserCaseWo userCaseWo=new UserCaseWo();
        userCaseWo.setScene("抓拍机");
                userCaseWo.setModule("AIOT_Personalize");

       List<UserCaseWoWithBLOBs> listcases=userCaseWoDAO.selectCases(userCaseWo);
       int init=200;
        int count=0;
       for(UserCaseWoWithBLOBs iter:listcases){
           String apiName=iter.getApiName();
           String caseName=iter.getUserCaseName();
         ApiNameQuery apiNameQuery= apiNameQueryDAO.selectByName(apiName);
             String personal="WO_";
          String  caseNameNew=personal+caseName;
           String apiNameNew=personal+apiName;
           apiNameQuery.setApiName(apiNameNew);
           apiNameQuery.setApiCategory("AIOT_Personalize");
           apiNameQuery.setId(null);
           iter.setUserCaseName(caseNameNew);
           iter.setApiName(apiNameNew);
           iter.setModule("AIOT_Personalize");
           iter.setScene("AIOT_Personalize_Outside");
           iter.setAdaptiveVersion("wo-parking");
           Integer seq=iter.getExcuteSequence();
           iter.setExcuteSequence(seq+init);
           iter.setId(null);
           System.out.println("count:"+count);
           count++;

           ApiNameQuery isExist=apiNameQueryDAO.selectByName(apiNameNew);
           if(isExist == null ){
               apiNameQueryDAO.insert(apiNameQuery);
           }

           UserCaseWoWithBLOBs resU=   userCaseWoDAO.selectByUserName(caseNameNew);
           resU.setScene("wo-parking");

           userCaseWoDAO.insert(iter);
         //  userCaseWoDAO.updateByPrimaryKeyWithBLOBs(resU);

       }
        System.out.println("count:"+count);

    }




//    @org.testng.annotations.Test
//    public void seqenceChange(){
//
//        UserCaseWo userCaseWo=new UserCaseWo();
//        userCaseWo.setScene("AIOT_Env");
//        userCaseWo.setModule("AIOT");
//       List<UserCaseWo>  list=userCaseWoDAO.selectCasesBySceneDes(userCaseWo);
//       int count=0;
//       for (UserCaseWo iter:list){
//           int temp=iter.getExcuteSequence();
//           iter.setExcuteSequence(count);
//           userCaseWoDAO.updateByPrimaryKeyWithBLOBs(iter);
//           System.out.println("temp:"+temp +" 改为："+count);
//           count++;
//
//
//       }
//        System.out.println("执行完成："+count);
//    }

     @Test
    public void  copyCase(){


         CaseExcuteResult record=new CaseExcuteResult();

         record.setEnv("线上");
         record.setScene("AIOT_CAS");
         record.setCaseModule("AIOT");
         List<CaseExcuteResultWithBLOBs>   reportList=caseExcuteResultDAO.selectReportByScene(record);

         for(CaseExcuteResultWithBLOBs iter:reportList){
             int seq=Integer.parseInt(iter.getExcuteSequence());
             String name=iter.getCaseName();
             UserCaseWoWithBLOBs userCaseWo=userCaseWoDAO.selectByUserName(name);

             userCaseWo.setExcuteSequence(seq);
             userCaseWoDAO.updateByPrimaryKeyWithBLOBs(userCaseWo);
         }
    }
//
//
//    @Test
//    public void  dealSingle(){
//       String uu="{\n" +
//               "\"pass\":\"1\",\n" +
//               "\"config\":'{\"recModePasswordEnable\": 0}'\n" +
//               "}";
//
//       String deal=uu.replaceAll("\'\\{","\\{");
//         deal=deal.replaceAll("\\}\'","\\}");
//
//
//        System.out.println(deal);
//    }



   @Test
    public void apiGroup(){
//        screenCutService.getScreenInfo();

//       CaseExcuteJob caseExcuteJob1= new CaseExcuteJob();
//       caseExcuteJob1.aiOTExcute();
//       caseExcuteJob1.woTuExcute();
//       caseExcuteJob.aiOTExcute();
     //  navigateNameService.insertNaviget();
//       navigateNameService.getList();
       String time=getLinuxTime.getTime();
       System.out.println(time);

    }

    @Test
    public void  navegatInsert(){
        navigateNameService.insertNaviget();
    }

    @Test
    public void  httpGet(){
      //  System.out.println("[\"{\\\\\"appId\\\\\":18560002,\\\\\"attributes\\\\\":\\\\\"{\\\\\\\\\\\\\"consumer\\\\\\\\\\\\\":false,\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\":1622209651309,\\\\\\\\\\\\\"dataMsg\\\\\\\\\\\\\":\\\\\\\\\\\\\"PwVYFQUcFS8vAAAAAAbLYwAFrQQFxQIBAAAIAAA=\\\\\\\\\\\\\",\\\\\\\\\\\\\"deviceModel\\\\\\\\\\\\\":{\\\\\\\\\\\\\"appId\\\\\\\\\\\\\":18560002,\\\\\\\\\\\\\"channelId\\\\\\\\\\\\\":0,\\\\\\\\\\\\\"deviceId\\\\\\\\\\\\\":18560002001662,\\\\\\\\\\\\\"deviceNo\\\\\\\\\\\\\":\\\\\\\\\\\\\"4130136\\\\\\\\\\\\\",\\\\\\\\\\\\\"did\\\\\\\\\\\\\":18560002001662,\\\\\\\\\\\\\"emptyAppId\\\\\\\\\\\\\":false,\\\\\\\\\\\\\"emptyDevcieNo\\\\\\\\\\\\\":false,\\\\\\\\\\\\\"emptyDeviceId\\\\\\\\\\\\\":false,\\\\\\\\\\\\\"emptySceneId\\\\\\\\\\\\\":true,\\\\\\\\\\\\\"emptyUserId\\\\\\\\\\\\\":false,\\\\\\\\\\\\\"productKey\\\\\\\\\\\\\":\\\\\\\\\\\\\"\\\\\\\\\\\\\",\\\\\\\\\\\\\"serverIp\\\\\\\\\\\\\":0,\\\\\\\\\\\\\"source\\\\\\\\\\\\\":510004,\\\\\\\\\\\\\"status\\\\\\\\\\\\\":0,\\\\\\\\\\\\\"uid\\\\\\\\\\\\\":1856,\\\\\\\\\\\\\"unifiedId\\\\\\\\\\\\\":\\\\\\\\\\\\\"183681026261593653259301\\\\\\\\\\\\\",\\\\\\\\\\\\\"userId\\\\\\\\\\\\\":1856,\\\\\\\\\\\\\"version\\\\\\\\\\\\\":\\\\\\\\\\\\\"\\\\\\\\\\\\\"},\\\\\\\\\\\\\"deviceNo\\\\\\\\\\\\\":\\\\\\\\\\\\\"4130136\\\\\\\\\\\\\",\\\\\\\\\\\\\"endCode\\\\\\\\\\\\\":-869022916,\\\\\\\\\\\\\"headCode\\\\\\\\\\\\\":-23206,\\\\\\\\\\\\\"length\\\\\\\\\\\\\":33,\\\\\\\\\\\\\"linkTime\\\\\\\\\\\\\":1622209651309,\\\\\\\\\\\\\"sendTime\\\\\\\\\\\\\":1622209651309,\\\\\\\\\\\\\"sourceType\\\\\\\\\\\\\":\\\\\\\\\\\\\"CLOUD_DATA\\\\\\\\\\\\\",\\\\\\\\\\\\\"spenId\\\\\\\\\\\\\":0,\\\\\\\\\\\\\"type\\\\\\\\\\\\\":16,\\\\\\\\\\\\\"valid\\\\\\\\\\\\\":20,\\\\\\\\\\\\\"version\\\\\\\\\\\\\":1}\\\\\",\\\\\"channelId\\\\\":0,\\\\\"deviceId\\\\\":18560002001662,\\\\\"deviceNo\\\\\":\\\\\"4130136\\\\\",\\\\\"deviceVersion\\\\\":\\\\\"\\\\\",\\\\\"endTime\\\\\":1622209651309,\\\\\"eventId\\\\\":4755490165872742399,\\\\\"eventType\\\\\":52,\\\\\"productKey\\\\\":\\\\\"\\\\\",\\\\\"requestId\\\\\":\\\\\"C0A806C316222075001241000p4078\\\\\",\\\\\"startTime\\\\\":1622209651309,\\\\\"storageObject\\\\\":\\\\\"\\\\\",\\\\\"type\\\\\":1,\\\\\"unifiedId\\\\\":\\\\\"\\\\\",\\\\\"userId\\\\\":1856}\"]");
//      String res=  httpClientGet.sendRequest("http://192.168.6.140:8080/public/account/captcha/xinyue",null,null);
//        System.out.println("res:"+res);
            String ss="File||wewewe";
            String[] ee=ss.split("\\|\\|");
        System.out.println(ee.length);

            File file= new File("D:\\code_automation\\ideaProject\\equipment_api_test\\src\\main\\resources\\yidong.txt");
        System.out.println(file);
        try {
            FileInputStream fileInputStream=   new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("11");


    }
    @Test
    public void mkfile(){

        try {
        BufferedWriter out = new BufferedWriter(new FileWriter("D:\\二十万数据.txt"));
        Long num = 12000000000L;
        for(int i = 0;i<200000;i++){
            num = num+1;
            out.write(String.valueOf(num));
            out.write("\r\n");
        }
        out.close();
        System.out.println("文件创建成功！");
    } catch (IOException e) {
    }
    }

    @Test
    public void selectUser() {
        Socket socket = new Socket();
        String FILEPATH = "D:\\shareUbuntu";
        String EXEC = ".exec";
        // 获取文件夹
        File mergeDir = new File(FILEPATH);
        // 获取文件名称集合
        File[] files = mergeDir.listFiles();
        // 取指定后缀名为.exec的文件
        files = Stream.of(files).filter(file -> file.getAbsolutePath().endsWith(EXEC)).toArray(File[]::new);
        // create loader
        ExecFileLoader loader = new ExecFileLoader();
        try {
            for (File file : files) {
                loader.load(file);
                loader.save(socket.getOutputStream());
            }

        } catch (final IOException e) {
            e.printStackTrace();
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
    @Test
    public void trggerUnitCover(){
        ServerJacocoManager   serverJacocoManager= serverJacocoManagerDAO.selectByPrimaryKey(3);
        serverJacocoManager.setNowVersion("feature/2.2.0");
        serverJacocoManager.setBaseVersion("develop");
//        serverJacocoManager.setNowVersion("master");
//        serverJacocoManager.setBaseVersion("master");
        serverJacocoManager.setJacocoType("2");
        jacocoManagerServer.triggerUnitCoverage(serverJacocoManager);


    }

    @Test
    public void getUnitCover(){
        String uuid="1634529427870";
        jacocoManagerServer.updateUnitReport(uuid);


    }


    @Test
    public void trggerCover(){
        ServerJacocoManager   serverJacocoManager= serverJacocoManagerDAO.selectByPrimaryKey(2);
        serverJacocoManager.setNowVersion("master");
        serverJacocoManager.setBaseVersion("master");
//        serverJacocoManager.setNowVersion("master");
//        serverJacocoManager.setBaseVersion("master");
        serverJacocoManager.setJacocoType("1");
        jacocoManagerServer.triggerCoverage(serverJacocoManager);


    }

    @Test
    public void gettrggerCover(){
        String uuid="1634527111659";
        jacocoManagerServer.updateReport(uuid);


    }

    @Test
    public void trggerVersionCover(){
        ServerJacocoManager   serverJacocoManager= serverJacocoManagerDAO.selectByPrimaryKey(2);
        serverJacocoManager.setNowVersion("master");
        serverJacocoManager.setBaseVersion("master");
//        serverJacocoManager.setNowVersion("master");
//        serverJacocoManager.setBaseVersion("master");
        serverJacocoManager.setJacocoType("1");
        jacocoManagerServer.triggerCoverage(serverJacocoManager);


    }


}
