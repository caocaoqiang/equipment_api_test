CREATE TABLE `CALLBACK_PATH` (
    `ID` INT(11) NOT NULL AUTO_INCREMENT, 
    `CALLBACK_SCENE` VARCHAR(128) CHARACTER SET UTF8 COLLATE UTF8_GENERAL_CI DEFAULT NULL, 
    `PATH` VARCHAR(255) CHARACTER SET UTF8 COLLATE UTF8_GENERAL_CI DEFAULT NULL, 
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = INNODB DEFAULT CHARSET = UTF8 ROW_FORMAT = DYNAMIC