CREATE TABLE `SUPER_JACOCO` (
    `ID` INT(11) NOT NULL AUTO_INCREMENT, 
    `MODULE_NAME` VARCHAR(64) CHARACTER SET UTF8 COLLATE UTF8_GENERAL_CI NOT NULL, 
    `CREATE_TIME` VARCHAR(64) CHARACTER SET UTF8 COLLATE UTF8_GENERAL_CI NOT NULL, 
    `JACOCO_SERVER` VARCHAR(255) CHARACTER SET UTF8 COLLATE UTF8_GENERAL_CI NOT NULL, 
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = INNODB AUTO_INCREMENT = 3 DEFAULT CHARSET = UTF8 ROW_FORMAT = DYNAMIC