CREATE TABLE `DUBBO_CASES` (
    `USER_CASE_NAME` VARCHAR(255) CHARACTER SET UTF8 COLLATE UTF8_GENERAL_CI DEFAULT NULL, 
    `SERVER_NAME` VARCHAR(255) CHARACTER SET UTF8 COLLATE UTF8_GENERAL_CI DEFAULT NULL, 
    `MODULE_NAME` VARCHAR(255) CHARACTER SET UTF8 COLLATE UTF8_GENERAL_CI DEFAULT NULL, 
    `REQUEST_BODY` VARCHAR(255) CHARACTER SET UTF8 COLLATE UTF8_GENERAL_CI DEFAULT NULL, 
    `SCENE` VARCHAR(255) CHARACTER SET UTF8 COLLATE UTF8_GENERAL_CI DEFAULT NULL
) ENGINE = INNODB DEFAULT CHARSET = UTF8 ROW_FORMAT = DYNAMIC