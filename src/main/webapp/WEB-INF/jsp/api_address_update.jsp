<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <title>http接口修改</title>
    <link rel="shortcut icon" href="../img/ico.png"  type="image/x-icon">
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/json.format.js"></script>
</head>
<body>
<div align="center">
    <h2>http接口修改</h2>
    <form action="update_api.do" method="post" id="clean_data_update" enctype="multipart/form-data">
        接口ID：<input type="text" name="id" readonly="readonly" style="background:#CCCCCC"
                    value="${list.id}" size="50"/>
        <br/>
        接口名称：<input type="text" name="apiName"   style="background:#CCCCCC"
                    value="${list.apiName}" size="50"/>

        <br/>
        接口地址：<input type="text" name="apiValue"   value="${list.apiValue}"
                    size="50"/>
        <br/>
        contentType：<input type="text" id="apiType" name="apiType" value="${list.apiType}" size="50"/>
        <br/>
        请求方法：<input type="text" id="requestType" name="requestType" value="${list.requestType}" size="50"/>
        <br/>
        <br/>
        接口类型：<input type="text" id="apiCategory" name="apiCategory" value="${list.apiCategory}" size="50"/>
        <br/>

        接口描述：<input type="text" id="describe" name="describe" value="${list.describe}" size="50"/>
        <br/>
        <input type="submit" value="提交">
    </form>
    <br/>
     请输入head信息
    <br/>
    <textarea rows="10" cols="80" name="headInfo" id="headInfo" form="clean_data_update">${list.headInfo}</textarea>
    <br/>
</div>
</body>
</html>
