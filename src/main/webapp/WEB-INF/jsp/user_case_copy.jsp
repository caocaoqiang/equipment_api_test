<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <title>用例复制界面</title>
    <link rel="shortcut icon" href="../img/ico.png"  type="image/x-icon">
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/json.format.js"></script>
</head>
<body>
<div align="center">
    <h2>用例复制界面</h2>
    <form action="copy_user_case.do" method="post" id="copy_user_case" enctype="multipart/form-data">
        用例名称：<input type="text" id="userCaseName" name="userCaseName" value="${list.userCaseName}" size="50"/>
        <br/>
        接口名字：<input type="text" id="apiName" name="apiName" value="${list.apiName}" size="50"/>
        <br/>
        执行顺序：<input type="text" id="excuteSequence" name="excuteSequence" value="${list.excuteSequence}" size="50"/>
        <br/>
        所属场景：<input type="text" id="scene" name="scene" value="${list.scene}" size="50"/>
        <br/>
        平台类型：<input type="text" id="module" name="module" value="${list.module}" size="50"/>
        <br/>
        <br/>
        适用版本：<input type="text" id="adaptiveVersion" name="adaptiveVersion" value="${list.adaptiveVersion}" size="50"/>
        <br/>
        <input type="submit" value="提交">
    </form>
    请修改body数据
    <br/>
    <textarea rows="10" cols="80" name="requestBody" id="requestBody" form="copy_user_case">${list.requestBody}</textarea>
    <br/>
    请输入检测点
    <br/>
    <textarea rows="10" cols="80" name="checkPath" id="checkPath" form="copy_user_case">${list.checkPath}</textarea>
    <br/>
    请输入需要保存的变量
    <br/>
    <textarea rows="10" cols="80" name="parameterSavePath" id="parameterSavePath" form="copy_user_case">${list.parameterSavePath}</textarea>
</div>
</body>
</html>
