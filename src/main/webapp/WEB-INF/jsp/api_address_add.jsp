<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <title>http接口地址添加</title>
    <link rel="shortcut icon" href="../img/ico.png" type="image/x-icon">
</head>
<body>
<div align="center">
    <h2>http接口地址添加</h2>
    <form action="add_one_api" method="post" id="add_one_api" enctype="multipart/form-data">
        <br/>
        接口类型：<input type="text" id="apiCategory" name="apiCategory" value="${list.apiCategory}" size="50"/>
        <br/>
        接口名称：<input type="text" id="apiName" name="apiName" value="${list.apiName}"
                    size="50"/>
        <br/>
        接口路径：<input type="text" id="apiValue" name="apiValue" value="${list.apiValue}" size="50"/>
        <br/>
        contentType：<input type="text" id="apiType" name="apiType" value="${list.apiType}" size="50"/>
        <br/>
        请求方法：<input type="text" id="requestType" name="requestType" value="${list.requestType}" size="50"/>
        <br/>
        <br/>
        接口描述：<input type="text" id="describe" name="describe" value="${list.describe}" size="50"/>
        <br/>
        <input type="submit" value="提交">

    </form>
    请输入head信息
    <br/>
    <textarea rows="10" cols="80" name="headInfo" id="headInfo" form="add_one_api">${list.headInfo}</textarea>
    <br/>
</div>
</body>
</html>
