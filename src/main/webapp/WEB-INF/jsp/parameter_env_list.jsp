<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="<%=basePath%>">
    <title>报表测试数据</title>
    <link rel="shortcut icon" href="../img/ico.png" type="image/x-icon">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
        function add() {
            window.location = "getParaEnvAddWeb"
        }

    </script>
</head>
<body>
<a href="#">返回到首页</a>
<br/>
<br/>
<input type="button" value="获取总条数" id="btn">
<h2 id="count"></h2>
<div >
    <form   action="query_para_env.do" method="get" id="list">
        <div align="center">
            <h2 align="center">环境参数配置</h2>
            变量名字：<input type="text" name="parameterName"/>
            所属场景scene：<input type="text" name="scene"/>
            所属环境：<input type="text" name="env"/>
            所属平台：<input type="text" name="module"/>


            <input type="submit" value="查询"/>
            <input type="button" value="添加" onclick="add()"/>

        </div>

        <table border="2" align="center" >
            <tr bgcolor="#a52a2a">
                <th>变量id</th>
                <th>变量名字</th>
                <th>所属场景</th>
                <th>所属平台</th>
                <th>所属环境</th>
                <th>操作</th>

            </tr>
            <c:forEach items="${list}" var="EnviromentParameter">
                <tr bgcolor=#C7EDCC style=" width: 200px; background-color: #f5f5f5; position: relative" class="first-table">
                    <td>
                        <nobr>${EnviromentParameter.id}</nobr>
                    </td>
                    <td>
                        <nobr>${EnviromentParameter.parameterName}</nobr>
                    </td>
                    <td>
                        <nobr>${EnviromentParameter.scene}</nobr>
                    </td>
                    <td>
                        <nobr>${EnviromentParameter.module}</nobr>
                    </td>
                    <td>
                        <nobr>${EnviromentParameter.env}</nobr>
                    </td>
                    <td bgcolor="aqua" align="center">
                        <nobr><a href="load_paraEnv_update?id=${EnviromentParameter.id}">修改</a></nobr>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </form>
</div>
</body>
</html>
