<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <title>环境变量修改</title>
    <link rel="shortcut icon" href="../img/ico.png"  type="image/x-icon">
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/json.format.js"></script>
</head>
<body>
<div align="center">
    <h2>环境变量修改</h2>
    <form action="update_paramEnv.do" method="post" id="param_env_update" enctype="multipart/form-data">
        变量ID：<input type="text" name="id" readonly="readonly" style="background:#CCCCCC"
                    value="${list.id}" size="50"/>
        <br/>
        所属平台：<input type="text" name="module"  style="background:#CCCCCC"
                    value="${list.module}" size="50"/>
        <br/>
        所属场景scene：<input type="text" id="scene" name="scene" value="${list.scene}" size="50"/>
        <br/>
        所属环境：<input type="text" name="env"   value="${list.env}"
                    size="50"/>
        <br/>
        变量名字：<input type="text" name="parameterName"   value="${list.parameterName}"
                    size="50"/>
        <br/>
        <input type="submit" value="提交">
    </form>
    变量值:
    <br/>
    <textarea rows="30" cols="80" name="parameterValue" id="parameterValue" form="param_env_update">${list.parameterValue}</textarea>
    <br/>
</div>
</body>
</html>
