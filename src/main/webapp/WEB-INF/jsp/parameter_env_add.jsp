<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <title>环境变量新增</title>
    <link rel="shortcut icon" href="../img/ico.png" type="image/x-icon">
</head>
<body>
<div align="center">
    <h2>环境变量新增</h2>
    <form action="add_parameterEnv" method="post" id="add_parameterEnv" enctype="multipart/form-data">
        <br/>
        平台类型：<input type="text" id="module" name="module" value="${list.module}" size="50"/>
        <br/>
        所属场景scene：<input type="text" id="scene" name="scene" value="${list.scene}"
                    size="50"/>
        <br/>
        所属环境：<input type="text" id="env" name="env" value="${list.env}"
                         size="50"/>
        <br/>
        变量名字：<input type="text" id="parameterName" name="parameterName" value="${list.parameterName}" size="50"/>
        <br/>
        <input type="submit" value="提交">

    </form>
    变量值:
    <br/>
    <textarea rows="30" cols="80" name="parameterValue" id="parameterValue" form="add_parameterEnv">${list.parameterValue}</textarea>
    <br/>
</div>
</body>
</html>
