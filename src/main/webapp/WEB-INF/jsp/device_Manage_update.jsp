<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <title>设备借还记录更新</title>
    <link rel="shortcut icon" href="../img/ico.png" type="image/x-icon">
</head>
<body>
<div align="center">
    <h2>设备借还记录添加</h2>
    <form action="device_borrow_record_update" method="post" id="device_borrow_record_update" enctype="multipart/form-data">
        <br/>
        设备名字：<input type="text" id="name" name="name" value="${list.name}" size="50"/>
        <br/>
        设备序列号：<input type="text" id="deviceno" name="deviceno" value="${list.deviceno}"
                    size="50"/>
        <br/>
        设备目前所借人员：<input type="text" id="borrowName" name="borrowName" value="${list.borrowName}" size="50"/>
        <br/>
        设备所属人员：<input type="text" id="belongName" name="belongName" value="${list.belongName}" size="50"/>
        <br/>
        接口描述：<input type="text" id="describe" name="describe" value="${list.describe}" size="50"/>
        <br/>
        <input type="submit" value="提交">

    </form>
</div>
</body>
</html>
