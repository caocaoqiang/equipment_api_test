<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="<%=basePath%>">
    <title>报表测试数据</title>
    <link rel="shortcut icon" href="../img/ico.png" type="image/x-icon">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
        function add() {
            window.location = "addApi"
        }

    </script>
</head>
<body>
<a href="#">返回到首页</a>
<br/>
<br/>
<input type="button" value="获取总条数" id="btn">
<h2 id="count"></h2>
<div>
    <form action="query_report.do" method="get" id="list">
        <div align="center">
            <h2 align="center">http接口配置</h2>
            接口名字：<input type="text" name="apiName"/>
            接口地址：<input type="text" name="apiValue"/>
            接口类型：<input type="text" name="apiCatogrory"/>


            <input type="submit" value="查询"/>
            <input type="button" value="添加" onclick="add()"/>

        </div>

        <table border="2" align="center">
            <tr bgcolor="#a52a2a">
                <th>接口id</th>
                <th>接口名字</th>
                <th>接口地址</th>
                <th>接口类型</th>
                <th>contentType</th>
                <th>请求方法</th>
                <th>head信息</th>
                <th>描述</th>
                <th>操</th>
                <th>作</th>

            </tr>
            <c:forEach items="${list}" var="ApiNameQuery">
                <tr bgcolor=#C7EDCC>
                    <td>
                        <nobr>${ApiNameQuery.id}</nobr>
                    </td>
                    <td>
                        <nobr>${ApiNameQuery.apiName}</nobr>
                    </td>
                    <td>
                        <nobr>${ApiNameQuery.apiValue}</nobr>
                    </td>
                    <td>
                        <nobr>${ApiNameQuery.apiCategory}</nobr>
                    </td>
                    <td>
                        <nobr>${ApiNameQuery.apiType}</nobr>
                    </td>
                    <td>
                        <nobr>${ApiNameQuery.requestType}</nobr>
                    </td>
                    <td>
                        <nobr>${ApiNameQuery.headInfo}</nobr>
                    </td>

                    <td>
                        <nobr>${ApiNameQuery.describe}</nobr>
                    </td>
                    <td bgcolor="aqua" align="center">
                        <nobr><a href="load_api_update?id=${ApiNameQuery.id}">修改</a></nobr>
                    </td>
                    <td bgcolor="aqua" align="center">
                        <nobr><a href="load_api_copy?id=${ApiNameQuery.id}">复制</a></nobr>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </form>
</div>
</body>
</html>
