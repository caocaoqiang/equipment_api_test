<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="<%=basePath%>">
    <title>设备借还记录</title>
    <link rel="shortcut icon" href="../img/ico.png" type="image/x-icon">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
        function add() {
            window.location = "addDeviceRecord"
        }

    </script>
</head>
<body>
<a href="#">返回到首页</a>
<br/>
<br/>
<input type="button" value="获取总条数" id="btn">
<h2 id="count"></h2>
<div>
    <form action="query_device_manage.do" method="get" id="list">
        <div align="center">
            <h2 align="center">设备借还记录</h2>
            设备名字：<input type="text" name="name"/>
            设备deviceNo：<input type="text" name="deviceno"/>


            <input type="submit" value="查询"/>
            <input type="button" value="添加" onclick="add()"/>

        </div>

        <table border="2" align="center">
            <tr bgcolor="#a52a2a">
                <th>id</th>
                <th>设备名字</th>
                <th>设备deviceNo</th>
                <th>设备所借人员</th>
                <th>设备所属人员</th>
                <th>登记时间</th>
                <th>描述</th>
                <th>操</th>
                <th>作</th>

            </tr>
            <c:forEach items="${list}" var="DeviceManage">
                <tr bgcolor=#C7EDCC>
                    <td>
                        <nobr>${DeviceManage.id}</nobr>
                    </td>
                    <td>
                        <nobr>${DeviceManage.name}</nobr>
                    </td>
                    <td>
                        <nobr>${DeviceManage.deviceno}</nobr>
                    </td>
                    <td>
                        <nobr>${DeviceManage.borrowName}</nobr>
                    </td>
                    <td>
                        <nobr>${DeviceManage.belongName}</nobr>
                    </td>
                    <td>
                        <nobr>${DeviceManage.time}</nobr>
                    </td>
                    <td>
                        <nobr>${DeviceManage.describe}</nobr>
                    </td>
                    <td bgcolor="aqua" align="center">
                        <nobr><a href="load_device_update?id=${DeviceManage.id}">修改</a></nobr>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </form>
</div>
</body>
</html>
