package com.shebei.api.equipment_api_test.testNgQuery;


import com.shebei.api.equipment_api_test.dao.UserCaseWoDAO;
import com.shebei.api.equipment_api_test.model.UserCaseWoWithBLOBs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class UserTestCase {

    @Autowired
    UserCaseWoDAO userCaseWoDAO;


    private static AtomicInteger i = new AtomicInteger(-1);

    private static List<String> testAllCases = new ArrayList<>();


    public void setALLUserCases(String scene ,String adaptiveVersion) {
        UserCaseWoWithBLOBs userCaseWo=new UserCaseWoWithBLOBs();
        userCaseWo.setScene(scene);
        userCaseWo.setAdaptiveVersion(adaptiveVersion);
        testAllCases = userCaseWoDAO.selectCasesByScene(userCaseWo);
    }

    public String getPerTestCase() {
        int temp = i.incrementAndGet();
        return testAllCases.get(temp);
    }
}
