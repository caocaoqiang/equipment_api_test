package com.shebei.api.equipment_api_test.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

@Component
public class TestFilter implements Filter {
    public Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        System.out.println("TestFilter,"+request.getRequestURI());

        request.setCharacterEncoding("UTF-8");

//获得用户请求的URI,并进行截取

        String url= request.getRequestURI();

        String[] path =url.split("/");

// System.out.println(path);

        Enumeration enu=request.getParameterNames();

        String paraString="";

        while(enu.hasMoreElements()){

            String paraName=(String)enu.nextElement();

            paraString=paraString+paraName+":"+request.getParameter(paraName)+"";

        }

        logger.info("用户请求地址为:"+url+"请求参数为:"+paraString);


        //执行
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
