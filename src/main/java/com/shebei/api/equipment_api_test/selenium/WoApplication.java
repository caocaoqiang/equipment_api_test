package com.shebei.api.equipment_api_test.selenium;


import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.http.HttpClientFormUrlencoded;
import com.shebei.api.equipment_api_test.utils.GetLinuxTime;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.testng.Assert;

import java.util.List;

@Component
public class WoApplication {


    @Autowired
    ClientDriver clientDriver;

    @Autowired
    HttpClientFormUrlencoded httpClientFormUrlencoded;

    @Autowired
    UtilSelenium utilSelenium;


    @Autowired
    GetLinuxTime getLinuxTime;

    public ChromeDriver driver=null;

    public String sheBeiName="设备UI";

    public void  test(){
        System.setProperty("webdriver.chrome.driver", "D:\\webDriver\\chromedriver.exe");
        driver=clientDriver.getChrome();
        login();
//        enterApplation();
//        getTeminal();
//
//        configmentClick();
       // driver.close();


    }


    //wo平台登录
    public void login(){
    /*    System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");*/

        String url = "http://test.admin.dazhen.com/user/login?redirect=http%3A%2F%2Ftest.admin.dazhen.com%2Fwelcome";
        driver.get(url);


//        WebElement image=   driver.findElementByXPath("//span[@class=\"el-input__suffix-inner\"]/div/img");
//        String src= image.getAttribute("src");
//        String [] srcList=src.split(",");
//        String base64=srcList[srcList.length-1];
//        System.out.println("base64："+base64);
//        JSONObject parms= new JSONObject();
//        parms.put("imageBase64",base64);
//        String jsonString=parms.toJSONString();
//        String ocrUrl="http://192.168.1.232:8888/getOCRText";
//
//        String yzCode=   httpClientFormUrlencoded.sendRequest(ocrUrl,jsonString,null);
//
//        System.out.println("验证码："+yzCode);
//        System.out.println("验证码："+yzCode.trim());
//        driver.findElementByXPath("//input[@placeholder=\"请输入邮箱账号/手机账号\"]").sendKeys("18368102626");
//        driver.findElementByXPath("//input[@placeholder=\"请输入密码\"]").sendKeys("200407858wq@");
//        driver.findElementByXPath("//input[@placeholder=\"请输入验证码\"]").sendKeys(yzCode.trim());
//        driver.findElementByXPath("//button[@class=\"el-button home__btn el-button--primary\"]").click();
//
//
//        utilSelenium.sleep(5000L);
//        driver.findElementByXPath("//button[@class=\"el-button el-button--default el-button--small el-button--primary \"]").click();
//        utilSelenium.sleep(3000L);
//        driver.findElementByXPath("//span[contains(text(), \"控制台\")]").click();
    }


    public  WebElement  getXpath(String path){
        utilSelenium.sleep(1000L);
      Boolean isExist=  isElementExistByXpath( path);

        Assert.assertTrue(isExist);
        if(isExist){
         return    driver.findElementByXPath(path);
        }

        return null;
    }

    public List<WebElement> getXpaths(String path){
        utilSelenium.sleep(1000L);
        Boolean isExist=  isElementExistByXpath( path);

        Assert.assertTrue(isExist);
        if(isExist){
            return    driver.findElementsByXPath(path);
        }

        return null;
    }

    private boolean isElementExistByXpath(String path){

        boolean isExist = false;
        try {
                   this.driver.findElement(By.xpath(path));
                     isExist = true;
                    } catch (NoSuchElementException nsee) {
                     // this.logger.error(nsee);
                       nsee.printStackTrace();
                    }

                return isExist;
    }

    //进入应用
    public void  enterApplation(){
        utilSelenium.sleep(3000L);
       List<WebElement>  webElements=getXpaths("//span[contains(text(), \"设备管理\")]");

        webElements.get(1).click();
    }


    //查询配置
    public void getTeminal(){
        utilSelenium.sleep(1000L);
        driver.findElementByXPath("//input[@placeholder=\"请输入名称\"]").sendKeys("TV100");
      WebElement table=  driver.findElementByTagName("form");
        utilSelenium.sleep(1000L);
        table.findElement(By.tagName("button")).click();


    }


//修改配置



    public void  configmentClick(){
        utilSelenium.sleep(1000L);
        WebElement table=  driver.findElementByXPath("//div[contains(text(), \"TV100\")]");
        System.out.println(table.getText());




    }





    public void  addAuth(){
        utilSelenium.sleep(3000L);
        getXpath("//input[@placeholder=\"请输入内容\"]").sendKeys("UI自动化1598426076620");
        getXpath("//span[contains(text(), \"查询\")]").click();
        utilSelenium.sleep(1000L);

       WebElement  element= getXpath("//*[@id=\"device-configuration\"]/div/div[2]/div/main[1]/div[1]/div[1]/section/div[2]/div[2]/table/thead/tr/th[1]/div/label/span/span");

       driver.executeScript("arguments[0].click();",element); //选取人员

      WebElement element1=  getXpath("//*[@id=\"device-configuration\"]/div/div[2]/div/main[1]/div[1]/div[3]/div[9]/div/div[1]/div[1]/label/span[1]");
        driver.executeScript("arguments[0].click();",element1);   //获取本地

        getXpath("//span[contains(text(), \"下一步\")]").click();
    //授权 请选择设备


        getXpath("//input[@placeholder=\"请输入名称\"]").sendKeys(sheBeiName);
        getXpath("//span[contains(text(), \"搜索\")]").click();
        getXpath("//div[@class=\"cell\"]/span[contains(text(), \"授权\")]").click();
        getXpath("//button[@class=\"el-button footer__btn el-button--primary\"]/span[contains(text(), \"完成\")]").click();

        utilSelenium.sleep(3000L);


    }




}
