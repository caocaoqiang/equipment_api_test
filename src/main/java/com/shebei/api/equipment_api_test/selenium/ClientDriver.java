package com.shebei.api.equipment_api_test.selenium;

import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.http.HttpClientFormUrlencoded;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Component
public class ClientDriver {

    @Autowired
    HttpClientFormUrlencoded httpClientFormUrlencoded;

    @Autowired
    UtilSelenium utilSelenium;

    public static ChromeDriver driver= null;

    public static ChromeDriver getChrome(){
        if (driver==null){
            driver=new ChromeDriver();
        }

        return driver;
    }



    public  WebElement  getXpath(String path){

        return driver.findElementByXPath(path);
    }


}