package com.shebei.api.equipment_api_test.socket;

import java.net.InetAddress;
import java.net.Socket;
import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;

public class DeviceMsg {

    public static int readSize = 1024*4;
    public  static  String sendMsg(String message) throws Exception{

        int length = message.getBytes().length;
        String backMsg = "";

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Socket socket = new Socket("192.168.4.92", 10000); //IP端口
            OutputStream os = socket.getOutputStream();
            BufferedOutputStream bos = new BufferedOutputStream(os);

//            baos.write(heads.getBytes()); //报文长度转成byte数组  //报文头
           byte[] aa=message.getBytes();
          //  System.out.println("bytes:"+aa );
            byte[] dd=  hexStr2Byte("5A551D0B000C01333341422D543030303031230123230101274523883401F76A69");
           // baos.write(message.getBytes());  //报文体
            baos.write(dd);  //报文体
            baos.close();
            bos.write(baos.toByteArray());
            bos.flush();

            //接收响应报文
            InputStream is = socket.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            baos = new ByteArrayOutputStream();

            //缓冲输入流循环读取一段就写入字节数组输出流
            byte[] buf = new byte[readSize];
            int i = bis.read(buf); //缓冲输入流读取数据存入buf字节数组
            int total = bytes2int(buf); //报文总长，读取报文头前四个字节
            int got = 0;
            while(i != -1){
                System.out.println("等待接收数据:");
                if(i < readSize){
                    byte[] lastByte = new byte[i];
                    baos.write(lastByte);
                } else{
                    baos.write(buf);//buf字节数组写入字节数组输出流
                }
                got += i;
                if(got >= total){
                    break;
                }
                i = bis.read(buf);
            }
            byte[] received = baos.toByteArray();
            backMsg = new String(received);

            baos.close();
            bis.close();
            bos.close();
            socket.close();
        } catch(Exception e){

            e.printStackTrace();
        } finally{
        }

        return backMsg;


    }

    public static int bytes2int(byte[] b){
        int mask = 0xff;
        int temp = 0;
        int res = 0;
        for(int i = 0; i < 4; i++){
            res <<= 8;
            temp = b[i] & mask; //按位与运算,取最低8位，忽略负数
            res |= temp; //按位或运算
        }
        return res;
    }

    public static byte[] int2bytes(int num){
        byte[] b = new byte[4];
        for (int i = 0; i < 4; i++){
            b[i] = (byte)(num >>> (24 - i*8));
        }
        return b;
    }


    /**
     * 字符串转化成为16进制字符串
     * @param s
     * @return
     */
    public static String strTo16(String s) {
        String str = "";
        for (int i = 0; i < s.length(); i++) {
            int ch = (int) s.charAt(i);
            String s4 = Integer.toHexString(ch);
            str = str + s4;
        }
        return str;
    }

    /**
     * 16进制转换成为string类型字符串
     * @param s
     * @return
     */
    public static String hexStringToString(String s) {
        if (s == null || s.equals("")) {
            return null;
        }
        s = s.replace(" ", "");
        byte[] baKeyword = new byte[s.length() / 2];
        for (int i = 0; i < baKeyword.length; i++) {
            try {
                baKeyword[i] = (byte) (0xff & Integer.parseInt(s.substring(i * 2, i * 2 + 2), 16));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            s = new String(baKeyword, "UTF-8");
            new String();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return s;
    }

    public static byte[] hexStr2Byte(String hex) {
        ByteBuffer bf = ByteBuffer.allocate(hex.length() / 2);
        for (int i = 0; i < hex.length(); i++) {
            String hexStr = hex.charAt(i) + "";
            i++;
            hexStr += hex.charAt(i);
            byte b = (byte) Integer.parseInt(hexStr, 16);
            bf.put(b);
        }
        return bf.array();
    }

    public static byte[] getStringbytes(){



        return null;
    }
    public static void main(String[] args) {

//       String mss="##0338QN=20180529092400000;ST=22;CN=2011;PW=w-5044;MN=1440-0028-sclw-5044;Flag=5;CP=&&DataTime=20180529092400;a34002-Rtd=79.4,a34002-Flag=N;a34004-Rtd=50.0,a34004-Flag=N;a01001-Rtd=25.2,a01001-Flag=N;a01002-Rtd=66.0,a01002-Flag=N;a01008-Rtd=303.2,a01008-Flag=N;a01007-Rtd=0.1,a01007-Flag=N;a01006-Rtd=100.6,a01006-Flag=N;LA-Rtd=35.9,LA-Flag=N&&1C80";
//
//        System.out.println("aa");
//       String ss=strTo16(mss);
//        System.out.println("16进:"+ss+"0d0a");
        String response= null;
        try {
            response = sendMsg("123");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(response);

//        String aa= hexStringToString("5A551D0B000C01333341422D543030303031230123230101274523883401F76A69");
//        System.out.println(aa);

//        byte[] dd=  hexStr2Byte("5A551D0B000C01333341422D543030303031230123230101274523883401F76A69");
//        System.out.println(dd);

    }
}
