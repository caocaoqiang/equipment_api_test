package com.shebei.api.equipment_api_test.service.vueservice;


import com.shebei.api.equipment_api_test.dao.UserCaseWoDAO;
import com.shebei.api.equipment_api_test.model.ApiNameQuery;
import com.shebei.api.equipment_api_test.model.UserCaseWo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApiResService {

    @Autowired
    UserCaseWoDAO userCaseWoDAO;



    public void updateCaseApiGroup(ApiNameQuery apiNameQuery){

        String apiName=apiNameQuery.getApiName();
        String apiGroup=apiNameQuery.getApiGroup();
       if(apiName!=null && !apiName.equals("")){
           UserCaseWo userCaseWo=new UserCaseWo();
           userCaseWo.setApiName(apiName);
           userCaseWo.setApiGroup(apiGroup);

           userCaseWoDAO.updateApiGroup(userCaseWo);
       }


    }
}
