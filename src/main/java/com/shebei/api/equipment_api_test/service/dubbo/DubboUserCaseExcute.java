//package com.shebei.api.equipment_api_test.service.dubbo;
//
//import com.alibaba.fastjson.JSONObject;
//import com.shebei.api.equipment_api_test.dao.ApiNameQueryDAO;
//import com.shebei.api.equipment_api_test.dao.CaseExcuteResultDAO;
//import com.shebei.api.equipment_api_test.dao.EnviromentParameterDAO;
//import com.shebei.api.equipment_api_test.dao.UserCaseWoDAO;
//import com.shebei.api.equipment_api_test.dubbo.EventMsg;
//import com.shebei.api.equipment_api_test.dubbo.dubboClient.DubboBase;
//import com.shebei.api.equipment_api_test.dubbo.factory.DubboFactory;
//import com.shebei.api.equipment_api_test.http.HttpClienInterface;
//import com.shebei.api.equipment_api_test.http.HttpClientFactory;
//import com.shebei.api.equipment_api_test.model.ApiNameQuery;
//import com.shebei.api.equipment_api_test.model.CaseExcuteResultWithBLOBs;
//import com.shebei.api.equipment_api_test.model.EnviromentParameter;
//import com.shebei.api.equipment_api_test.model.UserCaseWoWithBLOBs;
//import com.shebei.api.equipment_api_test.model.responseModel.ApiResModel;
//import com.shebei.api.equipment_api_test.selenium.UtilSelenium;
//import com.shebei.api.equipment_api_test.service.*;
//import com.shebei.api.equipment_api_test.utils.GetConfig;
//import com.uniubi.cloud.record.core.model.internal.util.GuidUtil;
//import com.uniubi.cloud.record.facade.request.event.EventRecordRequest;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//
//@Component
//public class DubboUserCaseExcute {
//
//
//    @Autowired
//  private   UserCaseWoDAO userCaseWoDAO;
//
//
//    @Autowired
//   private ParamParsing paramParsing;
//
//
//
//    @Autowired
//    private  CheckPathParsing checkPathParsing;
//    @Autowired
//    private  ParamSave paramSave;
//
//    @Autowired
//    private CaseExcuteResultDAO caseExcuteResultDAO;
//
//    @Autowired
//    UtilSelenium utilSelenium;
//    @Autowired
//    DubboFactory dubboFactory;
//
//
//    @Autowired
//    EnviromentParameterDAO enviromentParameterDAO;
//    @Autowired
//    UserCaseExcute userCaseExcute;
//
//
//
//    //执行一个用例
//    public ApiResModel vueExcuteOneCase(Integer id, String env){
//
//
//
//        UserCaseWoWithBLOBs userCaseWo = userCaseWoDAO.selectByPrimaryKey(id);
//            String userName = userCaseWo.getUserCaseName();
//
//            String checkPath = userCaseWo.getCheckPath();
//            String requestBody = userCaseWo.getRequestBody();
//            String dubboMethod=userCaseWo.getRequestType();
//            String dubboType=userCaseWo.getApiGroup();
//            String scene = userCaseWo.getScene();
//            String saveParam = userCaseWo.getParameterSavePath();
//            String catgoryModule = userCaseWo.getModule();
//            System.out.println("开始执行用例：" + userName + "  模块：" + catgoryModule + " 环境为：" + env);
//        //JSONObject.parseObject(jsonuser,User.class)
//        String bodyValue=paramParsing.parsingValue(requestBody,scene,catgoryModule,env); //解析body请求中的变量。
//
//        DubboBase dubboBase= dubboFactory.getDubboApi(dubboType);
//        String response =null;
//        if(dubboBase==null){
//            response="请配置dubbo接口";
//        }else {
//            response= dubboBase.sendMsg(bodyValue,dubboMethod);
//        }
//
//
//
//        System.out.println("response:" + response);
//        Map<String, String> errorResult = new ConcurrentHashMap<>();
//
//        String compareResult = checkPathParsing.checkResponse(checkPath, response, errorResult, scene, catgoryModule, env); //检查点比对 并记录比对结果到errorResult
//
//        paramSave.save(response, saveParam, scene, catgoryModule, compareResult, env); //关联参数存储；
//        ApiResModel apiResModel=new ApiResModel();
//
//
//        apiResModel.setUrl("");
//        apiResModel.setResponse(response);
//        apiResModel.setRequestBody(bodyValue);
//        apiResModel.setExcuteResult(compareResult);
//
//        return apiResModel;
//    }
//
//    public String sceneExcute(String scene,String module,String env) throws InterruptedException {
//
//        EnviromentParameter enviromentParameter=new EnviromentParameter();
//        enviromentParameter.setModule(module);
//        enviromentParameter.setEnv(env);
//        enviromentParameter.setScene(scene);
//        List<String> sceneList=enviromentParameterDAO.selectScene(enviromentParameter);
//        if(sceneList==null){
//            return "没有场景可以执行，请在环境变量中创建场景";
//        }
//        int count=0;
//        for(String iter:sceneList){
//
//            CaseExcuteResultWithBLOBs caseExcuteResultWithBLOBs=caseExcuteDelBefore( iter, module, env);
//
//            UserCaseWoWithBLOBs userCaseWo=new UserCaseWoWithBLOBs();
//            userCaseWo.setModule(module);
//            userCaseWo.setScene(iter);
//            List<UserCaseWoWithBLOBs>  usercases= userCaseWoDAO.selectCasesVue(userCaseWo);
//
//            for(UserCaseWoWithBLOBs iter1:usercases){
//
//                Date d = new Date();
//                System.out.println(d);
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                String dateNowStr = sdf.format(d);
//                System.out.println("************开始执行用例："+iter1);
//                String caseName=iter1.getUserCaseName();
//                caseExcuteResultWithBLOBs.setCaseName(caseName);
//                caseExcuteResultWithBLOBs.setExcuteTime(dateNowStr);
//                caseExcuteResultWithBLOBs.setExcuteResult("excuting");
//                caseExcuteResultWithBLOBs.setScene(iter);
//                caseExcuteResultWithBLOBs.setExcuteSequence(iter1.getExcuteSequence().toString());
//                caseExcuteResultWithBLOBs.setRequestType(iter1.getRequestType());
//                caseExcuteResultWithBLOBs.setCaseGroup(iter1.getAdaptiveVersion());
//                caseExcuteResultDAO.insert(caseExcuteResultWithBLOBs);
//                sceneExcuteOne(iter1,env,caseExcuteResultWithBLOBs);
//                count++;
//                Integer  timeWait=iter1.getTimeWait();
//                System.out.println("等待实际为："+timeWait);
//                if(timeWait!=null && timeWait!=0){
//                    Thread.currentThread().sleep(timeWait*1000);
//                }
//
//
//
//            }
//        }
//        System.out.println("执行用例数："+count);
//
//        return  "执行用例数："+count;
//    }
//
//
//    private void sceneExcuteOne(UserCaseWoWithBLOBs userCaseWo, String env, CaseExcuteResultWithBLOBs caseExcuteResultWithBLOBs){
//
//        String apiName = userCaseWo.getApiName();
//        String checkPath = userCaseWo.getCheckPath();
//        String requestBody = userCaseWo.getRequestBody();
//        String scene = userCaseWo.getScene();
//        String apiValue=userCaseWo.getApiValue();
//        String saveParam = userCaseWo.getParameterSavePath();
//        String catgoryModule = userCaseWo.getModule();
//        String userName=userCaseWo.getUserCaseName();
//
//
//        System.out.println("dubbo开始执行用例：" + userName + "  模块：" + catgoryModule + " 环境为：" + env);
//        try {
//            String dubboMethod = userCaseWo.getRequestType();
//            String dubboType = userCaseWo.getApiGroup(); //dubbo interface
//
//            System.out.println("开始执行用例：" + userName + "  模块：" + catgoryModule + " 环境为：" + env);
//            //JSONObject.parseObject(jsonuser,User.class)
//            String bodyValue = paramParsing.parsingValue(requestBody, scene, catgoryModule, env); //解析body请求中的变量。
//
//            DubboBase dubboBase = dubboFactory.getDubboApi(dubboType);
//            String response = dubboBase.sendMsg(bodyValue,dubboMethod);
//
//
//            System.out.println("response:" + response);
//            Map<String, String> errorResult = new ConcurrentHashMap<>();
//
//            String compareResult = checkPathParsing.checkResponse(checkPath, response, errorResult, scene, catgoryModule, env); //检查点比对 并记录比对结果到errorResult
//
//            paramSave.save(response, saveParam, scene, catgoryModule, compareResult, env); //关联参数存储；
//
//            caseExcuteResultWithBLOBs.setRequestBody(bodyValue);
//            caseExcuteResultWithBLOBs.setApiVaule(apiValue);
//            caseExcuteResultWithBLOBs.setApiName(apiName);
//            caseExcuteResultWithBLOBs.setErrorMsg(errorResult.toString());
//            caseExcuteResultWithBLOBs.setResponseResult(response);
//            caseExcuteResultWithBLOBs.setExcuteResult(compareResult);
//            caseExcuteResultDAO.updateByPrimaryKeyWithBLOBs(caseExcuteResultWithBLOBs);
//        }catch (Exception e){
//            caseExcuteResultWithBLOBs.setExcuteResult("false");
//            String errMsg=userName +"用例执行异常";
//            caseExcuteResultWithBLOBs.setErrorMsg(errMsg);
//            caseExcuteResultDAO.updateByPrimaryKeyWithBLOBs(caseExcuteResultWithBLOBs);
//
//        }
//
//    }
//    private CaseExcuteResultWithBLOBs caseExcuteDelBefore(String scene,String module,String env){
//
//
//        CaseExcuteResultWithBLOBs caseExcuteResultWithBLOBs = new CaseExcuteResultWithBLOBs();
//        caseExcuteResultWithBLOBs.setScene(scene);
//        caseExcuteResultWithBLOBs.setCaseModule(module);
//        caseExcuteResultWithBLOBs.setEnv(env);
//        caseExcuteResultDAO.deleteByScene(caseExcuteResultWithBLOBs);
//
//        return caseExcuteResultWithBLOBs;
//    }
//
//    private  void isNull(HttpClienInterface httpClient,String userName ){
//
//        if (httpClient ==null){
//            ResultAssertion.assertNull(userName);
//        }
//
//    }
//
//
//    private String getEnv(){
//        return GetConfig.getAddress("env").trim();
//    }
//
//
//
//}
