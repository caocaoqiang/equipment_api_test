package com.shebei.api.equipment_api_test.service.vueservice;

import com.shebei.api.equipment_api_test.dao.CallbackReceiptDAO;
import com.shebei.api.equipment_api_test.model.CallbackReceipt;
import com.shebei.api.equipment_api_test.utils.GetLinuxTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CallBackService {

    @Autowired
    GetLinuxTime getLinuxTime;
    @Autowired
    CallbackReceiptDAO callbackReceiptDAO;

    public void  saveCallBack( Map<String, String> map ,String name){
        CallbackReceipt callbackReceipt=new CallbackReceipt();
        String guid=map.get("guid");
        String msg=map.get("msg");
        String type=map.get("type");
        callbackReceipt.setGuid(guid);
        callbackReceipt.setMsg(msg);
        callbackReceipt.setCallbackScene(name);
        callbackReceipt.setCallbackType(type);
        callbackReceipt.setResponse(map.toString());
        String dataTime=getLinuxTime.getData();
        callbackReceipt.setTimeCreate(dataTime);
        callbackReceiptDAO.insert(callbackReceipt);
    }


    public void  deleteByScene(String scene){
        CallbackReceipt callbackReceipt=new CallbackReceipt();
        callbackReceipt.setCallbackScene(scene);
        callbackReceiptDAO.deleteByScene(callbackReceipt);
    }
}

