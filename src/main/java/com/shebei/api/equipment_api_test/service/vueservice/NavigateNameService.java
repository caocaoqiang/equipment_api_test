package com.shebei.api.equipment_api_test.service.vueservice;


import com.alibaba.fastjson.JSONArray;
import com.shebei.api.equipment_api_test.dao.NavigateNameDAO;
import com.shebei.api.equipment_api_test.dao.UserCaseWoDAO;
import com.shebei.api.equipment_api_test.model.NavigateName;
import com.shebei.api.equipment_api_test.model.responseModel.NavigateNameNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class NavigateNameService {


     @Autowired
     NavigateNameDAO navigateNameDAO;

    @Autowired
    UserCaseWoDAO userCaseWoDAO;
     public void  insertNaviget(){

         NavigateName navigateName=new NavigateName();
         navigateName.setModule("WO");
         navigateName.setScene("WO_Env");
         navigateName.setGroupName("base_group");
         Date now =new Date();
         long timestamp=now.getTime();
          String uniqueNo=String.valueOf(timestamp);
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         String dateNowStr = sdf.format(now );
         navigateName.setTimeCreate(dateNowStr);
         navigateName.setUniqueValue(uniqueNo);
         navigateNameDAO.insert(navigateName);
     }

     public int addGroup( String module ,
                         String scene,
                          String groupName ){


         NavigateName navigateName=new NavigateName();
         navigateName.setModule(module);
         navigateName.setScene(scene);
         navigateName.setGroupName(groupName);
         Date now =new Date();
         long timestamp=now.getTime();
         String uniqueNo=String.valueOf(timestamp);
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         String dateNowStr = sdf.format(now );
         navigateName.setTimeCreate(dateNowStr);
         navigateName.setUniqueValue(uniqueNo);
       return navigateNameDAO.insert(navigateName);
     }
    public int addFatherGroup( String module ,
                         String scene,
                         String groupName ){


        NavigateName navigateName=new NavigateName();
        navigateName.setModule(module);
        navigateName.setScene(scene);
        navigateName.setGroupName(groupName);
        Date now =new Date();
        long timestamp=now.getTime();
        String uniqueNo=String.valueOf(timestamp);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateNowStr = sdf.format(now );
        navigateName.setTimeCreate(dateNowStr);
        navigateName.setUniqueValue(uniqueNo);
        navigateName.setParentId(0);
        return navigateNameDAO.insert(navigateName);
    }


    public int addNode( String module ,
                         String scene,Integer parentId,
                         String groupName ){
        NavigateName parentNode= navigateNameDAO.selectByPrimaryKey(parentId);

        NavigateName navigateName=new NavigateName();
        navigateName.setModule(module);
        navigateName.setScene(parentNode.getScene());
        navigateName.setGroupName(groupName);
        Date now =new Date();
        long timestamp=now.getTime();
        String uniqueNo=String.valueOf(timestamp);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateNowStr = sdf.format(now );
        navigateName.setTimeCreate(dateNowStr);
        navigateName.setUniqueValue(uniqueNo);
        navigateName.setParentId(parentId);
        return navigateNameDAO.insert(navigateName);
    }
     public List<NavigateName> getList(){
       return   navigateNameDAO.selectGroups("WO","WO_Env");
     }
      //组内有用例 不能删除
     public String deleteGroup(String uniqueKey){
        int size= userCaseWoDAO.selectCaseGoup(uniqueKey);
       Boolean childNotExit=childExit(uniqueKey);

        if(size==0&&childNotExit){
            int res=  navigateNameDAO.deleteByUniqueKey(uniqueKey);
            return "success";
        }
        return "failed";
     }

    private boolean childExit(String uniqueKey){
        List<NavigateName>  lists=navigateNameDAO.selectChildisExist(uniqueKey);
        if(lists.size()==0){
            return true; //表示 没有子节点
        }
       return false;
     }


     public int editGroupName(String groupName,String uniqueKey){
         NavigateName navigateName=new NavigateName();
         navigateName.setUniqueValue(uniqueKey);
         navigateName.setGroupName(groupName);
        return navigateNameDAO.updateByUniqueKey(navigateName);
     }

     public String getNote(String module,String scene){
        List<NavigateNameNode>  navigateNameNodes=new ArrayList<NavigateNameNode>();
       List<NavigateName> parents= navigateNameDAO.selectParent(module,scene);
        for(NavigateName iter:parents){
            NavigateNameNode  tmp= new NavigateNameNode();
            tmp.setId(iter.getId());
            tmp.setGroupName(iter.getGroupName());
            tmp.setScene(iter.getScene());
            tmp.setLabel(iter.getGroupName());
            tmp.setUniqueValue(iter.getUniqueValue());
            getChildrens(iter.getId(),tmp);
            navigateNameNodes.add(tmp);

        }
        String res =JSONArray.toJSONString(navigateNameNodes);
         System.out.println(res);
         return res;
     }

     public List<String> getCaseVersionList(String module,String scene){
         List<String> list=new ArrayList<>();
         List<NavigateName> parents= navigateNameDAO.selectParent(module,scene);
         for(NavigateName iter:parents){
;           list.add(iter.getUniqueValue());
             getVersionChildrens(iter.getId(),list);



         }
         return list;
     }
     private void getChildrens(Integer parentId,NavigateNameNode parentNode){
           List<NavigateNameNode>  childs=new ArrayList<NavigateNameNode>();
         List<NavigateName> res= navigateNameDAO.selectChild(parentId);
         if(res.size()==0){
             return;
         }
         for(NavigateName iter:res){
             NavigateNameNode  tmp = new NavigateNameNode();
             tmp.setId(iter.getId());
             tmp.setGroupName(iter.getGroupName());
             tmp.setLabel(iter.getGroupName());
             tmp.setScene(iter.getScene());
             tmp.setUniqueValue(iter.getUniqueValue());
             getChildrens(iter.getId(),tmp);
             childs.add(tmp);
         }

         parentNode.setChildren(childs);

     }

    private void getVersionChildrens(Integer parentId, List<String> list){
        List<NavigateName> res= navigateNameDAO.selectChild(parentId);
        if(res.size()==0){
            return;
        }
        for(NavigateName iter:res){
            list.add(iter.getUniqueValue());
            getVersionChildrens(iter.getId(),list);

        }



    }
}
