package com.shebei.api.equipment_api_test.service;

import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.dao.EnviromentParameterDAO;
import com.shebei.api.equipment_api_test.model.EnviromentParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.Map;


@Component
public class ParamSave {


  @Autowired
  private   EnviromentParameterDAO enviromentParameterDAO;
    @Autowired
    private  JsonKeyParsing jsonKeyParsing;


    public void save(String reponse,String jsonPath,String scene,String catgoryModule,String compareRes,String env){

        if(compareRes.equals("false")){ // 该请求返回结果错误  直接返回 不存储关联参数
            return;
        }

        if(jsonPath==null||jsonPath.equals("")){ //没有关联参数路径 直接返回
            return;
        }
        JSONObject jsonToMap =  JSONObject.parseObject(jsonPath);

        Iterator it =jsonToMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Object> entry = (Map.Entry<String, Object>) it.next();

            String keyPath=entry.getKey();
            String paraName= (String) entry.getValue(); //获取需要存数据库的参数
            String value=jsonKeyParsing.getJsonKey(reponse,keyPath); //从结果中解析出参数值

            EnviromentParameter enviromentParameter=new EnviromentParameter();
            enviromentParameter.setParameterName(paraName);
            enviromentParameter.setScene(scene);
            enviromentParameter.setModule(catgoryModule);
            enviromentParameter.setParameterValue(value);
            enviromentParameter.setEnv(env);
            saveValue(enviromentParameter);
        }
    }



    private void  saveValue(EnviromentParameter enviromentParameter){
        EnviromentParameter selectRes=enviromentParameterDAO.selectByName(enviromentParameter);
        if(selectRes==null){  //环境变量中没有该参数 直接插入
            enviromentParameter.setEnvType("related");  //新增时插入， 性能压测关联参数
            enviromentParameterDAO.insert(enviromentParameter);
            return;
        }

        enviromentParameterDAO.updateByName(enviromentParameter);
    }




}
