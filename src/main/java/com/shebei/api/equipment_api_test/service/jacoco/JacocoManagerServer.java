package com.shebei.api.equipment_api_test.service.jacoco;


import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.dao.JacocoReportDAO;
import com.shebei.api.equipment_api_test.dao.SuperJacocoDAO;
import com.shebei.api.equipment_api_test.http.HttpClientGet;
import com.shebei.api.equipment_api_test.http.HttpClientJson;
import com.shebei.api.equipment_api_test.model.JacocoReport;
import com.shebei.api.equipment_api_test.model.ServerJacocoManager;
import com.shebei.api.equipment_api_test.model.SuperJacoco;
import com.shebei.api.equipment_api_test.utils.GetConfig;
import com.shebei.api.equipment_api_test.utils.GetLinuxTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Component
public class JacocoManagerServer {

    @Autowired
    HttpClientJson httpClientJson;
    @Autowired
    JacocoReportDAO jacocoReportDAO;
    @Autowired
    HttpClientGet httpClientGet;
    @Autowired
    SuperJacocoDAO superJacocoDAO;
    @Autowired
    JacocoTestVersionService jacocoTestVersionService;
    public String triggerCoverage(ServerJacocoManager serverJacocoManager){
        String jacocoType=serverJacocoManager.getJacocoType();
        JSONObject jsonObject=new JSONObject();
        Date now =new Date();
        long timestamp=now.getTime();
       Integer jacocoServerId=serverJacocoManager.getSuperJacocoId();
      // String url=GetConfig.getAddress("jacocoServer").trim()+"/cov/triggerEnvCov";
        SuperJacoco jacocoSever= superJacocoDAO.selectByPrimaryKey(jacocoServerId);
        String url=jacocoSever.getJacocoServer().trim()+"/cov/triggerEnvCov";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateNowStr = sdf.format(now);
        String reportUUid=String.valueOf(timestamp);

        jsonObject.put("uuid",reportUUid);
       if(jacocoType.equals("1")){
           jsonObject.put("type",1); //全量覆盖率
       }else {
           jsonObject.put("type",2); //增量覆盖率
       }

        jsonObject.put("subModule",serverJacocoManager.getSubModule().trim());
        jsonObject.put("baseVersion",serverJacocoManager.getBaseVersion().trim());
        jsonObject.put("nowVersion",serverJacocoManager.getNowVersion().trim());
        jsonObject.put("address",serverJacocoManager.getServerAddress());
        jsonObject.put("port",serverJacocoManager.getPortJacoco());
        jsonObject.put("gitUrl",serverJacocoManager.getGitUrl());
        jsonObject.put("whiteModules",serverJacocoManager.getWhiteModules());
        System.out.println("获取覆盖率:"+url);
        System.out.println("获取覆盖率body:"+jsonObject.toJSONString());
      String reponse=  httpClientJson.sendRequest(url,jsonObject.toJSONString(),null);
        System.out.println("代码覆盖率请求结果：++++++++++++："+reponse);
        addReport( serverJacocoManager, timestamp, dateNowStr,0);  //0 表示环境覆盖率报告 0 表示环境测试报告，1 表示单测报告
        return "success";
    }


    private   void  addReport(ServerJacocoManager serverJacocoManager,long uuid,String dataSt,Integer isUnitTest){
        String jacocoType=serverJacocoManager.getJacocoType();
        JacocoReport jacocoReport=new JacocoReport();
        jacocoReport.setRecordUuid(String.valueOf(uuid));
        jacocoReport.setModuleName(serverJacocoManager.getModuleName());
        jacocoReport.setServerName(serverJacocoManager.getServerName());
        jacocoReport.setBaseVersion(serverJacocoManager.getBaseVersion());
        jacocoReport.setNowVersion(serverJacocoManager.getNowVersion());
        if(jacocoType.equals("1")){
            jacocoReport.setJacocoType("全量覆盖率");
        }else {
            jacocoReport.setJacocoType("增量覆盖率");
        }
        jacocoReport.setUnitTestType(isUnitTest);
        jacocoReport.setCreateTime(dataSt);
        jacocoReport.setReportResult("统计中");
        jacocoReport.setJacocoSeverId(serverJacocoManager.getSuperJacocoId());
        jacocoReportDAO.insert(jacocoReport);

    }


    public int updateReport(String uuid){

       // String url=GetConfig.getAddress("jacocoServer").trim()+"/cov/getEnvCoverResult";
         String url=getJacocoSeverUrl(uuid).trim()+"/cov/getEnvCoverResult";
        JSONObject jsonParam= new JSONObject();

        jsonParam.put("uuid",uuid);
        String   res= httpClientGet.sendGetRequset(url,jsonParam.toJSONString(),null);
       // System.out.println("获取报告response："+res);
        Map param = JSONObject.parseObject(res);
        JSONObject data= (JSONObject) ((JSONObject) param).get("data");

        String errMsg=data.getString("errMsg");
        String reportUrl=data.getString("reportUrl");
        System.out.println("报告获取。。。+" +reportUrl +" error:"+errMsg);
        JacocoReport jacocoReport=new JacocoReport();
        jacocoReport.setRecordUuid(uuid);
        jacocoReport.setReportUrl(reportUrl);
        if(reportUrl!=null && reportUrl.contains("index.html")){

            jacocoReport.setReportResult("success");

        }else {
            jacocoReport.setReportResult(errMsg);
            jacocoReport.setErrMsg(errMsg);
        }
       int res1= jacocoReportDAO.updateByPrimaryKeySelective(jacocoReport);

        return res1;
    }

    private String getJacocoSeverUrl(String uuid){
       JacocoReport report= jacocoReportDAO.selectByPrimaryKey(uuid);
       Integer jacocoId=report.getJacocoSeverId();
       SuperJacoco jacoco= superJacocoDAO.selectByPrimaryKey(jacocoId);
       String url=jacoco.getJacocoServer();
       return url;

    }


    public String triggerVersionCoverage(ServerJacocoManager serverJacocoManager,Integer versionId){
        String jacocoType=serverJacocoManager.getJacocoType();
        JSONObject jsonObject=new JSONObject();
        Date now =new Date();
        long timestamp=now.getTime();
        Integer jacocoServerId=serverJacocoManager.getSuperJacocoId();
        // String url=GetConfig.getAddress("jacocoServer").trim()+"/cov/triggerEnvCov";
        SuperJacoco jacocoSever= superJacocoDAO.selectByPrimaryKey(jacocoServerId);
        String url=jacocoSever.getJacocoServer().trim()+"/cov/triggerVersionEnvCov";
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String reportUUid=String.valueOf(timestamp);
        String lastUuid= jacocoTestVersionService.getLastUUid(serverJacocoManager,versionId);
        jsonObject.put("uuid",reportUUid);
        if(jacocoType.equals("1")){
            jsonObject.put("type",1); //全量覆盖率
        }else {
            jsonObject.put("type",2); //增量覆盖率
        }

        jsonObject.put("subModule",serverJacocoManager.getSubModule().trim());
        jsonObject.put("baseVersion",serverJacocoManager.getBaseVersion().trim());
        jsonObject.put("nowVersion",serverJacocoManager.getNowVersion().trim());
        jsonObject.put("address",serverJacocoManager.getServerAddress());
        jsonObject.put("port",serverJacocoManager.getPortJacoco());
        jsonObject.put("gitUrl",serverJacocoManager.getGitUrl());
        jsonObject.put("whiteModules",serverJacocoManager.getWhiteModules());
        jsonObject.put("lastUuid",lastUuid);
        System.out.println("获取覆盖率:"+url);
        System.out.println("获取覆盖率body:"+jsonObject.toJSONString());
        String reponse=  httpClientJson.sendRequest(url,jsonObject.toJSONString(),null);
        System.out.println("代码覆盖率请求结果：++++++++++++："+reponse);
        jacocoTestVersionService.addReport( serverJacocoManager, timestamp, versionId,lastUuid);
        return "success";
    }

    public String triggerUnitCoverage(ServerJacocoManager serverJacocoManager){
        String jacocoType=serverJacocoManager.getJacocoType();
        JSONObject jsonObject=new JSONObject();
        Date now =new Date();
        long timestamp=now.getTime();
        Integer jacocoServerId=serverJacocoManager.getSuperJacocoId();
        // String url=GetConfig.getAddress("jacocoServer").trim()+"/cov/triggerEnvCov";
        SuperJacoco jacocoSever= superJacocoDAO.selectByPrimaryKey(jacocoServerId);
        String url=jacocoSever.getJacocoServer().trim()+"/cov/triggerUnitCover";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateNowStr = sdf.format(now);
        String reportUUid=String.valueOf(timestamp);

        jsonObject.put("uuid",reportUUid);
        if(jacocoType.equals("1")){
            jsonObject.put("type",1); //全量覆盖率
        }else {
            jsonObject.put("type",2); //增量覆盖率
        }

        jsonObject.put("subModule",serverJacocoManager.getSubModule().trim());
        jsonObject.put("baseVersion",serverJacocoManager.getBaseVersion().trim());
        jsonObject.put("nowVersion",serverJacocoManager.getNowVersion().trim());
        jsonObject.put("address",serverJacocoManager.getServerAddress());
        jsonObject.put("port",serverJacocoManager.getPortJacoco());
        jsonObject.put("gitUrl",serverJacocoManager.getGitUrl());
        jsonObject.put("whiteModules",serverJacocoManager.getWhiteModules());
        jsonObject.put("envType","-Ptest");
        System.out.println("获取覆盖率:"+url);
        System.out.println("获取覆盖率body:"+jsonObject.toJSONString());
        String reponse=  httpClientJson.sendRequest(url,jsonObject.toJSONString(),null);
        System.out.println("代码覆盖率请求结果：++++++++++++："+reponse);
        addReport( serverJacocoManager, timestamp, dateNowStr,1); //1 表示真 为单测覆盖率报告 0 表示环境测试报告，1 表示单测报告
        return "success";
    }


    public int updateUnitReport(String uuid){

        // String url=GetConfig.getAddress("jacocoServer").trim()+"/cov/getEnvCoverResult";
        String url=getJacocoSeverUrl(uuid).trim()+"/cov/getEnvCoverResult";
        JSONObject jsonParam= new JSONObject();

        jsonParam.put("uuid",uuid);
        String   res= httpClientGet.sendGetRequset(url,jsonParam.toJSONString(),null);
        // System.out.println("获取报告response："+res);
        Map param = JSONObject.parseObject(res);
        JSONObject data= (JSONObject) ((JSONObject) param).get("data");

        String errMsg=data.getString("errMsg");
        String reportUrl=data.getString("reportUrl");
        System.out.println("报告获取。。。+" +reportUrl +" error:"+errMsg);
        JacocoReport jacocoReport=new JacocoReport();
        jacocoReport.setRecordUuid(uuid);
        jacocoReport.setReportUrl(reportUrl);
        if(reportUrl!=null && reportUrl.contains("index.html")){

            jacocoReport.setReportResult("success");

        }else {
            jacocoReport.setReportResult(errMsg);
            jacocoReport.setErrMsg(errMsg);
        }
        int res1= jacocoReportDAO.updateByPrimaryKeySelective(jacocoReport);

        return res1;
    }

}
