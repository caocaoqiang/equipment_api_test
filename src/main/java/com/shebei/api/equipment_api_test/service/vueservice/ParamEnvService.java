package com.shebei.api.equipment_api_test.service.vueservice;

import com.shebei.api.equipment_api_test.dao.EnviromentParameterDAO;
import com.shebei.api.equipment_api_test.model.EnviromentParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParamEnvService {

    @Autowired
    EnviromentParameterDAO enviromentParameterDAO;


    public     List<EnviromentParameter>  getList(EnviromentParameter record){
        List<EnviromentParameter> list= enviromentParameterDAO.selectPara(record);
        for(EnviromentParameter iter:list){
            String envType=iter.getEnvType();
            if(envType!=null && envType.equals("photo")){  //photo 代表base64参数 太大了 不直接显示
                continue;
            }
            System.out.println("查询得到参数:"+iter.getParameterName());
            EnviromentParameter tmp= enviromentParameterDAO.selectByName(iter);

            iter.setParameterValue(tmp.getParameterValue());
        }

        return list;

    }
}
