package com.shebei.api.equipment_api_test.service;

import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.dao.EnviromentParameterDAO;
import com.shebei.api.equipment_api_test.model.EnviromentParameter;
import com.shebei.api.equipment_api_test.utils.GetConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class ParamParsing {

    @Autowired
   private EnviromentParameterDAO enviromentParameterDAO;



    public String parsingValue(String containParamString,String scene ,String catogoryModule,String env){

       if(containParamString==null|| containParamString.equals("")){
           return null;
       }
        String reg="\\{\\{(.+?)\\}\\}";
      String[] paras=  getRegResult(reg,containParamString);

      if (paras.length>0){
          for(int i=0;i<paras.length;i++){
              String paraName=paras[i];
              String paraVal=getParamName(paraName); //去大括号 获取变量
              EnviromentParameter paraResult=getParamValue(paraVal, scene,catogoryModule,env); //获取变量所对应的值
              String paraValue=paraResult.getParameterValue();
              String paraType=paraResult.getEnvType();
              String patten="\\{\\{"+paraVal+"\\}\\}";
              if(paraValue!=null){

                  if(paraType!=null && paraType.equals("File")){
                      paraValue="File||"+paraValue;
                  }
                  containParamString=containParamString.replaceAll(patten,paraValue);   //将原字符串中的变量替换为对应的值

              }


          }
      }


        System.out.println("变量替换后的值为："+containParamString);


        return containParamString;
    }




    public String getURL(String scene,String catogoryModule,String path,String env){
        EnviromentParameter enviromentParameter=new EnviromentParameter();
        enviromentParameter.setModule(catogoryModule);
        enviromentParameter.setEnv(env);
        enviromentParameter.setScene(scene);
        enviromentParameter.setParameterName("url");
        EnviromentParameter res= enviromentParameterDAO.selectByName(enviromentParameter);
        String fullUrl=res.getParameterValue()+path;
        return fullUrl;
    }



    //根据变量名和场景找到变量的值
    private EnviromentParameter getParamValue(String paraVal,String scene,String catogoryModule,String env){


        EnviromentParameter enviromentParameter=new EnviromentParameter();
        enviromentParameter.setParameterName(paraVal);
        enviromentParameter.setScene(scene);
        enviromentParameter.setEnv(env);
        enviromentParameter.setModule(catogoryModule);
        EnviromentParameter result=enviromentParameterDAO.selectByName(enviromentParameter);
        if(result==null){
            System.out.println("变量："+paraVal+" 场景："+scene+"没有定义");
            return null;
        }

        return result;

    }





    //去掉双大括号 获取内部变量
    private String getParamName(String value){
        String reg="\\{\\{|\\}\\}";
        String paramName=value.replaceAll(reg,"");
        System.out.println("变量名字为："+paramName);
        return paramName;

    }


    /**
     * 该函数功能是正则匹配出所有结果到一个String数组中
     */
    private  String[] getRegResult(String pattern, String Sourcecode) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(Sourcecode);

        ArrayList<String> tempList = new ArrayList<String>();
        while (m.find()) {
            tempList.add(m.group());
        }
        String[] res = new String[tempList.size()];
        int i = 0;
        for (String temp : tempList) {
            res[i] = temp;
            i++;
        }
        return res;

    }





}
