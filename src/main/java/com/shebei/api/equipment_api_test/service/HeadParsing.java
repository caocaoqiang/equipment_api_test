package com.shebei.api.equipment_api_test.service;

import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.dao.EnviromentParameterDAO;
import com.shebei.api.equipment_api_test.model.EnviromentParameter;
import com.shebei.api.equipment_api_test.utils.AESUtilsHaiWai;
import com.shebei.api.equipment_api_test.utils.CookieUtil;
import org.apache.http.message.BasicNameValuePair;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sun.misc.BASE64Encoder;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class HeadParsing {


     @Autowired
    private  ParamParsing paramParsing;

     @Autowired
     private EnviromentParameterDAO enviromentParameterDAO;

    @Autowired
     private CookieUtil cookieUtil;


    public Map<String ,String> getHead(String headInfo,String scene,String catgoryModule,String env){


        if(headInfo ==null ||  headInfo.equals("")){

            return null;
        }
            String headInfoVaule=paramParsing.parsingValue(headInfo,scene,catgoryModule,env);

            Map parseObject = JSONObject.parseObject(headInfoVaule);


            exitSign( parseObject,scene,env);   //特殊处理

        System.out.println("变量替换后head信息："+parseObject.toString());
        return parseObject;
    }

  //判断是否存在sign 并替换timesamp 和MD5加密
    public void exitSign(Map<String ,String> head,String scene,String env) {

        if(head==null || head.equals("")){
            return ;
        }

        for (Map.Entry<String,String> entry : head.entrySet()) {
            String key = entry.getKey();
            String value = String.valueOf(entry.getValue());

//            if (key .equals("Cookie")) {
//                String token=head.get("Cookie");
//               String userId=getUserSessionId(scene,env);
//              String newVaule=  cookieUtil.setCookie(userId,token);
//                head.put("Cookie",newVaule);
////                putTime( head, String.valueOf(timestamp));  //传人linux时间
//
//            };
//            if(key .equals("accessToken")){ //离线客户端海外云平台 token
//                Date now =new Date();
//                long timestamp=now.getTime();
//
//                String mingWen=head.get("accessToken")+timestamp;
//                String miYao=head.get("miYao");
//               String sceret= AESUtilsHaiWai.encryptAES(mingWen,miYao);
//                head.put("accessToken",sceret);
//
//            }


        }



    }



    private String encodeByMd5(String str) {

        byte[] result = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance("md5");
            result  = md5.digest(str.getBytes("utf-8"));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //16是表示转换为16进制数
        String md5Str = new BigInteger(1, result).toString(16);
        return  md5Str;

    }

        private String getUserSessionId(String scene,String env){

        EnviromentParameter enviromentParameter=new EnviromentParameter();
        enviromentParameter.setScene(scene);
            enviromentParameter.setEnv(env);
        enviromentParameter.setParameterName("USERSESSIONID");
        EnviromentParameter result=  enviromentParameterDAO.selectByName(enviromentParameter);

        return result.getParameterValue();
    }


    private    void putTime(Map<String ,String> head,String timestamp){

        for (Map.Entry<String,String> entry : head.entrySet()) {
            String key = entry.getKey();
             if(key.equals("unixTime")){
                 head.put("unixTime",String.valueOf(timestamp));


             }
             if(key.equals("timestamp")){
                 head.put("timestamp",String.valueOf(timestamp));
             }
        }

    }
}
