package com.shebei.api.equipment_api_test.service.vueservice;


import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.dao.NavigateNameDAO;
import com.shebei.api.equipment_api_test.dao.UserCaseWoDAO;
import com.shebei.api.equipment_api_test.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
public class UserCaseService {


    @Autowired
    UserCaseWoDAO userCaseWoDAO;

    @Autowired
    NavigateNameDAO navigateNameDAO;
    @Autowired
    NavigateNameService navigateNameService;

    public List<NavigateName> getCaseGroup(String module,String apiGroup,String scene){



       List<NavigateName> caseGrps= navigateNameDAO.selectGroups(module,scene);

        return caseGrps;

    }


    public void caseReRange(String module,String scene){
       List<String> caseVersions= navigateNameService.getCaseVersionList(module,scene);
       int total=0;
       for(int i=0;i<caseVersions.size();i++){
           UserCaseWoWithBLOBs record=new UserCaseWoWithBLOBs();

           record.setScene(scene);
           record.setModule(module);
           record.setAdaptiveVersion(caseVersions.get(i));
           List<UserCaseGroup> list= userCaseWoDAO.selectCasesBySceneDes(record);
           int count=list.size();
           if(count!=0){
            int iterCout=count;
               for(UserCaseWoWithBLOBs iter:list){
                   int temp=iter.getExcuteSequence();
                   iter.setExcuteSequence(count+total);
                   userCaseWoDAO.updateByPrimaryKeyWithBLOBs(iter);
                   System.out.println("temp:"+temp +" 改为："+count);
                   count--;
               }
               total=iterCout+total;
           }
       }
        System.out.println("执行完成："+total);
    }


    public String caseIsJson(UserCaseWoWithBLOBs userCaseWo){
      //  String body=userCaseWo.getRequestBody();
        String check=userCaseWo.getCheckPath();
        Boolean pathcheck=isJson( check);
        if(!pathcheck){ //不是json
            return "检查路径不是json格式，请填写json格式";
        }
        String save=userCaseWo.getParameterSavePath();
        Boolean savePath=isJson( save);
        if(!savePath){
            return  "参数保存不是json格式，请填写json格式";
        }

        return "success";
    }

    private  boolean isJson(String value){
        if(value==null){
            return true;
        }
        if(value.equals("")){
            return true;
        }
        try{
            JSONObject jsonObject= JSONObject.parseObject(value);
            return true;
        }catch (Exception e){
            return false;
        }


    }

    private String saveIsString(String value){
        if(value==null){
            return "success";
        }
        if(value.equals("")){
            return "success";
        }
        JSONObject jsonToMap =  JSONObject.parseObject(value);

        Iterator it =jsonToMap.entrySet().iterator();
        List<Boolean> booleanList= new ArrayList<>();
        while (it.hasNext()) {
            Map.Entry<String, Object> entry = (Map.Entry<String, Object>) it.next();

            String keyPath=entry.getKey();
            String paraName= (String) entry.getValue(); //获取需要存数据库的参数

           char first=paraName.charAt(0);

            Boolean isDigit=Character.isDigit(first);
            if(isDigit){
                booleanList.add(true);

            }else {
                booleanList.add(false);
            }
        }
        for(int i=0; i<booleanList.size();i++){
             if(booleanList.get(i)){
                 int num=i+1;
                 String warn="参数保存"+"第"+num+"条首字母应该为字符类型";
                 return  warn;
             }
        }
      return "success";
    }

}
