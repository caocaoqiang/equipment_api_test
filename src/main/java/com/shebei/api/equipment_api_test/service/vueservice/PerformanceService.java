package com.shebei.api.equipment_api_test.service.vueservice;


import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.dao.ApiNameQueryDAO;
import com.shebei.api.equipment_api_test.dao.UserCaseWoDAO;
import com.shebei.api.equipment_api_test.http.HttpClientFormUrlencoded;
import com.shebei.api.equipment_api_test.http.performance.HttpClientPostObject;
import com.shebei.api.equipment_api_test.model.ApiNameQuery;
import com.shebei.api.equipment_api_test.model.UserCaseWoWithBLOBs;
import com.shebei.api.equipment_api_test.model.responseModel.ApiResModel;
import com.shebei.api.equipment_api_test.model.responseModel.PerForSceneModel;
import com.shebei.api.equipment_api_test.model.responseModel.PerforResModel;
import com.shebei.api.equipment_api_test.service.ParamParsing;
import com.shebei.api.equipment_api_test.service.UserCaseExcute;
import com.shebei.api.equipment_api_test.thread.CaseExcuteRunnable;
import com.shebei.api.equipment_api_test.thread.PerformanceSingleRunnable;
import com.shebei.api.equipment_api_test.utils.GetConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PerformanceService {

    @Autowired
    UserCaseExcute userCaseExcute;

    @Autowired
    ParamParsing paramParsing;

    @Autowired
    private UserCaseWoDAO userCaseWoDAO;

    @Autowired
    private ApiNameQueryDAO apiNameQueryDAO;

    @Autowired
    HttpClientFormUrlencoded httpClientFormUrlencoded;

    @Autowired
    HttpClientPostObject httpClientPostObject;

    public String performanceSingle(Integer id, String env,Integer users,Integer timeValue,String module){
        if(users==0||timeValue==0){
            return "并发数和时间不能为0";
        }
//       List<String> excuteList= userCaseWoDAO.selectExcutePerformance(module);
//        if(excuteList!=null && excuteList.size()>0){
//            return excuteList.toString()+"正在执行中,请稍后再执行";
//        }
       // ApiResModel  apiResModel= userCaseExcute.vueExcuteOneCase(id,env);
     //   String excuteResult=apiResModel.getExcuteResult();

            PerformanceSingleRunnable performanceSingleRunnable=new PerformanceSingleRunnable(id,env,users,timeValue);
            Thread thread1=new Thread(performanceSingleRunnable);
            thread1.start();
            return "开始执行";




    }

    public String performanceScne(PerForSceneModel perForSceneModel){
        Integer users=perForSceneModel.getUsers();
        Integer timeValue=perForSceneModel.getTimeValue();
        if(users==0||timeValue==0){
            return "并发数和时间不能为0";
        }

        String performanUrl= GetConfig.getAddress("performaceSceneAdddress");
        String reslut=   httpClientPostObject.doPostJson(performanUrl,perForSceneModel,null);

       if(reslut.equals("")){
           return "性能服务器没有开启";
       }
        return reslut;
    }

    public String excuteSingle(Integer id, String env,Integer users,Integer timeValue){
        UserCaseWoWithBLOBs userCaseWo = userCaseWoDAO.selectByPrimaryKey(id);
        String userName = userCaseWo.getUserCaseName();
        String apiName = userCaseWo.getApiName();
        String requestBody = userCaseWo.getRequestBody();
        String scene = userCaseWo.getScene();
        String catgoryModule = userCaseWo.getModule();
        String checkPath=userCaseWo.getCheckPath();
        ApiNameQuery apiNameQuery = apiNameQueryDAO.selectByName(apiName);
        String path = apiNameQuery.getApiValue();
        String requestType = apiNameQuery.getRequestType(); //get post
        String headInfo = apiNameQuery.getHeadInfo();
        String apiType=apiNameQuery.getApiType();// application/json
        if(apiType==null||apiType.equals("")){
            apiType="param";
        }

        String performanUrl= GetConfig.getAddress("performaceIpAddress");
        String pathValue = paramParsing.parsingValue(path, scene, catgoryModule, env);  //解析请求path路径中的变量
        String url = paramParsing.getURL(scene, catgoryModule, pathValue, env);

        String bodyValue = paramParsing.parsingValue(requestBody, scene, catgoryModule, env); //解析body请求中的变量。

        String headValue=paramParsing.parsingValue(headInfo, scene, catgoryModule, env);

        String checkValue=paramParsing.parsingValue(checkPath,scene,catgoryModule,env);



        PerforResModel perforResModel=new PerforResModel();
        perforResModel.setMethod(requestType);
        perforResModel.setUrl(url);
        perforResModel.setBody(bodyValue);
        perforResModel.setHeaders(headValue);
        perforResModel.setFormat(apiType);
        perforResModel.setChecks(checkValue);
        perforResModel.setUserName(userName);
        perforResModel.setUsersVaule(users);
        perforResModel.setTimesValue(timeValue);
        perforResModel.setId(id);




        String reslut=   httpClientPostObject.doPostJson(performanUrl,perforResModel,null);
        System.out.println("请求压力机结果:"+reslut);
        return reslut;
    }

}
