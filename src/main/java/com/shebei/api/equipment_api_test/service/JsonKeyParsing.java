package com.shebei.api.equipment_api_test.service;

import com.alibaba.fastjson.JSONPath;
import org.springframework.stereotype.Component;

@Component
public class JsonKeyParsing {




    //从返回json结果中获取check的实际结果
    public    String getJsonKey(String json, String path) {
        Object object = JSONPath.read(json, path );
         if(object==null){
             return "null";
         }
        String result=object.toString();
        return result;
    }
}
