package com.shebei.api.equipment_api_test.service.vueservice;


import com.alibaba.fastjson.JSONArray;
import com.shebei.api.equipment_api_test.dao.ApiNameQueryDAO;
import com.shebei.api.equipment_api_test.dao.NavigateNameApiDAO;
import com.shebei.api.equipment_api_test.dao.NavigateNameDAO;
import com.shebei.api.equipment_api_test.dao.UserCaseWoDAO;
import com.shebei.api.equipment_api_test.model.NavigateName;
import com.shebei.api.equipment_api_test.model.NavigateNameApi;
import com.shebei.api.equipment_api_test.model.responseModel.NavigateNameNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class NavigateNameApiService {


     @Autowired
     NavigateNameApiDAO navigateNameDAO;

    @Autowired
    ApiNameQueryDAO apiNameQueryDAO;
     public void  insertNaviget(){

         NavigateNameApi navigateName=new NavigateNameApi();
         navigateName.setModule("WO");
         navigateName.setScene("WO_Env");
         navigateName.setGroupName("base_group");
         Date now =new Date();
         long timestamp=now.getTime();
          String uniqueNo=String.valueOf(timestamp);
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         String dateNowStr = sdf.format(now );
         navigateName.setTimeCreate(dateNowStr);
         navigateName.setUniqueValue(uniqueNo);
         navigateNameDAO.insert(navigateName);
     }

     public int addGroup( String module ,
                         String scene,
                          String groupName ){


         NavigateNameApi navigateName=new NavigateNameApi();
         navigateName.setModule(module);
         navigateName.setScene(scene);
         navigateName.setGroupName(groupName);
         Date now =new Date();
         long timestamp=now.getTime();
         String uniqueNo=String.valueOf(timestamp);
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         String dateNowStr = sdf.format(now );
         navigateName.setTimeCreate(dateNowStr);
         navigateName.setUniqueValue(uniqueNo);
       return navigateNameDAO.insert(navigateName);
     }
    public int addFatherGroup( String module ,
                         String scene,
                         String groupName ){


        NavigateNameApi navigateName=new NavigateNameApi();
        navigateName.setModule(module);
        navigateName.setScene(scene);
        navigateName.setGroupName(groupName);
        Date now =new Date();
        long timestamp=now.getTime();
        String uniqueNo=String.valueOf(timestamp);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateNowStr = sdf.format(now );
        navigateName.setTimeCreate(dateNowStr);
        navigateName.setUniqueValue(uniqueNo);
        navigateName.setParentId(0);
        return navigateNameDAO.insert(navigateName);
    }


    public int addNode( String module ,
                         String scene,Integer parentId,
                         String groupName ){
        NavigateNameApi parentNode= navigateNameDAO.selectByPrimaryKey(parentId);

        NavigateNameApi navigateName=new NavigateNameApi();
        navigateName.setModule(module);
        navigateName.setScene(parentNode.getScene());
        navigateName.setGroupName(groupName);
        Date now =new Date();
        long timestamp=now.getTime();
        String uniqueNo=String.valueOf(timestamp);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateNowStr = sdf.format(now );
        navigateName.setTimeCreate(dateNowStr);
        navigateName.setUniqueValue(uniqueNo);
        navigateName.setParentId(parentId);
        return navigateNameDAO.insert(navigateName);
    }
     public List<NavigateNameApi> getList(){
       return   navigateNameDAO.selectGroups("WO","WO_Env");
     }
      //组内有用例 不能删除
     public String deleteGroup(String uniqueKey){
        int size= apiNameQueryDAO.selectApiExist(uniqueKey);
       Boolean childNotExit=childExit(uniqueKey);

        if(size==0&&childNotExit){
            int res=  navigateNameDAO.deleteByUniqueKey(uniqueKey);
            return "success";
        }
        return "failed";
     }

    private boolean childExit(String uniqueKey){
        List<NavigateNameApi>  lists=navigateNameDAO.selectChildisExist(uniqueKey);
        if(lists.size()==0){
            return true; //表示 没有子节点
        }
       return false;
     }


     public int editGroupName(String groupName,String uniqueKey){
         NavigateNameApi navigateName=new NavigateNameApi();
         navigateName.setUniqueValue(uniqueKey);
         navigateName.setGroupName(groupName);
        return navigateNameDAO.updateByUniqueKey(navigateName);
     }

     public String getNote(String module,String scene){
        List<NavigateNameNode>  navigateNameNodes=new ArrayList<NavigateNameNode>();
       List<NavigateNameApi> parents= navigateNameDAO.selectParent(module,scene);
        for(NavigateNameApi iter:parents){
            NavigateNameNode  tmp= new NavigateNameNode();
            tmp.setId(iter.getId());
            tmp.setGroupName(iter.getGroupName());
            tmp.setScene(iter.getScene());
            tmp.setLabel(iter.getGroupName());
            tmp.setUniqueValue(iter.getUniqueValue());
            getChildrens(iter.getId(),tmp);
            navigateNameNodes.add(tmp);

        }
        String res =JSONArray.toJSONString(navigateNameNodes);
         System.out.println(res);
         return res;
     }
    public List<NavigateNameNode> getApiNote(String module,String scene){
        List<NavigateNameNode>  navigateNameNodes=new ArrayList<NavigateNameNode>();
        List<NavigateNameApi> parents= navigateNameDAO.selectParent(module,scene);
        for(NavigateNameApi iter:parents){
            NavigateNameNode  tmp= new NavigateNameNode();
            tmp.setId(iter.getId());
            tmp.setGroupName(iter.getGroupName());
            tmp.setScene(iter.getScene());
            tmp.setLabel(iter.getGroupName());
            tmp.setUniqueValue(iter.getUniqueValue());
            navigateNameNodes.add(tmp);
            getApiChildrens(iter.getId(),navigateNameNodes);


        }
        String res =JSONArray.toJSONString(navigateNameNodes);
        System.out.println(res);
        return navigateNameNodes;
    }

     public List<String> getCaseVersionList(String module,String scene){
         List<String> list=new ArrayList<>();
         List<NavigateNameApi> parents= navigateNameDAO.selectParent(module,scene);
         for(NavigateNameApi iter:parents){
;           list.add(iter.getUniqueValue());
             getVersionChildrens(iter.getId(),list);



         }
         return list;
     }
     private void getChildrens(Integer parentId,NavigateNameNode parentNode){
           List<NavigateNameNode>  childs=new ArrayList<NavigateNameNode>();
         List<NavigateNameApi> res= navigateNameDAO.selectChild(parentId);
         if(res.size()==0){
             return;
         }
         for(NavigateNameApi iter:res){
             NavigateNameNode  tmp = new NavigateNameNode();
             tmp.setId(iter.getId());
             tmp.setGroupName(iter.getGroupName());
             tmp.setLabel(iter.getGroupName());
             tmp.setScene(iter.getScene());
             tmp.setUniqueValue(iter.getUniqueValue());
             getChildrens(iter.getId(),tmp);
             childs.add(tmp);
         }

         parentNode.setChildren(childs);

     }

    private void getApiChildrens(Integer parentId,  List<NavigateNameNode> parentNode){
     //   List<NavigateNameNode>  childs=new ArrayList<NavigateNameNode>();
        List<NavigateNameApi> res= navigateNameDAO.selectChild(parentId);
        if(res.size()==0){
            return;
        }
        for(NavigateNameApi iter:res){
            NavigateNameNode  tmp = new NavigateNameNode();
            tmp.setId(iter.getId());
            tmp.setGroupName(iter.getGroupName());
            tmp.setLabel(iter.getGroupName());
            tmp.setScene(iter.getScene());
            tmp.setUniqueValue(iter.getUniqueValue());
            parentNode.add(tmp);
            getApiChildrens(iter.getId(),parentNode);

        }


    }

    private void getVersionChildrens(Integer parentId, List<String> list){
        List<NavigateNameApi> res= navigateNameDAO.selectChild(parentId);
        if(res.size()==0){
            return;
        }
        for(NavigateNameApi iter:res){
            list.add(iter.getUniqueValue());
            getVersionChildrens(iter.getId(),list);

        }



    }
}
