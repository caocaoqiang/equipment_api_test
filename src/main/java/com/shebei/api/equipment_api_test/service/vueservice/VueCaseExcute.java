package com.shebei.api.equipment_api_test.service.vueservice;


import com.shebei.api.equipment_api_test.dao.*;
import com.shebei.api.equipment_api_test.model.*;
import com.shebei.api.equipment_api_test.service.UserCaseExcute;
import com.shebei.api.equipment_api_test.utils.GetLinuxTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;



@Component
public class VueCaseExcute {

     @Autowired
     CaseExcuteResultDAO caseExcuteResultDAO;
    @Autowired
    UserCaseWoDAO userCaseWoDAO;

    @Autowired
    EnviromentParameterDAO enviromentParameterDAO;
    @Autowired
    UserCaseExcute userCaseExcute;
    @Autowired
    GetLinuxTime getLinuxTime;

    public String sceneExcute(String scene,String module,String env) throws InterruptedException {

        EnviromentParameter enviromentParameter=new EnviromentParameter();
        enviromentParameter.setModule(module);
        enviromentParameter.setEnv(env);
        enviromentParameter.setScene(scene);
        List<String> sceneList=enviromentParameterDAO.selectScene(enviromentParameter);
         if(sceneList==null){
             return "没有场景可以执行，请在环境变量中创建场景";
         }
         int count=0;
        for(String iter:sceneList){

            CaseExcuteResultWithBLOBs caseExcuteResult =caseExcuteDelBefore( iter, module, env);  //删除之前的报告

            UserCaseWoWithBLOBs userCaseWo=new UserCaseWoWithBLOBs();
            userCaseWo.setModule(module);
            userCaseWo.setScene(iter);
            List<UserCaseWoWithBLOBs>  usercases= userCaseWoDAO.selectCasesVue(userCaseWo);

              for(UserCaseWoWithBLOBs iter1:usercases){
                  CaseExcuteResultWithBLOBs caseExcuteResultWithBLOBs = new CaseExcuteResultWithBLOBs();
                  Date d = new Date();
                  System.out.println(d);
                  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                  String dateNowStr = sdf.format(d);
                  System.out.println("************开始执行用例："+iter1);
                  String caseName=iter1.getUserCaseName();
                  caseExcuteResultWithBLOBs.setCaseName(caseName);
                  caseExcuteResultWithBLOBs.setCaseModule(caseExcuteResult.getCaseModule());
                  caseExcuteResultWithBLOBs.setEnv(caseExcuteResult.getEnv());
                  caseExcuteResultWithBLOBs.setExcuteTime(dateNowStr);
                  caseExcuteResultWithBLOBs.setExcuteResult("excuting");
                  caseExcuteResultWithBLOBs.setScene(iter);
                  String linuxTime=getLinuxTime.getTime();
                  caseExcuteResultWithBLOBs.setLinuxTime(linuxTime); //用linux时间做
                  caseExcuteResultWithBLOBs.setExcuteSequence(iter1.getExcuteSequence().toString());
                  caseExcuteResultWithBLOBs.setRequestType(iter1.getRequestType());
                  caseExcuteResultWithBLOBs.setCaseGroup(iter1.getAdaptiveVersion());
                  caseExcuteResultDAO.insert(caseExcuteResultWithBLOBs);
                  userCaseExcute.vueSceneExcute(iter1,env,caseExcuteResultWithBLOBs);
                  count++;
                  Integer  timeWait=iter1.getTimeWait();
                  System.out.println("等待实际为："+timeWait);
                  if(timeWait!=null && timeWait!=0){
                      Thread.currentThread().sleep(timeWait*1000);
                  }
                  Boolean isErr=caseExcuteResultWithBLOBs.getExcuteResult().equals("false"); //判断用例是否执行错误
                  String apiHttp=caseExcuteResultWithBLOBs.getApiVaule();
                  Boolean isTokenHttp=apiHttp.contains("/auth");  //判断是否为鉴权http

                  if(isErr&&isTokenHttp){  //如果是鉴权接口 且鉴权执行错了 再执行一次
                      userCaseExcute.vueSceneExcute(iter1,env,caseExcuteResultWithBLOBs);
                  }


              }
        }
        System.out.println("执行用例数："+count);

     return  "执行用例数："+count;
    }





    private CaseExcuteResultWithBLOBs caseExcuteDelBefore(String scene,String module,String env){


        CaseExcuteResultWithBLOBs caseExcuteResultWithBLOBs = new CaseExcuteResultWithBLOBs();
        caseExcuteResultWithBLOBs.setScene(scene);
        caseExcuteResultWithBLOBs.setCaseModule(module);
        caseExcuteResultWithBLOBs.setEnv(env);
        caseExcuteResultDAO.deleteByScene(caseExcuteResultWithBLOBs);
       return  caseExcuteResultWithBLOBs;

    }

}
