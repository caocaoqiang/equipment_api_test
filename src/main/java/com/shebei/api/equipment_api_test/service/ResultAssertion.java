package com.shebei.api.equipment_api_test.service;


import com.shebei.api.equipment_api_test.model.UserCaseWoWithBLOBs;
import org.testng.Assert;

import java.util.Map;


public class ResultAssertion {
    private static final String NULL = "null";

    public static void assertCleanResult(UserCaseWoWithBLOBs userCaseWo, String isTrue, Map<String ,String> errorResult) {

      String userCaseName=userCaseWo.getUserCaseName();
      String apiName=userCaseWo.getApiName();
      String apiValue=userCaseWo.getApiValue();
      String requestType=userCaseWo.getRequestType();
      Integer userId=userCaseWo.getId();

        String errMsg = "<p style=\"color:red;\">******************************失败用例******************************</p>" + "\n" +
                "用例Name=" + userCaseName + "\n<br/>" +
                "用例Id=" + userId + "\n<br/>" +
                "接口名字=" + apiName + "\n<br/>" +
                "接口地址=" + apiValue + "\n<br/>" +
                "接口类型=" + requestType + "\n<br/>" +
                "<font color=red>运行结果为=" + errorResult.toString() + "</font>\n<br/>" +
                "<font color=green>期望结果=" + true + "</font>\n<br/>";
        if (NULL.equals(isTrue)) {
            Assert.assertNull(isTrue, errMsg);
        } else {
            Assert.assertEquals(isTrue, "true", errMsg);
        }
    }


    public static void assertNull(String userCaseName) {

        String errMsg = "<p style=\"color:red;\">******************************失败用例******************************</p>" + "\n" +
                "用例Name=" + userCaseName + "\n<br/>" +
                "接口名字=" + userCaseName + "\n<br/>" +
                "<font color=red>运行结果为=" + "没有找到对应的http请求" + "</font>\n<br/>" +
                "<font color=green>期望结果=" + "接口的method 为post,get,put,delete,ContentType分别为：application/json，form-data,application/x-www-form-urlencoded" + "</font>\n<br/>";

            Assert.assertEquals("fasle", "true", errMsg);

    }


}
