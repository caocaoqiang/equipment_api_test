package com.shebei.api.equipment_api_test.job.mail;


import com.shebei.api.equipment_api_test.dao.CaseExcuteResultDAO;
import com.shebei.api.equipment_api_test.dao.UserCaseWoDAO;
import com.shebei.api.equipment_api_test.model.CaseExcuteResult;
import com.shebei.api.equipment_api_test.model.CaseExcuteResultWithBLOBs;
import com.shebei.api.equipment_api_test.model.UserCaseWoWithBLOBs;
import com.shebei.api.equipment_api_test.model.responseModel.ResCount;
import com.shebei.api.equipment_api_test.model.responseModel.ResModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResultCasesJobService {

    @Autowired
    CaseExcuteResultDAO caseExcuteResultDAO;

    @Autowired
    UserCaseWoDAO userCaseWoDAO;

    public ResCount getReport(String scene,String module,String env ,String reportName){
        CaseExcuteResult record=new CaseExcuteResult();
        record.setScene(scene);
        record.setCaseModule(module);
        record.setEnv(env);
        UserCaseWoWithBLOBs userCaseWo=new UserCaseWoWithBLOBs();
        userCaseWo.setScene(scene);
        userCaseWo.setModule(module);

        int total=userCaseWoDAO.selectCountByScene(userCaseWo);


        record.setExcuteResult("true");
        int   success=caseExcuteResultDAO.selectResultCountByScene(record);
        record.setExcuteResult("false");
        int   failed=caseExcuteResultDAO.selectResultCountByScene(record);
        record.setExcuteResult("excuting");
        int   excuting=caseExcuteResultDAO.selectResultCountByScene(record);


        ResCount resCount=new ResCount();
        resCount.setTotal(total);
        resCount.setSuccess(success);
        resCount.setFailed(failed);

        String reportDetail=getDetialResult( scene, module, env);

        resCount.setDetailReport(reportDetail);
        resCount.setReportName(reportName);

        return resCount;

    }


  private String  getDetialResult(String scene,String module,String env){
      ResModel resModel= new ResModel();
      CaseExcuteResult record=new CaseExcuteResult();

      record.setEnv(env);


      record.setScene(scene);
      record.setCaseModule(module);

      List<CaseExcuteResultWithBLOBs> reportList=caseExcuteResultDAO.selectReportByScene(record);

      String reportRes=getDetailReportNew( reportList);
      return reportRes;


  }

  private String getDetailReport(List<CaseExcuteResultWithBLOBs> reportList){
        StringBuffer detaillReport= new StringBuffer();
        int count =reportList.size();
        for(CaseExcuteResultWithBLOBs iter:reportList){
            String isTrue=iter.getExcuteResult();
//            if(isTrue.equals("false")){
//                count--;
//                String errMsg = "<p style=\"color:red;\">******************************失败用例******************************</p>" + "\n" +
//                        "用例执行顺序："+count+ "\n<br/>" +
//                        "用例Name=" + iter.getCaseName() + "\n<br/>" +
//                        "接口名字=" + iter.getApiName() + "\n<br/>" +
//                        "接口地址=" + iter.getApiVaule() + "\n<br/>" +
//                        "请求body体=" + iter.getRequestBody() + "\n<br/>" +
//                        "执行时间=" + iter.getExcuteTime() + "\n<br/>"+
//                        "响应结果="+iter.getResponseResult() + "\n<br/>"+
//                       "<font color=red>运行结果为=" + iter.getExcuteResult() + "</font>\n<br/>" +
//                        "<font color=red>错误信息=" + iter.getErrorMsg() + "</font>\n<br/>";
//
//                detaillReport.append(errMsg);
//            }else {
//                count--;
//                String successMsg="<p style=\"color:green;\">******************************成功用例******************************</p>"+"\n"+
//                        "用例执行顺序："+count+ "\n<br/>" +
//                        "用例Name=" + iter.getCaseName() + "\n<br/>" +
//                        "接口名字=" + iter.getApiName() + "\n<br/>" +
//                        "接口地址=" + iter.getApiVaule() + "\n<br/>" +
//                        "执行时间=" + iter.getExcuteTime() + "\n<br/>"+
//                        "<font color=green>运行结果为=" + iter.getExcuteResult() + "</font>\n<br/>";
//
//                detaillReport.append(successMsg);
//            }




        }
        return detaillReport.toString();

    }

    private String getDetailReportNew(List<CaseExcuteResultWithBLOBs> reportList){
        StringBuffer detaillReport= new StringBuffer();
        int count =reportList.size();
        String tableHead="  <tr>\n" +
                "               <th width=100px> 用例名字</th>\n" +
                "               <th width=120px>接口路径</th>\n" +
                "               <th width=130px>请求body</th>\n" +
                "               <th width=60px>错误信息</th>\n" +
                "               <th width=200px>返回reponse</th>\n" +
                "               <th width=15px>执行顺序</th>\n" +
                "               <th width=15px>执行结果</th>\n" +
                "               <th width=30px>执行时间</th>\n" +
                "           </tr>";
       String talbeLab="  <caption>用例执行详情</caption>";
        detaillReport.append(talbeLab);
        detaillReport.append(tableHead);
        for(CaseExcuteResultWithBLOBs iter:reportList){
            String isTrue=iter.getExcuteResult();
            String requstBodyTmp=iter.getRequestBody();
            if(isTrue.equals("false")){
                 isTrue="<font color=red>" + iter.getExcuteResult() + "</font>\n<br/>" ;
            }
            if(requstBodyTmp!=null&&requstBodyTmp.length()>2000){
                requstBodyTmp="base64 太长不展示";
            }
              String contend=" <tr>\n" +
                      "               <td>"+iter.getCaseName()+"    </td>\n" +
                      "               <td>"+iter.getApiVaule()+"    </td>\n" +
                      "               <td>"+requstBodyTmp+"    </td>\n" +
                      "               <td>"+iter.getErrorMsg()+"    </td>\n" +
                      "               <td>"+iter.getResponseResult()+"    </td>\n" +
                      "               <td>"+iter.getExcuteSequence()+"</td>\n" +
                      "               <td>"+isTrue+ "</td>\n" +
                      "               <td>"+iter.getExcuteTime()+ "</td>\n" +
                      "           </tr>";

                detaillReport.append(contend);





        }
        return detaillReport.toString();

    }
}
