package com.shebei.api.equipment_api_test.job;


import com.shebei.api.equipment_api_test.job.mail.ResultCasesJobService;
import com.shebei.api.equipment_api_test.model.responseModel.ResCount;
import com.shebei.api.equipment_api_test.service.vueservice.VueCaseExcute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class CaseExcuteJob {

    @Autowired
    SendMail sendMail;
    @Autowired
    ResultCasesJobService resultCasesJobService;
    @Autowired
    VueCaseExcute vueCaseExcute;

//    @Scheduled(cron="0 0 3 * * ?")
//    public  void  myTest(){
//        try {
//            String scene="Wo_Env";
//            String module="WO";
//            String env="线上";
//            //执行用例
//            vueCaseExcute.sceneExcute(scene, module, env);
//            //获取报告
//            ResCount resCount=resultCasesJobService.getReport(scene,module,env,"WO平台线上接口回归");
//            //发邮件
//            sendMail.send(resCount,null);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    @Scheduled(cron="0 0 4 * * ?")
//    public  void  woTuExcute(){
//        try {
//            String scene="自动回归";
//            String module="WoTu";
//            String env="线上";
//            //执行用例
//            vueCaseExcute.sceneExcute(scene, module, env);
//            //获取报告
//            ResCount resCount=resultCasesJobService.getReport(scene,module,env,"沃土平台接口线上回归");
//            //发邮件
//            sendMail.send(resCount,null);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//
//
//    @Scheduled(cron="0 0 2 * * ?")
//    public  void  aiOTExcute(){
//        try {
//            String scene=null;
//            String module="AIOT";
//            String env="线上";
//            //执行用例
//            vueCaseExcute.sceneExcute(scene, module, env);
//            //获取报告
//            ResCount resCount=resultCasesJobService.getReport(scene,module,env,"AIOT平台接口线上回归");
//            //发邮件
//            sendMail.send(resCount,null);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//
//    @Scheduled(cron="0 0 0 * * ?")
//    public  void  zhihuiSheQuExcute(){
//        try {
//            String scene="智慧社区冒烟";
//            String module="ZhiHuiSheQu";
//            String env="线上";
//            //执行用例
//            vueCaseExcute.sceneExcute(scene, module, env);
//            //获取报告
//            ResCount resCount=resultCasesJobService.getReport(scene,module,env,"智慧社区线上冒烟");
//            //发邮件
//            String reciveMail="qianshen@uni-ubi.com,mifeng@uni-ubi.com,sqyf@uni-ubi.com,ptcs@uni-ubi.com,panlei1@uni-ubi.com";
//            sendMail.send(resCount,reciveMail);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
}
