package com.shebei.api.equipment_api_test.thread;

import com.shebei.api.equipment_api_test.service.vueservice.VueCaseExcute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


public class CaseExcuteRunnable implements Runnable {

  public   String scene;
  public String module;
  public String env;


   private VueCaseExcute vueCaseExcute;

  public  CaseExcuteRunnable(String scene,String env,String module){
      this.scene=scene;
      this.module=module;
      this.env=env;
      this.vueCaseExcute = (VueCaseExcute) ApplicationContextProvider.getBean("vueCaseExcute");

  }

    @Override
    public void run() {
        //System.out.println("VueCaseExcute 实力"+vueCaseExcute.toString());
        try {
            String result=vueCaseExcute.sceneExcute(scene, module, env);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
