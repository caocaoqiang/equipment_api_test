package com.shebei.api.equipment_api_test.thread;

import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.dao.ApiNameQueryDAO;
import com.shebei.api.equipment_api_test.dao.UserCaseWoDAO;
import com.shebei.api.equipment_api_test.http.HttpClientFormUrlencoded;
import com.shebei.api.equipment_api_test.http.HttpClientJson;
import com.shebei.api.equipment_api_test.http.performance.HttpClientPostObject;
import com.shebei.api.equipment_api_test.model.ApiNameQuery;
import com.shebei.api.equipment_api_test.model.UserCaseWoWithBLOBs;
import com.shebei.api.equipment_api_test.model.responseModel.PerforResModel;
import com.shebei.api.equipment_api_test.service.ParamParsing;
import com.shebei.api.equipment_api_test.service.UserCaseExcute;
import com.shebei.api.equipment_api_test.utils.GetConfig;
import org.springframework.beans.factory.annotation.Autowired;

public class PerformanceSingleRunnable    implements Runnable{



   private Integer id;
   private  String env;

   private Integer users;

   private Integer timeValue;

    private  ParamParsing paramParsing;


    private UserCaseWoDAO userCaseWoDAO;


    private ApiNameQueryDAO apiNameQueryDAO;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }


    HttpClientPostObject httpClientPostObject;

    public PerformanceSingleRunnable(Integer id,String env,Integer users,Integer timeValue){
        this.users=users;
        this.timeValue=timeValue;
        this.id=id;
        this.env=env;
        this.paramParsing = (ParamParsing) ApplicationContextProvider.getBean("paramParsing");
        this.userCaseWoDAO = (UserCaseWoDAO) ApplicationContextProvider.getBean("userCaseWoDAO");
        this.apiNameQueryDAO = (ApiNameQueryDAO) ApplicationContextProvider.getBean("apiNameQueryDAO");
        this.httpClientPostObject= (HttpClientPostObject) ApplicationContextProvider.getBean("httpClientPostObject");
    }

    @Override
    public void run() {

        excuteSingle(id,env,users,timeValue);
    }


    private void excuteSingle(Integer id, String env,Integer users,Integer timeValue){
        UserCaseWoWithBLOBs userCaseWo = userCaseWoDAO.selectByPrimaryKey(id);
        String userName = userCaseWo.getUserCaseName();
        String apiName = userCaseWo.getApiName();
        String requestBody = userCaseWo.getRequestBody();
        String scene = userCaseWo.getScene();
        String catgoryModule = userCaseWo.getModule();
        String checkPath=userCaseWo.getCheckPath();
        ApiNameQuery apiNameQuery = apiNameQueryDAO.selectByName(apiName);
        String path = apiNameQuery.getApiValue();
        String requestType = apiNameQuery.getRequestType(); //get post
        String headInfo = apiNameQuery.getHeadInfo();
        String apiType=apiNameQuery.getApiType();// application/json
        if(apiType==null||apiType.equals("")){
            apiType="param";
        }

        String performanUrl= GetConfig.getAddress("performaceIpAddress");
        String pathValue = paramParsing.parsingValue(path, scene, catgoryModule, env);  //解析请求path路径中的变量
        String url = paramParsing.getURL(scene, catgoryModule, pathValue, env);

        String bodyValue = paramParsing.parsingValue(requestBody, scene, catgoryModule, env); //解析body请求中的变量。

        String headValue=paramParsing.parsingValue(headInfo, scene, catgoryModule, env);

        String checkValue=paramParsing.parsingValue(checkPath,scene,catgoryModule,env);



        PerforResModel perforResModel=new PerforResModel();
        perforResModel.setMethod(requestType);
        perforResModel.setUrl(url);
        perforResModel.setBody(bodyValue);
        perforResModel.setHeaders(headValue);
        perforResModel.setFormat(apiType);
        perforResModel.setChecks(checkValue);
        perforResModel.setUserName(userName);
        perforResModel.setUsersVaule(users);
        perforResModel.setTimesValue(timeValue);
        perforResModel.setId(id);




     String reslut=   httpClientPostObject.doPostJson(performanUrl,perforResModel,null);
        System.out.println("请求压力机结果:"+reslut);
    }
}
