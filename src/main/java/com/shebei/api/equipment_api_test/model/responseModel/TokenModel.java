package com.shebei.api.equipment_api_test.model.responseModel;

public class TokenModel {

    String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
