package com.shebei.api.equipment_api_test.model;

import java.util.ArrayList;
import java.util.List;

public class JacocoReportExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Integer offset;

    public JacocoReportExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRecordUuidIsNull() {
            addCriterion("record_uuid is null");
            return (Criteria) this;
        }

        public Criteria andRecordUuidIsNotNull() {
            addCriterion("record_uuid is not null");
            return (Criteria) this;
        }

        public Criteria andRecordUuidEqualTo(String value) {
            addCriterion("record_uuid =", value, "recordUuid");
            return (Criteria) this;
        }

        public Criteria andRecordUuidNotEqualTo(String value) {
            addCriterion("record_uuid <>", value, "recordUuid");
            return (Criteria) this;
        }

        public Criteria andRecordUuidGreaterThan(String value) {
            addCriterion("record_uuid >", value, "recordUuid");
            return (Criteria) this;
        }

        public Criteria andRecordUuidGreaterThanOrEqualTo(String value) {
            addCriterion("record_uuid >=", value, "recordUuid");
            return (Criteria) this;
        }

        public Criteria andRecordUuidLessThan(String value) {
            addCriterion("record_uuid <", value, "recordUuid");
            return (Criteria) this;
        }

        public Criteria andRecordUuidLessThanOrEqualTo(String value) {
            addCriterion("record_uuid <=", value, "recordUuid");
            return (Criteria) this;
        }

        public Criteria andRecordUuidLike(String value) {
            addCriterion("record_uuid like", value, "recordUuid");
            return (Criteria) this;
        }

        public Criteria andRecordUuidNotLike(String value) {
            addCriterion("record_uuid not like", value, "recordUuid");
            return (Criteria) this;
        }

        public Criteria andRecordUuidIn(List<String> values) {
            addCriterion("record_uuid in", values, "recordUuid");
            return (Criteria) this;
        }

        public Criteria andRecordUuidNotIn(List<String> values) {
            addCriterion("record_uuid not in", values, "recordUuid");
            return (Criteria) this;
        }

        public Criteria andRecordUuidBetween(String value1, String value2) {
            addCriterion("record_uuid between", value1, value2, "recordUuid");
            return (Criteria) this;
        }

        public Criteria andRecordUuidNotBetween(String value1, String value2) {
            addCriterion("record_uuid not between", value1, value2, "recordUuid");
            return (Criteria) this;
        }

        public Criteria andModuleNameIsNull() {
            addCriterion("module_name is null");
            return (Criteria) this;
        }

        public Criteria andModuleNameIsNotNull() {
            addCriterion("module_name is not null");
            return (Criteria) this;
        }

        public Criteria andModuleNameEqualTo(String value) {
            addCriterion("module_name =", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameNotEqualTo(String value) {
            addCriterion("module_name <>", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameGreaterThan(String value) {
            addCriterion("module_name >", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameGreaterThanOrEqualTo(String value) {
            addCriterion("module_name >=", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameLessThan(String value) {
            addCriterion("module_name <", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameLessThanOrEqualTo(String value) {
            addCriterion("module_name <=", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameLike(String value) {
            addCriterion("module_name like", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameNotLike(String value) {
            addCriterion("module_name not like", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameIn(List<String> values) {
            addCriterion("module_name in", values, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameNotIn(List<String> values) {
            addCriterion("module_name not in", values, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameBetween(String value1, String value2) {
            addCriterion("module_name between", value1, value2, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameNotBetween(String value1, String value2) {
            addCriterion("module_name not between", value1, value2, "moduleName");
            return (Criteria) this;
        }

        public Criteria andServerNameIsNull() {
            addCriterion("server_name is null");
            return (Criteria) this;
        }

        public Criteria andServerNameIsNotNull() {
            addCriterion("server_name is not null");
            return (Criteria) this;
        }

        public Criteria andServerNameEqualTo(String value) {
            addCriterion("server_name =", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameNotEqualTo(String value) {
            addCriterion("server_name <>", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameGreaterThan(String value) {
            addCriterion("server_name >", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameGreaterThanOrEqualTo(String value) {
            addCriterion("server_name >=", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameLessThan(String value) {
            addCriterion("server_name <", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameLessThanOrEqualTo(String value) {
            addCriterion("server_name <=", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameLike(String value) {
            addCriterion("server_name like", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameNotLike(String value) {
            addCriterion("server_name not like", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameIn(List<String> values) {
            addCriterion("server_name in", values, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameNotIn(List<String> values) {
            addCriterion("server_name not in", values, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameBetween(String value1, String value2) {
            addCriterion("server_name between", value1, value2, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameNotBetween(String value1, String value2) {
            addCriterion("server_name not between", value1, value2, "serverName");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(String value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(String value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(String value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(String value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(String value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(String value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLike(String value) {
            addCriterion("create_time like", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotLike(String value) {
            addCriterion("create_time not like", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<String> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<String> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(String value1, String value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(String value1, String value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andBaseVersionIsNull() {
            addCriterion("base_version is null");
            return (Criteria) this;
        }

        public Criteria andBaseVersionIsNotNull() {
            addCriterion("base_version is not null");
            return (Criteria) this;
        }

        public Criteria andBaseVersionEqualTo(String value) {
            addCriterion("base_version =", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionNotEqualTo(String value) {
            addCriterion("base_version <>", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionGreaterThan(String value) {
            addCriterion("base_version >", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionGreaterThanOrEqualTo(String value) {
            addCriterion("base_version >=", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionLessThan(String value) {
            addCriterion("base_version <", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionLessThanOrEqualTo(String value) {
            addCriterion("base_version <=", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionLike(String value) {
            addCriterion("base_version like", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionNotLike(String value) {
            addCriterion("base_version not like", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionIn(List<String> values) {
            addCriterion("base_version in", values, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionNotIn(List<String> values) {
            addCriterion("base_version not in", values, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionBetween(String value1, String value2) {
            addCriterion("base_version between", value1, value2, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionNotBetween(String value1, String value2) {
            addCriterion("base_version not between", value1, value2, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionIsNull() {
            addCriterion("now_version is null");
            return (Criteria) this;
        }

        public Criteria andNowVersionIsNotNull() {
            addCriterion("now_version is not null");
            return (Criteria) this;
        }

        public Criteria andNowVersionEqualTo(String value) {
            addCriterion("now_version =", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionNotEqualTo(String value) {
            addCriterion("now_version <>", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionGreaterThan(String value) {
            addCriterion("now_version >", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionGreaterThanOrEqualTo(String value) {
            addCriterion("now_version >=", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionLessThan(String value) {
            addCriterion("now_version <", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionLessThanOrEqualTo(String value) {
            addCriterion("now_version <=", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionLike(String value) {
            addCriterion("now_version like", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionNotLike(String value) {
            addCriterion("now_version not like", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionIn(List<String> values) {
            addCriterion("now_version in", values, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionNotIn(List<String> values) {
            addCriterion("now_version not in", values, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionBetween(String value1, String value2) {
            addCriterion("now_version between", value1, value2, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionNotBetween(String value1, String value2) {
            addCriterion("now_version not between", value1, value2, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeIsNull() {
            addCriterion("jacoco_type is null");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeIsNotNull() {
            addCriterion("jacoco_type is not null");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeEqualTo(String value) {
            addCriterion("jacoco_type =", value, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeNotEqualTo(String value) {
            addCriterion("jacoco_type <>", value, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeGreaterThan(String value) {
            addCriterion("jacoco_type >", value, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeGreaterThanOrEqualTo(String value) {
            addCriterion("jacoco_type >=", value, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeLessThan(String value) {
            addCriterion("jacoco_type <", value, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeLessThanOrEqualTo(String value) {
            addCriterion("jacoco_type <=", value, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeLike(String value) {
            addCriterion("jacoco_type like", value, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeNotLike(String value) {
            addCriterion("jacoco_type not like", value, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeIn(List<String> values) {
            addCriterion("jacoco_type in", values, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeNotIn(List<String> values) {
            addCriterion("jacoco_type not in", values, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeBetween(String value1, String value2) {
            addCriterion("jacoco_type between", value1, value2, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeNotBetween(String value1, String value2) {
            addCriterion("jacoco_type not between", value1, value2, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andReportUrlIsNull() {
            addCriterion("report_url is null");
            return (Criteria) this;
        }

        public Criteria andReportUrlIsNotNull() {
            addCriterion("report_url is not null");
            return (Criteria) this;
        }

        public Criteria andReportUrlEqualTo(String value) {
            addCriterion("report_url =", value, "reportUrl");
            return (Criteria) this;
        }

        public Criteria andReportUrlNotEqualTo(String value) {
            addCriterion("report_url <>", value, "reportUrl");
            return (Criteria) this;
        }

        public Criteria andReportUrlGreaterThan(String value) {
            addCriterion("report_url >", value, "reportUrl");
            return (Criteria) this;
        }

        public Criteria andReportUrlGreaterThanOrEqualTo(String value) {
            addCriterion("report_url >=", value, "reportUrl");
            return (Criteria) this;
        }

        public Criteria andReportUrlLessThan(String value) {
            addCriterion("report_url <", value, "reportUrl");
            return (Criteria) this;
        }

        public Criteria andReportUrlLessThanOrEqualTo(String value) {
            addCriterion("report_url <=", value, "reportUrl");
            return (Criteria) this;
        }

        public Criteria andReportUrlLike(String value) {
            addCriterion("report_url like", value, "reportUrl");
            return (Criteria) this;
        }

        public Criteria andReportUrlNotLike(String value) {
            addCriterion("report_url not like", value, "reportUrl");
            return (Criteria) this;
        }

        public Criteria andReportUrlIn(List<String> values) {
            addCriterion("report_url in", values, "reportUrl");
            return (Criteria) this;
        }

        public Criteria andReportUrlNotIn(List<String> values) {
            addCriterion("report_url not in", values, "reportUrl");
            return (Criteria) this;
        }

        public Criteria andReportUrlBetween(String value1, String value2) {
            addCriterion("report_url between", value1, value2, "reportUrl");
            return (Criteria) this;
        }

        public Criteria andReportUrlNotBetween(String value1, String value2) {
            addCriterion("report_url not between", value1, value2, "reportUrl");
            return (Criteria) this;
        }

        public Criteria andReportResultIsNull() {
            addCriterion("report_result is null");
            return (Criteria) this;
        }

        public Criteria andReportResultIsNotNull() {
            addCriterion("report_result is not null");
            return (Criteria) this;
        }

        public Criteria andReportResultEqualTo(String value) {
            addCriterion("report_result =", value, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultNotEqualTo(String value) {
            addCriterion("report_result <>", value, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultGreaterThan(String value) {
            addCriterion("report_result >", value, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultGreaterThanOrEqualTo(String value) {
            addCriterion("report_result >=", value, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultLessThan(String value) {
            addCriterion("report_result <", value, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultLessThanOrEqualTo(String value) {
            addCriterion("report_result <=", value, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultLike(String value) {
            addCriterion("report_result like", value, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultNotLike(String value) {
            addCriterion("report_result not like", value, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultIn(List<String> values) {
            addCriterion("report_result in", values, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultNotIn(List<String> values) {
            addCriterion("report_result not in", values, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultBetween(String value1, String value2) {
            addCriterion("report_result between", value1, value2, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultNotBetween(String value1, String value2) {
            addCriterion("report_result not between", value1, value2, "reportResult");
            return (Criteria) this;
        }

        public Criteria andErrMsgIsNull() {
            addCriterion("err_msg is null");
            return (Criteria) this;
        }

        public Criteria andErrMsgIsNotNull() {
            addCriterion("err_msg is not null");
            return (Criteria) this;
        }

        public Criteria andErrMsgEqualTo(String value) {
            addCriterion("err_msg =", value, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgNotEqualTo(String value) {
            addCriterion("err_msg <>", value, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgGreaterThan(String value) {
            addCriterion("err_msg >", value, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgGreaterThanOrEqualTo(String value) {
            addCriterion("err_msg >=", value, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgLessThan(String value) {
            addCriterion("err_msg <", value, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgLessThanOrEqualTo(String value) {
            addCriterion("err_msg <=", value, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgLike(String value) {
            addCriterion("err_msg like", value, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgNotLike(String value) {
            addCriterion("err_msg not like", value, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgIn(List<String> values) {
            addCriterion("err_msg in", values, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgNotIn(List<String> values) {
            addCriterion("err_msg not in", values, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgBetween(String value1, String value2) {
            addCriterion("err_msg between", value1, value2, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgNotBetween(String value1, String value2) {
            addCriterion("err_msg not between", value1, value2, "errMsg");
            return (Criteria) this;
        }

        public Criteria andJacocoSeverIdIsNull() {
            addCriterion("jacoco_sever_id is null");
            return (Criteria) this;
        }

        public Criteria andJacocoSeverIdIsNotNull() {
            addCriterion("jacoco_sever_id is not null");
            return (Criteria) this;
        }

        public Criteria andJacocoSeverIdEqualTo(Integer value) {
            addCriterion("jacoco_sever_id =", value, "jacocoSeverId");
            return (Criteria) this;
        }

        public Criteria andJacocoSeverIdNotEqualTo(Integer value) {
            addCriterion("jacoco_sever_id <>", value, "jacocoSeverId");
            return (Criteria) this;
        }

        public Criteria andJacocoSeverIdGreaterThan(Integer value) {
            addCriterion("jacoco_sever_id >", value, "jacocoSeverId");
            return (Criteria) this;
        }

        public Criteria andJacocoSeverIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("jacoco_sever_id >=", value, "jacocoSeverId");
            return (Criteria) this;
        }

        public Criteria andJacocoSeverIdLessThan(Integer value) {
            addCriterion("jacoco_sever_id <", value, "jacocoSeverId");
            return (Criteria) this;
        }

        public Criteria andJacocoSeverIdLessThanOrEqualTo(Integer value) {
            addCriterion("jacoco_sever_id <=", value, "jacocoSeverId");
            return (Criteria) this;
        }

        public Criteria andJacocoSeverIdIn(List<Integer> values) {
            addCriterion("jacoco_sever_id in", values, "jacocoSeverId");
            return (Criteria) this;
        }

        public Criteria andJacocoSeverIdNotIn(List<Integer> values) {
            addCriterion("jacoco_sever_id not in", values, "jacocoSeverId");
            return (Criteria) this;
        }

        public Criteria andJacocoSeverIdBetween(Integer value1, Integer value2) {
            addCriterion("jacoco_sever_id between", value1, value2, "jacocoSeverId");
            return (Criteria) this;
        }

        public Criteria andJacocoSeverIdNotBetween(Integer value1, Integer value2) {
            addCriterion("jacoco_sever_id not between", value1, value2, "jacocoSeverId");
            return (Criteria) this;
        }

        public Criteria andUnitTestTypeIsNull() {
            addCriterion("unit_test_type is null");
            return (Criteria) this;
        }

        public Criteria andUnitTestTypeIsNotNull() {
            addCriterion("unit_test_type is not null");
            return (Criteria) this;
        }

        public Criteria andUnitTestTypeEqualTo(Integer value) {
            addCriterion("unit_test_type =", value, "unitTestType");
            return (Criteria) this;
        }

        public Criteria andUnitTestTypeNotEqualTo(Integer value) {
            addCriterion("unit_test_type <>", value, "unitTestType");
            return (Criteria) this;
        }

        public Criteria andUnitTestTypeGreaterThan(Integer value) {
            addCriterion("unit_test_type >", value, "unitTestType");
            return (Criteria) this;
        }

        public Criteria andUnitTestTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("unit_test_type >=", value, "unitTestType");
            return (Criteria) this;
        }

        public Criteria andUnitTestTypeLessThan(Integer value) {
            addCriterion("unit_test_type <", value, "unitTestType");
            return (Criteria) this;
        }

        public Criteria andUnitTestTypeLessThanOrEqualTo(Integer value) {
            addCriterion("unit_test_type <=", value, "unitTestType");
            return (Criteria) this;
        }

        public Criteria andUnitTestTypeIn(List<Integer> values) {
            addCriterion("unit_test_type in", values, "unitTestType");
            return (Criteria) this;
        }

        public Criteria andUnitTestTypeNotIn(List<Integer> values) {
            addCriterion("unit_test_type not in", values, "unitTestType");
            return (Criteria) this;
        }

        public Criteria andUnitTestTypeBetween(Integer value1, Integer value2) {
            addCriterion("unit_test_type between", value1, value2, "unitTestType");
            return (Criteria) this;
        }

        public Criteria andUnitTestTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("unit_test_type not between", value1, value2, "unitTestType");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}