package com.shebei.api.equipment_api_test.model;

import java.io.Serializable;

/**
 * @author 
 */
public class UserCaseWoWithBLOBs extends UserCaseWo implements Serializable {
    private String checkPath;

    private String requestBody;

    private static final long serialVersionUID = 1L;

    public String getCheckPath() {
        return checkPath;
    }

    public void setCheckPath(String checkPath) {
        this.checkPath = checkPath;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        UserCaseWoWithBLOBs other = (UserCaseWoWithBLOBs) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getModule() == null ? other.getModule() == null : this.getModule().equals(other.getModule()))
            && (this.getUserCaseName() == null ? other.getUserCaseName() == null : this.getUserCaseName().equals(other.getUserCaseName()))
            && (this.getApiName() == null ? other.getApiName() == null : this.getApiName().equals(other.getApiName()))
            && (this.getApiValue() == null ? other.getApiValue() == null : this.getApiValue().equals(other.getApiValue()))
            && (this.getRequestType() == null ? other.getRequestType() == null : this.getRequestType().equals(other.getRequestType()))
            && (this.getScene() == null ? other.getScene() == null : this.getScene().equals(other.getScene()))
            && (this.getExcuteSequence() == null ? other.getExcuteSequence() == null : this.getExcuteSequence().equals(other.getExcuteSequence()))
            && (this.getParameterSavePath() == null ? other.getParameterSavePath() == null : this.getParameterSavePath().equals(other.getParameterSavePath()))
            && (this.getAdaptiveVersion() == null ? other.getAdaptiveVersion() == null : this.getAdaptiveVersion().equals(other.getAdaptiveVersion()))
            && (this.getApiGroup() == null ? other.getApiGroup() == null : this.getApiGroup().equals(other.getApiGroup()))
            && (this.getTimeWait() == null ? other.getTimeWait() == null : this.getTimeWait().equals(other.getTimeWait()))
            && (this.getPerformanceExcute() == null ? other.getPerformanceExcute() == null : this.getPerformanceExcute().equals(other.getPerformanceExcute()))
            && (this.getIsDelete() == null ? other.getIsDelete() == null : this.getIsDelete().equals(other.getIsDelete()))
            && (this.getCheckPath() == null ? other.getCheckPath() == null : this.getCheckPath().equals(other.getCheckPath()))
            && (this.getRequestBody() == null ? other.getRequestBody() == null : this.getRequestBody().equals(other.getRequestBody()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getModule() == null) ? 0 : getModule().hashCode());
        result = prime * result + ((getUserCaseName() == null) ? 0 : getUserCaseName().hashCode());
        result = prime * result + ((getApiName() == null) ? 0 : getApiName().hashCode());
        result = prime * result + ((getApiValue() == null) ? 0 : getApiValue().hashCode());
        result = prime * result + ((getRequestType() == null) ? 0 : getRequestType().hashCode());
        result = prime * result + ((getScene() == null) ? 0 : getScene().hashCode());
        result = prime * result + ((getExcuteSequence() == null) ? 0 : getExcuteSequence().hashCode());
        result = prime * result + ((getParameterSavePath() == null) ? 0 : getParameterSavePath().hashCode());
        result = prime * result + ((getAdaptiveVersion() == null) ? 0 : getAdaptiveVersion().hashCode());
        result = prime * result + ((getApiGroup() == null) ? 0 : getApiGroup().hashCode());
        result = prime * result + ((getTimeWait() == null) ? 0 : getTimeWait().hashCode());
        result = prime * result + ((getPerformanceExcute() == null) ? 0 : getPerformanceExcute().hashCode());
        result = prime * result + ((getIsDelete() == null) ? 0 : getIsDelete().hashCode());
        result = prime * result + ((getCheckPath() == null) ? 0 : getCheckPath().hashCode());
        result = prime * result + ((getRequestBody() == null) ? 0 : getRequestBody().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", checkPath=").append(checkPath);
        sb.append(", requestBody=").append(requestBody);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}