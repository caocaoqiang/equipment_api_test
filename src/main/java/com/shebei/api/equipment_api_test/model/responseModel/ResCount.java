package com.shebei.api.equipment_api_test.model.responseModel;

public class ResCount {

    Integer total;
    Integer success;
    Integer failed;

    String detailReport;
    String reportName;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public Integer getFailed() {
        return failed;
    }

    public void setFailed(Integer failed) {
        this.failed = failed;
    }

    public String getDetailReport() {
        return detailReport;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public void setDetailReport(String detailReport) {
        this.detailReport = detailReport;
    }
}
