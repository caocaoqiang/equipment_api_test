package com.shebei.api.equipment_api_test.model.responseModel;

public class PerforResModel {



    private  String concurrentType;

    private  Integer usersVaule;

    private  Integer timesValue;

    private String url;

    private String body;

    private String headers;

    private String format;

    private String checks;

    private String userName;

    private String method;

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getConcurrentType() {
        return concurrentType;
    }

    public void setConcurrentType(String concurrentType) {
        this.concurrentType = concurrentType;
    }

    public Integer getUsersVaule() {
        return usersVaule;
    }

    public void setUsersVaule(Integer usersVaule) {
        this.usersVaule = usersVaule;
    }

    public Integer getTimesValue() {
        return timesValue;
    }

    public void setTimesValue(Integer timesValue) {
        this.timesValue = timesValue;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getHeaders() {
        return headers;
    }

    public void setHeaders(String headers) {
        this.headers = headers;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getChecks() {
        return checks;
    }

    public void setChecks(String checks) {
        this.checks = checks;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
