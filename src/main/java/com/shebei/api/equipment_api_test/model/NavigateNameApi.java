package com.shebei.api.equipment_api_test.model;

import java.io.Serializable;

/**
 * @author 
 */
public class NavigateNameApi implements Serializable {
    private Integer id;

    private String groupName;

    private String timeCreate;

    private String module;

    private String scene;

    private String uniqueValue;

    private String excuteResult;

    private String excuteTime;

    private String groupState;

    private Integer parentId;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getTimeCreate() {
        return timeCreate;
    }

    public void setTimeCreate(String timeCreate) {
        this.timeCreate = timeCreate;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    public String getUniqueValue() {
        return uniqueValue;
    }

    public void setUniqueValue(String uniqueValue) {
        this.uniqueValue = uniqueValue;
    }

    public String getExcuteResult() {
        return excuteResult;
    }

    public void setExcuteResult(String excuteResult) {
        this.excuteResult = excuteResult;
    }

    public String getExcuteTime() {
        return excuteTime;
    }

    public void setExcuteTime(String excuteTime) {
        this.excuteTime = excuteTime;
    }

    public String getGroupState() {
        return groupState;
    }

    public void setGroupState(String groupState) {
        this.groupState = groupState;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        NavigateNameApi other = (NavigateNameApi) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getGroupName() == null ? other.getGroupName() == null : this.getGroupName().equals(other.getGroupName()))
            && (this.getTimeCreate() == null ? other.getTimeCreate() == null : this.getTimeCreate().equals(other.getTimeCreate()))
            && (this.getModule() == null ? other.getModule() == null : this.getModule().equals(other.getModule()))
            && (this.getScene() == null ? other.getScene() == null : this.getScene().equals(other.getScene()))
            && (this.getUniqueValue() == null ? other.getUniqueValue() == null : this.getUniqueValue().equals(other.getUniqueValue()))
            && (this.getExcuteResult() == null ? other.getExcuteResult() == null : this.getExcuteResult().equals(other.getExcuteResult()))
            && (this.getExcuteTime() == null ? other.getExcuteTime() == null : this.getExcuteTime().equals(other.getExcuteTime()))
            && (this.getGroupState() == null ? other.getGroupState() == null : this.getGroupState().equals(other.getGroupState()))
            && (this.getParentId() == null ? other.getParentId() == null : this.getParentId().equals(other.getParentId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getGroupName() == null) ? 0 : getGroupName().hashCode());
        result = prime * result + ((getTimeCreate() == null) ? 0 : getTimeCreate().hashCode());
        result = prime * result + ((getModule() == null) ? 0 : getModule().hashCode());
        result = prime * result + ((getScene() == null) ? 0 : getScene().hashCode());
        result = prime * result + ((getUniqueValue() == null) ? 0 : getUniqueValue().hashCode());
        result = prime * result + ((getExcuteResult() == null) ? 0 : getExcuteResult().hashCode());
        result = prime * result + ((getExcuteTime() == null) ? 0 : getExcuteTime().hashCode());
        result = prime * result + ((getGroupState() == null) ? 0 : getGroupState().hashCode());
        result = prime * result + ((getParentId() == null) ? 0 : getParentId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", groupName=").append(groupName);
        sb.append(", timeCreate=").append(timeCreate);
        sb.append(", module=").append(module);
        sb.append(", scene=").append(scene);
        sb.append(", uniqueValue=").append(uniqueValue);
        sb.append(", excuteResult=").append(excuteResult);
        sb.append(", excuteTime=").append(excuteTime);
        sb.append(", groupState=").append(groupState);
        sb.append(", parentId=").append(parentId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}