package com.shebei.api.equipment_api_test.model.responseModel;

public class ApiResModel {

    private  String url;
    private String requestBody;
    private String response;

    private String excuteResult;

    private String errMsg;


    public String getExcuteResult() {
        return excuteResult;
    }

    public void setExcuteResult(String excuteResult) {
        this.excuteResult = excuteResult;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
}
