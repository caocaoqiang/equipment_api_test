package com.shebei.api.equipment_api_test.model.responseModel;

public class JacocoConfigs {
    private Integer id;



    private String nowVersion;

    private String baseVersion;

   private  Integer versionId;

    private String jacocoType;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNowVersion() {
        return nowVersion;
    }

    public void setNowVersion(String nowVersion) {
        this.nowVersion = nowVersion;
    }

    public String getBaseVersion() {
        return baseVersion;
    }

    public void setBaseVersion(String baseVersion) {
        this.baseVersion = baseVersion;
    }

    public Integer getVersionId() {
        return versionId;
    }

    public void setVersionId(Integer versionId) {
        this.versionId = versionId;
    }

    public String getJacocoType() {
        return jacocoType;
    }

    public void setJacocoType(String jacocoType) {
        this.jacocoType = jacocoType;
    }
}
