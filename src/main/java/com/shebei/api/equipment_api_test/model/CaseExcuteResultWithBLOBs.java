package com.shebei.api.equipment_api_test.model;

import java.io.Serializable;

/**
 * @author 
 */
public class CaseExcuteResultWithBLOBs extends CaseExcuteResult implements Serializable {
    private String responseResult;

    private String errorMsg;

    private String requestBody;

    private static final long serialVersionUID = 1L;

    public String getResponseResult() {
        return responseResult;
    }

    public void setResponseResult(String responseResult) {
        this.responseResult = responseResult;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        CaseExcuteResultWithBLOBs other = (CaseExcuteResultWithBLOBs) that;
        return (this.getLinuxTime() == null ? other.getLinuxTime() == null : this.getLinuxTime().equals(other.getLinuxTime()))
            && (this.getExcuteTime() == null ? other.getExcuteTime() == null : this.getExcuteTime().equals(other.getExcuteTime()))
            && (this.getCaseName() == null ? other.getCaseName() == null : this.getCaseName().equals(other.getCaseName()))
            && (this.getApiName() == null ? other.getApiName() == null : this.getApiName().equals(other.getApiName()))
            && (this.getApiVaule() == null ? other.getApiVaule() == null : this.getApiVaule().equals(other.getApiVaule()))
            && (this.getCaseModule() == null ? other.getCaseModule() == null : this.getCaseModule().equals(other.getCaseModule()))
            && (this.getScene() == null ? other.getScene() == null : this.getScene().equals(other.getScene()))
            && (this.getEnv() == null ? other.getEnv() == null : this.getEnv().equals(other.getEnv()))
            && (this.getRequestType() == null ? other.getRequestType() == null : this.getRequestType().equals(other.getRequestType()))
            && (this.getExcuteResult() == null ? other.getExcuteResult() == null : this.getExcuteResult().equals(other.getExcuteResult()))
            && (this.getExcuteSequence() == null ? other.getExcuteSequence() == null : this.getExcuteSequence().equals(other.getExcuteSequence()))
            && (this.getCaseGroup() == null ? other.getCaseGroup() == null : this.getCaseGroup().equals(other.getCaseGroup()))
            && (this.getResponseResult() == null ? other.getResponseResult() == null : this.getResponseResult().equals(other.getResponseResult()))
            && (this.getErrorMsg() == null ? other.getErrorMsg() == null : this.getErrorMsg().equals(other.getErrorMsg()))
            && (this.getRequestBody() == null ? other.getRequestBody() == null : this.getRequestBody().equals(other.getRequestBody()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getLinuxTime() == null) ? 0 : getLinuxTime().hashCode());
        result = prime * result + ((getExcuteTime() == null) ? 0 : getExcuteTime().hashCode());
        result = prime * result + ((getCaseName() == null) ? 0 : getCaseName().hashCode());
        result = prime * result + ((getApiName() == null) ? 0 : getApiName().hashCode());
        result = prime * result + ((getApiVaule() == null) ? 0 : getApiVaule().hashCode());
        result = prime * result + ((getCaseModule() == null) ? 0 : getCaseModule().hashCode());
        result = prime * result + ((getScene() == null) ? 0 : getScene().hashCode());
        result = prime * result + ((getEnv() == null) ? 0 : getEnv().hashCode());
        result = prime * result + ((getRequestType() == null) ? 0 : getRequestType().hashCode());
        result = prime * result + ((getExcuteResult() == null) ? 0 : getExcuteResult().hashCode());
        result = prime * result + ((getExcuteSequence() == null) ? 0 : getExcuteSequence().hashCode());
        result = prime * result + ((getCaseGroup() == null) ? 0 : getCaseGroup().hashCode());
        result = prime * result + ((getResponseResult() == null) ? 0 : getResponseResult().hashCode());
        result = prime * result + ((getErrorMsg() == null) ? 0 : getErrorMsg().hashCode());
        result = prime * result + ((getRequestBody() == null) ? 0 : getRequestBody().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", responseResult=").append(responseResult);
        sb.append(", errorMsg=").append(errorMsg);
        sb.append(", requestBody=").append(requestBody);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}