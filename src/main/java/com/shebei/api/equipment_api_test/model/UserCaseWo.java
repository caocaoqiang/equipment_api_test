package com.shebei.api.equipment_api_test.model;

import java.io.Serializable;

/**
 * @author 
 */
public class UserCaseWo implements Serializable {
    private Integer id;

    private String module;

    private String userCaseName;

    private String apiName;

    private String apiValue;

    private String requestType;

    private String scene;

    private Integer excuteSequence;

    private String parameterSavePath;

    private String adaptiveVersion;

    private String apiGroup;

    private Integer timeWait;

    private String performanceExcute;

    private Integer isDelete;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getUserCaseName() {
        return userCaseName;
    }

    public void setUserCaseName(String userCaseName) {
        this.userCaseName = userCaseName;
    }

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public String getApiValue() {
        return apiValue;
    }

    public void setApiValue(String apiValue) {
        this.apiValue = apiValue;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    public Integer getExcuteSequence() {
        return excuteSequence;
    }

    public void setExcuteSequence(Integer excuteSequence) {
        this.excuteSequence = excuteSequence;
    }

    public String getParameterSavePath() {
        return parameterSavePath;
    }

    public void setParameterSavePath(String parameterSavePath) {
        this.parameterSavePath = parameterSavePath;
    }

    public String getAdaptiveVersion() {
        return adaptiveVersion;
    }

    public void setAdaptiveVersion(String adaptiveVersion) {
        this.adaptiveVersion = adaptiveVersion;
    }

    public String getApiGroup() {
        return apiGroup;
    }

    public void setApiGroup(String apiGroup) {
        this.apiGroup = apiGroup;
    }

    public Integer getTimeWait() {
        return timeWait;
    }

    public void setTimeWait(Integer timeWait) {
        this.timeWait = timeWait;
    }

    public String getPerformanceExcute() {
        return performanceExcute;
    }

    public void setPerformanceExcute(String performanceExcute) {
        this.performanceExcute = performanceExcute;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        UserCaseWo other = (UserCaseWo) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getModule() == null ? other.getModule() == null : this.getModule().equals(other.getModule()))
            && (this.getUserCaseName() == null ? other.getUserCaseName() == null : this.getUserCaseName().equals(other.getUserCaseName()))
            && (this.getApiName() == null ? other.getApiName() == null : this.getApiName().equals(other.getApiName()))
            && (this.getApiValue() == null ? other.getApiValue() == null : this.getApiValue().equals(other.getApiValue()))
            && (this.getRequestType() == null ? other.getRequestType() == null : this.getRequestType().equals(other.getRequestType()))
            && (this.getScene() == null ? other.getScene() == null : this.getScene().equals(other.getScene()))
            && (this.getExcuteSequence() == null ? other.getExcuteSequence() == null : this.getExcuteSequence().equals(other.getExcuteSequence()))
            && (this.getParameterSavePath() == null ? other.getParameterSavePath() == null : this.getParameterSavePath().equals(other.getParameterSavePath()))
            && (this.getAdaptiveVersion() == null ? other.getAdaptiveVersion() == null : this.getAdaptiveVersion().equals(other.getAdaptiveVersion()))
            && (this.getApiGroup() == null ? other.getApiGroup() == null : this.getApiGroup().equals(other.getApiGroup()))
            && (this.getTimeWait() == null ? other.getTimeWait() == null : this.getTimeWait().equals(other.getTimeWait()))
            && (this.getPerformanceExcute() == null ? other.getPerformanceExcute() == null : this.getPerformanceExcute().equals(other.getPerformanceExcute()))
            && (this.getIsDelete() == null ? other.getIsDelete() == null : this.getIsDelete().equals(other.getIsDelete()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getModule() == null) ? 0 : getModule().hashCode());
        result = prime * result + ((getUserCaseName() == null) ? 0 : getUserCaseName().hashCode());
        result = prime * result + ((getApiName() == null) ? 0 : getApiName().hashCode());
        result = prime * result + ((getApiValue() == null) ? 0 : getApiValue().hashCode());
        result = prime * result + ((getRequestType() == null) ? 0 : getRequestType().hashCode());
        result = prime * result + ((getScene() == null) ? 0 : getScene().hashCode());
        result = prime * result + ((getExcuteSequence() == null) ? 0 : getExcuteSequence().hashCode());
        result = prime * result + ((getParameterSavePath() == null) ? 0 : getParameterSavePath().hashCode());
        result = prime * result + ((getAdaptiveVersion() == null) ? 0 : getAdaptiveVersion().hashCode());
        result = prime * result + ((getApiGroup() == null) ? 0 : getApiGroup().hashCode());
        result = prime * result + ((getTimeWait() == null) ? 0 : getTimeWait().hashCode());
        result = prime * result + ((getPerformanceExcute() == null) ? 0 : getPerformanceExcute().hashCode());
        result = prime * result + ((getIsDelete() == null) ? 0 : getIsDelete().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", module=").append(module);
        sb.append(", userCaseName=").append(userCaseName);
        sb.append(", apiName=").append(apiName);
        sb.append(", apiValue=").append(apiValue);
        sb.append(", requestType=").append(requestType);
        sb.append(", scene=").append(scene);
        sb.append(", excuteSequence=").append(excuteSequence);
        sb.append(", parameterSavePath=").append(parameterSavePath);
        sb.append(", adaptiveVersion=").append(adaptiveVersion);
        sb.append(", apiGroup=").append(apiGroup);
        sb.append(", timeWait=").append(timeWait);
        sb.append(", performanceExcute=").append(performanceExcute);
        sb.append(", isDelete=").append(isDelete);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}