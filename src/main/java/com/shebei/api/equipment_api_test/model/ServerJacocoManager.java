package com.shebei.api.equipment_api_test.model;

import java.io.Serializable;

/**
 * @author 
 */
public class ServerJacocoManager implements Serializable {
    private Integer id;

    private String gitUrl;

    private String nowVersion;

    private String baseVersion;

    private String subModule;

    private String portJacoco;

    private String serverName;

    private String serverAddress;

    private String moduleName;

    private Integer superJacocoId;

    private String whiteModules;

    private String jacocoType;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGitUrl() {
        return gitUrl;
    }

    public void setGitUrl(String gitUrl) {
        this.gitUrl = gitUrl;
    }

    public String getNowVersion() {
        return nowVersion;
    }

    public void setNowVersion(String nowVersion) {
        this.nowVersion = nowVersion;
    }

    public String getBaseVersion() {
        return baseVersion;
    }

    public void setBaseVersion(String baseVersion) {
        this.baseVersion = baseVersion;
    }

    public String getSubModule() {
        return subModule;
    }

    public void setSubModule(String subModule) {
        this.subModule = subModule;
    }

    public String getPortJacoco() {
        return portJacoco;
    }

    public void setPortJacoco(String portJacoco) {
        this.portJacoco = portJacoco;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public Integer getSuperJacocoId() {
        return superJacocoId;
    }

    public void setSuperJacocoId(Integer superJacocoId) {
        this.superJacocoId = superJacocoId;
    }

    public String getWhiteModules() {
        return whiteModules;
    }

    public void setWhiteModules(String whiteModules) {
        this.whiteModules = whiteModules;
    }

    public String getJacocoType() {
        return jacocoType;
    }

    public void setJacocoType(String jacocoType) {
        this.jacocoType = jacocoType;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ServerJacocoManager other = (ServerJacocoManager) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getGitUrl() == null ? other.getGitUrl() == null : this.getGitUrl().equals(other.getGitUrl()))
            && (this.getNowVersion() == null ? other.getNowVersion() == null : this.getNowVersion().equals(other.getNowVersion()))
            && (this.getBaseVersion() == null ? other.getBaseVersion() == null : this.getBaseVersion().equals(other.getBaseVersion()))
            && (this.getSubModule() == null ? other.getSubModule() == null : this.getSubModule().equals(other.getSubModule()))
            && (this.getPortJacoco() == null ? other.getPortJacoco() == null : this.getPortJacoco().equals(other.getPortJacoco()))
            && (this.getServerName() == null ? other.getServerName() == null : this.getServerName().equals(other.getServerName()))
            && (this.getServerAddress() == null ? other.getServerAddress() == null : this.getServerAddress().equals(other.getServerAddress()))
            && (this.getModuleName() == null ? other.getModuleName() == null : this.getModuleName().equals(other.getModuleName()))
            && (this.getSuperJacocoId() == null ? other.getSuperJacocoId() == null : this.getSuperJacocoId().equals(other.getSuperJacocoId()))
            && (this.getWhiteModules() == null ? other.getWhiteModules() == null : this.getWhiteModules().equals(other.getWhiteModules()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getGitUrl() == null) ? 0 : getGitUrl().hashCode());
        result = prime * result + ((getNowVersion() == null) ? 0 : getNowVersion().hashCode());
        result = prime * result + ((getBaseVersion() == null) ? 0 : getBaseVersion().hashCode());
        result = prime * result + ((getSubModule() == null) ? 0 : getSubModule().hashCode());
        result = prime * result + ((getPortJacoco() == null) ? 0 : getPortJacoco().hashCode());
        result = prime * result + ((getServerName() == null) ? 0 : getServerName().hashCode());
        result = prime * result + ((getServerAddress() == null) ? 0 : getServerAddress().hashCode());
        result = prime * result + ((getModuleName() == null) ? 0 : getModuleName().hashCode());
        result = prime * result + ((getSuperJacocoId() == null) ? 0 : getSuperJacocoId().hashCode());
        result = prime * result + ((getWhiteModules() == null) ? 0 : getWhiteModules().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gitUrl=").append(gitUrl);
        sb.append(", nowVersion=").append(nowVersion);
        sb.append(", baseVersion=").append(baseVersion);
        sb.append(", subModule=").append(subModule);
        sb.append(", portJacoco=").append(portJacoco);
        sb.append(", serverName=").append(serverName);
        sb.append(", serverAddress=").append(serverAddress);
        sb.append(", moduleName=").append(moduleName);
        sb.append(", superJacocoId=").append(superJacocoId);
        sb.append(", whiteModules=").append(whiteModules);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}