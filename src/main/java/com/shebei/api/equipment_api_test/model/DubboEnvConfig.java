package com.shebei.api.equipment_api_test.model;

import java.io.Serializable;

/**
 * dubbo_env_config
 * @author 
 */
public class DubboEnvConfig implements Serializable {
    private Integer id;

    private String moduleName;

    private String severName;

    private String dubboName;

    private String methodName;

    private String segmentName;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getSeverName() {
        return severName;
    }

    public void setSeverName(String severName) {
        this.severName = severName;
    }

    public String getDubboName() {
        return dubboName;
    }

    public void setDubboName(String dubboName) {
        this.dubboName = dubboName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getSegmentName() {
        return segmentName;
    }

    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }
}