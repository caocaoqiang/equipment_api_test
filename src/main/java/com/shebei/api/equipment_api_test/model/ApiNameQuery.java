package com.shebei.api.equipment_api_test.model;

import java.io.Serializable;

/**
 * @author 
 */
public class ApiNameQuery implements Serializable {
    private Integer id;

    private String apiCategory;

    private String apiName;

    private String apiValue;

    private String apiType;

    private String headInfo;

    private String requestType;

    private String apiDescribe;

    private String apiGroup;

    private String apiNavigateValue;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getApiCategory() {
        return apiCategory;
    }

    public void setApiCategory(String apiCategory) {
        this.apiCategory = apiCategory;
    }

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public String getApiValue() {
        return apiValue;
    }

    public void setApiValue(String apiValue) {
        this.apiValue = apiValue;
    }

    public String getApiType() {
        return apiType;
    }

    public void setApiType(String apiType) {
        this.apiType = apiType;
    }

    public String getHeadInfo() {
        return headInfo;
    }

    public void setHeadInfo(String headInfo) {
        this.headInfo = headInfo;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getApiDescribe() {
        return apiDescribe;
    }

    public void setApiDescribe(String apiDescribe) {
        this.apiDescribe = apiDescribe;
    }

    public String getApiGroup() {
        return apiGroup;
    }

    public void setApiGroup(String apiGroup) {
        this.apiGroup = apiGroup;
    }

    public String getApiNavigateValue() {
        return apiNavigateValue;
    }

    public void setApiNavigateValue(String apiNavigateValue) {
        this.apiNavigateValue = apiNavigateValue;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ApiNameQuery other = (ApiNameQuery) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getApiCategory() == null ? other.getApiCategory() == null : this.getApiCategory().equals(other.getApiCategory()))
            && (this.getApiName() == null ? other.getApiName() == null : this.getApiName().equals(other.getApiName()))
            && (this.getApiValue() == null ? other.getApiValue() == null : this.getApiValue().equals(other.getApiValue()))
            && (this.getApiType() == null ? other.getApiType() == null : this.getApiType().equals(other.getApiType()))
            && (this.getHeadInfo() == null ? other.getHeadInfo() == null : this.getHeadInfo().equals(other.getHeadInfo()))
            && (this.getRequestType() == null ? other.getRequestType() == null : this.getRequestType().equals(other.getRequestType()))
            && (this.getApiDescribe() == null ? other.getApiDescribe() == null : this.getApiDescribe().equals(other.getApiDescribe()))
            && (this.getApiGroup() == null ? other.getApiGroup() == null : this.getApiGroup().equals(other.getApiGroup()))
            && (this.getApiNavigateValue() == null ? other.getApiNavigateValue() == null : this.getApiNavigateValue().equals(other.getApiNavigateValue()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getApiCategory() == null) ? 0 : getApiCategory().hashCode());
        result = prime * result + ((getApiName() == null) ? 0 : getApiName().hashCode());
        result = prime * result + ((getApiValue() == null) ? 0 : getApiValue().hashCode());
        result = prime * result + ((getApiType() == null) ? 0 : getApiType().hashCode());
        result = prime * result + ((getHeadInfo() == null) ? 0 : getHeadInfo().hashCode());
        result = prime * result + ((getRequestType() == null) ? 0 : getRequestType().hashCode());
        result = prime * result + ((getApiDescribe() == null) ? 0 : getApiDescribe().hashCode());
        result = prime * result + ((getApiGroup() == null) ? 0 : getApiGroup().hashCode());
        result = prime * result + ((getApiNavigateValue() == null) ? 0 : getApiNavigateValue().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", apiCategory=").append(apiCategory);
        sb.append(", apiName=").append(apiName);
        sb.append(", apiValue=").append(apiValue);
        sb.append(", apiType=").append(apiType);
        sb.append(", headInfo=").append(headInfo);
        sb.append(", requestType=").append(requestType);
        sb.append(", apiDescribe=").append(apiDescribe);
        sb.append(", apiGroup=").append(apiGroup);
        sb.append(", apiNavigateValue=").append(apiNavigateValue);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}