package com.shebei.api.equipment_api_test.model;

import java.util.ArrayList;
import java.util.List;

public class TestVersionRecordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Integer offset;

    public TestVersionRecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andModuleNameIsNull() {
            addCriterion("module_name is null");
            return (Criteria) this;
        }

        public Criteria andModuleNameIsNotNull() {
            addCriterion("module_name is not null");
            return (Criteria) this;
        }

        public Criteria andModuleNameEqualTo(String value) {
            addCriterion("module_name =", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameNotEqualTo(String value) {
            addCriterion("module_name <>", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameGreaterThan(String value) {
            addCriterion("module_name >", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameGreaterThanOrEqualTo(String value) {
            addCriterion("module_name >=", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameLessThan(String value) {
            addCriterion("module_name <", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameLessThanOrEqualTo(String value) {
            addCriterion("module_name <=", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameLike(String value) {
            addCriterion("module_name like", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameNotLike(String value) {
            addCriterion("module_name not like", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameIn(List<String> values) {
            addCriterion("module_name in", values, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameNotIn(List<String> values) {
            addCriterion("module_name not in", values, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameBetween(String value1, String value2) {
            addCriterion("module_name between", value1, value2, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameNotBetween(String value1, String value2) {
            addCriterion("module_name not between", value1, value2, "moduleName");
            return (Criteria) this;
        }

        public Criteria andTestVersionIsNull() {
            addCriterion("test_version is null");
            return (Criteria) this;
        }

        public Criteria andTestVersionIsNotNull() {
            addCriterion("test_version is not null");
            return (Criteria) this;
        }

        public Criteria andTestVersionEqualTo(String value) {
            addCriterion("test_version =", value, "testVersion");
            return (Criteria) this;
        }

        public Criteria andTestVersionNotEqualTo(String value) {
            addCriterion("test_version <>", value, "testVersion");
            return (Criteria) this;
        }

        public Criteria andTestVersionGreaterThan(String value) {
            addCriterion("test_version >", value, "testVersion");
            return (Criteria) this;
        }

        public Criteria andTestVersionGreaterThanOrEqualTo(String value) {
            addCriterion("test_version >=", value, "testVersion");
            return (Criteria) this;
        }

        public Criteria andTestVersionLessThan(String value) {
            addCriterion("test_version <", value, "testVersion");
            return (Criteria) this;
        }

        public Criteria andTestVersionLessThanOrEqualTo(String value) {
            addCriterion("test_version <=", value, "testVersion");
            return (Criteria) this;
        }

        public Criteria andTestVersionLike(String value) {
            addCriterion("test_version like", value, "testVersion");
            return (Criteria) this;
        }

        public Criteria andTestVersionNotLike(String value) {
            addCriterion("test_version not like", value, "testVersion");
            return (Criteria) this;
        }

        public Criteria andTestVersionIn(List<String> values) {
            addCriterion("test_version in", values, "testVersion");
            return (Criteria) this;
        }

        public Criteria andTestVersionNotIn(List<String> values) {
            addCriterion("test_version not in", values, "testVersion");
            return (Criteria) this;
        }

        public Criteria andTestVersionBetween(String value1, String value2) {
            addCriterion("test_version between", value1, value2, "testVersion");
            return (Criteria) this;
        }

        public Criteria andTestVersionNotBetween(String value1, String value2) {
            addCriterion("test_version not between", value1, value2, "testVersion");
            return (Criteria) this;
        }

        public Criteria andServerNameIsNull() {
            addCriterion("server_name is null");
            return (Criteria) this;
        }

        public Criteria andServerNameIsNotNull() {
            addCriterion("server_name is not null");
            return (Criteria) this;
        }

        public Criteria andServerNameEqualTo(String value) {
            addCriterion("server_name =", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameNotEqualTo(String value) {
            addCriterion("server_name <>", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameGreaterThan(String value) {
            addCriterion("server_name >", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameGreaterThanOrEqualTo(String value) {
            addCriterion("server_name >=", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameLessThan(String value) {
            addCriterion("server_name <", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameLessThanOrEqualTo(String value) {
            addCriterion("server_name <=", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameLike(String value) {
            addCriterion("server_name like", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameNotLike(String value) {
            addCriterion("server_name not like", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameIn(List<String> values) {
            addCriterion("server_name in", values, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameNotIn(List<String> values) {
            addCriterion("server_name not in", values, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameBetween(String value1, String value2) {
            addCriterion("server_name between", value1, value2, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameNotBetween(String value1, String value2) {
            addCriterion("server_name not between", value1, value2, "serverName");
            return (Criteria) this;
        }

        public Criteria andUuidRecordIsNull() {
            addCriterion("uuid_record is null");
            return (Criteria) this;
        }

        public Criteria andUuidRecordIsNotNull() {
            addCriterion("uuid_record is not null");
            return (Criteria) this;
        }

        public Criteria andUuidRecordEqualTo(String value) {
            addCriterion("uuid_record =", value, "uuidRecord");
            return (Criteria) this;
        }

        public Criteria andUuidRecordNotEqualTo(String value) {
            addCriterion("uuid_record <>", value, "uuidRecord");
            return (Criteria) this;
        }

        public Criteria andUuidRecordGreaterThan(String value) {
            addCriterion("uuid_record >", value, "uuidRecord");
            return (Criteria) this;
        }

        public Criteria andUuidRecordGreaterThanOrEqualTo(String value) {
            addCriterion("uuid_record >=", value, "uuidRecord");
            return (Criteria) this;
        }

        public Criteria andUuidRecordLessThan(String value) {
            addCriterion("uuid_record <", value, "uuidRecord");
            return (Criteria) this;
        }

        public Criteria andUuidRecordLessThanOrEqualTo(String value) {
            addCriterion("uuid_record <=", value, "uuidRecord");
            return (Criteria) this;
        }

        public Criteria andUuidRecordLike(String value) {
            addCriterion("uuid_record like", value, "uuidRecord");
            return (Criteria) this;
        }

        public Criteria andUuidRecordNotLike(String value) {
            addCriterion("uuid_record not like", value, "uuidRecord");
            return (Criteria) this;
        }

        public Criteria andUuidRecordIn(List<String> values) {
            addCriterion("uuid_record in", values, "uuidRecord");
            return (Criteria) this;
        }

        public Criteria andUuidRecordNotIn(List<String> values) {
            addCriterion("uuid_record not in", values, "uuidRecord");
            return (Criteria) this;
        }

        public Criteria andUuidRecordBetween(String value1, String value2) {
            addCriterion("uuid_record between", value1, value2, "uuidRecord");
            return (Criteria) this;
        }

        public Criteria andUuidRecordNotBetween(String value1, String value2) {
            addCriterion("uuid_record not between", value1, value2, "uuidRecord");
            return (Criteria) this;
        }

        public Criteria andLastUuidRecordIsNull() {
            addCriterion("last_uuid_record is null");
            return (Criteria) this;
        }

        public Criteria andLastUuidRecordIsNotNull() {
            addCriterion("last_uuid_record is not null");
            return (Criteria) this;
        }

        public Criteria andLastUuidRecordEqualTo(String value) {
            addCriterion("last_uuid_record =", value, "lastUuidRecord");
            return (Criteria) this;
        }

        public Criteria andLastUuidRecordNotEqualTo(String value) {
            addCriterion("last_uuid_record <>", value, "lastUuidRecord");
            return (Criteria) this;
        }

        public Criteria andLastUuidRecordGreaterThan(String value) {
            addCriterion("last_uuid_record >", value, "lastUuidRecord");
            return (Criteria) this;
        }

        public Criteria andLastUuidRecordGreaterThanOrEqualTo(String value) {
            addCriterion("last_uuid_record >=", value, "lastUuidRecord");
            return (Criteria) this;
        }

        public Criteria andLastUuidRecordLessThan(String value) {
            addCriterion("last_uuid_record <", value, "lastUuidRecord");
            return (Criteria) this;
        }

        public Criteria andLastUuidRecordLessThanOrEqualTo(String value) {
            addCriterion("last_uuid_record <=", value, "lastUuidRecord");
            return (Criteria) this;
        }

        public Criteria andLastUuidRecordLike(String value) {
            addCriterion("last_uuid_record like", value, "lastUuidRecord");
            return (Criteria) this;
        }

        public Criteria andLastUuidRecordNotLike(String value) {
            addCriterion("last_uuid_record not like", value, "lastUuidRecord");
            return (Criteria) this;
        }

        public Criteria andLastUuidRecordIn(List<String> values) {
            addCriterion("last_uuid_record in", values, "lastUuidRecord");
            return (Criteria) this;
        }

        public Criteria andLastUuidRecordNotIn(List<String> values) {
            addCriterion("last_uuid_record not in", values, "lastUuidRecord");
            return (Criteria) this;
        }

        public Criteria andLastUuidRecordBetween(String value1, String value2) {
            addCriterion("last_uuid_record between", value1, value2, "lastUuidRecord");
            return (Criteria) this;
        }

        public Criteria andLastUuidRecordNotBetween(String value1, String value2) {
            addCriterion("last_uuid_record not between", value1, value2, "lastUuidRecord");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeIsNull() {
            addCriterion("jacoco_type is null");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeIsNotNull() {
            addCriterion("jacoco_type is not null");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeEqualTo(String value) {
            addCriterion("jacoco_type =", value, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeNotEqualTo(String value) {
            addCriterion("jacoco_type <>", value, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeGreaterThan(String value) {
            addCriterion("jacoco_type >", value, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeGreaterThanOrEqualTo(String value) {
            addCriterion("jacoco_type >=", value, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeLessThan(String value) {
            addCriterion("jacoco_type <", value, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeLessThanOrEqualTo(String value) {
            addCriterion("jacoco_type <=", value, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeLike(String value) {
            addCriterion("jacoco_type like", value, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeNotLike(String value) {
            addCriterion("jacoco_type not like", value, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeIn(List<String> values) {
            addCriterion("jacoco_type in", values, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeNotIn(List<String> values) {
            addCriterion("jacoco_type not in", values, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeBetween(String value1, String value2) {
            addCriterion("jacoco_type between", value1, value2, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andJacocoTypeNotBetween(String value1, String value2) {
            addCriterion("jacoco_type not between", value1, value2, "jacocoType");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(String value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(String value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(String value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(String value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(String value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(String value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLike(String value) {
            addCriterion("create_time like", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotLike(String value) {
            addCriterion("create_time not like", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<String> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<String> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(String value1, String value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(String value1, String value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(String value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(String value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(String value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(String value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(String value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(String value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLike(String value) {
            addCriterion("update_time like", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotLike(String value) {
            addCriterion("update_time not like", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<String> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<String> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(String value1, String value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(String value1, String value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andLastestReportIsNull() {
            addCriterion("lastest_report is null");
            return (Criteria) this;
        }

        public Criteria andLastestReportIsNotNull() {
            addCriterion("lastest_report is not null");
            return (Criteria) this;
        }

        public Criteria andLastestReportEqualTo(String value) {
            addCriterion("lastest_report =", value, "lastestReport");
            return (Criteria) this;
        }

        public Criteria andLastestReportNotEqualTo(String value) {
            addCriterion("lastest_report <>", value, "lastestReport");
            return (Criteria) this;
        }

        public Criteria andLastestReportGreaterThan(String value) {
            addCriterion("lastest_report >", value, "lastestReport");
            return (Criteria) this;
        }

        public Criteria andLastestReportGreaterThanOrEqualTo(String value) {
            addCriterion("lastest_report >=", value, "lastestReport");
            return (Criteria) this;
        }

        public Criteria andLastestReportLessThan(String value) {
            addCriterion("lastest_report <", value, "lastestReport");
            return (Criteria) this;
        }

        public Criteria andLastestReportLessThanOrEqualTo(String value) {
            addCriterion("lastest_report <=", value, "lastestReport");
            return (Criteria) this;
        }

        public Criteria andLastestReportLike(String value) {
            addCriterion("lastest_report like", value, "lastestReport");
            return (Criteria) this;
        }

        public Criteria andLastestReportNotLike(String value) {
            addCriterion("lastest_report not like", value, "lastestReport");
            return (Criteria) this;
        }

        public Criteria andLastestReportIn(List<String> values) {
            addCriterion("lastest_report in", values, "lastestReport");
            return (Criteria) this;
        }

        public Criteria andLastestReportNotIn(List<String> values) {
            addCriterion("lastest_report not in", values, "lastestReport");
            return (Criteria) this;
        }

        public Criteria andLastestReportBetween(String value1, String value2) {
            addCriterion("lastest_report between", value1, value2, "lastestReport");
            return (Criteria) this;
        }

        public Criteria andLastestReportNotBetween(String value1, String value2) {
            addCriterion("lastest_report not between", value1, value2, "lastestReport");
            return (Criteria) this;
        }

        public Criteria andUuidBackupIsNull() {
            addCriterion("uuid_backup is null");
            return (Criteria) this;
        }

        public Criteria andUuidBackupIsNotNull() {
            addCriterion("uuid_backup is not null");
            return (Criteria) this;
        }

        public Criteria andUuidBackupEqualTo(String value) {
            addCriterion("uuid_backup =", value, "uuidBackup");
            return (Criteria) this;
        }

        public Criteria andUuidBackupNotEqualTo(String value) {
            addCriterion("uuid_backup <>", value, "uuidBackup");
            return (Criteria) this;
        }

        public Criteria andUuidBackupGreaterThan(String value) {
            addCriterion("uuid_backup >", value, "uuidBackup");
            return (Criteria) this;
        }

        public Criteria andUuidBackupGreaterThanOrEqualTo(String value) {
            addCriterion("uuid_backup >=", value, "uuidBackup");
            return (Criteria) this;
        }

        public Criteria andUuidBackupLessThan(String value) {
            addCriterion("uuid_backup <", value, "uuidBackup");
            return (Criteria) this;
        }

        public Criteria andUuidBackupLessThanOrEqualTo(String value) {
            addCriterion("uuid_backup <=", value, "uuidBackup");
            return (Criteria) this;
        }

        public Criteria andUuidBackupLike(String value) {
            addCriterion("uuid_backup like", value, "uuidBackup");
            return (Criteria) this;
        }

        public Criteria andUuidBackupNotLike(String value) {
            addCriterion("uuid_backup not like", value, "uuidBackup");
            return (Criteria) this;
        }

        public Criteria andUuidBackupIn(List<String> values) {
            addCriterion("uuid_backup in", values, "uuidBackup");
            return (Criteria) this;
        }

        public Criteria andUuidBackupNotIn(List<String> values) {
            addCriterion("uuid_backup not in", values, "uuidBackup");
            return (Criteria) this;
        }

        public Criteria andUuidBackupBetween(String value1, String value2) {
            addCriterion("uuid_backup between", value1, value2, "uuidBackup");
            return (Criteria) this;
        }

        public Criteria andUuidBackupNotBetween(String value1, String value2) {
            addCriterion("uuid_backup not between", value1, value2, "uuidBackup");
            return (Criteria) this;
        }

        public Criteria andVIdIsNull() {
            addCriterion("v_id is null");
            return (Criteria) this;
        }

        public Criteria andVIdIsNotNull() {
            addCriterion("v_id is not null");
            return (Criteria) this;
        }

        public Criteria andVIdEqualTo(Integer value) {
            addCriterion("v_id =", value, "vId");
            return (Criteria) this;
        }

        public Criteria andVIdNotEqualTo(Integer value) {
            addCriterion("v_id <>", value, "vId");
            return (Criteria) this;
        }

        public Criteria andVIdGreaterThan(Integer value) {
            addCriterion("v_id >", value, "vId");
            return (Criteria) this;
        }

        public Criteria andVIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("v_id >=", value, "vId");
            return (Criteria) this;
        }

        public Criteria andVIdLessThan(Integer value) {
            addCriterion("v_id <", value, "vId");
            return (Criteria) this;
        }

        public Criteria andVIdLessThanOrEqualTo(Integer value) {
            addCriterion("v_id <=", value, "vId");
            return (Criteria) this;
        }

        public Criteria andVIdIn(List<Integer> values) {
            addCriterion("v_id in", values, "vId");
            return (Criteria) this;
        }

        public Criteria andVIdNotIn(List<Integer> values) {
            addCriterion("v_id not in", values, "vId");
            return (Criteria) this;
        }

        public Criteria andVIdBetween(Integer value1, Integer value2) {
            addCriterion("v_id between", value1, value2, "vId");
            return (Criteria) this;
        }

        public Criteria andVIdNotBetween(Integer value1, Integer value2) {
            addCriterion("v_id not between", value1, value2, "vId");
            return (Criteria) this;
        }

        public Criteria andReportResultIsNull() {
            addCriterion("report_result is null");
            return (Criteria) this;
        }

        public Criteria andReportResultIsNotNull() {
            addCriterion("report_result is not null");
            return (Criteria) this;
        }

        public Criteria andReportResultEqualTo(String value) {
            addCriterion("report_result =", value, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultNotEqualTo(String value) {
            addCriterion("report_result <>", value, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultGreaterThan(String value) {
            addCriterion("report_result >", value, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultGreaterThanOrEqualTo(String value) {
            addCriterion("report_result >=", value, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultLessThan(String value) {
            addCriterion("report_result <", value, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultLessThanOrEqualTo(String value) {
            addCriterion("report_result <=", value, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultLike(String value) {
            addCriterion("report_result like", value, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultNotLike(String value) {
            addCriterion("report_result not like", value, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultIn(List<String> values) {
            addCriterion("report_result in", values, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultNotIn(List<String> values) {
            addCriterion("report_result not in", values, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultBetween(String value1, String value2) {
            addCriterion("report_result between", value1, value2, "reportResult");
            return (Criteria) this;
        }

        public Criteria andReportResultNotBetween(String value1, String value2) {
            addCriterion("report_result not between", value1, value2, "reportResult");
            return (Criteria) this;
        }

        public Criteria andErrMsgIsNull() {
            addCriterion("err_msg is null");
            return (Criteria) this;
        }

        public Criteria andErrMsgIsNotNull() {
            addCriterion("err_msg is not null");
            return (Criteria) this;
        }

        public Criteria andErrMsgEqualTo(String value) {
            addCriterion("err_msg =", value, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgNotEqualTo(String value) {
            addCriterion("err_msg <>", value, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgGreaterThan(String value) {
            addCriterion("err_msg >", value, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgGreaterThanOrEqualTo(String value) {
            addCriterion("err_msg >=", value, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgLessThan(String value) {
            addCriterion("err_msg <", value, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgLessThanOrEqualTo(String value) {
            addCriterion("err_msg <=", value, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgLike(String value) {
            addCriterion("err_msg like", value, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgNotLike(String value) {
            addCriterion("err_msg not like", value, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgIn(List<String> values) {
            addCriterion("err_msg in", values, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgNotIn(List<String> values) {
            addCriterion("err_msg not in", values, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgBetween(String value1, String value2) {
            addCriterion("err_msg between", value1, value2, "errMsg");
            return (Criteria) this;
        }

        public Criteria andErrMsgNotBetween(String value1, String value2) {
            addCriterion("err_msg not between", value1, value2, "errMsg");
            return (Criteria) this;
        }

        public Criteria andBaseVersionIsNull() {
            addCriterion("base_version is null");
            return (Criteria) this;
        }

        public Criteria andBaseVersionIsNotNull() {
            addCriterion("base_version is not null");
            return (Criteria) this;
        }

        public Criteria andBaseVersionEqualTo(String value) {
            addCriterion("base_version =", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionNotEqualTo(String value) {
            addCriterion("base_version <>", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionGreaterThan(String value) {
            addCriterion("base_version >", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionGreaterThanOrEqualTo(String value) {
            addCriterion("base_version >=", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionLessThan(String value) {
            addCriterion("base_version <", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionLessThanOrEqualTo(String value) {
            addCriterion("base_version <=", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionLike(String value) {
            addCriterion("base_version like", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionNotLike(String value) {
            addCriterion("base_version not like", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionIn(List<String> values) {
            addCriterion("base_version in", values, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionNotIn(List<String> values) {
            addCriterion("base_version not in", values, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionBetween(String value1, String value2) {
            addCriterion("base_version between", value1, value2, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionNotBetween(String value1, String value2) {
            addCriterion("base_version not between", value1, value2, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionIsNull() {
            addCriterion("now_version is null");
            return (Criteria) this;
        }

        public Criteria andNowVersionIsNotNull() {
            addCriterion("now_version is not null");
            return (Criteria) this;
        }

        public Criteria andNowVersionEqualTo(String value) {
            addCriterion("now_version =", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionNotEqualTo(String value) {
            addCriterion("now_version <>", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionGreaterThan(String value) {
            addCriterion("now_version >", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionGreaterThanOrEqualTo(String value) {
            addCriterion("now_version >=", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionLessThan(String value) {
            addCriterion("now_version <", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionLessThanOrEqualTo(String value) {
            addCriterion("now_version <=", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionLike(String value) {
            addCriterion("now_version like", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionNotLike(String value) {
            addCriterion("now_version not like", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionIn(List<String> values) {
            addCriterion("now_version in", values, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionNotIn(List<String> values) {
            addCriterion("now_version not in", values, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionBetween(String value1, String value2) {
            addCriterion("now_version between", value1, value2, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionNotBetween(String value1, String value2) {
            addCriterion("now_version not between", value1, value2, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andJacocoServerIdIsNull() {
            addCriterion("jacoco_server_id is null");
            return (Criteria) this;
        }

        public Criteria andJacocoServerIdIsNotNull() {
            addCriterion("jacoco_server_id is not null");
            return (Criteria) this;
        }

        public Criteria andJacocoServerIdEqualTo(Integer value) {
            addCriterion("jacoco_server_id =", value, "jacocoServerId");
            return (Criteria) this;
        }

        public Criteria andJacocoServerIdNotEqualTo(Integer value) {
            addCriterion("jacoco_server_id <>", value, "jacocoServerId");
            return (Criteria) this;
        }

        public Criteria andJacocoServerIdGreaterThan(Integer value) {
            addCriterion("jacoco_server_id >", value, "jacocoServerId");
            return (Criteria) this;
        }

        public Criteria andJacocoServerIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("jacoco_server_id >=", value, "jacocoServerId");
            return (Criteria) this;
        }

        public Criteria andJacocoServerIdLessThan(Integer value) {
            addCriterion("jacoco_server_id <", value, "jacocoServerId");
            return (Criteria) this;
        }

        public Criteria andJacocoServerIdLessThanOrEqualTo(Integer value) {
            addCriterion("jacoco_server_id <=", value, "jacocoServerId");
            return (Criteria) this;
        }

        public Criteria andJacocoServerIdIn(List<Integer> values) {
            addCriterion("jacoco_server_id in", values, "jacocoServerId");
            return (Criteria) this;
        }

        public Criteria andJacocoServerIdNotIn(List<Integer> values) {
            addCriterion("jacoco_server_id not in", values, "jacocoServerId");
            return (Criteria) this;
        }

        public Criteria andJacocoServerIdBetween(Integer value1, Integer value2) {
            addCriterion("jacoco_server_id between", value1, value2, "jacocoServerId");
            return (Criteria) this;
        }

        public Criteria andJacocoServerIdNotBetween(Integer value1, Integer value2) {
            addCriterion("jacoco_server_id not between", value1, value2, "jacocoServerId");
            return (Criteria) this;
        }

        public Criteria andExecStateIsNull() {
            addCriterion("exec_state is null");
            return (Criteria) this;
        }

        public Criteria andExecStateIsNotNull() {
            addCriterion("exec_state is not null");
            return (Criteria) this;
        }

        public Criteria andExecStateEqualTo(String value) {
            addCriterion("exec_state =", value, "execState");
            return (Criteria) this;
        }

        public Criteria andExecStateNotEqualTo(String value) {
            addCriterion("exec_state <>", value, "execState");
            return (Criteria) this;
        }

        public Criteria andExecStateGreaterThan(String value) {
            addCriterion("exec_state >", value, "execState");
            return (Criteria) this;
        }

        public Criteria andExecStateGreaterThanOrEqualTo(String value) {
            addCriterion("exec_state >=", value, "execState");
            return (Criteria) this;
        }

        public Criteria andExecStateLessThan(String value) {
            addCriterion("exec_state <", value, "execState");
            return (Criteria) this;
        }

        public Criteria andExecStateLessThanOrEqualTo(String value) {
            addCriterion("exec_state <=", value, "execState");
            return (Criteria) this;
        }

        public Criteria andExecStateLike(String value) {
            addCriterion("exec_state like", value, "execState");
            return (Criteria) this;
        }

        public Criteria andExecStateNotLike(String value) {
            addCriterion("exec_state not like", value, "execState");
            return (Criteria) this;
        }

        public Criteria andExecStateIn(List<String> values) {
            addCriterion("exec_state in", values, "execState");
            return (Criteria) this;
        }

        public Criteria andExecStateNotIn(List<String> values) {
            addCriterion("exec_state not in", values, "execState");
            return (Criteria) this;
        }

        public Criteria andExecStateBetween(String value1, String value2) {
            addCriterion("exec_state between", value1, value2, "execState");
            return (Criteria) this;
        }

        public Criteria andExecStateNotBetween(String value1, String value2) {
            addCriterion("exec_state not between", value1, value2, "execState");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}