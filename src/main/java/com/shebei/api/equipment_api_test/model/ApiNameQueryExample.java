package com.shebei.api.equipment_api_test.model;

import java.util.ArrayList;
import java.util.List;

public class ApiNameQueryExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Integer offset;

    public ApiNameQueryExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andApiCategoryIsNull() {
            addCriterion("api_category is null");
            return (Criteria) this;
        }

        public Criteria andApiCategoryIsNotNull() {
            addCriterion("api_category is not null");
            return (Criteria) this;
        }

        public Criteria andApiCategoryEqualTo(String value) {
            addCriterion("api_category =", value, "apiCategory");
            return (Criteria) this;
        }

        public Criteria andApiCategoryNotEqualTo(String value) {
            addCriterion("api_category <>", value, "apiCategory");
            return (Criteria) this;
        }

        public Criteria andApiCategoryGreaterThan(String value) {
            addCriterion("api_category >", value, "apiCategory");
            return (Criteria) this;
        }

        public Criteria andApiCategoryGreaterThanOrEqualTo(String value) {
            addCriterion("api_category >=", value, "apiCategory");
            return (Criteria) this;
        }

        public Criteria andApiCategoryLessThan(String value) {
            addCriterion("api_category <", value, "apiCategory");
            return (Criteria) this;
        }

        public Criteria andApiCategoryLessThanOrEqualTo(String value) {
            addCriterion("api_category <=", value, "apiCategory");
            return (Criteria) this;
        }

        public Criteria andApiCategoryLike(String value) {
            addCriterion("api_category like", value, "apiCategory");
            return (Criteria) this;
        }

        public Criteria andApiCategoryNotLike(String value) {
            addCriterion("api_category not like", value, "apiCategory");
            return (Criteria) this;
        }

        public Criteria andApiCategoryIn(List<String> values) {
            addCriterion("api_category in", values, "apiCategory");
            return (Criteria) this;
        }

        public Criteria andApiCategoryNotIn(List<String> values) {
            addCriterion("api_category not in", values, "apiCategory");
            return (Criteria) this;
        }

        public Criteria andApiCategoryBetween(String value1, String value2) {
            addCriterion("api_category between", value1, value2, "apiCategory");
            return (Criteria) this;
        }

        public Criteria andApiCategoryNotBetween(String value1, String value2) {
            addCriterion("api_category not between", value1, value2, "apiCategory");
            return (Criteria) this;
        }

        public Criteria andApiNameIsNull() {
            addCriterion("api_name is null");
            return (Criteria) this;
        }

        public Criteria andApiNameIsNotNull() {
            addCriterion("api_name is not null");
            return (Criteria) this;
        }

        public Criteria andApiNameEqualTo(String value) {
            addCriterion("api_name =", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameNotEqualTo(String value) {
            addCriterion("api_name <>", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameGreaterThan(String value) {
            addCriterion("api_name >", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameGreaterThanOrEqualTo(String value) {
            addCriterion("api_name >=", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameLessThan(String value) {
            addCriterion("api_name <", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameLessThanOrEqualTo(String value) {
            addCriterion("api_name <=", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameLike(String value) {
            addCriterion("api_name like", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameNotLike(String value) {
            addCriterion("api_name not like", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameIn(List<String> values) {
            addCriterion("api_name in", values, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameNotIn(List<String> values) {
            addCriterion("api_name not in", values, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameBetween(String value1, String value2) {
            addCriterion("api_name between", value1, value2, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameNotBetween(String value1, String value2) {
            addCriterion("api_name not between", value1, value2, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiValueIsNull() {
            addCriterion("api_value is null");
            return (Criteria) this;
        }

        public Criteria andApiValueIsNotNull() {
            addCriterion("api_value is not null");
            return (Criteria) this;
        }

        public Criteria andApiValueEqualTo(String value) {
            addCriterion("api_value =", value, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueNotEqualTo(String value) {
            addCriterion("api_value <>", value, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueGreaterThan(String value) {
            addCriterion("api_value >", value, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueGreaterThanOrEqualTo(String value) {
            addCriterion("api_value >=", value, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueLessThan(String value) {
            addCriterion("api_value <", value, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueLessThanOrEqualTo(String value) {
            addCriterion("api_value <=", value, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueLike(String value) {
            addCriterion("api_value like", value, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueNotLike(String value) {
            addCriterion("api_value not like", value, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueIn(List<String> values) {
            addCriterion("api_value in", values, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueNotIn(List<String> values) {
            addCriterion("api_value not in", values, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueBetween(String value1, String value2) {
            addCriterion("api_value between", value1, value2, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueNotBetween(String value1, String value2) {
            addCriterion("api_value not between", value1, value2, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiTypeIsNull() {
            addCriterion("api_type is null");
            return (Criteria) this;
        }

        public Criteria andApiTypeIsNotNull() {
            addCriterion("api_type is not null");
            return (Criteria) this;
        }

        public Criteria andApiTypeEqualTo(String value) {
            addCriterion("api_type =", value, "apiType");
            return (Criteria) this;
        }

        public Criteria andApiTypeNotEqualTo(String value) {
            addCriterion("api_type <>", value, "apiType");
            return (Criteria) this;
        }

        public Criteria andApiTypeGreaterThan(String value) {
            addCriterion("api_type >", value, "apiType");
            return (Criteria) this;
        }

        public Criteria andApiTypeGreaterThanOrEqualTo(String value) {
            addCriterion("api_type >=", value, "apiType");
            return (Criteria) this;
        }

        public Criteria andApiTypeLessThan(String value) {
            addCriterion("api_type <", value, "apiType");
            return (Criteria) this;
        }

        public Criteria andApiTypeLessThanOrEqualTo(String value) {
            addCriterion("api_type <=", value, "apiType");
            return (Criteria) this;
        }

        public Criteria andApiTypeLike(String value) {
            addCriterion("api_type like", value, "apiType");
            return (Criteria) this;
        }

        public Criteria andApiTypeNotLike(String value) {
            addCriterion("api_type not like", value, "apiType");
            return (Criteria) this;
        }

        public Criteria andApiTypeIn(List<String> values) {
            addCriterion("api_type in", values, "apiType");
            return (Criteria) this;
        }

        public Criteria andApiTypeNotIn(List<String> values) {
            addCriterion("api_type not in", values, "apiType");
            return (Criteria) this;
        }

        public Criteria andApiTypeBetween(String value1, String value2) {
            addCriterion("api_type between", value1, value2, "apiType");
            return (Criteria) this;
        }

        public Criteria andApiTypeNotBetween(String value1, String value2) {
            addCriterion("api_type not between", value1, value2, "apiType");
            return (Criteria) this;
        }

        public Criteria andHeadInfoIsNull() {
            addCriterion("head_info is null");
            return (Criteria) this;
        }

        public Criteria andHeadInfoIsNotNull() {
            addCriterion("head_info is not null");
            return (Criteria) this;
        }

        public Criteria andHeadInfoEqualTo(String value) {
            addCriterion("head_info =", value, "headInfo");
            return (Criteria) this;
        }

        public Criteria andHeadInfoNotEqualTo(String value) {
            addCriterion("head_info <>", value, "headInfo");
            return (Criteria) this;
        }

        public Criteria andHeadInfoGreaterThan(String value) {
            addCriterion("head_info >", value, "headInfo");
            return (Criteria) this;
        }

        public Criteria andHeadInfoGreaterThanOrEqualTo(String value) {
            addCriterion("head_info >=", value, "headInfo");
            return (Criteria) this;
        }

        public Criteria andHeadInfoLessThan(String value) {
            addCriterion("head_info <", value, "headInfo");
            return (Criteria) this;
        }

        public Criteria andHeadInfoLessThanOrEqualTo(String value) {
            addCriterion("head_info <=", value, "headInfo");
            return (Criteria) this;
        }

        public Criteria andHeadInfoLike(String value) {
            addCriterion("head_info like", value, "headInfo");
            return (Criteria) this;
        }

        public Criteria andHeadInfoNotLike(String value) {
            addCriterion("head_info not like", value, "headInfo");
            return (Criteria) this;
        }

        public Criteria andHeadInfoIn(List<String> values) {
            addCriterion("head_info in", values, "headInfo");
            return (Criteria) this;
        }

        public Criteria andHeadInfoNotIn(List<String> values) {
            addCriterion("head_info not in", values, "headInfo");
            return (Criteria) this;
        }

        public Criteria andHeadInfoBetween(String value1, String value2) {
            addCriterion("head_info between", value1, value2, "headInfo");
            return (Criteria) this;
        }

        public Criteria andHeadInfoNotBetween(String value1, String value2) {
            addCriterion("head_info not between", value1, value2, "headInfo");
            return (Criteria) this;
        }

        public Criteria andRequestTypeIsNull() {
            addCriterion("request_type is null");
            return (Criteria) this;
        }

        public Criteria andRequestTypeIsNotNull() {
            addCriterion("request_type is not null");
            return (Criteria) this;
        }

        public Criteria andRequestTypeEqualTo(String value) {
            addCriterion("request_type =", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeNotEqualTo(String value) {
            addCriterion("request_type <>", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeGreaterThan(String value) {
            addCriterion("request_type >", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeGreaterThanOrEqualTo(String value) {
            addCriterion("request_type >=", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeLessThan(String value) {
            addCriterion("request_type <", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeLessThanOrEqualTo(String value) {
            addCriterion("request_type <=", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeLike(String value) {
            addCriterion("request_type like", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeNotLike(String value) {
            addCriterion("request_type not like", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeIn(List<String> values) {
            addCriterion("request_type in", values, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeNotIn(List<String> values) {
            addCriterion("request_type not in", values, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeBetween(String value1, String value2) {
            addCriterion("request_type between", value1, value2, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeNotBetween(String value1, String value2) {
            addCriterion("request_type not between", value1, value2, "requestType");
            return (Criteria) this;
        }

        public Criteria andApiDescribeIsNull() {
            addCriterion("api_describe is null");
            return (Criteria) this;
        }

        public Criteria andApiDescribeIsNotNull() {
            addCriterion("api_describe is not null");
            return (Criteria) this;
        }

        public Criteria andApiDescribeEqualTo(String value) {
            addCriterion("api_describe =", value, "apiDescribe");
            return (Criteria) this;
        }

        public Criteria andApiDescribeNotEqualTo(String value) {
            addCriterion("api_describe <>", value, "apiDescribe");
            return (Criteria) this;
        }

        public Criteria andApiDescribeGreaterThan(String value) {
            addCriterion("api_describe >", value, "apiDescribe");
            return (Criteria) this;
        }

        public Criteria andApiDescribeGreaterThanOrEqualTo(String value) {
            addCriterion("api_describe >=", value, "apiDescribe");
            return (Criteria) this;
        }

        public Criteria andApiDescribeLessThan(String value) {
            addCriterion("api_describe <", value, "apiDescribe");
            return (Criteria) this;
        }

        public Criteria andApiDescribeLessThanOrEqualTo(String value) {
            addCriterion("api_describe <=", value, "apiDescribe");
            return (Criteria) this;
        }

        public Criteria andApiDescribeLike(String value) {
            addCriterion("api_describe like", value, "apiDescribe");
            return (Criteria) this;
        }

        public Criteria andApiDescribeNotLike(String value) {
            addCriterion("api_describe not like", value, "apiDescribe");
            return (Criteria) this;
        }

        public Criteria andApiDescribeIn(List<String> values) {
            addCriterion("api_describe in", values, "apiDescribe");
            return (Criteria) this;
        }

        public Criteria andApiDescribeNotIn(List<String> values) {
            addCriterion("api_describe not in", values, "apiDescribe");
            return (Criteria) this;
        }

        public Criteria andApiDescribeBetween(String value1, String value2) {
            addCriterion("api_describe between", value1, value2, "apiDescribe");
            return (Criteria) this;
        }

        public Criteria andApiDescribeNotBetween(String value1, String value2) {
            addCriterion("api_describe not between", value1, value2, "apiDescribe");
            return (Criteria) this;
        }

        public Criteria andApiGroupIsNull() {
            addCriterion("api_group is null");
            return (Criteria) this;
        }

        public Criteria andApiGroupIsNotNull() {
            addCriterion("api_group is not null");
            return (Criteria) this;
        }

        public Criteria andApiGroupEqualTo(String value) {
            addCriterion("api_group =", value, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupNotEqualTo(String value) {
            addCriterion("api_group <>", value, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupGreaterThan(String value) {
            addCriterion("api_group >", value, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupGreaterThanOrEqualTo(String value) {
            addCriterion("api_group >=", value, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupLessThan(String value) {
            addCriterion("api_group <", value, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupLessThanOrEqualTo(String value) {
            addCriterion("api_group <=", value, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupLike(String value) {
            addCriterion("api_group like", value, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupNotLike(String value) {
            addCriterion("api_group not like", value, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupIn(List<String> values) {
            addCriterion("api_group in", values, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupNotIn(List<String> values) {
            addCriterion("api_group not in", values, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupBetween(String value1, String value2) {
            addCriterion("api_group between", value1, value2, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupNotBetween(String value1, String value2) {
            addCriterion("api_group not between", value1, value2, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiNavigateValueIsNull() {
            addCriterion("api_navigate_value is null");
            return (Criteria) this;
        }

        public Criteria andApiNavigateValueIsNotNull() {
            addCriterion("api_navigate_value is not null");
            return (Criteria) this;
        }

        public Criteria andApiNavigateValueEqualTo(String value) {
            addCriterion("api_navigate_value =", value, "apiNavigateValue");
            return (Criteria) this;
        }

        public Criteria andApiNavigateValueNotEqualTo(String value) {
            addCriterion("api_navigate_value <>", value, "apiNavigateValue");
            return (Criteria) this;
        }

        public Criteria andApiNavigateValueGreaterThan(String value) {
            addCriterion("api_navigate_value >", value, "apiNavigateValue");
            return (Criteria) this;
        }

        public Criteria andApiNavigateValueGreaterThanOrEqualTo(String value) {
            addCriterion("api_navigate_value >=", value, "apiNavigateValue");
            return (Criteria) this;
        }

        public Criteria andApiNavigateValueLessThan(String value) {
            addCriterion("api_navigate_value <", value, "apiNavigateValue");
            return (Criteria) this;
        }

        public Criteria andApiNavigateValueLessThanOrEqualTo(String value) {
            addCriterion("api_navigate_value <=", value, "apiNavigateValue");
            return (Criteria) this;
        }

        public Criteria andApiNavigateValueLike(String value) {
            addCriterion("api_navigate_value like", value, "apiNavigateValue");
            return (Criteria) this;
        }

        public Criteria andApiNavigateValueNotLike(String value) {
            addCriterion("api_navigate_value not like", value, "apiNavigateValue");
            return (Criteria) this;
        }

        public Criteria andApiNavigateValueIn(List<String> values) {
            addCriterion("api_navigate_value in", values, "apiNavigateValue");
            return (Criteria) this;
        }

        public Criteria andApiNavigateValueNotIn(List<String> values) {
            addCriterion("api_navigate_value not in", values, "apiNavigateValue");
            return (Criteria) this;
        }

        public Criteria andApiNavigateValueBetween(String value1, String value2) {
            addCriterion("api_navigate_value between", value1, value2, "apiNavigateValue");
            return (Criteria) this;
        }

        public Criteria andApiNavigateValueNotBetween(String value1, String value2) {
            addCriterion("api_navigate_value not between", value1, value2, "apiNavigateValue");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}