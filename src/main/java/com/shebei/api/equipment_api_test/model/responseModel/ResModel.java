package com.shebei.api.equipment_api_test.model.responseModel;

public class ResModel {
    Integer code;

    Object data;


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
