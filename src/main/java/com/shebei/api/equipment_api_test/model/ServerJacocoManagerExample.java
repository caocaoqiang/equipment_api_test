package com.shebei.api.equipment_api_test.model;

import java.util.ArrayList;
import java.util.List;

public class ServerJacocoManagerExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Integer offset;

    public ServerJacocoManagerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGitUrlIsNull() {
            addCriterion("git_url is null");
            return (Criteria) this;
        }

        public Criteria andGitUrlIsNotNull() {
            addCriterion("git_url is not null");
            return (Criteria) this;
        }

        public Criteria andGitUrlEqualTo(String value) {
            addCriterion("git_url =", value, "gitUrl");
            return (Criteria) this;
        }

        public Criteria andGitUrlNotEqualTo(String value) {
            addCriterion("git_url <>", value, "gitUrl");
            return (Criteria) this;
        }

        public Criteria andGitUrlGreaterThan(String value) {
            addCriterion("git_url >", value, "gitUrl");
            return (Criteria) this;
        }

        public Criteria andGitUrlGreaterThanOrEqualTo(String value) {
            addCriterion("git_url >=", value, "gitUrl");
            return (Criteria) this;
        }

        public Criteria andGitUrlLessThan(String value) {
            addCriterion("git_url <", value, "gitUrl");
            return (Criteria) this;
        }

        public Criteria andGitUrlLessThanOrEqualTo(String value) {
            addCriterion("git_url <=", value, "gitUrl");
            return (Criteria) this;
        }

        public Criteria andGitUrlLike(String value) {
            addCriterion("git_url like", value, "gitUrl");
            return (Criteria) this;
        }

        public Criteria andGitUrlNotLike(String value) {
            addCriterion("git_url not like", value, "gitUrl");
            return (Criteria) this;
        }

        public Criteria andGitUrlIn(List<String> values) {
            addCriterion("git_url in", values, "gitUrl");
            return (Criteria) this;
        }

        public Criteria andGitUrlNotIn(List<String> values) {
            addCriterion("git_url not in", values, "gitUrl");
            return (Criteria) this;
        }

        public Criteria andGitUrlBetween(String value1, String value2) {
            addCriterion("git_url between", value1, value2, "gitUrl");
            return (Criteria) this;
        }

        public Criteria andGitUrlNotBetween(String value1, String value2) {
            addCriterion("git_url not between", value1, value2, "gitUrl");
            return (Criteria) this;
        }

        public Criteria andNowVersionIsNull() {
            addCriterion("now_version is null");
            return (Criteria) this;
        }

        public Criteria andNowVersionIsNotNull() {
            addCriterion("now_version is not null");
            return (Criteria) this;
        }

        public Criteria andNowVersionEqualTo(String value) {
            addCriterion("now_version =", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionNotEqualTo(String value) {
            addCriterion("now_version <>", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionGreaterThan(String value) {
            addCriterion("now_version >", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionGreaterThanOrEqualTo(String value) {
            addCriterion("now_version >=", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionLessThan(String value) {
            addCriterion("now_version <", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionLessThanOrEqualTo(String value) {
            addCriterion("now_version <=", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionLike(String value) {
            addCriterion("now_version like", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionNotLike(String value) {
            addCriterion("now_version not like", value, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionIn(List<String> values) {
            addCriterion("now_version in", values, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionNotIn(List<String> values) {
            addCriterion("now_version not in", values, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionBetween(String value1, String value2) {
            addCriterion("now_version between", value1, value2, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andNowVersionNotBetween(String value1, String value2) {
            addCriterion("now_version not between", value1, value2, "nowVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionIsNull() {
            addCriterion("base_version is null");
            return (Criteria) this;
        }

        public Criteria andBaseVersionIsNotNull() {
            addCriterion("base_version is not null");
            return (Criteria) this;
        }

        public Criteria andBaseVersionEqualTo(String value) {
            addCriterion("base_version =", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionNotEqualTo(String value) {
            addCriterion("base_version <>", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionGreaterThan(String value) {
            addCriterion("base_version >", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionGreaterThanOrEqualTo(String value) {
            addCriterion("base_version >=", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionLessThan(String value) {
            addCriterion("base_version <", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionLessThanOrEqualTo(String value) {
            addCriterion("base_version <=", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionLike(String value) {
            addCriterion("base_version like", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionNotLike(String value) {
            addCriterion("base_version not like", value, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionIn(List<String> values) {
            addCriterion("base_version in", values, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionNotIn(List<String> values) {
            addCriterion("base_version not in", values, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionBetween(String value1, String value2) {
            addCriterion("base_version between", value1, value2, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andBaseVersionNotBetween(String value1, String value2) {
            addCriterion("base_version not between", value1, value2, "baseVersion");
            return (Criteria) this;
        }

        public Criteria andSubModuleIsNull() {
            addCriterion("sub_module is null");
            return (Criteria) this;
        }

        public Criteria andSubModuleIsNotNull() {
            addCriterion("sub_module is not null");
            return (Criteria) this;
        }

        public Criteria andSubModuleEqualTo(String value) {
            addCriterion("sub_module =", value, "subModule");
            return (Criteria) this;
        }

        public Criteria andSubModuleNotEqualTo(String value) {
            addCriterion("sub_module <>", value, "subModule");
            return (Criteria) this;
        }

        public Criteria andSubModuleGreaterThan(String value) {
            addCriterion("sub_module >", value, "subModule");
            return (Criteria) this;
        }

        public Criteria andSubModuleGreaterThanOrEqualTo(String value) {
            addCriterion("sub_module >=", value, "subModule");
            return (Criteria) this;
        }

        public Criteria andSubModuleLessThan(String value) {
            addCriterion("sub_module <", value, "subModule");
            return (Criteria) this;
        }

        public Criteria andSubModuleLessThanOrEqualTo(String value) {
            addCriterion("sub_module <=", value, "subModule");
            return (Criteria) this;
        }

        public Criteria andSubModuleLike(String value) {
            addCriterion("sub_module like", value, "subModule");
            return (Criteria) this;
        }

        public Criteria andSubModuleNotLike(String value) {
            addCriterion("sub_module not like", value, "subModule");
            return (Criteria) this;
        }

        public Criteria andSubModuleIn(List<String> values) {
            addCriterion("sub_module in", values, "subModule");
            return (Criteria) this;
        }

        public Criteria andSubModuleNotIn(List<String> values) {
            addCriterion("sub_module not in", values, "subModule");
            return (Criteria) this;
        }

        public Criteria andSubModuleBetween(String value1, String value2) {
            addCriterion("sub_module between", value1, value2, "subModule");
            return (Criteria) this;
        }

        public Criteria andSubModuleNotBetween(String value1, String value2) {
            addCriterion("sub_module not between", value1, value2, "subModule");
            return (Criteria) this;
        }

        public Criteria andPortJacocoIsNull() {
            addCriterion("port_jacoco is null");
            return (Criteria) this;
        }

        public Criteria andPortJacocoIsNotNull() {
            addCriterion("port_jacoco is not null");
            return (Criteria) this;
        }

        public Criteria andPortJacocoEqualTo(String value) {
            addCriterion("port_jacoco =", value, "portJacoco");
            return (Criteria) this;
        }

        public Criteria andPortJacocoNotEqualTo(String value) {
            addCriterion("port_jacoco <>", value, "portJacoco");
            return (Criteria) this;
        }

        public Criteria andPortJacocoGreaterThan(String value) {
            addCriterion("port_jacoco >", value, "portJacoco");
            return (Criteria) this;
        }

        public Criteria andPortJacocoGreaterThanOrEqualTo(String value) {
            addCriterion("port_jacoco >=", value, "portJacoco");
            return (Criteria) this;
        }

        public Criteria andPortJacocoLessThan(String value) {
            addCriterion("port_jacoco <", value, "portJacoco");
            return (Criteria) this;
        }

        public Criteria andPortJacocoLessThanOrEqualTo(String value) {
            addCriterion("port_jacoco <=", value, "portJacoco");
            return (Criteria) this;
        }

        public Criteria andPortJacocoLike(String value) {
            addCriterion("port_jacoco like", value, "portJacoco");
            return (Criteria) this;
        }

        public Criteria andPortJacocoNotLike(String value) {
            addCriterion("port_jacoco not like", value, "portJacoco");
            return (Criteria) this;
        }

        public Criteria andPortJacocoIn(List<String> values) {
            addCriterion("port_jacoco in", values, "portJacoco");
            return (Criteria) this;
        }

        public Criteria andPortJacocoNotIn(List<String> values) {
            addCriterion("port_jacoco not in", values, "portJacoco");
            return (Criteria) this;
        }

        public Criteria andPortJacocoBetween(String value1, String value2) {
            addCriterion("port_jacoco between", value1, value2, "portJacoco");
            return (Criteria) this;
        }

        public Criteria andPortJacocoNotBetween(String value1, String value2) {
            addCriterion("port_jacoco not between", value1, value2, "portJacoco");
            return (Criteria) this;
        }

        public Criteria andServerNameIsNull() {
            addCriterion("server_name is null");
            return (Criteria) this;
        }

        public Criteria andServerNameIsNotNull() {
            addCriterion("server_name is not null");
            return (Criteria) this;
        }

        public Criteria andServerNameEqualTo(String value) {
            addCriterion("server_name =", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameNotEqualTo(String value) {
            addCriterion("server_name <>", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameGreaterThan(String value) {
            addCriterion("server_name >", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameGreaterThanOrEqualTo(String value) {
            addCriterion("server_name >=", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameLessThan(String value) {
            addCriterion("server_name <", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameLessThanOrEqualTo(String value) {
            addCriterion("server_name <=", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameLike(String value) {
            addCriterion("server_name like", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameNotLike(String value) {
            addCriterion("server_name not like", value, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameIn(List<String> values) {
            addCriterion("server_name in", values, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameNotIn(List<String> values) {
            addCriterion("server_name not in", values, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameBetween(String value1, String value2) {
            addCriterion("server_name between", value1, value2, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerNameNotBetween(String value1, String value2) {
            addCriterion("server_name not between", value1, value2, "serverName");
            return (Criteria) this;
        }

        public Criteria andServerAddressIsNull() {
            addCriterion("server_address is null");
            return (Criteria) this;
        }

        public Criteria andServerAddressIsNotNull() {
            addCriterion("server_address is not null");
            return (Criteria) this;
        }

        public Criteria andServerAddressEqualTo(String value) {
            addCriterion("server_address =", value, "serverAddress");
            return (Criteria) this;
        }

        public Criteria andServerAddressNotEqualTo(String value) {
            addCriterion("server_address <>", value, "serverAddress");
            return (Criteria) this;
        }

        public Criteria andServerAddressGreaterThan(String value) {
            addCriterion("server_address >", value, "serverAddress");
            return (Criteria) this;
        }

        public Criteria andServerAddressGreaterThanOrEqualTo(String value) {
            addCriterion("server_address >=", value, "serverAddress");
            return (Criteria) this;
        }

        public Criteria andServerAddressLessThan(String value) {
            addCriterion("server_address <", value, "serverAddress");
            return (Criteria) this;
        }

        public Criteria andServerAddressLessThanOrEqualTo(String value) {
            addCriterion("server_address <=", value, "serverAddress");
            return (Criteria) this;
        }

        public Criteria andServerAddressLike(String value) {
            addCriterion("server_address like", value, "serverAddress");
            return (Criteria) this;
        }

        public Criteria andServerAddressNotLike(String value) {
            addCriterion("server_address not like", value, "serverAddress");
            return (Criteria) this;
        }

        public Criteria andServerAddressIn(List<String> values) {
            addCriterion("server_address in", values, "serverAddress");
            return (Criteria) this;
        }

        public Criteria andServerAddressNotIn(List<String> values) {
            addCriterion("server_address not in", values, "serverAddress");
            return (Criteria) this;
        }

        public Criteria andServerAddressBetween(String value1, String value2) {
            addCriterion("server_address between", value1, value2, "serverAddress");
            return (Criteria) this;
        }

        public Criteria andServerAddressNotBetween(String value1, String value2) {
            addCriterion("server_address not between", value1, value2, "serverAddress");
            return (Criteria) this;
        }

        public Criteria andModuleNameIsNull() {
            addCriterion("module_name is null");
            return (Criteria) this;
        }

        public Criteria andModuleNameIsNotNull() {
            addCriterion("module_name is not null");
            return (Criteria) this;
        }

        public Criteria andModuleNameEqualTo(String value) {
            addCriterion("module_name =", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameNotEqualTo(String value) {
            addCriterion("module_name <>", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameGreaterThan(String value) {
            addCriterion("module_name >", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameGreaterThanOrEqualTo(String value) {
            addCriterion("module_name >=", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameLessThan(String value) {
            addCriterion("module_name <", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameLessThanOrEqualTo(String value) {
            addCriterion("module_name <=", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameLike(String value) {
            addCriterion("module_name like", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameNotLike(String value) {
            addCriterion("module_name not like", value, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameIn(List<String> values) {
            addCriterion("module_name in", values, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameNotIn(List<String> values) {
            addCriterion("module_name not in", values, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameBetween(String value1, String value2) {
            addCriterion("module_name between", value1, value2, "moduleName");
            return (Criteria) this;
        }

        public Criteria andModuleNameNotBetween(String value1, String value2) {
            addCriterion("module_name not between", value1, value2, "moduleName");
            return (Criteria) this;
        }

        public Criteria andSuperJacocoIdIsNull() {
            addCriterion("super_jacoco_id is null");
            return (Criteria) this;
        }

        public Criteria andSuperJacocoIdIsNotNull() {
            addCriterion("super_jacoco_id is not null");
            return (Criteria) this;
        }

        public Criteria andSuperJacocoIdEqualTo(Integer value) {
            addCriterion("super_jacoco_id =", value, "superJacocoId");
            return (Criteria) this;
        }

        public Criteria andSuperJacocoIdNotEqualTo(Integer value) {
            addCriterion("super_jacoco_id <>", value, "superJacocoId");
            return (Criteria) this;
        }

        public Criteria andSuperJacocoIdGreaterThan(Integer value) {
            addCriterion("super_jacoco_id >", value, "superJacocoId");
            return (Criteria) this;
        }

        public Criteria andSuperJacocoIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("super_jacoco_id >=", value, "superJacocoId");
            return (Criteria) this;
        }

        public Criteria andSuperJacocoIdLessThan(Integer value) {
            addCriterion("super_jacoco_id <", value, "superJacocoId");
            return (Criteria) this;
        }

        public Criteria andSuperJacocoIdLessThanOrEqualTo(Integer value) {
            addCriterion("super_jacoco_id <=", value, "superJacocoId");
            return (Criteria) this;
        }

        public Criteria andSuperJacocoIdIn(List<Integer> values) {
            addCriterion("super_jacoco_id in", values, "superJacocoId");
            return (Criteria) this;
        }

        public Criteria andSuperJacocoIdNotIn(List<Integer> values) {
            addCriterion("super_jacoco_id not in", values, "superJacocoId");
            return (Criteria) this;
        }

        public Criteria andSuperJacocoIdBetween(Integer value1, Integer value2) {
            addCriterion("super_jacoco_id between", value1, value2, "superJacocoId");
            return (Criteria) this;
        }

        public Criteria andSuperJacocoIdNotBetween(Integer value1, Integer value2) {
            addCriterion("super_jacoco_id not between", value1, value2, "superJacocoId");
            return (Criteria) this;
        }

        public Criteria andWhiteModulesIsNull() {
            addCriterion("white_modules is null");
            return (Criteria) this;
        }

        public Criteria andWhiteModulesIsNotNull() {
            addCriterion("white_modules is not null");
            return (Criteria) this;
        }

        public Criteria andWhiteModulesEqualTo(String value) {
            addCriterion("white_modules =", value, "whiteModules");
            return (Criteria) this;
        }

        public Criteria andWhiteModulesNotEqualTo(String value) {
            addCriterion("white_modules <>", value, "whiteModules");
            return (Criteria) this;
        }

        public Criteria andWhiteModulesGreaterThan(String value) {
            addCriterion("white_modules >", value, "whiteModules");
            return (Criteria) this;
        }

        public Criteria andWhiteModulesGreaterThanOrEqualTo(String value) {
            addCriterion("white_modules >=", value, "whiteModules");
            return (Criteria) this;
        }

        public Criteria andWhiteModulesLessThan(String value) {
            addCriterion("white_modules <", value, "whiteModules");
            return (Criteria) this;
        }

        public Criteria andWhiteModulesLessThanOrEqualTo(String value) {
            addCriterion("white_modules <=", value, "whiteModules");
            return (Criteria) this;
        }

        public Criteria andWhiteModulesLike(String value) {
            addCriterion("white_modules like", value, "whiteModules");
            return (Criteria) this;
        }

        public Criteria andWhiteModulesNotLike(String value) {
            addCriterion("white_modules not like", value, "whiteModules");
            return (Criteria) this;
        }

        public Criteria andWhiteModulesIn(List<String> values) {
            addCriterion("white_modules in", values, "whiteModules");
            return (Criteria) this;
        }

        public Criteria andWhiteModulesNotIn(List<String> values) {
            addCriterion("white_modules not in", values, "whiteModules");
            return (Criteria) this;
        }

        public Criteria andWhiteModulesBetween(String value1, String value2) {
            addCriterion("white_modules between", value1, value2, "whiteModules");
            return (Criteria) this;
        }

        public Criteria andWhiteModulesNotBetween(String value1, String value2) {
            addCriterion("white_modules not between", value1, value2, "whiteModules");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}