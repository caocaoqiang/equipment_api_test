package com.shebei.api.equipment_api_test.model;

import java.io.Serializable;

/**
 * user_case_wo
 * @author 
 */
public class UserCaseGroup extends UserCaseWoWithBLOBs implements Serializable {
    private String groupName;
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}