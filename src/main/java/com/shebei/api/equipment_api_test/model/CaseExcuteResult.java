package com.shebei.api.equipment_api_test.model;

import java.io.Serializable;

/**
 * @author 
 */
public class CaseExcuteResult extends CaseExcuteResultKey implements Serializable {
    private String apiName;

    private String apiVaule;

    private String caseModule;

    private String scene;

    private String env;

    private String requestType;

    private String excuteResult;

    private String excuteSequence;

    private String caseGroup;

    private static final long serialVersionUID = 1L;

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public String getApiVaule() {
        return apiVaule;
    }

    public void setApiVaule(String apiVaule) {
        this.apiVaule = apiVaule;
    }

    public String getCaseModule() {
        return caseModule;
    }

    public void setCaseModule(String caseModule) {
        this.caseModule = caseModule;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getExcuteResult() {
        return excuteResult;
    }

    public void setExcuteResult(String excuteResult) {
        this.excuteResult = excuteResult;
    }

    public String getExcuteSequence() {
        return excuteSequence;
    }

    public void setExcuteSequence(String excuteSequence) {
        this.excuteSequence = excuteSequence;
    }

    public String getCaseGroup() {
        return caseGroup;
    }

    public void setCaseGroup(String caseGroup) {
        this.caseGroup = caseGroup;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        CaseExcuteResult other = (CaseExcuteResult) that;
        return (this.getLinuxTime() == null ? other.getLinuxTime() == null : this.getLinuxTime().equals(other.getLinuxTime()))
            && (this.getExcuteTime() == null ? other.getExcuteTime() == null : this.getExcuteTime().equals(other.getExcuteTime()))
            && (this.getCaseName() == null ? other.getCaseName() == null : this.getCaseName().equals(other.getCaseName()))
            && (this.getApiName() == null ? other.getApiName() == null : this.getApiName().equals(other.getApiName()))
            && (this.getApiVaule() == null ? other.getApiVaule() == null : this.getApiVaule().equals(other.getApiVaule()))
            && (this.getCaseModule() == null ? other.getCaseModule() == null : this.getCaseModule().equals(other.getCaseModule()))
            && (this.getScene() == null ? other.getScene() == null : this.getScene().equals(other.getScene()))
            && (this.getEnv() == null ? other.getEnv() == null : this.getEnv().equals(other.getEnv()))
            && (this.getRequestType() == null ? other.getRequestType() == null : this.getRequestType().equals(other.getRequestType()))
            && (this.getExcuteResult() == null ? other.getExcuteResult() == null : this.getExcuteResult().equals(other.getExcuteResult()))
            && (this.getExcuteSequence() == null ? other.getExcuteSequence() == null : this.getExcuteSequence().equals(other.getExcuteSequence()))
            && (this.getCaseGroup() == null ? other.getCaseGroup() == null : this.getCaseGroup().equals(other.getCaseGroup()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getLinuxTime() == null) ? 0 : getLinuxTime().hashCode());
        result = prime * result + ((getExcuteTime() == null) ? 0 : getExcuteTime().hashCode());
        result = prime * result + ((getCaseName() == null) ? 0 : getCaseName().hashCode());
        result = prime * result + ((getApiName() == null) ? 0 : getApiName().hashCode());
        result = prime * result + ((getApiVaule() == null) ? 0 : getApiVaule().hashCode());
        result = prime * result + ((getCaseModule() == null) ? 0 : getCaseModule().hashCode());
        result = prime * result + ((getScene() == null) ? 0 : getScene().hashCode());
        result = prime * result + ((getEnv() == null) ? 0 : getEnv().hashCode());
        result = prime * result + ((getRequestType() == null) ? 0 : getRequestType().hashCode());
        result = prime * result + ((getExcuteResult() == null) ? 0 : getExcuteResult().hashCode());
        result = prime * result + ((getExcuteSequence() == null) ? 0 : getExcuteSequence().hashCode());
        result = prime * result + ((getCaseGroup() == null) ? 0 : getCaseGroup().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", apiName=").append(apiName);
        sb.append(", apiVaule=").append(apiVaule);
        sb.append(", caseModule=").append(caseModule);
        sb.append(", scene=").append(scene);
        sb.append(", env=").append(env);
        sb.append(", requestType=").append(requestType);
        sb.append(", excuteResult=").append(excuteResult);
        sb.append(", excuteSequence=").append(excuteSequence);
        sb.append(", caseGroup=").append(caseGroup);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}