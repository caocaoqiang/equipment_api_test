package com.shebei.api.equipment_api_test.model;

import java.util.ArrayList;
import java.util.List;

public class UserCaseWoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Integer offset;

    public UserCaseWoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andModuleIsNull() {
            addCriterion("module is null");
            return (Criteria) this;
        }

        public Criteria andModuleIsNotNull() {
            addCriterion("module is not null");
            return (Criteria) this;
        }

        public Criteria andModuleEqualTo(String value) {
            addCriterion("module =", value, "module");
            return (Criteria) this;
        }

        public Criteria andModuleNotEqualTo(String value) {
            addCriterion("module <>", value, "module");
            return (Criteria) this;
        }

        public Criteria andModuleGreaterThan(String value) {
            addCriterion("module >", value, "module");
            return (Criteria) this;
        }

        public Criteria andModuleGreaterThanOrEqualTo(String value) {
            addCriterion("module >=", value, "module");
            return (Criteria) this;
        }

        public Criteria andModuleLessThan(String value) {
            addCriterion("module <", value, "module");
            return (Criteria) this;
        }

        public Criteria andModuleLessThanOrEqualTo(String value) {
            addCriterion("module <=", value, "module");
            return (Criteria) this;
        }

        public Criteria andModuleLike(String value) {
            addCriterion("module like", value, "module");
            return (Criteria) this;
        }

        public Criteria andModuleNotLike(String value) {
            addCriterion("module not like", value, "module");
            return (Criteria) this;
        }

        public Criteria andModuleIn(List<String> values) {
            addCriterion("module in", values, "module");
            return (Criteria) this;
        }

        public Criteria andModuleNotIn(List<String> values) {
            addCriterion("module not in", values, "module");
            return (Criteria) this;
        }

        public Criteria andModuleBetween(String value1, String value2) {
            addCriterion("module between", value1, value2, "module");
            return (Criteria) this;
        }

        public Criteria andModuleNotBetween(String value1, String value2) {
            addCriterion("module not between", value1, value2, "module");
            return (Criteria) this;
        }

        public Criteria andUserCaseNameIsNull() {
            addCriterion("user_case_name is null");
            return (Criteria) this;
        }

        public Criteria andUserCaseNameIsNotNull() {
            addCriterion("user_case_name is not null");
            return (Criteria) this;
        }

        public Criteria andUserCaseNameEqualTo(String value) {
            addCriterion("user_case_name =", value, "userCaseName");
            return (Criteria) this;
        }

        public Criteria andUserCaseNameNotEqualTo(String value) {
            addCriterion("user_case_name <>", value, "userCaseName");
            return (Criteria) this;
        }

        public Criteria andUserCaseNameGreaterThan(String value) {
            addCriterion("user_case_name >", value, "userCaseName");
            return (Criteria) this;
        }

        public Criteria andUserCaseNameGreaterThanOrEqualTo(String value) {
            addCriterion("user_case_name >=", value, "userCaseName");
            return (Criteria) this;
        }

        public Criteria andUserCaseNameLessThan(String value) {
            addCriterion("user_case_name <", value, "userCaseName");
            return (Criteria) this;
        }

        public Criteria andUserCaseNameLessThanOrEqualTo(String value) {
            addCriterion("user_case_name <=", value, "userCaseName");
            return (Criteria) this;
        }

        public Criteria andUserCaseNameLike(String value) {
            addCriterion("user_case_name like", value, "userCaseName");
            return (Criteria) this;
        }

        public Criteria andUserCaseNameNotLike(String value) {
            addCriterion("user_case_name not like", value, "userCaseName");
            return (Criteria) this;
        }

        public Criteria andUserCaseNameIn(List<String> values) {
            addCriterion("user_case_name in", values, "userCaseName");
            return (Criteria) this;
        }

        public Criteria andUserCaseNameNotIn(List<String> values) {
            addCriterion("user_case_name not in", values, "userCaseName");
            return (Criteria) this;
        }

        public Criteria andUserCaseNameBetween(String value1, String value2) {
            addCriterion("user_case_name between", value1, value2, "userCaseName");
            return (Criteria) this;
        }

        public Criteria andUserCaseNameNotBetween(String value1, String value2) {
            addCriterion("user_case_name not between", value1, value2, "userCaseName");
            return (Criteria) this;
        }

        public Criteria andApiNameIsNull() {
            addCriterion("api_name is null");
            return (Criteria) this;
        }

        public Criteria andApiNameIsNotNull() {
            addCriterion("api_name is not null");
            return (Criteria) this;
        }

        public Criteria andApiNameEqualTo(String value) {
            addCriterion("api_name =", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameNotEqualTo(String value) {
            addCriterion("api_name <>", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameGreaterThan(String value) {
            addCriterion("api_name >", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameGreaterThanOrEqualTo(String value) {
            addCriterion("api_name >=", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameLessThan(String value) {
            addCriterion("api_name <", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameLessThanOrEqualTo(String value) {
            addCriterion("api_name <=", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameLike(String value) {
            addCriterion("api_name like", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameNotLike(String value) {
            addCriterion("api_name not like", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameIn(List<String> values) {
            addCriterion("api_name in", values, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameNotIn(List<String> values) {
            addCriterion("api_name not in", values, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameBetween(String value1, String value2) {
            addCriterion("api_name between", value1, value2, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameNotBetween(String value1, String value2) {
            addCriterion("api_name not between", value1, value2, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiValueIsNull() {
            addCriterion("api_value is null");
            return (Criteria) this;
        }

        public Criteria andApiValueIsNotNull() {
            addCriterion("api_value is not null");
            return (Criteria) this;
        }

        public Criteria andApiValueEqualTo(String value) {
            addCriterion("api_value =", value, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueNotEqualTo(String value) {
            addCriterion("api_value <>", value, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueGreaterThan(String value) {
            addCriterion("api_value >", value, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueGreaterThanOrEqualTo(String value) {
            addCriterion("api_value >=", value, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueLessThan(String value) {
            addCriterion("api_value <", value, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueLessThanOrEqualTo(String value) {
            addCriterion("api_value <=", value, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueLike(String value) {
            addCriterion("api_value like", value, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueNotLike(String value) {
            addCriterion("api_value not like", value, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueIn(List<String> values) {
            addCriterion("api_value in", values, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueNotIn(List<String> values) {
            addCriterion("api_value not in", values, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueBetween(String value1, String value2) {
            addCriterion("api_value between", value1, value2, "apiValue");
            return (Criteria) this;
        }

        public Criteria andApiValueNotBetween(String value1, String value2) {
            addCriterion("api_value not between", value1, value2, "apiValue");
            return (Criteria) this;
        }

        public Criteria andRequestTypeIsNull() {
            addCriterion("request_type is null");
            return (Criteria) this;
        }

        public Criteria andRequestTypeIsNotNull() {
            addCriterion("request_type is not null");
            return (Criteria) this;
        }

        public Criteria andRequestTypeEqualTo(String value) {
            addCriterion("request_type =", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeNotEqualTo(String value) {
            addCriterion("request_type <>", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeGreaterThan(String value) {
            addCriterion("request_type >", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeGreaterThanOrEqualTo(String value) {
            addCriterion("request_type >=", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeLessThan(String value) {
            addCriterion("request_type <", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeLessThanOrEqualTo(String value) {
            addCriterion("request_type <=", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeLike(String value) {
            addCriterion("request_type like", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeNotLike(String value) {
            addCriterion("request_type not like", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeIn(List<String> values) {
            addCriterion("request_type in", values, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeNotIn(List<String> values) {
            addCriterion("request_type not in", values, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeBetween(String value1, String value2) {
            addCriterion("request_type between", value1, value2, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeNotBetween(String value1, String value2) {
            addCriterion("request_type not between", value1, value2, "requestType");
            return (Criteria) this;
        }

        public Criteria andSceneIsNull() {
            addCriterion("scene is null");
            return (Criteria) this;
        }

        public Criteria andSceneIsNotNull() {
            addCriterion("scene is not null");
            return (Criteria) this;
        }

        public Criteria andSceneEqualTo(String value) {
            addCriterion("scene =", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneNotEqualTo(String value) {
            addCriterion("scene <>", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneGreaterThan(String value) {
            addCriterion("scene >", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneGreaterThanOrEqualTo(String value) {
            addCriterion("scene >=", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneLessThan(String value) {
            addCriterion("scene <", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneLessThanOrEqualTo(String value) {
            addCriterion("scene <=", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneLike(String value) {
            addCriterion("scene like", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneNotLike(String value) {
            addCriterion("scene not like", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneIn(List<String> values) {
            addCriterion("scene in", values, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneNotIn(List<String> values) {
            addCriterion("scene not in", values, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneBetween(String value1, String value2) {
            addCriterion("scene between", value1, value2, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneNotBetween(String value1, String value2) {
            addCriterion("scene not between", value1, value2, "scene");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceIsNull() {
            addCriterion("excute_sequence is null");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceIsNotNull() {
            addCriterion("excute_sequence is not null");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceEqualTo(Integer value) {
            addCriterion("excute_sequence =", value, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceNotEqualTo(Integer value) {
            addCriterion("excute_sequence <>", value, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceGreaterThan(Integer value) {
            addCriterion("excute_sequence >", value, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceGreaterThanOrEqualTo(Integer value) {
            addCriterion("excute_sequence >=", value, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceLessThan(Integer value) {
            addCriterion("excute_sequence <", value, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceLessThanOrEqualTo(Integer value) {
            addCriterion("excute_sequence <=", value, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceIn(List<Integer> values) {
            addCriterion("excute_sequence in", values, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceNotIn(List<Integer> values) {
            addCriterion("excute_sequence not in", values, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceBetween(Integer value1, Integer value2) {
            addCriterion("excute_sequence between", value1, value2, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceNotBetween(Integer value1, Integer value2) {
            addCriterion("excute_sequence not between", value1, value2, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andParameterSavePathIsNull() {
            addCriterion("parameter_save_path is null");
            return (Criteria) this;
        }

        public Criteria andParameterSavePathIsNotNull() {
            addCriterion("parameter_save_path is not null");
            return (Criteria) this;
        }

        public Criteria andParameterSavePathEqualTo(String value) {
            addCriterion("parameter_save_path =", value, "parameterSavePath");
            return (Criteria) this;
        }

        public Criteria andParameterSavePathNotEqualTo(String value) {
            addCriterion("parameter_save_path <>", value, "parameterSavePath");
            return (Criteria) this;
        }

        public Criteria andParameterSavePathGreaterThan(String value) {
            addCriterion("parameter_save_path >", value, "parameterSavePath");
            return (Criteria) this;
        }

        public Criteria andParameterSavePathGreaterThanOrEqualTo(String value) {
            addCriterion("parameter_save_path >=", value, "parameterSavePath");
            return (Criteria) this;
        }

        public Criteria andParameterSavePathLessThan(String value) {
            addCriterion("parameter_save_path <", value, "parameterSavePath");
            return (Criteria) this;
        }

        public Criteria andParameterSavePathLessThanOrEqualTo(String value) {
            addCriterion("parameter_save_path <=", value, "parameterSavePath");
            return (Criteria) this;
        }

        public Criteria andParameterSavePathLike(String value) {
            addCriterion("parameter_save_path like", value, "parameterSavePath");
            return (Criteria) this;
        }

        public Criteria andParameterSavePathNotLike(String value) {
            addCriterion("parameter_save_path not like", value, "parameterSavePath");
            return (Criteria) this;
        }

        public Criteria andParameterSavePathIn(List<String> values) {
            addCriterion("parameter_save_path in", values, "parameterSavePath");
            return (Criteria) this;
        }

        public Criteria andParameterSavePathNotIn(List<String> values) {
            addCriterion("parameter_save_path not in", values, "parameterSavePath");
            return (Criteria) this;
        }

        public Criteria andParameterSavePathBetween(String value1, String value2) {
            addCriterion("parameter_save_path between", value1, value2, "parameterSavePath");
            return (Criteria) this;
        }

        public Criteria andParameterSavePathNotBetween(String value1, String value2) {
            addCriterion("parameter_save_path not between", value1, value2, "parameterSavePath");
            return (Criteria) this;
        }

        public Criteria andAdaptiveVersionIsNull() {
            addCriterion("adaptive_version is null");
            return (Criteria) this;
        }

        public Criteria andAdaptiveVersionIsNotNull() {
            addCriterion("adaptive_version is not null");
            return (Criteria) this;
        }

        public Criteria andAdaptiveVersionEqualTo(String value) {
            addCriterion("adaptive_version =", value, "adaptiveVersion");
            return (Criteria) this;
        }

        public Criteria andAdaptiveVersionNotEqualTo(String value) {
            addCriterion("adaptive_version <>", value, "adaptiveVersion");
            return (Criteria) this;
        }

        public Criteria andAdaptiveVersionGreaterThan(String value) {
            addCriterion("adaptive_version >", value, "adaptiveVersion");
            return (Criteria) this;
        }

        public Criteria andAdaptiveVersionGreaterThanOrEqualTo(String value) {
            addCriterion("adaptive_version >=", value, "adaptiveVersion");
            return (Criteria) this;
        }

        public Criteria andAdaptiveVersionLessThan(String value) {
            addCriterion("adaptive_version <", value, "adaptiveVersion");
            return (Criteria) this;
        }

        public Criteria andAdaptiveVersionLessThanOrEqualTo(String value) {
            addCriterion("adaptive_version <=", value, "adaptiveVersion");
            return (Criteria) this;
        }

        public Criteria andAdaptiveVersionLike(String value) {
            addCriterion("adaptive_version like", value, "adaptiveVersion");
            return (Criteria) this;
        }

        public Criteria andAdaptiveVersionNotLike(String value) {
            addCriterion("adaptive_version not like", value, "adaptiveVersion");
            return (Criteria) this;
        }

        public Criteria andAdaptiveVersionIn(List<String> values) {
            addCriterion("adaptive_version in", values, "adaptiveVersion");
            return (Criteria) this;
        }

        public Criteria andAdaptiveVersionNotIn(List<String> values) {
            addCriterion("adaptive_version not in", values, "adaptiveVersion");
            return (Criteria) this;
        }

        public Criteria andAdaptiveVersionBetween(String value1, String value2) {
            addCriterion("adaptive_version between", value1, value2, "adaptiveVersion");
            return (Criteria) this;
        }

        public Criteria andAdaptiveVersionNotBetween(String value1, String value2) {
            addCriterion("adaptive_version not between", value1, value2, "adaptiveVersion");
            return (Criteria) this;
        }

        public Criteria andApiGroupIsNull() {
            addCriterion("api_group is null");
            return (Criteria) this;
        }

        public Criteria andApiGroupIsNotNull() {
            addCriterion("api_group is not null");
            return (Criteria) this;
        }

        public Criteria andApiGroupEqualTo(String value) {
            addCriterion("api_group =", value, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupNotEqualTo(String value) {
            addCriterion("api_group <>", value, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupGreaterThan(String value) {
            addCriterion("api_group >", value, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupGreaterThanOrEqualTo(String value) {
            addCriterion("api_group >=", value, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupLessThan(String value) {
            addCriterion("api_group <", value, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupLessThanOrEqualTo(String value) {
            addCriterion("api_group <=", value, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupLike(String value) {
            addCriterion("api_group like", value, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupNotLike(String value) {
            addCriterion("api_group not like", value, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupIn(List<String> values) {
            addCriterion("api_group in", values, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupNotIn(List<String> values) {
            addCriterion("api_group not in", values, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupBetween(String value1, String value2) {
            addCriterion("api_group between", value1, value2, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andApiGroupNotBetween(String value1, String value2) {
            addCriterion("api_group not between", value1, value2, "apiGroup");
            return (Criteria) this;
        }

        public Criteria andTimeWaitIsNull() {
            addCriterion("time_wait is null");
            return (Criteria) this;
        }

        public Criteria andTimeWaitIsNotNull() {
            addCriterion("time_wait is not null");
            return (Criteria) this;
        }

        public Criteria andTimeWaitEqualTo(Integer value) {
            addCriterion("time_wait =", value, "timeWait");
            return (Criteria) this;
        }

        public Criteria andTimeWaitNotEqualTo(Integer value) {
            addCriterion("time_wait <>", value, "timeWait");
            return (Criteria) this;
        }

        public Criteria andTimeWaitGreaterThan(Integer value) {
            addCriterion("time_wait >", value, "timeWait");
            return (Criteria) this;
        }

        public Criteria andTimeWaitGreaterThanOrEqualTo(Integer value) {
            addCriterion("time_wait >=", value, "timeWait");
            return (Criteria) this;
        }

        public Criteria andTimeWaitLessThan(Integer value) {
            addCriterion("time_wait <", value, "timeWait");
            return (Criteria) this;
        }

        public Criteria andTimeWaitLessThanOrEqualTo(Integer value) {
            addCriterion("time_wait <=", value, "timeWait");
            return (Criteria) this;
        }

        public Criteria andTimeWaitIn(List<Integer> values) {
            addCriterion("time_wait in", values, "timeWait");
            return (Criteria) this;
        }

        public Criteria andTimeWaitNotIn(List<Integer> values) {
            addCriterion("time_wait not in", values, "timeWait");
            return (Criteria) this;
        }

        public Criteria andTimeWaitBetween(Integer value1, Integer value2) {
            addCriterion("time_wait between", value1, value2, "timeWait");
            return (Criteria) this;
        }

        public Criteria andTimeWaitNotBetween(Integer value1, Integer value2) {
            addCriterion("time_wait not between", value1, value2, "timeWait");
            return (Criteria) this;
        }

        public Criteria andPerformanceExcuteIsNull() {
            addCriterion("performance_excute is null");
            return (Criteria) this;
        }

        public Criteria andPerformanceExcuteIsNotNull() {
            addCriterion("performance_excute is not null");
            return (Criteria) this;
        }

        public Criteria andPerformanceExcuteEqualTo(String value) {
            addCriterion("performance_excute =", value, "performanceExcute");
            return (Criteria) this;
        }

        public Criteria andPerformanceExcuteNotEqualTo(String value) {
            addCriterion("performance_excute <>", value, "performanceExcute");
            return (Criteria) this;
        }

        public Criteria andPerformanceExcuteGreaterThan(String value) {
            addCriterion("performance_excute >", value, "performanceExcute");
            return (Criteria) this;
        }

        public Criteria andPerformanceExcuteGreaterThanOrEqualTo(String value) {
            addCriterion("performance_excute >=", value, "performanceExcute");
            return (Criteria) this;
        }

        public Criteria andPerformanceExcuteLessThan(String value) {
            addCriterion("performance_excute <", value, "performanceExcute");
            return (Criteria) this;
        }

        public Criteria andPerformanceExcuteLessThanOrEqualTo(String value) {
            addCriterion("performance_excute <=", value, "performanceExcute");
            return (Criteria) this;
        }

        public Criteria andPerformanceExcuteLike(String value) {
            addCriterion("performance_excute like", value, "performanceExcute");
            return (Criteria) this;
        }

        public Criteria andPerformanceExcuteNotLike(String value) {
            addCriterion("performance_excute not like", value, "performanceExcute");
            return (Criteria) this;
        }

        public Criteria andPerformanceExcuteIn(List<String> values) {
            addCriterion("performance_excute in", values, "performanceExcute");
            return (Criteria) this;
        }

        public Criteria andPerformanceExcuteNotIn(List<String> values) {
            addCriterion("performance_excute not in", values, "performanceExcute");
            return (Criteria) this;
        }

        public Criteria andPerformanceExcuteBetween(String value1, String value2) {
            addCriterion("performance_excute between", value1, value2, "performanceExcute");
            return (Criteria) this;
        }

        public Criteria andPerformanceExcuteNotBetween(String value1, String value2) {
            addCriterion("performance_excute not between", value1, value2, "performanceExcute");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIsNull() {
            addCriterion("is_delete is null");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIsNotNull() {
            addCriterion("is_delete is not null");
            return (Criteria) this;
        }

        public Criteria andIsDeleteEqualTo(Integer value) {
            addCriterion("is_delete =", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotEqualTo(Integer value) {
            addCriterion("is_delete <>", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteGreaterThan(Integer value) {
            addCriterion("is_delete >", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_delete >=", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteLessThan(Integer value) {
            addCriterion("is_delete <", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteLessThanOrEqualTo(Integer value) {
            addCriterion("is_delete <=", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIn(List<Integer> values) {
            addCriterion("is_delete in", values, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotIn(List<Integer> values) {
            addCriterion("is_delete not in", values, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteBetween(Integer value1, Integer value2) {
            addCriterion("is_delete between", value1, value2, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotBetween(Integer value1, Integer value2) {
            addCriterion("is_delete not between", value1, value2, "isDelete");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}