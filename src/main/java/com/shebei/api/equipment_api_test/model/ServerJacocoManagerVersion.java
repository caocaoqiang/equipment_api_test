package com.shebei.api.equipment_api_test.model;

import java.io.Serializable;

/**
 * @author 
 */
public class ServerJacocoManagerVersion extends  ServerJacocoManager{

    private String testVersion;

    private Integer versionId;
    public String getTestVersion() {
        return testVersion;
    }

    public void setTestVersion(String testVersion) {
        this.testVersion = testVersion;
    }

    public Integer getVersionId() {
        return versionId;
    }

    public void setVersionId(Integer versionId) {
        this.versionId = versionId;
    }
}