package com.shebei.api.equipment_api_test.model;

import java.io.Serializable;

/**
 * callback_receipt
 * @author 
 */
public class CallbackReceipt implements Serializable {
    private Integer id;

    private String guid;

    private String callbackType;

    private String msg;

    private String msgData;

    private String timeCreate;

    private String deviceKey;

    private String callbackScene;

    private String response;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getCallbackType() {
        return callbackType;
    }

    public void setCallbackType(String callbackType) {
        this.callbackType = callbackType;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsgData() {
        return msgData;
    }

    public void setMsgData(String msgData) {
        this.msgData = msgData;
    }

    public String getTimeCreate() {
        return timeCreate;
    }

    public void setTimeCreate(String timeCreate) {
        this.timeCreate = timeCreate;
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }

    public String getCallbackScene() {
        return callbackScene;
    }

    public void setCallbackScene(String callbackScene) {
        this.callbackScene = callbackScene;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}