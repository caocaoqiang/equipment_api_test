package com.shebei.api.equipment_api_test.model.responseModel;

import java.io.Serializable;
import java.util.List;

/**
 * navigate_name
 * @author 
 */
public class NavigateNameNode implements Serializable {
    private Integer id;

    private String groupName;

    private String module;

    private String scene;

    private String uniqueValue;

    private Integer parentId;

    private String label;

   private String moduleName;

   private int isDelete;

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    private List<NavigateNameNode> children;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    public String getUniqueValue() {
        return uniqueValue;
    }

    public void setUniqueValue(String uniqueValue) {
        this.uniqueValue = uniqueValue;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<NavigateNameNode> getChildren() {
        return children;
    }

    public void setChildren(List<NavigateNameNode> children) {
        this.children = children;
    }

}