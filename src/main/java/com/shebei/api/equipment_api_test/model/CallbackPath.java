package com.shebei.api.equipment_api_test.model;

import java.io.Serializable;

/**
 * callback_path
 * @author 
 */
public class CallbackPath implements Serializable {
    private Integer id;

    private String callbackScene;

    private String path;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCallbackScene() {
        return callbackScene;
    }

    public void setCallbackScene(String callbackScene) {
        this.callbackScene = callbackScene;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}