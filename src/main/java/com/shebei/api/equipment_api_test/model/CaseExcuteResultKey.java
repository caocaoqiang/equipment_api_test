package com.shebei.api.equipment_api_test.model;

import java.io.Serializable;

/**
 * @author 
 */
public class CaseExcuteResultKey implements Serializable {
    private String linuxTime;

    private String excuteTime;

    private String caseName;

    private static final long serialVersionUID = 1L;

    public String getLinuxTime() {
        return linuxTime;
    }

    public void setLinuxTime(String linuxTime) {
        this.linuxTime = linuxTime;
    }

    public String getExcuteTime() {
        return excuteTime;
    }

    public void setExcuteTime(String excuteTime) {
        this.excuteTime = excuteTime;
    }

    public String getCaseName() {
        return caseName;
    }

    public void setCaseName(String caseName) {
        this.caseName = caseName;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        CaseExcuteResultKey other = (CaseExcuteResultKey) that;
        return (this.getLinuxTime() == null ? other.getLinuxTime() == null : this.getLinuxTime().equals(other.getLinuxTime()))
            && (this.getExcuteTime() == null ? other.getExcuteTime() == null : this.getExcuteTime().equals(other.getExcuteTime()))
            && (this.getCaseName() == null ? other.getCaseName() == null : this.getCaseName().equals(other.getCaseName()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getLinuxTime() == null) ? 0 : getLinuxTime().hashCode());
        result = prime * result + ((getExcuteTime() == null) ? 0 : getExcuteTime().hashCode());
        result = prime * result + ((getCaseName() == null) ? 0 : getCaseName().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", linuxTime=").append(linuxTime);
        sb.append(", excuteTime=").append(excuteTime);
        sb.append(", caseName=").append(caseName);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}