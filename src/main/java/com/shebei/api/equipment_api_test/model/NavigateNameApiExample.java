package com.shebei.api.equipment_api_test.model;

import java.util.ArrayList;
import java.util.List;

public class NavigateNameApiExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Integer offset;

    public NavigateNameApiExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGroupNameIsNull() {
            addCriterion("group_name is null");
            return (Criteria) this;
        }

        public Criteria andGroupNameIsNotNull() {
            addCriterion("group_name is not null");
            return (Criteria) this;
        }

        public Criteria andGroupNameEqualTo(String value) {
            addCriterion("group_name =", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameNotEqualTo(String value) {
            addCriterion("group_name <>", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameGreaterThan(String value) {
            addCriterion("group_name >", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameGreaterThanOrEqualTo(String value) {
            addCriterion("group_name >=", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameLessThan(String value) {
            addCriterion("group_name <", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameLessThanOrEqualTo(String value) {
            addCriterion("group_name <=", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameLike(String value) {
            addCriterion("group_name like", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameNotLike(String value) {
            addCriterion("group_name not like", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameIn(List<String> values) {
            addCriterion("group_name in", values, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameNotIn(List<String> values) {
            addCriterion("group_name not in", values, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameBetween(String value1, String value2) {
            addCriterion("group_name between", value1, value2, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameNotBetween(String value1, String value2) {
            addCriterion("group_name not between", value1, value2, "groupName");
            return (Criteria) this;
        }

        public Criteria andTimeCreateIsNull() {
            addCriterion("time_create is null");
            return (Criteria) this;
        }

        public Criteria andTimeCreateIsNotNull() {
            addCriterion("time_create is not null");
            return (Criteria) this;
        }

        public Criteria andTimeCreateEqualTo(String value) {
            addCriterion("time_create =", value, "timeCreate");
            return (Criteria) this;
        }

        public Criteria andTimeCreateNotEqualTo(String value) {
            addCriterion("time_create <>", value, "timeCreate");
            return (Criteria) this;
        }

        public Criteria andTimeCreateGreaterThan(String value) {
            addCriterion("time_create >", value, "timeCreate");
            return (Criteria) this;
        }

        public Criteria andTimeCreateGreaterThanOrEqualTo(String value) {
            addCriterion("time_create >=", value, "timeCreate");
            return (Criteria) this;
        }

        public Criteria andTimeCreateLessThan(String value) {
            addCriterion("time_create <", value, "timeCreate");
            return (Criteria) this;
        }

        public Criteria andTimeCreateLessThanOrEqualTo(String value) {
            addCriterion("time_create <=", value, "timeCreate");
            return (Criteria) this;
        }

        public Criteria andTimeCreateLike(String value) {
            addCriterion("time_create like", value, "timeCreate");
            return (Criteria) this;
        }

        public Criteria andTimeCreateNotLike(String value) {
            addCriterion("time_create not like", value, "timeCreate");
            return (Criteria) this;
        }

        public Criteria andTimeCreateIn(List<String> values) {
            addCriterion("time_create in", values, "timeCreate");
            return (Criteria) this;
        }

        public Criteria andTimeCreateNotIn(List<String> values) {
            addCriterion("time_create not in", values, "timeCreate");
            return (Criteria) this;
        }

        public Criteria andTimeCreateBetween(String value1, String value2) {
            addCriterion("time_create between", value1, value2, "timeCreate");
            return (Criteria) this;
        }

        public Criteria andTimeCreateNotBetween(String value1, String value2) {
            addCriterion("time_create not between", value1, value2, "timeCreate");
            return (Criteria) this;
        }

        public Criteria andModuleIsNull() {
            addCriterion("module is null");
            return (Criteria) this;
        }

        public Criteria andModuleIsNotNull() {
            addCriterion("module is not null");
            return (Criteria) this;
        }

        public Criteria andModuleEqualTo(String value) {
            addCriterion("module =", value, "module");
            return (Criteria) this;
        }

        public Criteria andModuleNotEqualTo(String value) {
            addCriterion("module <>", value, "module");
            return (Criteria) this;
        }

        public Criteria andModuleGreaterThan(String value) {
            addCriterion("module >", value, "module");
            return (Criteria) this;
        }

        public Criteria andModuleGreaterThanOrEqualTo(String value) {
            addCriterion("module >=", value, "module");
            return (Criteria) this;
        }

        public Criteria andModuleLessThan(String value) {
            addCriterion("module <", value, "module");
            return (Criteria) this;
        }

        public Criteria andModuleLessThanOrEqualTo(String value) {
            addCriterion("module <=", value, "module");
            return (Criteria) this;
        }

        public Criteria andModuleLike(String value) {
            addCriterion("module like", value, "module");
            return (Criteria) this;
        }

        public Criteria andModuleNotLike(String value) {
            addCriterion("module not like", value, "module");
            return (Criteria) this;
        }

        public Criteria andModuleIn(List<String> values) {
            addCriterion("module in", values, "module");
            return (Criteria) this;
        }

        public Criteria andModuleNotIn(List<String> values) {
            addCriterion("module not in", values, "module");
            return (Criteria) this;
        }

        public Criteria andModuleBetween(String value1, String value2) {
            addCriterion("module between", value1, value2, "module");
            return (Criteria) this;
        }

        public Criteria andModuleNotBetween(String value1, String value2) {
            addCriterion("module not between", value1, value2, "module");
            return (Criteria) this;
        }

        public Criteria andSceneIsNull() {
            addCriterion("scene is null");
            return (Criteria) this;
        }

        public Criteria andSceneIsNotNull() {
            addCriterion("scene is not null");
            return (Criteria) this;
        }

        public Criteria andSceneEqualTo(String value) {
            addCriterion("scene =", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneNotEqualTo(String value) {
            addCriterion("scene <>", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneGreaterThan(String value) {
            addCriterion("scene >", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneGreaterThanOrEqualTo(String value) {
            addCriterion("scene >=", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneLessThan(String value) {
            addCriterion("scene <", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneLessThanOrEqualTo(String value) {
            addCriterion("scene <=", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneLike(String value) {
            addCriterion("scene like", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneNotLike(String value) {
            addCriterion("scene not like", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneIn(List<String> values) {
            addCriterion("scene in", values, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneNotIn(List<String> values) {
            addCriterion("scene not in", values, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneBetween(String value1, String value2) {
            addCriterion("scene between", value1, value2, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneNotBetween(String value1, String value2) {
            addCriterion("scene not between", value1, value2, "scene");
            return (Criteria) this;
        }

        public Criteria andUniqueValueIsNull() {
            addCriterion("unique_value is null");
            return (Criteria) this;
        }

        public Criteria andUniqueValueIsNotNull() {
            addCriterion("unique_value is not null");
            return (Criteria) this;
        }

        public Criteria andUniqueValueEqualTo(String value) {
            addCriterion("unique_value =", value, "uniqueValue");
            return (Criteria) this;
        }

        public Criteria andUniqueValueNotEqualTo(String value) {
            addCriterion("unique_value <>", value, "uniqueValue");
            return (Criteria) this;
        }

        public Criteria andUniqueValueGreaterThan(String value) {
            addCriterion("unique_value >", value, "uniqueValue");
            return (Criteria) this;
        }

        public Criteria andUniqueValueGreaterThanOrEqualTo(String value) {
            addCriterion("unique_value >=", value, "uniqueValue");
            return (Criteria) this;
        }

        public Criteria andUniqueValueLessThan(String value) {
            addCriterion("unique_value <", value, "uniqueValue");
            return (Criteria) this;
        }

        public Criteria andUniqueValueLessThanOrEqualTo(String value) {
            addCriterion("unique_value <=", value, "uniqueValue");
            return (Criteria) this;
        }

        public Criteria andUniqueValueLike(String value) {
            addCriterion("unique_value like", value, "uniqueValue");
            return (Criteria) this;
        }

        public Criteria andUniqueValueNotLike(String value) {
            addCriterion("unique_value not like", value, "uniqueValue");
            return (Criteria) this;
        }

        public Criteria andUniqueValueIn(List<String> values) {
            addCriterion("unique_value in", values, "uniqueValue");
            return (Criteria) this;
        }

        public Criteria andUniqueValueNotIn(List<String> values) {
            addCriterion("unique_value not in", values, "uniqueValue");
            return (Criteria) this;
        }

        public Criteria andUniqueValueBetween(String value1, String value2) {
            addCriterion("unique_value between", value1, value2, "uniqueValue");
            return (Criteria) this;
        }

        public Criteria andUniqueValueNotBetween(String value1, String value2) {
            addCriterion("unique_value not between", value1, value2, "uniqueValue");
            return (Criteria) this;
        }

        public Criteria andExcuteResultIsNull() {
            addCriterion("excute_result is null");
            return (Criteria) this;
        }

        public Criteria andExcuteResultIsNotNull() {
            addCriterion("excute_result is not null");
            return (Criteria) this;
        }

        public Criteria andExcuteResultEqualTo(String value) {
            addCriterion("excute_result =", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultNotEqualTo(String value) {
            addCriterion("excute_result <>", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultGreaterThan(String value) {
            addCriterion("excute_result >", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultGreaterThanOrEqualTo(String value) {
            addCriterion("excute_result >=", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultLessThan(String value) {
            addCriterion("excute_result <", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultLessThanOrEqualTo(String value) {
            addCriterion("excute_result <=", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultLike(String value) {
            addCriterion("excute_result like", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultNotLike(String value) {
            addCriterion("excute_result not like", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultIn(List<String> values) {
            addCriterion("excute_result in", values, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultNotIn(List<String> values) {
            addCriterion("excute_result not in", values, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultBetween(String value1, String value2) {
            addCriterion("excute_result between", value1, value2, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultNotBetween(String value1, String value2) {
            addCriterion("excute_result not between", value1, value2, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeIsNull() {
            addCriterion("excute_time is null");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeIsNotNull() {
            addCriterion("excute_time is not null");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeEqualTo(String value) {
            addCriterion("excute_time =", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeNotEqualTo(String value) {
            addCriterion("excute_time <>", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeGreaterThan(String value) {
            addCriterion("excute_time >", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeGreaterThanOrEqualTo(String value) {
            addCriterion("excute_time >=", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeLessThan(String value) {
            addCriterion("excute_time <", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeLessThanOrEqualTo(String value) {
            addCriterion("excute_time <=", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeLike(String value) {
            addCriterion("excute_time like", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeNotLike(String value) {
            addCriterion("excute_time not like", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeIn(List<String> values) {
            addCriterion("excute_time in", values, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeNotIn(List<String> values) {
            addCriterion("excute_time not in", values, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeBetween(String value1, String value2) {
            addCriterion("excute_time between", value1, value2, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeNotBetween(String value1, String value2) {
            addCriterion("excute_time not between", value1, value2, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andGroupStateIsNull() {
            addCriterion("group_state is null");
            return (Criteria) this;
        }

        public Criteria andGroupStateIsNotNull() {
            addCriterion("group_state is not null");
            return (Criteria) this;
        }

        public Criteria andGroupStateEqualTo(String value) {
            addCriterion("group_state =", value, "groupState");
            return (Criteria) this;
        }

        public Criteria andGroupStateNotEqualTo(String value) {
            addCriterion("group_state <>", value, "groupState");
            return (Criteria) this;
        }

        public Criteria andGroupStateGreaterThan(String value) {
            addCriterion("group_state >", value, "groupState");
            return (Criteria) this;
        }

        public Criteria andGroupStateGreaterThanOrEqualTo(String value) {
            addCriterion("group_state >=", value, "groupState");
            return (Criteria) this;
        }

        public Criteria andGroupStateLessThan(String value) {
            addCriterion("group_state <", value, "groupState");
            return (Criteria) this;
        }

        public Criteria andGroupStateLessThanOrEqualTo(String value) {
            addCriterion("group_state <=", value, "groupState");
            return (Criteria) this;
        }

        public Criteria andGroupStateLike(String value) {
            addCriterion("group_state like", value, "groupState");
            return (Criteria) this;
        }

        public Criteria andGroupStateNotLike(String value) {
            addCriterion("group_state not like", value, "groupState");
            return (Criteria) this;
        }

        public Criteria andGroupStateIn(List<String> values) {
            addCriterion("group_state in", values, "groupState");
            return (Criteria) this;
        }

        public Criteria andGroupStateNotIn(List<String> values) {
            addCriterion("group_state not in", values, "groupState");
            return (Criteria) this;
        }

        public Criteria andGroupStateBetween(String value1, String value2) {
            addCriterion("group_state between", value1, value2, "groupState");
            return (Criteria) this;
        }

        public Criteria andGroupStateNotBetween(String value1, String value2) {
            addCriterion("group_state not between", value1, value2, "groupState");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNull() {
            addCriterion("parent_id is null");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNotNull() {
            addCriterion("parent_id is not null");
            return (Criteria) this;
        }

        public Criteria andParentIdEqualTo(Integer value) {
            addCriterion("parent_id =", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotEqualTo(Integer value) {
            addCriterion("parent_id <>", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThan(Integer value) {
            addCriterion("parent_id >", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("parent_id >=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThan(Integer value) {
            addCriterion("parent_id <", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThanOrEqualTo(Integer value) {
            addCriterion("parent_id <=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdIn(List<Integer> values) {
            addCriterion("parent_id in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotIn(List<Integer> values) {
            addCriterion("parent_id not in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdBetween(Integer value1, Integer value2) {
            addCriterion("parent_id between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("parent_id not between", value1, value2, "parentId");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}