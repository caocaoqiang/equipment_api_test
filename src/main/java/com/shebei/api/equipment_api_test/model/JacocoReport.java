package com.shebei.api.equipment_api_test.model;

import java.io.Serializable;

/**
 * @author 
 */
public class JacocoReport implements Serializable {
    private String recordUuid;

    private String moduleName;

    private String serverName;

    private String createTime;

    private String baseVersion;

    private String nowVersion;

    private String jacocoType;

    private String reportUrl;

    private String reportResult;

    private String errMsg;

    private Integer jacocoSeverId;

    private Integer unitTestType;

    private String logFile;

    private static final long serialVersionUID = 1L;

    public String getRecordUuid() {
        return recordUuid;
    }

    public void setRecordUuid(String recordUuid) {
        this.recordUuid = recordUuid;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getBaseVersion() {
        return baseVersion;
    }

    public void setBaseVersion(String baseVersion) {
        this.baseVersion = baseVersion;
    }

    public String getNowVersion() {
        return nowVersion;
    }

    public void setNowVersion(String nowVersion) {
        this.nowVersion = nowVersion;
    }

    public String getJacocoType() {
        return jacocoType;
    }

    public void setJacocoType(String jacocoType) {
        this.jacocoType = jacocoType;
    }

    public String getReportUrl() {
        return reportUrl;
    }

    public void setReportUrl(String reportUrl) {
        this.reportUrl = reportUrl;
    }

    public String getReportResult() {
        return reportResult;
    }

    public void setReportResult(String reportResult) {
        this.reportResult = reportResult;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public Integer getJacocoSeverId() {
        return jacocoSeverId;
    }

    public void setJacocoSeverId(Integer jacocoSeverId) {
        this.jacocoSeverId = jacocoSeverId;
    }

    public Integer getUnitTestType() {
        return unitTestType;
    }

    public void setUnitTestType(Integer unitTestType) {
        this.unitTestType = unitTestType;
    }

    public String getLogFile() {
        return logFile;
    }

    public void setLogFile(String logFile) {
        this.logFile = logFile;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        JacocoReport other = (JacocoReport) that;
        return (this.getRecordUuid() == null ? other.getRecordUuid() == null : this.getRecordUuid().equals(other.getRecordUuid()))
            && (this.getModuleName() == null ? other.getModuleName() == null : this.getModuleName().equals(other.getModuleName()))
            && (this.getServerName() == null ? other.getServerName() == null : this.getServerName().equals(other.getServerName()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getBaseVersion() == null ? other.getBaseVersion() == null : this.getBaseVersion().equals(other.getBaseVersion()))
            && (this.getNowVersion() == null ? other.getNowVersion() == null : this.getNowVersion().equals(other.getNowVersion()))
            && (this.getJacocoType() == null ? other.getJacocoType() == null : this.getJacocoType().equals(other.getJacocoType()))
            && (this.getReportUrl() == null ? other.getReportUrl() == null : this.getReportUrl().equals(other.getReportUrl()))
            && (this.getReportResult() == null ? other.getReportResult() == null : this.getReportResult().equals(other.getReportResult()))
            && (this.getErrMsg() == null ? other.getErrMsg() == null : this.getErrMsg().equals(other.getErrMsg()))
            && (this.getJacocoSeverId() == null ? other.getJacocoSeverId() == null : this.getJacocoSeverId().equals(other.getJacocoSeverId()))
            && (this.getUnitTestType() == null ? other.getUnitTestType() == null : this.getUnitTestType().equals(other.getUnitTestType()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getRecordUuid() == null) ? 0 : getRecordUuid().hashCode());
        result = prime * result + ((getModuleName() == null) ? 0 : getModuleName().hashCode());
        result = prime * result + ((getServerName() == null) ? 0 : getServerName().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getBaseVersion() == null) ? 0 : getBaseVersion().hashCode());
        result = prime * result + ((getNowVersion() == null) ? 0 : getNowVersion().hashCode());
        result = prime * result + ((getJacocoType() == null) ? 0 : getJacocoType().hashCode());
        result = prime * result + ((getReportUrl() == null) ? 0 : getReportUrl().hashCode());
        result = prime * result + ((getReportResult() == null) ? 0 : getReportResult().hashCode());
        result = prime * result + ((getErrMsg() == null) ? 0 : getErrMsg().hashCode());
        result = prime * result + ((getJacocoSeverId() == null) ? 0 : getJacocoSeverId().hashCode());
        result = prime * result + ((getUnitTestType() == null) ? 0 : getUnitTestType().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", recordUuid=").append(recordUuid);
        sb.append(", moduleName=").append(moduleName);
        sb.append(", serverName=").append(serverName);
        sb.append(", createTime=").append(createTime);
        sb.append(", baseVersion=").append(baseVersion);
        sb.append(", nowVersion=").append(nowVersion);
        sb.append(", jacocoType=").append(jacocoType);
        sb.append(", reportUrl=").append(reportUrl);
        sb.append(", reportResult=").append(reportResult);
        sb.append(", errMsg=").append(errMsg);
        sb.append(", jacocoSeverId=").append(jacocoSeverId);
        sb.append(", unitTestType=").append(unitTestType);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}