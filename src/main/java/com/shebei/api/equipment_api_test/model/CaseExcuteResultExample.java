package com.shebei.api.equipment_api_test.model;

import java.util.ArrayList;
import java.util.List;

public class CaseExcuteResultExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Integer offset;

    public CaseExcuteResultExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andLinuxTimeIsNull() {
            addCriterion("linux_time is null");
            return (Criteria) this;
        }

        public Criteria andLinuxTimeIsNotNull() {
            addCriterion("linux_time is not null");
            return (Criteria) this;
        }

        public Criteria andLinuxTimeEqualTo(String value) {
            addCriterion("linux_time =", value, "linuxTime");
            return (Criteria) this;
        }

        public Criteria andLinuxTimeNotEqualTo(String value) {
            addCriterion("linux_time <>", value, "linuxTime");
            return (Criteria) this;
        }

        public Criteria andLinuxTimeGreaterThan(String value) {
            addCriterion("linux_time >", value, "linuxTime");
            return (Criteria) this;
        }

        public Criteria andLinuxTimeGreaterThanOrEqualTo(String value) {
            addCriterion("linux_time >=", value, "linuxTime");
            return (Criteria) this;
        }

        public Criteria andLinuxTimeLessThan(String value) {
            addCriterion("linux_time <", value, "linuxTime");
            return (Criteria) this;
        }

        public Criteria andLinuxTimeLessThanOrEqualTo(String value) {
            addCriterion("linux_time <=", value, "linuxTime");
            return (Criteria) this;
        }

        public Criteria andLinuxTimeLike(String value) {
            addCriterion("linux_time like", value, "linuxTime");
            return (Criteria) this;
        }

        public Criteria andLinuxTimeNotLike(String value) {
            addCriterion("linux_time not like", value, "linuxTime");
            return (Criteria) this;
        }

        public Criteria andLinuxTimeIn(List<String> values) {
            addCriterion("linux_time in", values, "linuxTime");
            return (Criteria) this;
        }

        public Criteria andLinuxTimeNotIn(List<String> values) {
            addCriterion("linux_time not in", values, "linuxTime");
            return (Criteria) this;
        }

        public Criteria andLinuxTimeBetween(String value1, String value2) {
            addCriterion("linux_time between", value1, value2, "linuxTime");
            return (Criteria) this;
        }

        public Criteria andLinuxTimeNotBetween(String value1, String value2) {
            addCriterion("linux_time not between", value1, value2, "linuxTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeIsNull() {
            addCriterion("excute_time is null");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeIsNotNull() {
            addCriterion("excute_time is not null");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeEqualTo(String value) {
            addCriterion("excute_time =", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeNotEqualTo(String value) {
            addCriterion("excute_time <>", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeGreaterThan(String value) {
            addCriterion("excute_time >", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeGreaterThanOrEqualTo(String value) {
            addCriterion("excute_time >=", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeLessThan(String value) {
            addCriterion("excute_time <", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeLessThanOrEqualTo(String value) {
            addCriterion("excute_time <=", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeLike(String value) {
            addCriterion("excute_time like", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeNotLike(String value) {
            addCriterion("excute_time not like", value, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeIn(List<String> values) {
            addCriterion("excute_time in", values, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeNotIn(List<String> values) {
            addCriterion("excute_time not in", values, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeBetween(String value1, String value2) {
            addCriterion("excute_time between", value1, value2, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andExcuteTimeNotBetween(String value1, String value2) {
            addCriterion("excute_time not between", value1, value2, "excuteTime");
            return (Criteria) this;
        }

        public Criteria andCaseNameIsNull() {
            addCriterion("case_name is null");
            return (Criteria) this;
        }

        public Criteria andCaseNameIsNotNull() {
            addCriterion("case_name is not null");
            return (Criteria) this;
        }

        public Criteria andCaseNameEqualTo(String value) {
            addCriterion("case_name =", value, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameNotEqualTo(String value) {
            addCriterion("case_name <>", value, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameGreaterThan(String value) {
            addCriterion("case_name >", value, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameGreaterThanOrEqualTo(String value) {
            addCriterion("case_name >=", value, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameLessThan(String value) {
            addCriterion("case_name <", value, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameLessThanOrEqualTo(String value) {
            addCriterion("case_name <=", value, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameLike(String value) {
            addCriterion("case_name like", value, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameNotLike(String value) {
            addCriterion("case_name not like", value, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameIn(List<String> values) {
            addCriterion("case_name in", values, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameNotIn(List<String> values) {
            addCriterion("case_name not in", values, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameBetween(String value1, String value2) {
            addCriterion("case_name between", value1, value2, "caseName");
            return (Criteria) this;
        }

        public Criteria andCaseNameNotBetween(String value1, String value2) {
            addCriterion("case_name not between", value1, value2, "caseName");
            return (Criteria) this;
        }

        public Criteria andApiNameIsNull() {
            addCriterion("api_name is null");
            return (Criteria) this;
        }

        public Criteria andApiNameIsNotNull() {
            addCriterion("api_name is not null");
            return (Criteria) this;
        }

        public Criteria andApiNameEqualTo(String value) {
            addCriterion("api_name =", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameNotEqualTo(String value) {
            addCriterion("api_name <>", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameGreaterThan(String value) {
            addCriterion("api_name >", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameGreaterThanOrEqualTo(String value) {
            addCriterion("api_name >=", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameLessThan(String value) {
            addCriterion("api_name <", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameLessThanOrEqualTo(String value) {
            addCriterion("api_name <=", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameLike(String value) {
            addCriterion("api_name like", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameNotLike(String value) {
            addCriterion("api_name not like", value, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameIn(List<String> values) {
            addCriterion("api_name in", values, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameNotIn(List<String> values) {
            addCriterion("api_name not in", values, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameBetween(String value1, String value2) {
            addCriterion("api_name between", value1, value2, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiNameNotBetween(String value1, String value2) {
            addCriterion("api_name not between", value1, value2, "apiName");
            return (Criteria) this;
        }

        public Criteria andApiVauleIsNull() {
            addCriterion("api_vaule is null");
            return (Criteria) this;
        }

        public Criteria andApiVauleIsNotNull() {
            addCriterion("api_vaule is not null");
            return (Criteria) this;
        }

        public Criteria andApiVauleEqualTo(String value) {
            addCriterion("api_vaule =", value, "apiVaule");
            return (Criteria) this;
        }

        public Criteria andApiVauleNotEqualTo(String value) {
            addCriterion("api_vaule <>", value, "apiVaule");
            return (Criteria) this;
        }

        public Criteria andApiVauleGreaterThan(String value) {
            addCriterion("api_vaule >", value, "apiVaule");
            return (Criteria) this;
        }

        public Criteria andApiVauleGreaterThanOrEqualTo(String value) {
            addCriterion("api_vaule >=", value, "apiVaule");
            return (Criteria) this;
        }

        public Criteria andApiVauleLessThan(String value) {
            addCriterion("api_vaule <", value, "apiVaule");
            return (Criteria) this;
        }

        public Criteria andApiVauleLessThanOrEqualTo(String value) {
            addCriterion("api_vaule <=", value, "apiVaule");
            return (Criteria) this;
        }

        public Criteria andApiVauleLike(String value) {
            addCriterion("api_vaule like", value, "apiVaule");
            return (Criteria) this;
        }

        public Criteria andApiVauleNotLike(String value) {
            addCriterion("api_vaule not like", value, "apiVaule");
            return (Criteria) this;
        }

        public Criteria andApiVauleIn(List<String> values) {
            addCriterion("api_vaule in", values, "apiVaule");
            return (Criteria) this;
        }

        public Criteria andApiVauleNotIn(List<String> values) {
            addCriterion("api_vaule not in", values, "apiVaule");
            return (Criteria) this;
        }

        public Criteria andApiVauleBetween(String value1, String value2) {
            addCriterion("api_vaule between", value1, value2, "apiVaule");
            return (Criteria) this;
        }

        public Criteria andApiVauleNotBetween(String value1, String value2) {
            addCriterion("api_vaule not between", value1, value2, "apiVaule");
            return (Criteria) this;
        }

        public Criteria andCaseModuleIsNull() {
            addCriterion("case_module is null");
            return (Criteria) this;
        }

        public Criteria andCaseModuleIsNotNull() {
            addCriterion("case_module is not null");
            return (Criteria) this;
        }

        public Criteria andCaseModuleEqualTo(String value) {
            addCriterion("case_module =", value, "caseModule");
            return (Criteria) this;
        }

        public Criteria andCaseModuleNotEqualTo(String value) {
            addCriterion("case_module <>", value, "caseModule");
            return (Criteria) this;
        }

        public Criteria andCaseModuleGreaterThan(String value) {
            addCriterion("case_module >", value, "caseModule");
            return (Criteria) this;
        }

        public Criteria andCaseModuleGreaterThanOrEqualTo(String value) {
            addCriterion("case_module >=", value, "caseModule");
            return (Criteria) this;
        }

        public Criteria andCaseModuleLessThan(String value) {
            addCriterion("case_module <", value, "caseModule");
            return (Criteria) this;
        }

        public Criteria andCaseModuleLessThanOrEqualTo(String value) {
            addCriterion("case_module <=", value, "caseModule");
            return (Criteria) this;
        }

        public Criteria andCaseModuleLike(String value) {
            addCriterion("case_module like", value, "caseModule");
            return (Criteria) this;
        }

        public Criteria andCaseModuleNotLike(String value) {
            addCriterion("case_module not like", value, "caseModule");
            return (Criteria) this;
        }

        public Criteria andCaseModuleIn(List<String> values) {
            addCriterion("case_module in", values, "caseModule");
            return (Criteria) this;
        }

        public Criteria andCaseModuleNotIn(List<String> values) {
            addCriterion("case_module not in", values, "caseModule");
            return (Criteria) this;
        }

        public Criteria andCaseModuleBetween(String value1, String value2) {
            addCriterion("case_module between", value1, value2, "caseModule");
            return (Criteria) this;
        }

        public Criteria andCaseModuleNotBetween(String value1, String value2) {
            addCriterion("case_module not between", value1, value2, "caseModule");
            return (Criteria) this;
        }

        public Criteria andSceneIsNull() {
            addCriterion("scene is null");
            return (Criteria) this;
        }

        public Criteria andSceneIsNotNull() {
            addCriterion("scene is not null");
            return (Criteria) this;
        }

        public Criteria andSceneEqualTo(String value) {
            addCriterion("scene =", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneNotEqualTo(String value) {
            addCriterion("scene <>", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneGreaterThan(String value) {
            addCriterion("scene >", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneGreaterThanOrEqualTo(String value) {
            addCriterion("scene >=", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneLessThan(String value) {
            addCriterion("scene <", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneLessThanOrEqualTo(String value) {
            addCriterion("scene <=", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneLike(String value) {
            addCriterion("scene like", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneNotLike(String value) {
            addCriterion("scene not like", value, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneIn(List<String> values) {
            addCriterion("scene in", values, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneNotIn(List<String> values) {
            addCriterion("scene not in", values, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneBetween(String value1, String value2) {
            addCriterion("scene between", value1, value2, "scene");
            return (Criteria) this;
        }

        public Criteria andSceneNotBetween(String value1, String value2) {
            addCriterion("scene not between", value1, value2, "scene");
            return (Criteria) this;
        }

        public Criteria andEnvIsNull() {
            addCriterion("env is null");
            return (Criteria) this;
        }

        public Criteria andEnvIsNotNull() {
            addCriterion("env is not null");
            return (Criteria) this;
        }

        public Criteria andEnvEqualTo(String value) {
            addCriterion("env =", value, "env");
            return (Criteria) this;
        }

        public Criteria andEnvNotEqualTo(String value) {
            addCriterion("env <>", value, "env");
            return (Criteria) this;
        }

        public Criteria andEnvGreaterThan(String value) {
            addCriterion("env >", value, "env");
            return (Criteria) this;
        }

        public Criteria andEnvGreaterThanOrEqualTo(String value) {
            addCriterion("env >=", value, "env");
            return (Criteria) this;
        }

        public Criteria andEnvLessThan(String value) {
            addCriterion("env <", value, "env");
            return (Criteria) this;
        }

        public Criteria andEnvLessThanOrEqualTo(String value) {
            addCriterion("env <=", value, "env");
            return (Criteria) this;
        }

        public Criteria andEnvLike(String value) {
            addCriterion("env like", value, "env");
            return (Criteria) this;
        }

        public Criteria andEnvNotLike(String value) {
            addCriterion("env not like", value, "env");
            return (Criteria) this;
        }

        public Criteria andEnvIn(List<String> values) {
            addCriterion("env in", values, "env");
            return (Criteria) this;
        }

        public Criteria andEnvNotIn(List<String> values) {
            addCriterion("env not in", values, "env");
            return (Criteria) this;
        }

        public Criteria andEnvBetween(String value1, String value2) {
            addCriterion("env between", value1, value2, "env");
            return (Criteria) this;
        }

        public Criteria andEnvNotBetween(String value1, String value2) {
            addCriterion("env not between", value1, value2, "env");
            return (Criteria) this;
        }

        public Criteria andRequestTypeIsNull() {
            addCriterion("request_type is null");
            return (Criteria) this;
        }

        public Criteria andRequestTypeIsNotNull() {
            addCriterion("request_type is not null");
            return (Criteria) this;
        }

        public Criteria andRequestTypeEqualTo(String value) {
            addCriterion("request_type =", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeNotEqualTo(String value) {
            addCriterion("request_type <>", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeGreaterThan(String value) {
            addCriterion("request_type >", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeGreaterThanOrEqualTo(String value) {
            addCriterion("request_type >=", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeLessThan(String value) {
            addCriterion("request_type <", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeLessThanOrEqualTo(String value) {
            addCriterion("request_type <=", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeLike(String value) {
            addCriterion("request_type like", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeNotLike(String value) {
            addCriterion("request_type not like", value, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeIn(List<String> values) {
            addCriterion("request_type in", values, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeNotIn(List<String> values) {
            addCriterion("request_type not in", values, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeBetween(String value1, String value2) {
            addCriterion("request_type between", value1, value2, "requestType");
            return (Criteria) this;
        }

        public Criteria andRequestTypeNotBetween(String value1, String value2) {
            addCriterion("request_type not between", value1, value2, "requestType");
            return (Criteria) this;
        }

        public Criteria andExcuteResultIsNull() {
            addCriterion("excute_result is null");
            return (Criteria) this;
        }

        public Criteria andExcuteResultIsNotNull() {
            addCriterion("excute_result is not null");
            return (Criteria) this;
        }

        public Criteria andExcuteResultEqualTo(String value) {
            addCriterion("excute_result =", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultNotEqualTo(String value) {
            addCriterion("excute_result <>", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultGreaterThan(String value) {
            addCriterion("excute_result >", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultGreaterThanOrEqualTo(String value) {
            addCriterion("excute_result >=", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultLessThan(String value) {
            addCriterion("excute_result <", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultLessThanOrEqualTo(String value) {
            addCriterion("excute_result <=", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultLike(String value) {
            addCriterion("excute_result like", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultNotLike(String value) {
            addCriterion("excute_result not like", value, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultIn(List<String> values) {
            addCriterion("excute_result in", values, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultNotIn(List<String> values) {
            addCriterion("excute_result not in", values, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultBetween(String value1, String value2) {
            addCriterion("excute_result between", value1, value2, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteResultNotBetween(String value1, String value2) {
            addCriterion("excute_result not between", value1, value2, "excuteResult");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceIsNull() {
            addCriterion("excute_sequence is null");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceIsNotNull() {
            addCriterion("excute_sequence is not null");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceEqualTo(String value) {
            addCriterion("excute_sequence =", value, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceNotEqualTo(String value) {
            addCriterion("excute_sequence <>", value, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceGreaterThan(String value) {
            addCriterion("excute_sequence >", value, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceGreaterThanOrEqualTo(String value) {
            addCriterion("excute_sequence >=", value, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceLessThan(String value) {
            addCriterion("excute_sequence <", value, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceLessThanOrEqualTo(String value) {
            addCriterion("excute_sequence <=", value, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceLike(String value) {
            addCriterion("excute_sequence like", value, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceNotLike(String value) {
            addCriterion("excute_sequence not like", value, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceIn(List<String> values) {
            addCriterion("excute_sequence in", values, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceNotIn(List<String> values) {
            addCriterion("excute_sequence not in", values, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceBetween(String value1, String value2) {
            addCriterion("excute_sequence between", value1, value2, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andExcuteSequenceNotBetween(String value1, String value2) {
            addCriterion("excute_sequence not between", value1, value2, "excuteSequence");
            return (Criteria) this;
        }

        public Criteria andCaseGroupIsNull() {
            addCriterion("case_group is null");
            return (Criteria) this;
        }

        public Criteria andCaseGroupIsNotNull() {
            addCriterion("case_group is not null");
            return (Criteria) this;
        }

        public Criteria andCaseGroupEqualTo(String value) {
            addCriterion("case_group =", value, "caseGroup");
            return (Criteria) this;
        }

        public Criteria andCaseGroupNotEqualTo(String value) {
            addCriterion("case_group <>", value, "caseGroup");
            return (Criteria) this;
        }

        public Criteria andCaseGroupGreaterThan(String value) {
            addCriterion("case_group >", value, "caseGroup");
            return (Criteria) this;
        }

        public Criteria andCaseGroupGreaterThanOrEqualTo(String value) {
            addCriterion("case_group >=", value, "caseGroup");
            return (Criteria) this;
        }

        public Criteria andCaseGroupLessThan(String value) {
            addCriterion("case_group <", value, "caseGroup");
            return (Criteria) this;
        }

        public Criteria andCaseGroupLessThanOrEqualTo(String value) {
            addCriterion("case_group <=", value, "caseGroup");
            return (Criteria) this;
        }

        public Criteria andCaseGroupLike(String value) {
            addCriterion("case_group like", value, "caseGroup");
            return (Criteria) this;
        }

        public Criteria andCaseGroupNotLike(String value) {
            addCriterion("case_group not like", value, "caseGroup");
            return (Criteria) this;
        }

        public Criteria andCaseGroupIn(List<String> values) {
            addCriterion("case_group in", values, "caseGroup");
            return (Criteria) this;
        }

        public Criteria andCaseGroupNotIn(List<String> values) {
            addCriterion("case_group not in", values, "caseGroup");
            return (Criteria) this;
        }

        public Criteria andCaseGroupBetween(String value1, String value2) {
            addCriterion("case_group between", value1, value2, "caseGroup");
            return (Criteria) this;
        }

        public Criteria andCaseGroupNotBetween(String value1, String value2) {
            addCriterion("case_group not between", value1, value2, "caseGroup");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}