package com.shebei.api.equipment_api_test.model;

import java.io.Serializable;

/**
 * @author 
 */
public class TestVersionRecord implements Serializable {
    private Integer id;

    private String moduleName;

    private String testVersion;

    private String serverName;

    private String uuidRecord;

    private String lastUuidRecord;

    private String jacocoType;

    private String createTime;

    private String updateTime;

    private String lastestReport;

    private String uuidBackup;

    private Integer vId;

    private String reportResult;

    private String errMsg;

    private String baseVersion;

    private String nowVersion;

    private Integer jacocoServerId;

    private String execState;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getTestVersion() {
        return testVersion;
    }

    public void setTestVersion(String testVersion) {
        this.testVersion = testVersion;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getUuidRecord() {
        return uuidRecord;
    }

    public void setUuidRecord(String uuidRecord) {
        this.uuidRecord = uuidRecord;
    }

    public String getLastUuidRecord() {
        return lastUuidRecord;
    }

    public void setLastUuidRecord(String lastUuidRecord) {
        this.lastUuidRecord = lastUuidRecord;
    }

    public String getJacocoType() {
        return jacocoType;
    }

    public void setJacocoType(String jacocoType) {
        this.jacocoType = jacocoType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getLastestReport() {
        return lastestReport;
    }

    public void setLastestReport(String lastestReport) {
        this.lastestReport = lastestReport;
    }

    public String getUuidBackup() {
        return uuidBackup;
    }

    public void setUuidBackup(String uuidBackup) {
        this.uuidBackup = uuidBackup;
    }

    public Integer getvId() {
        return vId;
    }

    public void setvId(Integer vId) {
        this.vId = vId;
    }

    public String getReportResult() {
        return reportResult;
    }

    public void setReportResult(String reportResult) {
        this.reportResult = reportResult;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getBaseVersion() {
        return baseVersion;
    }

    public void setBaseVersion(String baseVersion) {
        this.baseVersion = baseVersion;
    }

    public String getNowVersion() {
        return nowVersion;
    }

    public void setNowVersion(String nowVersion) {
        this.nowVersion = nowVersion;
    }

    public Integer getJacocoServerId() {
        return jacocoServerId;
    }

    public void setJacocoServerId(Integer jacocoServerId) {
        this.jacocoServerId = jacocoServerId;
    }

    public String getExecState() {
        return execState;
    }

    public void setExecState(String execState) {
        this.execState = execState;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        TestVersionRecord other = (TestVersionRecord) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getModuleName() == null ? other.getModuleName() == null : this.getModuleName().equals(other.getModuleName()))
            && (this.getTestVersion() == null ? other.getTestVersion() == null : this.getTestVersion().equals(other.getTestVersion()))
            && (this.getServerName() == null ? other.getServerName() == null : this.getServerName().equals(other.getServerName()))
            && (this.getUuidRecord() == null ? other.getUuidRecord() == null : this.getUuidRecord().equals(other.getUuidRecord()))
            && (this.getLastUuidRecord() == null ? other.getLastUuidRecord() == null : this.getLastUuidRecord().equals(other.getLastUuidRecord()))
            && (this.getJacocoType() == null ? other.getJacocoType() == null : this.getJacocoType().equals(other.getJacocoType()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getLastestReport() == null ? other.getLastestReport() == null : this.getLastestReport().equals(other.getLastestReport()))
            && (this.getUuidBackup() == null ? other.getUuidBackup() == null : this.getUuidBackup().equals(other.getUuidBackup()))
            && (this.getvId() == null ? other.getvId() == null : this.getvId().equals(other.getvId()))
            && (this.getReportResult() == null ? other.getReportResult() == null : this.getReportResult().equals(other.getReportResult()))
            && (this.getErrMsg() == null ? other.getErrMsg() == null : this.getErrMsg().equals(other.getErrMsg()))
            && (this.getBaseVersion() == null ? other.getBaseVersion() == null : this.getBaseVersion().equals(other.getBaseVersion()))
            && (this.getNowVersion() == null ? other.getNowVersion() == null : this.getNowVersion().equals(other.getNowVersion()))
            && (this.getJacocoServerId() == null ? other.getJacocoServerId() == null : this.getJacocoServerId().equals(other.getJacocoServerId()))
            && (this.getExecState() == null ? other.getExecState() == null : this.getExecState().equals(other.getExecState()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getModuleName() == null) ? 0 : getModuleName().hashCode());
        result = prime * result + ((getTestVersion() == null) ? 0 : getTestVersion().hashCode());
        result = prime * result + ((getServerName() == null) ? 0 : getServerName().hashCode());
        result = prime * result + ((getUuidRecord() == null) ? 0 : getUuidRecord().hashCode());
        result = prime * result + ((getLastUuidRecord() == null) ? 0 : getLastUuidRecord().hashCode());
        result = prime * result + ((getJacocoType() == null) ? 0 : getJacocoType().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getLastestReport() == null) ? 0 : getLastestReport().hashCode());
        result = prime * result + ((getUuidBackup() == null) ? 0 : getUuidBackup().hashCode());
        result = prime * result + ((getvId() == null) ? 0 : getvId().hashCode());
        result = prime * result + ((getReportResult() == null) ? 0 : getReportResult().hashCode());
        result = prime * result + ((getErrMsg() == null) ? 0 : getErrMsg().hashCode());
        result = prime * result + ((getBaseVersion() == null) ? 0 : getBaseVersion().hashCode());
        result = prime * result + ((getNowVersion() == null) ? 0 : getNowVersion().hashCode());
        result = prime * result + ((getJacocoServerId() == null) ? 0 : getJacocoServerId().hashCode());
        result = prime * result + ((getExecState() == null) ? 0 : getExecState().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", moduleName=").append(moduleName);
        sb.append(", testVersion=").append(testVersion);
        sb.append(", serverName=").append(serverName);
        sb.append(", uuidRecord=").append(uuidRecord);
        sb.append(", lastUuidRecord=").append(lastUuidRecord);
        sb.append(", jacocoType=").append(jacocoType);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", lastestReport=").append(lastestReport);
        sb.append(", uuidBackup=").append(uuidBackup);
        sb.append(", vId=").append(vId);
        sb.append(", reportResult=").append(reportResult);
        sb.append(", errMsg=").append(errMsg);
        sb.append(", baseVersion=").append(baseVersion);
        sb.append(", nowVersion=").append(nowVersion);
        sb.append(", jacocoServerId=").append(jacocoServerId);
        sb.append(", execState=").append(execState);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}