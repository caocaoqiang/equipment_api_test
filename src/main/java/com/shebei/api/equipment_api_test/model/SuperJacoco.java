package com.shebei.api.equipment_api_test.model;

import java.io.Serializable;

/**
 * super_jacoco
 * @author 
 */
public class SuperJacoco implements Serializable {
    private Integer id;

    private String moduleName;

    private String createTime;

    private String jacocoServer;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getJacocoServer() {
        return jacocoServer;
    }

    public void setJacocoServer(String jacocoServer) {
        this.jacocoServer = jacocoServer;
    }
}