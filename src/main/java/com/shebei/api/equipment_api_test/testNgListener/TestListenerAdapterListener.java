package com.shebei.api.equipment_api_test.testNgListener;

import org.springframework.stereotype.Component;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;

import java.util.Arrays;
import java.util.List;

/**
 * testng报告监听器
 */
@Component
public class TestListenerAdapterListener implements ITestListener {
    private static final List<String> testClasses = Arrays.asList(
            "com.shebei.api.equipment_api_test.EquipmentApiTestApplicationTests"
           );

    @Override
    public void onTestStart(ITestResult result) {
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        printLog(result);
    }

    @Override
    public void onTestFailure(ITestResult result) {
        if (testClasses.contains(result.getTestClass().getName())) {
            Reporter.log(result.getThrowable().getMessage());
            Reporter.log("\n");
            result.setThrowable(null);
        }
        printLog(result);
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        printLog(result);
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        printLog(result);
    }

    @Override
    public void onStart(ITestContext context) {

    }

    @Override
    public void onFinish(ITestContext context) {

    }

    private void printLog(ITestResult result) {
        Reporter.log(result.getMethod().getDescription());
    }
}
