package com.shebei.api.equipment_api_test.testNgListener;




import com.shebei.api.equipment_api_test.dao.UserCaseWoDAO;
import com.shebei.api.equipment_api_test.model.UserCaseWoWithBLOBs;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.IAnnotationTransformer;
import org.testng.annotations.ITestAnnotation;

import javax.servlet.ServletContextListener;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import com.shebei.api.equipment_api_test.utils.GetConfig;

/**
 * testng多线程监听器
 */
@ImportResource(locations = {"classpath:spring-mybatis.xml"})
public class TestNgThreadListener implements IAnnotationTransformer, ServletContextListener {

    private static final ApplicationContext mybatisContext = new ClassPathXmlApplicationContext("classpath:spring-mybatis.xml");


    private static UserCaseWoDAO userCaseWoMaper = mybatisContext.getBean(UserCaseWoDAO.class);

   // private static  Integer reportInvocationCount = userCaseWoMaper.selectCountByScene(GetConfig.getAddress("scene"));

    @Override
    public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
        if (testMethod != null) {
            switch (testMethod.getName()) {
                case "TestSceneCases":
                    // TestNgThreadListener testNgThreadListener=new TestNgThreadListener();
                      String scene=GetConfig.getAddress("scene").trim();
                    Integer reportInvocationCount = getCount();
                    annotation.setInvocationCount(reportInvocationCount);
                    break;



                default:
                    break;
            }
        }
    }


    public static Integer  getCount(){

        String scene=GetConfig.getAddress("scene").trim();
        String adptiveVersion=GetConfig.getAddress("adaptiveVersion").trim();
        UserCaseWoWithBLOBs record=new UserCaseWoWithBLOBs();
        record.setAdaptiveVersion(adptiveVersion);
        record.setScene(scene);
        Integer reportInvocationCount = userCaseWoMaper.selectCountByScene(record);

        return reportInvocationCount;
    }
}
