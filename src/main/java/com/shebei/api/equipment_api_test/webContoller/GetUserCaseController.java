package com.shebei.api.equipment_api_test.webContoller;


import com.shebei.api.equipment_api_test.dao.ApiNameQueryDAO;
import com.shebei.api.equipment_api_test.dao.UserCaseWoDAO;
import com.shebei.api.equipment_api_test.model.ApiNameQuery;
import com.shebei.api.equipment_api_test.model.UserCaseWo;
import com.shebei.api.equipment_api_test.model.UserCaseWoWithBLOBs;
import com.shebei.api.equipment_api_test.service.UserCaseExcute;
import com.shebei.api.equipment_api_test.utils.GetLinuxTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class GetUserCaseController {


    @Autowired
   UserCaseWoDAO userCaseWoDAO;

    @Autowired
    GetLinuxTime getLinuxTime;

    @Autowired
    ApiNameQueryDAO apiNameQueryDAO;
    @Autowired
    UserCaseExcute userCaseExcute;

    @RequestMapping("/query_user_case.do")
    public String getUserCases(UserCaseWoWithBLOBs record, Model model){
        deleteSpace(record);
       List<UserCaseWoWithBLOBs> list= userCaseWoDAO.selectCases(record);
        model.addAttribute("list", list);

        return "user_case_list";

    }

    @RequestMapping("/addUserCase")
    public String getUserCaseAdd(Model model){

        return "user_case_add";
    }


    @RequestMapping("/user_case_copy")
    public String getCopyWeb(Integer id,Model model){
        UserCaseWo userCase=  userCaseWoDAO.selectByPrimaryKey(id);
        model.addAttribute("list", userCase);
        return "user_case_copy";
    }
    @RequestMapping("/add_user_case.do")
    public String addUserCase(UserCaseWoWithBLOBs record,Model model){
        deleteSpace(record);

        String time= getLinuxTime.getTime();
        String userCaseName=record.getUserCaseName()+"_"+time;
        record.setUserCaseName(userCaseName);
        record= getApiValue( record);
        userCaseWoDAO.insert(record);
        return "user_case_list";
    }


    @RequestMapping("/copy_user_case.do")
    public String copyUserCase(UserCaseWoWithBLOBs record,Model model){
        deleteSpace(record);
        String time= getLinuxTime.getTime();
        String userCaseName=record.getUserCaseName()+"_"+time;
        record.setUserCaseName(userCaseName);
        record= getApiValue( record);
        userCaseWoDAO.insert(record);
        return "user_case_list";
    }

    @RequestMapping("/update_user_case.do")
    public String updateUserCase(UserCaseWoWithBLOBs record,Model model){
        deleteSpace(record);
         record= getApiValue( record);

        userCaseWoDAO.updateByPrimaryKeyWithBLOBs(record);
        return "user_case_list";
    }

    @RequestMapping("/query_user_update")
    public String queryUserUpdate(Integer id,Model model){

        UserCaseWo userCaseWo=   userCaseWoDAO.selectByPrimaryKey(id);
        model.addAttribute("list", userCaseWo);
        return "user_case_update";
    }

    //user_case_excute

    @RequestMapping("/user_case_excute")
    public String userCaseExcute(Integer id,Model model){


      String result=  userCaseExcute.excuteSigleOne(id);
        model.addAttribute("list", result);
        return "user_case_result";
    }

    public void deleteSpace(UserCaseWoWithBLOBs var) {
        if (var.getApiName() != null) {
            var.setApiName(var.getApiName().trim());
        }
        if (var.getModule() != null) {
            var.setModule(var.getModule().trim());
        }
        if (var.getUserCaseName() != null) {
            var.setUserCaseName(var.getUserCaseName().trim());
        }
        if (var.getScene() != null) {
            var.setScene(var.getScene().trim());
        }
        if (var.getCheckPath() != null) {
            var.setCheckPath(var.getCheckPath().trim());
        }

        if (var.getParameterSavePath() != null) {
            var.setParameterSavePath(var.getParameterSavePath().trim());
        }
        if (var.getRequestBody() != null) {
            var.setRequestBody(var.getRequestBody().trim());
        }
        if (var.getApiValue() != null) {
            var.setApiValue(var.getApiValue().trim());
        }
        if (var.getRequestType() != null) {
            var.setRequestType(var.getRequestType().trim());
        }
        if (var.getAdaptiveVersion() != null) {
            var.setAdaptiveVersion(var.getAdaptiveVersion().trim());
        }


    }


    private UserCaseWoWithBLOBs getApiValue(UserCaseWoWithBLOBs record){
        String apiName=record.getApiName();
        if(apiName==null||apiName.equals("")){
            return  record;
        }
        ApiNameQuery apiNameQuery=apiNameQueryDAO.selectByName(apiName);
        if(apiNameQuery!=null){
            record.setApiValue(apiNameQuery.getApiValue());
            record.setRequestType(apiNameQuery.getRequestType());
        }
        return record;
    }

   public void updateCases(){
       List<UserCaseWoWithBLOBs> list= userCaseWoDAO.selectCases(null);
       for(UserCaseWoWithBLOBs each:list){
           each= getApiValue( each);

           userCaseWoDAO.updateByPrimaryKeyWithBLOBs(each);
       }
   }

}
