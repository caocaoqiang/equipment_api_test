package com.shebei.api.equipment_api_test.webContoller;


import com.shebei.api.equipment_api_test.dao.ApiNameQueryDAO;
import com.shebei.api.equipment_api_test.model.ApiNameQuery;
import com.shebei.api.equipment_api_test.utils.GetLinuxTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class GetApiListController {

    @Autowired
    ApiNameQueryDAO apiNameQueryDAO;
    @Autowired
    GetLinuxTime getLinuxTime;
    @RequestMapping("/query_report.do")
    public String getApiList(ApiNameQuery record, Model model){
        deleteSpace(record);

       List<ApiNameQuery> apiNameQueryList=apiNameQueryDAO.selectApi(record);
        model.addAttribute("list", apiNameQueryList);
        return "api_address_list";

    }



    @RequestMapping("/add_one_api")
    public String addHttpApi(ApiNameQuery record,Model model){
        deleteSpace(record);
        String time=getLinuxTime.getTime();
        String apiName=record.getApiCategory()+"_"+record.getApiName()+time;
        record.setApiName(apiName);
         apiNameQueryDAO.insert(record);

            return "api_address_list";

    }


    @RequestMapping("/copy_one_api")
    public String copyOneApi(ApiNameQuery record,Model model){
        deleteSpace(record);
        String time=getLinuxTime.getTime();
        String apiName=record.getApiName()+time;
        record.setApiName(apiName);
        apiNameQueryDAO.insert(record);

        return "api_address_list";

    }

    @RequestMapping("/load_api_update")
    public String loadApiUpdate(Integer id,Model model){
        ;
        ApiNameQuery  apiList=apiNameQueryDAO.selectByPrimaryKey(id);

        model.addAttribute("list", apiList);
        return "api_address_update";
    }


    @RequestMapping("/update_api.do")
    public String updateApi(ApiNameQuery record,Model model){

        deleteSpace(record);
        apiNameQueryDAO.updateByPrimaryKeySelective(record);

        return "api_address_list";

    }


    @RequestMapping("/load_api_copy")
    public String loadApiCopy(Integer id,Model model){
        ;
        ApiNameQuery  apiList=apiNameQueryDAO.selectByPrimaryKey(id);

        model.addAttribute("list", apiList);
        return "api_address_copy";
    }


    private void deleteSpace(ApiNameQuery var) {
        if (var.getApiName() != null) {
            var.setApiName(var.getApiName().trim());
        }
        if (var.getApiCategory() != null) {
            var.setApiCategory(var.getApiCategory().trim());
        }
        if (var.getApiType() != null) {
            var.setApiType(var.getApiType().trim());
        }
        if (var.getApiValue() != null) {
            var.setApiValue(var.getApiValue().trim());
        }
        if (var.getApiDescribe() != null) {
            var.setApiDescribe(var.getApiDescribe().trim());
        }

        if (var.getHeadInfo() != null) {
            var.setHeadInfo(var.getHeadInfo().trim());
        }
        if (var.getRequestType() != null) {
            var.setRequestType(var.getRequestType().trim());
        }
    }

}
