package com.shebei.api.equipment_api_test.webContoller;

import com.shebei.api.equipment_api_test.dao.EnviromentParameterDAO;
import com.shebei.api.equipment_api_test.model.ApiNameQuery;
import com.shebei.api.equipment_api_test.model.EnviromentParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


@Controller
public class GetParaEnvController {

    @Autowired
    EnviromentParameterDAO enviromentParameterDAO;

    @RequestMapping("/query_para_env.do")
    public String getParaEnvList(EnviromentParameter record, Model model){
        deleteSpace(record);
        List<EnviromentParameter>   list= enviromentParameterDAO.selectPara(record);


        model.addAttribute("list", list);
        return "parameter_env_list";

    }


    @RequestMapping("/getParaEnvAddWeb")
    public String toParaAddWeb( Model model){

        return "parameter_env_add";

    }



    @RequestMapping("/add_parameterEnv")
    public String addParaEnv( EnviromentParameter record,Model model){
        deleteSpace(record);
        enviromentParameterDAO.insert(record);
        return "parameter_env_list";

    }


    @RequestMapping("/update_paramEnv.do")
    public String updateParaEnv( EnviromentParameter record,Model model){
        deleteSpace(record);
        enviromentParameterDAO.updateByPrimaryKeySelective(record);
        // List<ApiNameQuery> apiNameQueryList=apiNameQueryDAO.selectApi(record);
        // model.addAttribute("list", apiNameQueryList);
        return "parameter_env_list";

    }


    @RequestMapping("/load_paraEnv_update")
    public String loadParaUpdateWeb(Integer id, Model model){
        EnviromentParameter enviromentParameter=  enviromentParameterDAO.selectByPrimaryKey(id);
        model.addAttribute("list", enviromentParameter);
        return "parameter_env_update";

    }

    private void deleteSpace(EnviromentParameter var) {
        if (var.getParameterName() != null) {
            var.setParameterName(var.getParameterName().trim());
        }
        if (var.getModule() != null) {
            var.setModule(var.getModule().trim());
        }
        if (var.getParameterValue() != null) {
            var.setParameterValue(var.getParameterValue().trim());
        }
        if (var.getScene() != null) {
            var.setScene(var.getScene().trim());
        }
        if (var.getEnv() != null) {
            var.setEnv(var.getEnv().trim());
        }
    }

}
