package com.shebei.api.equipment_api_test.webContoller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {

    @RequestMapping("/index")
    public String index(Model model){

        return "index";
    }
    @RequestMapping("getApiList")
    public String getApi(Model model){

        return "api_address_list";
    }
    @RequestMapping("getUserManger")
    public String getUserManger(Model model){

        return "api_address_add";
    }

    @RequestMapping("addApi")
    public String addApi(Model model){

        return "api_address_add";
    }



    @RequestMapping("addDeviceRecord")
    public String getDeviceManageAdd(Model model){

        return "device_Manage_add";
    }


    @RequestMapping("getDeviceManageList")
    public String getDeviceManageList(Model model){

        return "device_Manage_list";
    }

    @RequestMapping("getUserList")
    public String getUserList(Model model){

        return "user_case_list";
    }


    @RequestMapping("getParaENList")
    public String getParaEnList(Model model){

        return "parameter_env_list";
    }




}
