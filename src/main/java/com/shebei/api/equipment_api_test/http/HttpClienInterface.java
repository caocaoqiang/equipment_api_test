package com.shebei.api.equipment_api_test.http;

import java.util.Map;

public interface HttpClienInterface {


    public String sendRequest(String url, String requestBody, Map<String,String> head);

}
