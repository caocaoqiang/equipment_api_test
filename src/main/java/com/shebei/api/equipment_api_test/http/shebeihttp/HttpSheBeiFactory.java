package com.shebei.api.equipment_api_test.http.shebeihttp;

import com.shebei.api.equipment_api_test.http.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class HttpSheBeiFactory {




    @Autowired
    private HttpClientGet httpClientGet;


    @Autowired
    private HttpClientJson httpClientJson;


    @Autowired
    private  HttpClientPutJson httpClientPutJson;

    public HttpClienInterface getHttpClient(String requestType){



        if(requestType.equals("get")){
            return httpClientGet;
        }

        if(requestType.equals("post") ){
            return httpClientJson;
        }

        if(requestType.equals("put") ){
            return httpClientPutJson;
        }

        return null;
    }
}
