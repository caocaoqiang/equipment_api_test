package com.shebei.api.equipment_api_test.http;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class HttpClientSQLConnect implements HttpClienInterface {



    @Override
    public String sendRequest(String url, String requestBody, Map<String, String> head) {




        String res=doPostJson( url,  requestBody,head);

        return res;
    }



    private   String doPostJson(String url, String json,Map<String, String> head) {
        // 创建Httpclient对象

        String driverClassName = "com.mysql.cj.jdbc.Driver";
         url = "jdbc:mysql://10.1.20.57:3306/smp?serverTimezone=Hongkong&characterEncoding=UTF-8&autoReconnect=true";

        String mysqlusername = "smp";
        String password = "lY7lBKU2hhJ6b3qhIlXHMcd13XNb3SuChJZ7eiQf";
        if (head != null){
            for (String key : head.keySet()) {
                mysqlusername=head.get("userName");
                password=head.get("password");
                url=head.get("url");
            }


        }
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        JSONObject result= new JSONObject();
        try {

            Class.forName(driverClassName);
            con = DriverManager.getConnection(url, mysqlusername, password);


            String sql = json;
            pstmt = con.prepareStatement(sql);


            rs = pstmt.executeQuery();
            JSONArray array= new JSONArray();
            if (rs == null) {

                result.put("success","false");
            }
            while (rs.next()) {
                ResultSetMetaData rsmd = rs.getMetaData();
                JSONObject tmp= new JSONObject();

                for (int i = 1; i < rsmd.getColumnCount(); i++) {
                    String name = rsmd.getColumnName(i);
                    String value = rs.getString(i);
                    System.out.println(name + ":" + value);
                    tmp.put(name,value);
                }
                array.add(tmp);


            }
            result.put("data",array);
            result.put("success","true");

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            //关闭资源,倒关
            try {
                if (rs != null) rs.close();
                if (pstmt != null) pstmt.close();
                if (con != null) con.close();  //必须要关
            } catch (Exception e) {
            }

        }
      return result.toJSONString();






    }



}
