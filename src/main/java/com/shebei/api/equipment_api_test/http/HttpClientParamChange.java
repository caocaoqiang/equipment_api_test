package com.shebei.api.equipment_api_test.http;


import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.service.HeadParsing;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.util.Map;

@Component
public class HttpClientParamChange implements HttpClienInterface {


    @Override
    public String sendRequest(String url, String requestBody, Map<String, String> head) {

        if(requestBody == null){
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("result","failed");
            jsonObject.put("msg","body为空，不知道需要替换哪个变量，请在body体里面填写需要替换的变量值");
            return jsonObject.toJSONString();
        }
        Map param = JSONObject.parseObject(requestBody);
        param.put("result","success");

        return ((JSONObject) param).toJSONString();
    }




}

