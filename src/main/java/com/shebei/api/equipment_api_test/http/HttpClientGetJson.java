package com.shebei.api.equipment_api_test.http;


import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.http.httpdeletejson.HttpGetWithBody;
import com.shebei.api.equipment_api_test.service.HeadParsing;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.util.Map;

@Component
public class HttpClientGetJson implements HttpClienInterface {

    @Autowired
   private HeadParsing headParsing;
    @Override
    public String sendRequest(String url, String requestBody, Map<String, String> head) {

           if(requestBody==null){

             return   doGet( url, null,head);
           }

        Map param = JSONObject.parseObject(requestBody);


      String response=  doGet( url, param, head);
        return response;
    }

    private   String doGet(String url, Map<String, String> param,Map<String, String> head) {

        // 创建Httpclient对象
        CloseableHttpClient httpclient = HttpClients.createDefault();
        String resultString = "";
        CloseableHttpResponse response = null;
        try {

            HttpGetWithBody httpGet = new HttpGetWithBody(url);


            if (param != null){
                StringEntity entity = new StringEntity(param.toString(), ContentType.APPLICATION_JSON);
                httpGet.setEntity(entity);


            }

            if (head != null){
                for (String key : head.keySet()) {
                    httpGet.setHeader(key,String.valueOf(head.get(key)));
                }


            }
            httpGet.addHeader("content-type", "application/json");
            httpGet.addHeader("Accept-Encoding", "gzip, deflate, br");
            // 执行请求
            response = httpclient.execute(httpGet);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == 200) {
                resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return resultString;
    }


}

