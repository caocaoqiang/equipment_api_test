package com.shebei.api.equipment_api_test.http;


import com.shebei.api.equipment_api_test.http.haiwaiyun.HttpTransform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HttpClientFactory {

    @Autowired
   private HttpClientJson httpClientJson;

    @Autowired
   private HttpClientFormData httpClientFormData;

    @Autowired
   private HttpClientGet httpClientGet;
    @Autowired
   private HttpClientDelete httpClientDelete;

    @Autowired
    private  HttpClientPutJson httpClientPutJson;

    @Autowired
    private  HttpClientPutForm httpClientPutForm;
    @Autowired
    private  HttpClientDeleteJson httpClientDeleteJson;

    @Autowired
    private  HttpClientFormUrlencoded httpClientFormUrlencoded;
    @Autowired
    private  HttpClientPutParam httpClientPutParam;
    @Autowired
    private HttpClientGetJson httpClientGetJson;
    @Autowired
    private  HttpClientParamChange httpClientParamChange;
    @Autowired
    private  HttpClientCreateTimeStamp httpClientCreateTimeStamp;
    @Autowired
    private HttpTransform httpTransform;
    @Autowired
    private HttpClientGetImage httpClientGetImage;
    @Autowired
    private HttpClientSQLConnect httpClientSQLConnect;
    @Autowired
    private HttpClientSQLConnectUpdate httpClientSQLConnectUpdate;

    public HttpClienInterface getHttpClient(String requestType,String contentType){

        if(requestType.equals("get")&&contentType.equals("application/json")){
            return httpClientGetJson;
        }

        if(requestType.equals("get")&&(contentType.equals("param")||contentType.equals(""))){
            return httpClientGet;
        }

        if(requestType.equals("delete")&&contentType.equals("application/json")){
            return httpClientDeleteJson;
        }
        if(requestType.equals("delete")&&contentType.equals("query")){
            return httpClientDelete;
        }
        if(requestType.equals("delete")){
            return httpClientDelete;
        }
        if(requestType.equals("post") && contentType.equals("application/json")){
            return httpClientJson;
        }
        if(requestType.equals("post") && contentType.equals("form-urlencoded")){
            return httpClientFormUrlencoded;
        }
        if(requestType.equals("post")&&  contentType.equals("form-data")){
            return httpClientFormData;
        }

        if(requestType.equals("put")&&  contentType.equals("form-data")){
            return httpClientPutForm;
        }
        if(requestType.equals("put")&&  contentType.equals("application/json")){
            return httpClientPutJson;
        }

        if(requestType.equals("put")&&  contentType.equals("param-none")){
            return httpClientPutParam;
        }

        if(requestType.equals("change")&&  contentType.equals("param")){
            return httpClientParamChange;
        }

        if(requestType.equals("change")&&  contentType.equals("timeStamp")){
            return httpClientCreateTimeStamp;
        }
        if(requestType.equals("change")&&  contentType.equals("haiwaiLogon")){
            return httpTransform;
        }

        if(requestType.equals("change")&&  contentType.equals("haiwaiVerifyCode")){
            return httpClientGetImage;
        }
        if(requestType.equals("mysql")&&  contentType.equals("query")){
            return httpClientSQLConnect;
        }
        if(requestType.equals("mysql")&&  contentType.equals("update")){
            return httpClientSQLConnectUpdate;
        }
        return null;
    }
}
