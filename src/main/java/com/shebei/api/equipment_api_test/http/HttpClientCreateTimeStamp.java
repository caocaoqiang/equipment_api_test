package com.shebei.api.equipment_api_test.http;


import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.utils.GetLinuxTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class HttpClientCreateTimeStamp implements HttpClienInterface {

   @Autowired
   GetLinuxTime getLinuxTime;


    @Override
    public String sendRequest(String url, String requestBody, Map<String, String> head) {


            JSONObject jsonObject=new JSONObject();
            String timeStamp =getLinuxTime.getTime();
            String dataTime=getLinuxTime.getData();
            jsonObject.put("result","success");
            jsonObject.put("timeStamp",timeStamp);
            jsonObject.put("datatime",dataTime);
             return jsonObject.toJSONString();
    }




}

