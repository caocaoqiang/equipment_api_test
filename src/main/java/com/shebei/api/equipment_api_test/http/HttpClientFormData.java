package com.shebei.api.equipment_api_test.http;


import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.service.HeadParsing;
import com.shebei.api.equipment_api_test.utils.GetConfig;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Component
public class HttpClientFormData implements HttpClienInterface{



    @Autowired
    private HeadParsing headParsing;
    @Override
    public String sendRequest(String url, String requestBody, Map<String, String> head) {
        if(requestBody==null){
            return doPostFormData( url,  null, head);
        }


        Map param = JSONObject.parseObject(requestBody);

      String res=  doPostFormData( url,  param, head);

        return res;
    }


    private   String doPostFormData(String url, Map<String, String> param,Map<String, String> heard )  {
        // 创建Httpclient对象
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String resultString = "";

        try {
            // 创建Http Post请求
            HttpPost httpPost = new HttpPost(url);
            if(heard!=null){
                for (String key : heard.keySet()) {
                    httpPost.setHeader(key,heard.get(key));
                }

            }
           // httpPost.setHeader("Content-Type", "multipart/form-data");
            // 创建参数列表
            if (param != null) {
                MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
                for (String key : param.keySet()) {
                    String valRes=String.valueOf(param.get(key));
                    String[] vals=valRes.split("\\|\\|");

                    if(vals[0].equals("File")){
                        String filePath= GetConfig.getAddress("filePath");

                            entityBuilder.addBinaryBody(key, new FileInputStream(filePath+vals[1]), ContentType.MULTIPART_FORM_DATA, vals[1]);


                    }else {
                        entityBuilder.addTextBody(key, String.valueOf(param.get(key)));
                    }
                }
                HttpEntity entity = entityBuilder.build();
                // 模拟表单

                httpPost.setEntity(entity);
            }
            // 执行http请求
            response = httpClient.execute(httpPost);
            resultString = EntityUtils.toString(response.getEntity(), "utf-8");
        } catch (Exception e) {

                e.printStackTrace();

        } finally {
            try {
                if (null!=response){
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return resultString;
    }
}
