package com.shebei.api.equipment_api_test.http;


import com.alibaba.fastjson.JSONObject;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.util.Map;

@Component
public class HttpClientDelete implements HttpClienInterface {


    @Override
    public String sendRequest(String url, String requestBody, Map<String, String> head) {

           if(requestBody==null){

             return   doDelete( url, null,head);
           }

        Map param = JSONObject.parseObject(requestBody);

      String response=  doDelete( url, param, head);
        return response;
    }

    private   String doDelete(String url, Map<String, String> param,Map<String, String> head) {

        // 创建Httpclient对象
        CloseableHttpClient httpclient = HttpClients.createDefault();

        String resultString = "";
        CloseableHttpResponse response = null;
        try {
            // 创建uri
            URIBuilder builder = new URIBuilder(url);
            if (param != null) {
                for (String key : param.keySet()) {
                    builder.addParameter(key, String.valueOf(param.get(key)));
                }
            }
            URI uri = builder.build();

            System.out.println("请求URL为："+uri);
            // 创建http GET请求
            HttpDelete httpDelete = new HttpDelete(uri);


            if (head != null){
                for (String key : head.keySet()) {
                    httpDelete.setHeader(key,String.valueOf(head.get(key)));
                }


            }

            // 执行请求
            response = httpclient.execute(httpDelete);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == 200) {
                resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return resultString;
    }


}

