package com.shebei.api.equipment_api_test.http.haiwaiyun;


import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.http.HttpClienInterface;
import com.shebei.api.equipment_api_test.utils.AESUtilsHaiWai;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Component
public class HttpTransform implements HttpClienInterface { //海外云平台登陆 变量产生


    @Override
    public String sendRequest(String url, String requestBody, Map<String, String> head) {

        if(requestBody == null){
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("result","failed");
            jsonObject.put("msg","body不能为空");
            return jsonObject.toJSONString();
        }
        Map param = JSONObject.parseObject(requestBody);
        String  miWen=doHaiWaiTransform(param);

        param.put("miWen",miWen);
        param.put("result","success");





        return ((JSONObject) param).toJSONString();
    }

//海外云平台转换

    private   String doHaiWaiTransform(Map<String, String> param)  {

        String random=null;
        String password=null;
        for (String key : param.keySet()) {

            if(key.equals("random")){
                random= param.get(key);
            }
            if(key.equals("password")){
                password=param.get(key);
            }




        }
        if(random!=null &&password!=null){


        String randomTmp=genRandomNum();
        String minWen=random+randomTmp+password;
        String miWen= AESUtilsHaiWai.encryptAES(minWen,password);
         return  miWen;
        }

        return "random或者password字段为空";
    }


    public String genRandomNum() {
        int maxNum = 36;
        int i;
        int count = 0;
        char[] str = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
                'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
                'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        StringBuffer pwd = new StringBuffer("");
        Random r = new Random();
        while (count < 8) {
            i = Math.abs(r.nextInt(maxNum));
            if (i >= 0 && i < str.length) {
                pwd.append(str[i]);
                count++;
            }
        }
        return pwd.toString();


    }

}

