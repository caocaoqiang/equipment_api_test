package com.shebei.api.equipment_api_test.utils;

import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;


@Component
public class GetLinuxTime {



    public String getTime(){
        Date now =new Date();
        long timestamp=now.getTime();

        return String.valueOf(timestamp);
    }


    public String getData(){
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateNowStr = sdf.format(d);
        return dateNowStr;
    }
}
