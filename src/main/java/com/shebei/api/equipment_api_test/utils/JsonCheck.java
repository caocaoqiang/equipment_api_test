package com.shebei.api.equipment_api_test.utils;

import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Component;


@Component
public class JsonCheck {


    public  boolean isJson(String content) {
        try {
            JSONObject.parseObject(content);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
