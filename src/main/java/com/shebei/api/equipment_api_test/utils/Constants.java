package com.shebei.api.equipment_api_test.utils;


public class Constants {

    private Constants(){}

    /****************************************start***************************************/

    /**
     * 通过email注册
     */
    public static final Integer COMMON_ONE = 1;

    public static final String APP_LOG_PREFIX = "eco";

    /**
     * eco账户登录标识:前端token名称
     */
    public static final String ECO_WEB_TOKEN = "eco_token";

    /**
     * redis存放的token前缀
     */
    public static final String ECO_REDIS_PREFIX_TOKEN ="ECO_TOKEN:";
    /*************************************end*************************************************/

    /*************************************register start*************************************************/
    /**
     * 默认验证码位数
     */
    public static final int DEFAULT_CAPTCHA_LENGTH = 6;

    /**
     * 验证码5分钟
     */
    public static final Long DEFAULT_CAPTCHA_EXPIRE_SECONDS = 300L;
    /**
     * 验证码前缀
     */
    public static final String VERIFY_CAPTCHA_PREFIX = "ECO_CODE:";
    /**
     * 通过email注册
     */
    public static final Integer REGISTER_EMAIL = 1;
    /**
     * 通过phone注册
     */
    public static final Integer REGISTER_PHONE = 2;
    /**
     * 业务类型：
     * 账号注册
     */
    public static final Integer ACCOUNT_REGISTER=1;
    /**
     * 业务类型：
     * 密码修改
     */
    public static final Integer PASSWORD_MODIFIED=2;

    public static final String CHECK_VALUE="@";
    /**
     * 前端图片地址
     */
    public static final String LOG_URL = "LOG_URL:";
    /*************************************end*************************************************/

    /***********************************************kafka start***************************************/
    /**
     *发送给sdk的kafka的主题
     */
    public static final String ECO_REGISTER_ACCOUNT_TOPIC = "ECO_REGISTER_ACCOUNT_TOPIC";
    /**
     * 发送给wo的kafka主体
     */
    public static final String ECO_WO_ACCOUNT_TOPIC = "ECO_WO_ACCOUNT_TOPIC";
    /***********************************************kafka end***************************************/

    /***********************************************account scene start***************************************/
    public static final String ACCOUNT_SCENE_SHARED = "shared";

    /***********************************************account scene end***************************************/

    /****************************************************aes start*************************************************************/
    public static final String AES_IV_STR = "EcoUbiCCSaveTime";

    public static final String AES_ENCODE_FORMAT = "AES";

    public static final String AES_ENCODE_FORMAT_DETAIL = "AES/CBC/PKCS5Padding";

    /****************************************************aes end*************************************************************/


    /****************************************************oauth2授权页面携带参数名称 oauth url params start*************************************************************/
    public static final String CLIENT_ID = "client_id";

    public static final  String RESPONSE_TYPE = "response_type";

    public static final String REDIRECT_URL = "redirect_uri";

    public static final String RESPONSE_TYPE_VALUE = "code";
    /****************************************************oauth url params end*************************************************************/

    /*********************************************EcoAccountDispatch business tye start*********************************************************/
    /**
     * 账号注册
     */
    public static final String ACCOUNT_REGISTER_VALUE= "REGISTER";
    /**
     * 密码修改
     */
    public static final String SECRET_MODIFIED_VALUE ="PASSWORD";

    /*********************************************EcoAccountDispatch business tye end*********************************************************/

    /**
     * 根部门上级部门id
     */
    public static final Long ROOT_ORG_PARENT_ID=-1L;

    public static final String PLATFORM_AES_KEY="uniubiecoplatform";
    public static final String PLATFORM_TOKEN_KEY="PLATFORM_TOKEN:";


    public static final String ACCESS_TOKEN_NAME = "SDK_ACCESS_TOKEN";

    public static final String SECRET_KEY_NAME = "SDK_SECRET_KEY";


    /*********************************************phone and email *********************************************************/
    public static final String VALIDATE_PHONE = "^((13[0-9])|(15[^4,\\D])|(16[6])|(18[0-9])|(19[1,8,9])|(14[1,4,5,6,7,8,9])|(17[0,1,3,5-8]))\\d{8}$";

    public static final String VALIDATE_EMAIL = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
    /*********************************************phone and email end*********************************************************/

    public static final String CACHE_PERMISSION_KEY_PREFIX = "permission";

    /**
     * 预定义权限数量
     */
    public static final Integer PREDEFINED_PERMISSION_COUNT = 1;


}
