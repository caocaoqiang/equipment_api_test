package com.shebei.api.equipment_api_test.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Base64;

/**
 * Aes加密解密类
 * @author zqp
 */
public class AesEncryptUtils {

    private static final Logger logger = LoggerFactory.getLogger(AesEncryptUtils.class);
    private static final String AES = "AES";

    private AesEncryptUtils(){}

    public static String aesEncode(String encodeRules, String content) {
        String aesEncode=null;
        try {
            SecretKey key = getSecretKey(encodeRules);
            Cipher cipher = Cipher.getInstance(Constants.AES_ENCODE_FORMAT_DETAIL);
            String IV_STR="UUUUUU4321euynix";
            cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(IV_STR.getBytes(StandardCharsets.UTF_8)));
            byte[] byteEncode = content.getBytes(StandardCharsets.UTF_8);
            byte[] byteAes = cipher.doFinal(byteEncode);
            aesEncode = new String(Base64.getEncoder().encode(byteAes));
        } catch (Exception e) {
            logger.error("AesEncryptUtils aesEncode error,encodeRules={},content={}",encodeRules,content,e);
        }
        return aesEncode;
    }

    /**
     * 解密 解密过程： 1.同加密1-4步 2.将加密后的字符串反纺成byte[]数组 3.将加密内容解密
     * @param encodeRules 密钥规则，类似于密钥
     * @param content     待加密内容
     * @return
     */
    public static String aesDecode(String encodeRules, String content) {
        String aesDecode = null;
        try {
            SecretKey key = getSecretKeyN(encodeRules);
            Cipher cipher = Cipher.getInstance(Constants.AES_ENCODE_FORMAT_DETAIL);
            String IV_STR="UUUUUU4321euynix";
            cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(IV_STR.getBytes(StandardCharsets.UTF_8)));
            byte[] byteContent = Base64.getDecoder().decode(content);
            byte[] byteDecode = cipher.doFinal(byteContent);
            aesDecode = new String(byteDecode, StandardCharsets.UTF_8);
        } catch (Exception e) {
            logger.error("AesEncryptUtils aesDecode error,encodeRules={},content={}",encodeRules,content,e);
        }
        return aesDecode;
    }


    private static SecretKey getSecretKey(String encodeRules){
        KeyGenerator keygen;
        SecretKey key=null;
        try {
            keygen = KeyGenerator.getInstance(Constants.AES_ENCODE_FORMAT);
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            secureRandom.setSeed(encodeRules.getBytes());
            keygen.init(128, secureRandom);
            SecretKey originalKey = keygen.generateKey();
            byte[] raw = originalKey.getEncoded();
            key = new SecretKeySpec(raw, Constants.AES_ENCODE_FORMAT);
        } catch (Exception e) {
            logger.error("AesEncryptUtils aesDecode error,encodeRules={},content={}",encodeRules,e);
        }
        return key;
    }


    private static SecretKey getSecretKeyN(String keys){
        KeyGenerator keygen;
        SecretKey key=null;
        try {

            byte[] raw = keys.getBytes("utf-8");
            key = new SecretKeySpec(raw, Constants.AES_ENCODE_FORMAT);
        } catch (Exception e) {
           // logger.error("AesEncryptUtils aesDecode error,encodeRules={},content={}",encodeRules,e);
        }
        return key;
    }
    private static String encryptPassword(String pwd){
        System.out.println(pwd.length());
        if(pwd.length()<=24){
            String UString="";
            for (int i=0;i<24-pwd.length();i++){
                UString=UString+"U";
            }
            pwd=pwd+UString;
        }else {
            pwd=pwd.substring(0,24) ;
        }
        return pwd;
    }

    private static String encryptIvStr(String password ){
          String  shortPass=password.substring(0,16);


       String reverseChar= new StringBuilder(shortPass).reverse().toString();

        return reverseChar;
    }
    public static void main(String[] args) {
        String pwd="xinyue1234";
        String tchpwd=encryptPassword(pwd);
        String randomGet="Us7wsX3R";
        String reverseChar=encryptIvStr(tchpwd);
        System.out.println("颠倒字符串:"+reverseChar);
        String miwen=randomGet+randomGet+pwd;
        System.out.println(miwen);
        String secret=aesEncode("xinyue1234UUUUUUUUUUUUUU","suDzHhB1XE87TFPGxinyue1234");
        System.out.println(secret);
    }

}
