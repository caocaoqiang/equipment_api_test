package com.shebei.api.equipment_api_test.utils;

import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

@Service
public class GetConfig {
    public static String getAddress(String url) {

        Properties prop = new Properties();


        try {
            InputStream is = GetConfig.class.getResourceAsStream("/urlconfig.properties");
            prop.load(new InputStreamReader(is,"UTF-8"));
            if (is != null) {

                is.close();
            }
        } catch (Exception e) {
        }
        return prop.getProperty(url);
    }

}