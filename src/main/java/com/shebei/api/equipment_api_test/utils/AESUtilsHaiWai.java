package com.shebei.api.equipment_api_test.utils;



import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.Security;
import java.util.Base64;


public class AESUtilsHaiWai {


    static {
        Security.addProvider(new BouncyCastleProvider());
    }
    private static String keySupply = "UUUUUUUUUUUUUUUUUUUUUUUU";

    /**
     * @param data 明文
     * @param key  密钥，长度16
     * @return 密文
     * @author miracle.qu
     * @Description AES算法加密明文
     */
    public static String encryptAES(String data, String key) {
        key = secretKey(key);
        String iv = reverse(key.substring(0, 16));
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            int blockSize = cipher.getBlockSize();
            byte[] dataBytes = data.getBytes();
            int plaintextLength = dataBytes.length;

            if (plaintextLength % blockSize != 0) {
                plaintextLength = plaintextLength + (blockSize - (plaintextLength % blockSize));
            }

            byte[] plaintext = new byte[plaintextLength];
            System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);

            SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
            // CBC模式，需要一个向量iv，可增加加密算法的强度
            IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());

            cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
            byte[] encrypted = cipher.doFinal(plaintext);
            // BASE64做转码。
            return AESUtilsHaiWai.encode(encrypted).trim();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param data 密文
     * @param key  密钥，长度16
     * @return 明文
     * @author miracle.qu
     * @Description AES算法解密密文
     */
    public static String decryptAES(String data, String key) {
        key = secretKey(key);
        String iv = reverse(key.substring(0, 16));
        try {
            byte[] encrypted1 = AESUtilsHaiWai.decode(data);//先用base64解密

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
            IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());

            cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);

            byte[] original = cipher.doFinal(encrypted1);
            String originalString = new String(original);
            return originalString.trim();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String secretKey(String key) {
        if (key.length() < 24) {
            return (key + keySupply).substring(0, 24);
        } else if (key.length() > 24) {
            return key.substring(0, 24);
        }
        return key;
    }

    private static String reverse(String str) {
        return new StringBuffer(str).reverse().toString();
    }

    /**
     * 编码
     *
     * @param byteArray
     * @return
     */
    private static String encode(byte[] byteArray) {
        return new String(Base64.getEncoder().encode(byteArray));
    }

    /**
     * 解码
     *
     * @param base64EncodedString
     * @return
     */
    private static byte[] decode(String base64EncodedString) {
        return Base64.getDecoder().decode(base64EncodedString);
    }

    public static void main(String[] args) {
        String dec = AESUtilsHaiWai.encryptAES("DRfM3prynXffTfS5xinyue1234", "xinyue1234");
      //  String dec = AESUtilsHaiWai.decryptAES("mgXe58t8FlTdZ8rlL0imVW/vJKRLceSK2yP4elCrgl37CTqWIY0VEuPYkN/ZvD98", "123456789qwertyasdfghjkl");
        System.out.println(dec);
    }
}