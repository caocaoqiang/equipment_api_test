package com.shebei.api.equipment_api_test.vueController;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.dao.EnviromentParameterDAO;
import com.shebei.api.equipment_api_test.model.ApiNameQuery;
import com.shebei.api.equipment_api_test.model.EnviromentParameter;
import com.shebei.api.equipment_api_test.model.responseModel.ResModel;
import com.shebei.api.equipment_api_test.service.vueservice.ParamEnvService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("envParam")
public class ParamEnvController {

    @Autowired
    EnviromentParameterDAO enviromentParameterDAO;
    @Autowired
    ParamEnvService paramEnvService;
    @RequestMapping(value="env/list",method = RequestMethod.GET)
    public ResModel getTableList(@RequestParam("parameterName")  String parameterName,
                                 @RequestParam("scene")  String scene,
                                 @RequestParam("env")  String env ,
                                 @RequestParam("module")  String module ){
        ResModel resModel= new ResModel();
        EnviromentParameter record=new EnviromentParameter();
        record.setParameterName(parameterName);
        record.setScene(scene);
        record.setEnv(env);
        record.setModule(module);
        deleteSpace(record);
        List<EnviromentParameter>   list= paramEnvService.getList(record);
        resModel.setCode(20000);
        JSONArray response= (JSONArray) JSONArray.toJSON(list);
        resModel.setData(response);
        return resModel;

    }
    @RequestMapping(value="env/edit",method = RequestMethod.POST)
    public JSONObject envEdit(HttpServletRequest request, @RequestBody EnviromentParameter enviromentParameter){

        deleteSpace(enviromentParameter);
        ResModel resModel= new ResModel();
        EnviromentParameter isExist=enviromentParameterDAO.selectByName(enviromentParameter);

        if(isExist==null){
            int updateRes=    enviromentParameterDAO.updateByPrimaryKeySelective(enviromentParameter);
            resModel.setData("true");

        }else {
             int sId=isExist.getId();
             int oId=enviromentParameter.getId();
             if(sId == oId){
                 int updateRes=    enviromentParameterDAO.updateByPrimaryKeySelective(enviromentParameter);
                 resModel.setData("true");
             }else {

                 String res="false";
                 resModel.setData(res);
             }


        }




        resModel.setCode(20000);


        String response = JSON.toJSONString(resModel);
        JSONObject responses = (JSONObject) JSONObject.parseObject(response);

        return responses;

    }

    @RequestMapping(value="env/del",method = RequestMethod.POST)
    public ResModel envDel(@RequestParam("id")  Integer id){
        ResModel resModel= new ResModel();



        Integer res=enviromentParameterDAO.deleteByPrimaryKey( id);
        resModel.setCode(20000);
        resModel.setData(res);
        return resModel;

    }

    @RequestMapping(value="env/query",method = RequestMethod.POST)
    public ResModel envQuery(@RequestParam("id")  Integer id){
        ResModel resModel= new ResModel();



        String res=enviromentParameterDAO.selectParamValue( id);
        resModel.setCode(20000);
        resModel.setData(res);
        return resModel;

    }

    @RequestMapping(value="env/copy",method = RequestMethod.POST)
    public JSONObject envCopy(HttpServletRequest request, @RequestBody EnviromentParameter enviromentParameter){


        ResModel resModel= new ResModel();
        deleteSpace(enviromentParameter);
        enviromentParameter.setId(null);

        EnviromentParameter isExist=enviromentParameterDAO.selectByName(enviromentParameter);
        if(isExist==null){
            int updateRes=    enviromentParameterDAO.insert(enviromentParameter);
            resModel.setData("true");
        }else {
            String res="false";
            resModel.setData(res);
        }




        resModel.setCode(20000);


        String response = JSON.toJSONString(resModel);
        JSONObject responses = (JSONObject) JSONObject.parseObject(response);

        return responses;

    }


    @RequestMapping(value="env/scene",method = RequestMethod.GET)
    public ResModel getEnvSecne(@RequestParam("module")  String module ){
        ResModel resModel= new ResModel();
        EnviromentParameter record=new EnviromentParameter();

        record.setModule(module);
        deleteSpace(record);
        List<String>   list= enviromentParameterDAO.selectScene(record);

        resModel.setCode(20000);
        JSONArray response= (JSONArray) JSONArray.toJSON(list);
        resModel.setData(response);
        return resModel;

    }



    @RequestMapping(value="env/excute/List",method = RequestMethod.GET)
    public ResModel getEnvList(@RequestParam("module")  String module ){
        ResModel resModel= new ResModel();
        EnviromentParameter record=new EnviromentParameter();

        record.setModule(module);
        deleteSpace(record);
        List<String>   list= enviromentParameterDAO.selectEnvList(record);

        resModel.setCode(20000);
        JSONArray response= (JSONArray) JSONArray.toJSON(list);
        resModel.setData(response);
        return resModel;

    }
    private void deleteSpace(EnviromentParameter var) {
        if (var.getParameterName() != null) {
            var.setParameterName(var.getParameterName().trim());
        }
        if (var.getModule() != null) {
            var.setModule(var.getModule().trim());
        }
        if (var.getParameterValue() != null) {
            var.setParameterValue(var.getParameterValue().trim());
        }
        if (var.getScene() != null) {
            var.setScene(var.getScene().trim());
        }
        if (var.getEnv() != null) {
            var.setEnv(var.getEnv().trim());
        }
    }
}
