package com.shebei.api.equipment_api_test.vueController;


import com.shebei.api.equipment_api_test.dao.NavigateNameApiDAO;
import com.shebei.api.equipment_api_test.dao.NavigateNameDAO;
import com.shebei.api.equipment_api_test.model.NavigateName;
import com.shebei.api.equipment_api_test.model.NavigateNameApi;
import com.shebei.api.equipment_api_test.model.responseModel.NavigateNameNode;
import com.shebei.api.equipment_api_test.model.responseModel.ResModel;
import com.shebei.api.equipment_api_test.service.vueservice.NavigateNameApiService;
import com.shebei.api.equipment_api_test.service.vueservice.NavigateNameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("navigateApi")
public class NavigateNameApiController {

    @Autowired
    NavigateNameApiDAO navigateNameDAO;
    @Autowired
    NavigateNameApiService navigateNameService;
    @RequestMapping(value="group/get",method = RequestMethod.POST)
    public ResModel getGroups(@RequestParam("module") String module ,@RequestParam("scene") String scene){


        ResModel resModel= new ResModel();
        resModel.setCode(20000);
        List<NavigateNameApi> list=  navigateNameDAO.selectGroups(module,scene);
        resModel.setData(list);
        return resModel;

    }

    @RequestMapping(value="group/add",method = RequestMethod.POST)
    public ResModel addGroup(@RequestParam("module") String module ,
                             @RequestParam("scene") String scene,
                             @RequestParam("groupName") String groupName){


        ResModel resModel= new ResModel();
        resModel.setCode(20000);
        int res=  navigateNameService.addGroup(module,scene,groupName);
        resModel.setData(res);
        return resModel;

    }



    @RequestMapping(value="group/delete",method = RequestMethod.POST)
    public ResModel deleteGroup(@RequestParam("uniqueKey") String uniqueKey
                             ){


        ResModel resModel= new ResModel();
        resModel.setCode(20000);
        String res=  navigateNameService.deleteGroup(uniqueKey);
        resModel.setData(res);
        return resModel;

    }
    @RequestMapping(value="group/edit",method = RequestMethod.POST)
    public ResModel editGroup(@RequestParam("groupName") String groupName,
                              @RequestParam("uniqueKey") String uniqueKey
    ){


        ResModel resModel= new ResModel();
        resModel.setCode(20000);
        int res=  navigateNameService.editGroupName(groupName,uniqueKey);
        resModel.setData(res);
        return resModel;

    }

    @RequestMapping(value="nodes/get",method = RequestMethod.POST)
    public ResModel getNodes(@RequestParam("module") String module ,@RequestParam("scene") String scene){


        ResModel resModel= new ResModel();
        resModel.setCode(20000);
        String list=  navigateNameService.getNote(module,scene);
        resModel.setData(list);
        return resModel;

    }
    @RequestMapping(value="apinodes/get",method = RequestMethod.POST)
    public ResModel getApiNodes(@RequestParam("module") String module ,@RequestParam("scene") String scene){


        ResModel resModel= new ResModel();
        resModel.setCode(20000);
        List<NavigateNameNode>  list=  navigateNameService.getApiNote(module,scene);
        resModel.setData(list);
        return resModel;

    }
    @RequestMapping(value="node/add",method = RequestMethod.POST)
    public ResModel addNodeGroup(@RequestParam("module") String module ,
                             @RequestParam("scene") String scene,  @RequestParam("parentId") Integer parentId,
                             @RequestParam("groupName") String groupName){


        ResModel resModel= new ResModel();
        resModel.setCode(20000);
        int res=  navigateNameService.addNode(module,scene,parentId,groupName);
        resModel.setData(res);
        return resModel;

    }

    @RequestMapping(value="node/father/add",method = RequestMethod.POST)
    public ResModel addNodeFather(@RequestParam("module") String module ,
                             @RequestParam("scene") String scene,
                             @RequestParam("groupName") String groupName){


        ResModel resModel= new ResModel();
        resModel.setCode(20000);
        int res=  navigateNameService.addFatherGroup(module,scene,groupName);
        resModel.setData(res);
        return resModel;

    }

}
