package com.shebei.api.equipment_api_test.vueController;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.dao.CaseExcuteResultDAO;
import com.shebei.api.equipment_api_test.dao.UserCaseWoDAO;
import com.shebei.api.equipment_api_test.model.CaseExcuteResult;
import com.shebei.api.equipment_api_test.model.CaseExcuteResultWithBLOBs;
import com.shebei.api.equipment_api_test.model.UserCaseWoWithBLOBs;
import com.shebei.api.equipment_api_test.model.responseModel.ResModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("result")
public class ResultCasesController {

    @Autowired
    CaseExcuteResultDAO caseExcuteResultDAO;
    @Autowired
    UserCaseWoDAO userCaseWoDAO;


    @RequestMapping(value="report/list",method = RequestMethod.GET)
    public ResModel getTableList(@RequestParam("caseName")  String caseName,
                                 @RequestParam("apiName")  String apiName,
                                 @RequestParam("apiAddress")  String apiAddress,
                                 @RequestParam("scene")  String scene,
                                 @RequestParam("module")  String module ,
                                 @RequestParam("caseGroup")  String caseGroup ,
                                 @RequestParam("env")  String env){
        ResModel resModel= new ResModel();
        CaseExcuteResult record=new CaseExcuteResult();
        record.setCaseName(caseName);
        record.setEnv(env);
        record.setApiName(apiName);
        record.setApiVaule(apiAddress);
        record.setScene(scene);
        record.setCaseModule(module);
        record.setCaseGroup(caseGroup);
        deleteSpace(record);
        List<CaseExcuteResultWithBLOBs>   reportList=caseExcuteResultDAO.selectReportByScene(record);

        resModel.setCode(20000);
        JSONArray response= (JSONArray) JSONArray.toJSON(reportList);
        resModel.setData(response);
        return resModel;

    }

    @RequestMapping(value="report/count",method = RequestMethod.GET)
    public ResModel getCount(@RequestParam("scene")  String scene,
                             @RequestParam("env")  String env,
                             @RequestParam("module")  String module ){
        ResModel resModel= new ResModel();
        CaseExcuteResult record=new CaseExcuteResult();
        record.setScene(scene);
        record.setCaseModule(module);
        record.setEnv(env);
        deleteSpace(record);
        UserCaseWoWithBLOBs userCaseWo=new UserCaseWoWithBLOBs();
        userCaseWo.setScene(scene);
        userCaseWo.setModule(module);

        int total=userCaseWoDAO.selectCountByScene(userCaseWo);


        record.setExcuteResult("true");
        int   success=caseExcuteResultDAO.selectResultCountByScene(record);
        record.setExcuteResult("false");
        int   failed=caseExcuteResultDAO.selectResultCountByScene(record);
        record.setExcuteResult("excuting");
        int   excuting=caseExcuteResultDAO.selectResultCountByScene(record);
        JSONObject reportCount=new JSONObject();

        reportCount.put("total",total);
        reportCount.put("success",success);
        reportCount.put("failed",failed);
        reportCount.put("excuting",excuting);
        resModel.setCode(20000);

        resModel.setData(reportCount);
        return resModel;

    }


    public void deleteSpace(CaseExcuteResult var) {
        if (var.getApiName() != null) {
            var.setApiName(var.getApiName().trim());
        }
        if (var.getCaseModule()!= null) {
            var.setCaseModule(var.getCaseModule().trim());
        }
        if (var.getCaseName() != null) {
            var.setCaseName(var.getCaseName().trim());
        }
        if (var.getScene() != null) {
            var.setScene(var.getScene().trim());
        }
        if (var.getEnv() != null) {
            var.setEnv(var.getEnv().trim());
        }

        if (var.getApiVaule() != null) {
            var.setApiVaule(var.getApiVaule().trim());
        }


    }

}
