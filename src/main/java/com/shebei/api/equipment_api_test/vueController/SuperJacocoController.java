package com.shebei.api.equipment_api_test.vueController;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.dao.ServerJacocoManagerDAO;
import com.shebei.api.equipment_api_test.dao.SuperJacocoDAO;
import com.shebei.api.equipment_api_test.model.ApiNameQuery;
import com.shebei.api.equipment_api_test.model.SuperJacoco;
import com.shebei.api.equipment_api_test.model.responseModel.ResModel;
import com.shebei.api.equipment_api_test.utils.GetLinuxTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("superJacoco")
public class SuperJacocoController {

    @Autowired
    SuperJacocoDAO superJacocoDAO;
    @Autowired
    GetLinuxTime getLinuxTime;
    @Autowired
    ServerJacocoManagerDAO serverJacocoManagerDAO;
    @RequestMapping(value="server/list",method = RequestMethod.GET)
    public ResModel getTableList(@RequestParam("moduleName")  String moduleName){
        ResModel resModel= new ResModel();
        SuperJacoco superJacoco=new SuperJacoco();
        List<SuperJacoco> list=superJacocoDAO.selectByModuleName(superJacoco);
        resModel.setCode(20000);
        JSONArray response= (JSONArray) JSONArray.toJSON(list);
        resModel.setData(response);
        return resModel;

    }



    @RequestMapping(value="server/add",method = RequestMethod.POST)
    public JSONObject serverJacocoAdd(HttpServletRequest request, @RequestBody SuperJacoco superJacoco){

        deleteSpace(superJacoco);
        String time=getLinuxTime.getData();

        superJacoco.setCreateTime(time);
        superJacoco.setId(null);
        int insertRes= superJacocoDAO.insert(superJacoco);
        ResModel resModel= new ResModel();


        resModel.setCode(20000);
        resModel.setData(insertRes);

        String response = JSON.toJSONString(resModel);
        JSONObject responses = (JSONObject) JSONObject.parseObject(response);

        return responses;

    }


    @RequestMapping(value="server/edit",method = RequestMethod.POST)
    public JSONObject serverJacocoEdit(HttpServletRequest request, @RequestBody SuperJacoco superJacoco){

        deleteSpace(superJacoco);

        int insertRes= superJacocoDAO.updateByPrimaryKey(superJacoco);
        ResModel resModel= new ResModel();


        resModel.setCode(20000);
        resModel.setData(insertRes);

        String response = JSON.toJSONString(resModel);
        JSONObject responses = (JSONObject) JSONObject.parseObject(response);

        return responses;

    }
    @RequestMapping(value="server/delete",method = RequestMethod.POST)
    public JSONObject serverJacocoDelete(@RequestParam("id")  Integer id){
        ResModel resModel= new ResModel();
       int num= serverJacocoManagerDAO.selectJacocoSeverId(id); //判断jacoco_id是否被serverJacocoManager引用
        if(num>0){
            resModel.setData("failed");
        }else {
            int insertRes= superJacocoDAO.deleteByPrimaryKey(id);
            resModel.setData("success");
        }





        resModel.setCode(20000);


        String response = JSON.toJSONString(resModel);
        JSONObject responses = (JSONObject) JSONObject.parseObject(response);

        return responses;

    }

    private void deleteSpace(SuperJacoco var) {
        if (var.getJacocoServer() != null) {
            var.setJacocoServer(var.getJacocoServer().trim());
        }
        if (var.getModuleName() != null) {
            var.setModuleName(var.getModuleName().trim());
        }

    }
}
