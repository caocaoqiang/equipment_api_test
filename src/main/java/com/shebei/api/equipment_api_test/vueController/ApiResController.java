package com.shebei.api.equipment_api_test.vueController;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.dao.ApiNameQueryDAO;
import com.shebei.api.equipment_api_test.model.ApiNameQuery;
import com.shebei.api.equipment_api_test.model.responseModel.ResModel;
import com.shebei.api.equipment_api_test.service.vueservice.ApiResService;
import com.shebei.api.equipment_api_test.utils.GetLinuxTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("apiRes")
public class ApiResController {


    @Autowired
    ApiNameQueryDAO apiNameQueryDAO;

    @Autowired
    GetLinuxTime getLinuxTime;
    @Autowired
    ApiResService apiResService;
    @RequestMapping(value="api/list",method = RequestMethod.GET)
    public ResModel getTableList(@RequestParam("apiName")  String apiName,
                                 @RequestParam("apiAddress")  String apiAddress,
                                 @RequestParam("module")  String module,
                                 @RequestParam("apiGroup")  String apiGroup,
                                 @RequestParam("navigateVersion")  String navigateVersion){
        ResModel resModel= new ResModel();
        ApiNameQuery record=new ApiNameQuery();
        record.setApiName(apiName);
        record.setApiValue(apiAddress);
        record.setApiCategory(module);
        record.setApiGroup(apiGroup);
        record.setApiNavigateValue(navigateVersion);
        deleteSpace(record);
        List<ApiNameQuery> list=apiNameQueryDAO.selectApi(record);
        resModel.setCode(20000);
        JSONArray response= (JSONArray) JSONArray.toJSON(list);
        resModel.setData(response);
        return resModel;

    }

    @RequestMapping(value="api/del",method = RequestMethod.POST)
    public ResModel apiDel(@RequestParam("id")  Integer id){
        ResModel resModel= new ResModel();



        Integer res=apiNameQueryDAO.deleteByPrimaryKey( id);
        resModel.setCode(20000);
        resModel.setData(res);
        return resModel;

    }


    @RequestMapping(value="api/group",method = RequestMethod.POST)
    public ResModel apiGroup(@RequestParam("module")  String module){
        ResModel resModel= new ResModel();



        List<String> res=apiNameQueryDAO.selectApiGroup( module);
        resModel.setCode(20000);
        resModel.setData(res);
        return resModel;

    }
    @RequestMapping(value="api/edit",method = RequestMethod.POST)
    public JSONObject apiEdit(HttpServletRequest request, @RequestBody ApiNameQuery apiNameQuery){

        deleteSpace(apiNameQuery);
        int updateRes=apiNameQueryDAO.updateByPrimaryKeySelective(apiNameQuery);
        apiResService.updateCaseApiGroup(apiNameQuery);
        ResModel resModel= new ResModel();


        resModel.setCode(20000);
        resModel.setData(updateRes);

        String response = JSON.toJSONString(resModel);
        JSONObject responses = (JSONObject) JSONObject.parseObject(response);

        return responses;

    }

    @RequestMapping(value="api/copy",method = RequestMethod.POST)
    public JSONObject userCaseCopy(HttpServletRequest request, @RequestBody  ApiNameQuery apiNameQuery){

        deleteSpace(apiNameQuery);
        String time=getLinuxTime.getTime();
        String apiName=apiNameQuery.getApiName()+time;
        apiNameQuery.setApiName(apiName);
        apiNameQuery.setId(null);
       int insertRes= apiNameQueryDAO.insert(apiNameQuery);
        ResModel resModel= new ResModel();


        resModel.setCode(20000);
        resModel.setData(insertRes);

        String response = JSON.toJSONString(resModel);
        JSONObject responses = (JSONObject) JSONObject.parseObject(response);

        return responses;

    }
    private void deleteSpace(ApiNameQuery var) {
        if (var.getApiName() != null) {
            var.setApiName(var.getApiName().trim());
        }
        if (var.getApiCategory() != null) {
            var.setApiCategory(var.getApiCategory().trim());
        }
        if (var.getApiType() != null) {
            var.setApiType(var.getApiType().trim());
        }
        if (var.getApiValue() != null) {
            var.setApiValue(var.getApiValue().trim());
        }
        if (var.getApiDescribe() != null) {
            var.setApiDescribe(var.getApiDescribe().trim());
        }

        if (var.getHeadInfo() != null) {
            var.setHeadInfo(var.getHeadInfo().trim());
        }
        if (var.getRequestType() != null) {
            var.setRequestType(var.getRequestType().trim());
        }
        if (var.getApiGroup() != null) {
            var.setApiGroup(var.getApiGroup().trim());
        }
        if (var.getApiNavigateValue() != null) {
            var.setApiNavigateValue(var.getApiNavigateValue().trim());
        }
    }

}
