package com.shebei.api.equipment_api_test.vueController;


import com.alibaba.fastjson.JSONArray;
import com.shebei.api.equipment_api_test.dao.CallbackPathDAO;
import com.shebei.api.equipment_api_test.model.CallbackPath;
import com.shebei.api.equipment_api_test.model.CallbackReceipt;
import com.shebei.api.equipment_api_test.model.responseModel.ResModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("callBackPath")
public class CallbackPathController {

    @Autowired
    CallbackPathDAO callbackPathDAO;

    @RequestMapping(value="callback/list",method = RequestMethod.GET)
    public ResModel getCallBackPath(@RequestParam("callbackScene")  String callbackScene){
        ResModel resModel= new ResModel();
        CallbackPath callbackPath=new CallbackPath();
        callbackPath.setCallbackScene(callbackScene);
        List<CallbackPath> callbackPaths= callbackPathDAO.selectList(callbackPath);

        resModel.setCode(20000);
        JSONArray response= (JSONArray) JSONArray.toJSON(callbackPaths);
        resModel.setData(response);
        return resModel;

    }

    @RequestMapping(value="callback/add",method = RequestMethod.POST)
    public ResModel addpath(@RequestParam("callbackScene")  String callbackScene,
                            @RequestParam("path")  String path){
        ResModel resModel= new ResModel();
        CallbackPath callbackPath=new CallbackPath();
        callbackPath.setCallbackScene(callbackScene);
        callbackPath.setPath(path);
        int res=callbackPathDAO.insert(callbackPath);

        resModel.setCode(20000);

        resModel.setData(res);
        return resModel;

    }

    @RequestMapping(value="callback/update",method = RequestMethod.POST)
    public ResModel updatepath(
            @RequestParam("id")  Integer id,
            @RequestParam("callbackScene")  String callbackScene,
                            @RequestParam("path")  String path){
        ResModel resModel= new ResModel();
        CallbackPath callbackPath=new CallbackPath();
        callbackPath.setId(id);
        callbackPath.setCallbackScene(callbackScene);
        callbackPath.setPath(path);
        int res=callbackPathDAO.updateByPrimaryKey(callbackPath);

        resModel.setCode(20000);

        resModel.setData(res);
        return resModel;

    }

    @RequestMapping(value="callback/delete",method = RequestMethod.POST)
    public ResModel updateDelete(
            @RequestParam("id")  Integer id){
        ResModel resModel= new ResModel();
        int res=callbackPathDAO.deleteByPrimaryKey(id);

        resModel.setCode(20000);

        resModel.setData(res);
        return resModel;

    }
}
