package com.shebei.api.equipment_api_test.vueController;


import com.alibaba.fastjson.JSONArray;
import com.shebei.api.equipment_api_test.dao.CallbackReceiptDAO;
import com.shebei.api.equipment_api_test.model.CallbackReceipt;
import com.shebei.api.equipment_api_test.model.CaseExcuteResult;
import com.shebei.api.equipment_api_test.model.CaseExcuteResultWithBLOBs;
import com.shebei.api.equipment_api_test.model.responseModel.ResModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("receipt")
public class CallbackReceiptController {


    @Autowired
    CallbackReceiptDAO callbackReceiptDAO;

    @RequestMapping(value="callback/list",method = RequestMethod.GET)
    public ResModel getCallBackList(@RequestParam("callbackScene")  String callbackScene){
        ResModel resModel= new ResModel();
        CallbackReceipt callbackReceipt=new CallbackReceipt();
        callbackReceipt.setCallbackScene(callbackScene);
        List<CallbackReceipt> callbackReceipts= callbackReceiptDAO.selectByScene(callbackReceipt);

        resModel.setCode(20000);
        JSONArray response= (JSONArray) JSONArray.toJSON(callbackReceipts);
        resModel.setData(response);
        return resModel;

    }


    @RequestMapping(value="callback/clear",method = RequestMethod.POST)
    public ResModel clearData(@RequestParam("callbackScene")  String callbackScene){
        ResModel resModel= new ResModel();
        CallbackReceipt callbackReceipt=new CallbackReceipt();
        callbackReceipt.setCallbackScene(callbackScene);
         int  res=callbackReceiptDAO.deleteByScene(callbackReceipt);

        resModel.setCode(20000);

        resModel.setData(res);
        return resModel;

    }
}
