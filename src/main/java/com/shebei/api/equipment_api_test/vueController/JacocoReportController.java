package com.shebei.api.equipment_api_test.vueController;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.dao.JacocoReportDAO;
import com.shebei.api.equipment_api_test.model.JacocoReport;
import com.shebei.api.equipment_api_test.model.ServerJacocoManager;
import com.shebei.api.equipment_api_test.model.responseModel.ResModel;
import com.shebei.api.equipment_api_test.service.jacoco.JacocoManagerServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("jaocoReport")
public class JacocoReportController {

   @Autowired
    JacocoReportDAO jacocoReportDAO;
    @Autowired
    JacocoManagerServer jacocoManagerServer;

    @RequestMapping(value="api/list",method = RequestMethod.GET)
    public ResModel getList(@RequestParam("moduleName")  String moduleName,
                            @RequestParam("serverName")  String serverName,
                            @RequestParam("isUnitTest")  Integer isUnitTest){
        ResModel resModel= new ResModel();
        JacocoReport record=new JacocoReport();
        record.setModuleName(moduleName.trim());
        record.setServerName(serverName.trim());
        record.setUnitTestType(isUnitTest);
        List<JacocoReport> list=jacocoReportDAO.selectList(record);
        resModel.setCode(20000);
        JSONArray response= (JSONArray) JSONArray.toJSON(list);
        resModel.setData(response);
        return resModel;

    }

    @RequestMapping(value="api/update",method = RequestMethod.POST)
    public JSONObject update(@RequestParam("uuid")  String recordUuid){


        Integer res=jacocoManagerServer.updateReport(recordUuid);

        ResModel resModel= new ResModel();


        resModel.setCode(20000);
        resModel.setData(res);

        String response = JSON.toJSONString(resModel);
        JSONObject responses = (JSONObject) JSONObject.parseObject(response);

        return responses;

    }
    @RequestMapping(value="api/getUnit/report",method = RequestMethod.POST)
    public JSONObject unitUpdate(@RequestParam("uuid")  String recordUuid){


        Integer res=jacocoManagerServer.updateUnitReport(recordUuid);

        ResModel resModel= new ResModel();


        resModel.setCode(20000);
        resModel.setData(res);

        String response = JSON.toJSONString(resModel);
        JSONObject responses = (JSONObject) JSONObject.parseObject(response);

        return responses;

    }
    @RequestMapping(value="api/delete",method = RequestMethod.POST)
    public JSONObject reportDel(@RequestParam("recordUuid")  String recordUuid){


        Integer res=jacocoReportDAO.deleteByPrimaryKey(recordUuid);

        ResModel resModel= new ResModel();


        resModel.setCode(20000);
        resModel.setData(res);

        String response = JSON.toJSONString(resModel);
        JSONObject responses = (JSONObject) JSONObject.parseObject(response);

        return responses;

    }



}
