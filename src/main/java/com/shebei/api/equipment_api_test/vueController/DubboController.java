//package com.shebei.api.equipment_api_test.vueController;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONObject;
//import com.shebei.api.equipment_api_test.dao.ApiNameQueryDAO;
//import com.shebei.api.equipment_api_test.dao.UserCaseWoDAO;
//import com.shebei.api.equipment_api_test.model.ApiNameQuery;
//import com.shebei.api.equipment_api_test.model.NavigateName;
//import com.shebei.api.equipment_api_test.model.UserCaseGroup;
//import com.shebei.api.equipment_api_test.model.UserCaseWoWithBLOBs;
//import com.shebei.api.equipment_api_test.model.responseModel.ApiResModel;
//import com.shebei.api.equipment_api_test.model.responseModel.ConcurrentModel;
//import com.shebei.api.equipment_api_test.model.responseModel.ResModel;
//import com.shebei.api.equipment_api_test.model.responseModel.TokenModel;
//import com.shebei.api.equipment_api_test.service.UserCaseExcute;
//import com.shebei.api.equipment_api_test.service.dubbo.DubboUserCaseExcute;
//import com.shebei.api.equipment_api_test.service.vueservice.PerformanceService;
//import com.shebei.api.equipment_api_test.service.vueservice.UserCaseService;
//import com.shebei.api.equipment_api_test.service.vueservice.VueCaseExcute;
//import com.shebei.api.equipment_api_test.thread.CaseExcuteRunnable;
//import com.shebei.api.equipment_api_test.thread.DubboCaseExcuteRunnable;
//import com.shebei.api.equipment_api_test.utils.GetLinuxTime;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.List;
//
//
//@RestController
//@RequestMapping("dubbo")
//public class DubboController {
//
//    @Autowired
//    UserCaseWoDAO userCaseWoDAO;
//
//
//    @Autowired
//    GetLinuxTime getLinuxTime;
//
//    @Autowired
//    ApiNameQueryDAO apiNameQueryDAO;
//
//    @Autowired
//    UserCaseExcute userCaseExcute;
//    @Autowired
//    VueCaseExcute vueCaseExcute;
//
//    @Autowired
//    UserCaseService userCaseService;
//
//    @Autowired
//    DubboUserCaseExcute dubboUserCaseExcute;
//
//
//
//    @RequestMapping(value="case/excute",method = RequestMethod.POST)
//    public JSONObject userCaseExcute(@RequestParam("id")  Integer id,@RequestParam("env")  String env){
//
//
//        ApiResModel result=  dubboUserCaseExcute.vueExcuteOneCase(id,env);
//        ResModel resModel= new ResModel();
//
//
//        resModel.setCode(20000);
//        resModel.setData(result);
//
//        String response = JSON.toJSONString(resModel);
//        JSONObject responses = (JSONObject) JSONObject.parseObject(response);
//
//        return responses;
//
//    }
//
//    @RequestMapping(value="case/edit",method = RequestMethod.POST)
//    public JSONObject userCaseEdit(HttpServletRequest request, @RequestBody UserCaseWoWithBLOBs userCaseWo){
//
//        deleteSpace(userCaseWo);
//
//        int updateRes= userCaseWoDAO.updateByPrimaryKeyWithBLOBs(userCaseWo);
//        ResModel resModel= new ResModel();
//
//
//        resModel.setCode(20000);
//        resModel.setData(updateRes);
//
//        String response = JSON.toJSONString(resModel);
//        JSONObject responses = (JSONObject) JSONObject.parseObject(response);
//
//        return responses;
//
//    }
//
//    @RequestMapping(value="case/copy",method = RequestMethod.POST)
//    public JSONObject userCaseCopy(HttpServletRequest request, @RequestBody UserCaseWoWithBLOBs userCaseWo){
//
//        deleteSpace(userCaseWo);
//        String time= getLinuxTime.getTime();
//        String userCaseName=userCaseWo.getUserCaseName()+"_"+time;
//        userCaseWo.setUserCaseName(userCaseName);
//        userCaseWo.setId(null);
//        int insertRes= userCaseWoDAO.insert(userCaseWo);
//        ResModel resModel= new ResModel();
//
//
//        resModel.setCode(20000);
//        resModel.setData(insertRes);
//
//        String response = JSON.toJSONString(resModel);
//        JSONObject responses = (JSONObject) JSONObject.parseObject(response);
//
//        return responses;
//
//    }
//
//
//
//    @RequestMapping(value="scene/casesExcute",method = RequestMethod.POST)
//    public JSONObject casesExcute(@RequestParam("scene")  String scene,
//                                  @RequestParam("env")  String env,
//                                  @RequestParam("module")  String module){
//
//
//        ResModel resModel= new ResModel();
//
//
//
//
//            DubboCaseExcuteRunnable dubboCaseExcuteRunnable= new DubboCaseExcuteRunnable(scene, env, module);
//            Thread thread1=new Thread(dubboCaseExcuteRunnable);
//            thread1.start();
//
//
//        resModel.setCode(20000);
//        if(scene.equals("")){
//            resModel.setData("开始执行模块："+module+"的所有场景，请到测试结果页面查看");
//        }else {
//            resModel.setData("开始执行模块："+module+"；场景："+scene+"，请到测试结果页面查看");
//        }
//
//
//        String response = JSON.toJSONString(resModel);
//        JSONObject responses = (JSONObject) JSONObject.parseObject(response);
//
//        return responses;
//
//    }
//
//    public void deleteSpace(UserCaseWoWithBLOBs var) {
//        if (var.getApiName() != null) {
//            var.setApiName(var.getApiName().trim());
//        }
//        if (var.getModule() != null) {
//            var.setModule(var.getModule().trim());
//        }
//        if (var.getUserCaseName() != null) {
//            var.setUserCaseName(var.getUserCaseName().trim());
//        }
//        if (var.getScene() != null) {
//            var.setScene(var.getScene().trim());
//        }
//        if (var.getCheckPath() != null) {
//            var.setCheckPath(var.getCheckPath().trim());
//        }
//
//        if (var.getParameterSavePath() != null) {
//            var.setParameterSavePath(var.getParameterSavePath().trim());
//        }
//        if (var.getRequestBody() != null) {
//            var.setRequestBody(var.getRequestBody().trim());
//        }
//        if (var.getApiValue() != null) {
//            var.setApiValue(var.getApiValue().trim());
//        }
//        if (var.getRequestType() != null) {
//            var.setRequestType(var.getRequestType().trim());
//        }
//        if (var.getAdaptiveVersion() != null) {
//            var.setAdaptiveVersion(var.getAdaptiveVersion().trim());
//        }
//        if (var.getApiGroup() != null) {
//            var.setApiGroup(var.getApiGroup().trim());
//        }
//
//        if (var.getRequestType() != null) {
//            var.setRequestType(var.getRequestType().trim());
//        }
//
//    }
//
//
//}
