package com.shebei.api.equipment_api_test.vueController;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.dao.TestVersionRecordDAO;
import com.shebei.api.equipment_api_test.model.TestVersionRecord;
import com.shebei.api.equipment_api_test.model.responseModel.JacocoConfigs;
import com.shebei.api.equipment_api_test.model.responseModel.ResModel;
import com.shebei.api.equipment_api_test.service.jacoco.JacocoTestVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("versionControl")
public class JacocoTestVersionController {
    @Autowired
    private TestVersionRecordDAO testVersionRecordDao;
    @Autowired
    private JacocoTestVersionService jacocoTestVersionService;

    @RequestMapping(value="record/list",method = RequestMethod.GET)
    public ResModel getList(@RequestParam("moduleName")  String moduleName,
                            @RequestParam("serverName")  String serverName,
                            @RequestParam("vId")  Integer vId ){
        ResModel resModel= new ResModel();
        TestVersionRecord record=new TestVersionRecord();
        record.setModuleName(moduleName.trim());
        if(serverName!=null){
            record.setServerName(serverName.trim());
        }
        record.setvId(vId);

        List<TestVersionRecord> list=testVersionRecordDao.selectRecords(record);
        resModel.setCode(20000);
        JSONArray response= (JSONArray) JSONArray.toJSON(list);
        resModel.setData(response);
        return resModel;

    }


    @RequestMapping(value="version/add",method = RequestMethod.POST)
    public ResModel addVersion(@RequestParam("moduleName") String moduleName ,
                             @RequestParam("testVersion") String testVersion){


        ResModel resModel= new ResModel();
        resModel.setCode(20000);
        String res=  jacocoTestVersionService.versionControlAdd(moduleName,testVersion);
        System.out.println("versionControlAdd success:"+res);
        resModel.setData(res);
        return resModel;

    }

    @RequestMapping(value="version/edit",method = RequestMethod.POST)
    public ResModel editVersion(@RequestParam("id") Integer id ,
                               @RequestParam("testVersion") String testVersion){


        ResModel resModel= new ResModel();
        resModel.setCode(20000);
        int res=  jacocoTestVersionService.versionControlEdit(id,testVersion,0);
        resModel.setData(res);
        return resModel;

    }
    @RequestMapping(value="version/del",method = RequestMethod.POST)
    public ResModel delVersion(@RequestParam("id") Integer id ){


        ResModel resModel= new ResModel();
        resModel.setCode(20000);
        int res=  jacocoTestVersionService.versionControlEdit(id,null,0);
        resModel.setData(res);
        return resModel;

    }

    @RequestMapping(value="version/select",method = RequestMethod.POST)
    public ResModel selectVersion(@RequestParam("moduleName") String moduleName ){


        ResModel resModel= new ResModel();
        resModel.setCode(20000);
        String res=  jacocoTestVersionService.versionControlSelect(moduleName);
        resModel.setData(res);
        return resModel;

    }

    @RequestMapping(value="version/coverage/trigger",method = RequestMethod.POST)
    public ResModel versionJacocoTriger(HttpServletRequest request, @RequestBody JacocoConfigs jacocoConfigs ){

        System.out.println("获得JsocoCongi:"+jacocoConfigs.getBaseVersion()+jacocoConfigs.getVersionId()+jacocoConfigs.getId()+jacocoConfigs.getNowVersion());
        ResModel resModel= new ResModel();
        resModel.setCode(20000);
//        String res=  jacocoTestVersionService.versionControlSelect(moduleName);
//        resModel.setData(res);
        return resModel;

    }

    @RequestMapping(value="api/update",method = RequestMethod.POST)
    public JSONObject update(@RequestParam("uuid")  String recordUuid){


        Integer res=jacocoTestVersionService.updateReport(recordUuid);

        ResModel resModel= new ResModel();


        resModel.setCode(20000);
        resModel.setData(res);

        String response = JSON.toJSONString(resModel);
        JSONObject responses = (JSONObject) JSONObject.parseObject(response);

        return responses;

    }



}
