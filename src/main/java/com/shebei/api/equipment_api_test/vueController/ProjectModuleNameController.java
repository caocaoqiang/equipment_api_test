package com.shebei.api.equipment_api_test.vueController;

import com.shebei.api.equipment_api_test.dao.ProjectModuleNameDAO;
import com.shebei.api.equipment_api_test.dao.UserCaseWoDAO;
import com.shebei.api.equipment_api_test.model.ProjectModuleName;
import com.shebei.api.equipment_api_test.model.UserCaseWo;
import com.shebei.api.equipment_api_test.model.UserCaseWoWithBLOBs;
import com.shebei.api.equipment_api_test.model.responseModel.ResModel;
import com.shebei.api.equipment_api_test.utils.GetLinuxTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("project")
public class ProjectModuleNameController {

  @Autowired
  private GetLinuxTime getLinuxTime;
    @Autowired
  private UserCaseWoDAO userCaseWoDAO;
    @Autowired
  private ProjectModuleNameDAO projectModuleNameDAO;
    @RequestMapping(value="module/add",method = RequestMethod.POST)
    public ResModel addGroup(@RequestParam("moduleName") String moduleName){


        ResModel resModel= new ResModel();
        resModel.setCode(20000);
        ProjectModuleName projectModuleName= new ProjectModuleName();
        projectModuleName.setModuleName(moduleName);
        projectModuleName.setCreateTime(getLinuxTime.getData());
        projectModuleName.setModuleUniqueId(getLinuxTime.getTime());
        int res=  projectModuleNameDAO.insert(projectModuleName);
        resModel.setData(res);
        return resModel;

    }

    @RequestMapping(value="module/list",method = RequestMethod.POST)
    public ResModel getList(@RequestParam("moduleName") String moduleName){


        ResModel resModel= new ResModel();
        resModel.setCode(20000);
        ProjectModuleName projectModuleName= new ProjectModuleName();
        projectModuleName.setModuleName(moduleName);
        List<ProjectModuleName> res=  projectModuleNameDAO.selectLists(projectModuleName);
        resModel.setData(res);
        return resModel;

    }

    @RequestMapping(value="module/delete",method = RequestMethod.POST)
    public ResModel delete(@RequestParam("id") Integer id){

        ProjectModuleName projectModuleName=projectModuleNameDAO.selectByPrimaryKey(id);
        UserCaseWo userCaseWo=new UserCaseWo();
        userCaseWo.setModule(projectModuleName.getModuleUniqueId());
        ResModel resModel= new ResModel();
        resModel.setCode(20000);
         List<UserCaseWoWithBLOBs> list=userCaseWoDAO.selectCases(userCaseWo);
         if(list.size()>0){
             resModel.setData("failed");
         }else {
             Integer res=  projectModuleNameDAO.deleteByPrimaryKey(id);
             resModel.setData(res);
         }

        return resModel;

    }

    @RequestMapping(value="module/update",method = RequestMethod.POST)
    public ResModel update(@RequestParam("id") Integer id,@RequestParam("moduleName") String moduleName){

        ProjectModuleName projectModuleName=new ProjectModuleName();
        projectModuleName.setId(id);
        projectModuleName.setModuleName(moduleName);
        ResModel resModel= new ResModel();
        resModel.setCode(20000);

        Integer res=  projectModuleNameDAO.updateByPrimaryKeySelective(projectModuleName);
        resModel.setData(res);
        return resModel;

    }


}
