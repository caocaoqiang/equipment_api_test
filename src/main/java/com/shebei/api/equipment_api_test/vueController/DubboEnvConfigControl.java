//package com.shebei.api.equipment_api_test.vueController;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import com.shebei.api.equipment_api_test.dao.ApiNameQueryDAO;
//import com.shebei.api.equipment_api_test.dao.DubboEnvConfigDAO;
//import com.shebei.api.equipment_api_test.dao.UserCaseWoDAO;
//import com.shebei.api.equipment_api_test.model.DubboEnvConfig;
//import com.shebei.api.equipment_api_test.model.UserCaseWoWithBLOBs;
//import com.shebei.api.equipment_api_test.model.responseModel.ApiResModel;
//import com.shebei.api.equipment_api_test.model.responseModel.ResModel;
//import com.shebei.api.equipment_api_test.service.UserCaseExcute;
//import com.shebei.api.equipment_api_test.service.dubbo.DubboUserCaseExcute;
//import com.shebei.api.equipment_api_test.service.vueservice.UserCaseService;
//import com.shebei.api.equipment_api_test.service.vueservice.VueCaseExcute;
//import com.shebei.api.equipment_api_test.utils.GetLinuxTime;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.List;
//
//
//@RestController
//@RequestMapping("dubboenv")
//public class DubboEnvConfigControl {
//
//    @Autowired
//    UserCaseWoDAO userCaseWoDAO;
//
//
//    @Autowired
//    GetLinuxTime getLinuxTime;
//
//    @Autowired
//    ApiNameQueryDAO apiNameQueryDAO;
//
//    @Autowired
//    UserCaseExcute userCaseExcute;
//    @Autowired
//    VueCaseExcute vueCaseExcute;
//
//    @Autowired
//    UserCaseService userCaseService;
//
//    @Autowired
//    DubboUserCaseExcute dubboUserCaseExcute;
//
//    @Autowired
//    DubboEnvConfigDAO dubboEnvConfigDAO;
//
//
//
//
//
//    @RequestMapping(value="config/add",method = RequestMethod.POST)
//    public JSONObject userCaseCopy(HttpServletRequest request, @RequestBody DubboEnvConfig dubboEnvConfig){
//
//        deleteSpace(dubboEnvConfig);
//
//        dubboEnvConfig.setId(null);
//        int insertRes= dubboEnvConfigDAO.insertSelective(dubboEnvConfig);
//        ResModel resModel= new ResModel();
//
//
//        resModel.setCode(20000);
//        resModel.setData(insertRes);
//
//        String response = JSON.toJSONString(resModel);
//        JSONObject responses = (JSONObject) JSONObject.parseObject(response);
//
//        return responses;
//
//    }
//
//
//    @RequestMapping(value="config/list",method = RequestMethod.POST)
//    public JSONObject userCaseList(HttpServletRequest request, @RequestBody DubboEnvConfig dubboEnvConfig){
//
//        deleteSpace(dubboEnvConfig);
//
//        List<DubboEnvConfig> insertRes= dubboEnvConfigDAO.selectList(dubboEnvConfig);
//        ResModel resModel= new ResModel();
//
//
//        resModel.setCode(20000);
//        resModel.setData(insertRes);
//
//        String response = JSON.toJSONString(resModel);
//        JSONObject responses = (JSONObject) JSONObject.parseObject(response);
//
//        return responses;
//
//    }
//
//    @RequestMapping(value="config/delete",method = RequestMethod.POST)
//    public JSONObject userCaseDelte(@RequestParam("id")  Integer id){
//
//
//
//      int  insertRes= dubboEnvConfigDAO.deleteByPrimaryKey(id);
//        ResModel resModel= new ResModel();
//
//
//        resModel.setCode(20000);
//        resModel.setData(insertRes);
//
//        String response = JSON.toJSONString(resModel);
//        JSONObject responses = (JSONObject) JSONObject.parseObject(response);
//
//        return responses;
//
//    }
//    @RequestMapping(value="config/edit",method = RequestMethod.POST)
//    public JSONObject userCaseEdit(HttpServletRequest request, @RequestBody DubboEnvConfig dubboEnvConfig){
//
//        deleteSpace(dubboEnvConfig);
//
//        int insertRes= dubboEnvConfigDAO.updateByPrimaryKeySelective(dubboEnvConfig);
//        ResModel resModel= new ResModel();
//
//
//        resModel.setCode(20000);
//        resModel.setData(insertRes);
//
//        String response = JSON.toJSONString(resModel);
//        JSONObject responses = (JSONObject) JSONObject.parseObject(response);
//
//        return responses;
//
//    }
//
//    @RequestMapping(value="sever/list",method = RequestMethod.POST)
//    public JSONObject getServerList(@RequestParam("apiName")  String apiName){ //pathValue apiName 属于哪个平台
//
//
//        DubboEnvConfig record=new DubboEnvConfig();
//        record.setModuleName(apiName);
//
//
//        List<DubboEnvConfig> lists=dubboEnvConfigDAO.selectSeverList(record);
//        ResModel resModel= new ResModel();
//
//
//        resModel.setCode(20000);
//        resModel.setData(lists);
//
//        String response = JSON.toJSONString(resModel);
//        JSONObject responses = (JSONObject) JSONObject.parseObject(response);
//
//        return responses;
//
//    }
//
//    @RequestMapping(value="dubbo/list",method = RequestMethod.POST)
//    public JSONObject getdubboList(@RequestParam("apiName")  String apiName,@RequestParam("apiAddress")  String apiValue){
//        //apiName 接口名字表示 属于哪个平台
//        //apiAddress 所属服务
//        DubboEnvConfig record=new DubboEnvConfig();
//        record.setModuleName(apiName);
//        record.setSeverName(apiValue);
//
//        List<DubboEnvConfig> lists=dubboEnvConfigDAO.selectDubboList(record);
//        ResModel resModel= new ResModel();
//
//
//        resModel.setCode(20000);
//        resModel.setData(lists);
//
//        String response = JSON.toJSONString(resModel);
//        JSONObject responses = (JSONObject) JSONObject.parseObject(response);
//
//        return responses;
//
//    }
//
//    @RequestMapping(value="method/list",method = RequestMethod.POST)
//    public JSONObject getMethodList(@RequestParam("apiName")  String apiName,
//                                    @RequestParam("apiValue")  String apiValue,
//                                    @RequestParam("apiGroup")  String apiGroup){ //apiName 接口名字表示 属于哪个平台
////apiGroup  dubbo接口名字， apiAddress 所属服务
//
//        DubboEnvConfig record=new DubboEnvConfig();
//        record.setModuleName(apiName);
//        record.setSeverName(apiValue);
//        record.setDubboName(apiGroup);
//        List<DubboEnvConfig> lists=dubboEnvConfigDAO.selectMethodList(record);
//        ResModel resModel= new ResModel();
//
//
//        resModel.setCode(20000);
//        resModel.setData(lists);
//
//        String response = JSON.toJSONString(resModel);
//        JSONObject responses = (JSONObject) JSONObject.parseObject(response);
//
//        return responses;
//
//    }
//
//    @RequestMapping(value="module/list",method = RequestMethod.POST)
//    public JSONObject getModuleList( ){ //apiName 接口名字表示 属于哪个平台
////apiGroup  dubbo接口名字， apiAddress 所属服务
//
//
//        List<DubboEnvConfig> lists=dubboEnvConfigDAO.selectModuleList();
//        ResModel resModel= new ResModel();
//
//
//        resModel.setCode(20000);
//        resModel.setData(lists);
//
//        String response = JSON.toJSONString(resModel);
//        JSONObject responses = (JSONObject) JSONObject.parseObject(response);
//
//        return responses;
//
//    }
//    public void deleteSpace(DubboEnvConfig var) {
//        if (var.getDubboName() != null) {
//            var.setDubboName(var.getDubboName().trim());
//        }
//        if (var.getMethodName() != null) {
//            var.setMethodName(var.getMethodName().trim());
//        }
//        if (var.getModuleName() != null) {
//            var.setModuleName(var.getModuleName().trim());
//        }
//        if (var.getSegmentName() != null) {
//            var.setSegmentName(var.getSegmentName().trim());
//        }
//        if (var.getSeverName() != null) {
//            var.setSeverName(var.getSeverName().trim());
//        }
//
//
//    }
//
//
//}
