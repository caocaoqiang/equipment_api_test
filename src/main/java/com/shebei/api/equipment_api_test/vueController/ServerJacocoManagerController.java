package com.shebei.api.equipment_api_test.vueController;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.shebei.api.equipment_api_test.dao.ServerJacocoManagerDAO;
import com.shebei.api.equipment_api_test.dao.SuperJacocoDAO;
import com.shebei.api.equipment_api_test.model.ServerJacocoManager;
import com.shebei.api.equipment_api_test.model.ServerJacocoManagerVersion;
import com.shebei.api.equipment_api_test.model.responseModel.ResModel;
import com.shebei.api.equipment_api_test.service.jacoco.JacocoManagerServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("jacocoManager")
public class ServerJacocoManagerController {


    @Autowired
    ServerJacocoManagerDAO serverJacocoManagerDAO;
    @Autowired
    JacocoManagerServer serverJacocoManagerServer;
    @Autowired
    SuperJacocoDAO superJacocoDAO;

    @RequestMapping(value="api/add",method = RequestMethod.POST)
    public JSONObject add(HttpServletRequest request, @RequestBody ServerJacocoManager serverJacocoManager){


        deleteSpace(serverJacocoManager);
        String moduleName=serverJacocoManager.getModuleName();
       Integer serverId= superJacocoDAO.selectIdByModuleName(moduleName);
        serverJacocoManager.setSuperJacocoId(serverId);
        int insertRes= serverJacocoManagerDAO.insert(serverJacocoManager);
        ResModel resModel= new ResModel();


        resModel.setCode(20000);
        resModel.setData(insertRes);

        String response = JSON.toJSONString(resModel);
        JSONObject responses = (JSONObject) JSONObject.parseObject(response);

        return responses;

    }

    @RequestMapping(value="api/list",method = RequestMethod.GET)
    public ResModel getList(@RequestParam("moduleName")  String moduleName,
                                 @RequestParam("serverName")  String serverName){
        ResModel resModel= new ResModel();
        ServerJacocoManager record=new ServerJacocoManager();
        record.setModuleName(moduleName);
        record.setServerName(serverName);
        deleteSpace(record);
        List<ServerJacocoManager> list=serverJacocoManagerDAO.selectApi(record);
        resModel.setCode(20000);
        JSONArray response= (JSONArray) JSONArray.toJSON(list);
        resModel.setData(response);
        return resModel;

    }

    @RequestMapping(value="module/name",method = RequestMethod.GET)
    public ResModel getModuleName(){
        ResModel resModel= new ResModel();


        List<String> list=superJacocoDAO.selectModuleName();
        resModel.setCode(20000);
        JSONArray response= (JSONArray) JSONArray.toJSON(list);
        resModel.setData(response);
        return resModel;

    }

    @RequestMapping(value="api/edit",method = RequestMethod.POST)
    public JSONObject edit(HttpServletRequest request, @RequestBody ServerJacocoManager serverJacocoManager){


        deleteSpace(serverJacocoManager);
        String moduleName=serverJacocoManager.getModuleName();
        Integer serverId= superJacocoDAO.selectIdByModuleName(moduleName);
        serverJacocoManager.setSuperJacocoId(serverId);
        int insertRes= serverJacocoManagerDAO.updateByPrimaryKeySelective(serverJacocoManager);
        ResModel resModel= new ResModel();


        resModel.setCode(20000);
        resModel.setData(insertRes);

        String response = JSON.toJSONString(resModel);
        JSONObject responses = (JSONObject) JSONObject.parseObject(response);

        return responses;

    }


    @RequestMapping(value="triger/coverage",method = RequestMethod.POST)
    public JSONObject coverageCal(HttpServletRequest request, @RequestBody ServerJacocoManager serverJacocoManager){


        deleteSpace(serverJacocoManager);
        String res= serverJacocoManagerServer.triggerCoverage(serverJacocoManager);
        ResModel resModel= new ResModel();


        resModel.setCode(20000);
        resModel.setData(res);

        String response = JSON.toJSONString(resModel);
        JSONObject responses = (JSONObject) JSONObject.parseObject(response);

        return responses;

    }

      @RequestMapping(value="triger/report/coverage",method = RequestMethod.POST)
    public JSONObject coverageReportCal(HttpServletRequest request, @RequestBody ServerJacocoManager serverJacocoManager){


        deleteSpace(serverJacocoManager);
        Integer id=serverJacocoManager.getId();
        ServerJacocoManager tmp=  serverJacocoManagerDAO.selectByPrimaryKey(id);
        tmp.setBaseVersion(serverJacocoManager.getBaseVersion());
        tmp.setNowVersion(serverJacocoManager.getNowVersion());
        tmp.setJacocoType(serverJacocoManager.getJacocoType());
        String res= serverJacocoManagerServer.triggerCoverage(tmp);
        ResModel resModel= new ResModel();


        resModel.setCode(20000);
        resModel.setData(res);

        String response = JSON.toJSONString(resModel);
        JSONObject responses = (JSONObject) JSONObject.parseObject(response);

        return responses;

    }

    @RequestMapping(value="triger/unit/coverage",method = RequestMethod.POST)
    public JSONObject coverageUnitTriger(HttpServletRequest request, @RequestBody ServerJacocoManager serverJacocoManager){


        deleteSpace(serverJacocoManager);
        Integer id=serverJacocoManager.getId();
        ServerJacocoManager tmp=  serverJacocoManagerDAO.selectByPrimaryKey(id);
        tmp.setBaseVersion(serverJacocoManager.getBaseVersion());
        tmp.setNowVersion(serverJacocoManager.getNowVersion());
        tmp.setJacocoType(serverJacocoManager.getJacocoType());
        String res= serverJacocoManagerServer.triggerUnitCoverage(tmp);
        ResModel resModel= new ResModel();


        resModel.setCode(20000);
        resModel.setData(res);

        String response = JSON.toJSONString(resModel);
        JSONObject responses = (JSONObject) JSONObject.parseObject(response);

        return responses;

    }

    @RequestMapping(value="triger/version/coverage",method = RequestMethod.POST)
    public JSONObject versionCoverageReportCal(HttpServletRequest request, @RequestBody ServerJacocoManagerVersion serverJacocoManager){


        deleteSpace(serverJacocoManager);
        Integer id=serverJacocoManager.getId();
        ServerJacocoManager tmp=  serverJacocoManagerDAO.selectByPrimaryKey(id);
        tmp.setBaseVersion(serverJacocoManager.getBaseVersion());
        tmp.setNowVersion(serverJacocoManager.getNowVersion());
        tmp.setJacocoType(serverJacocoManager.getJacocoType());
      // String version=serverJacocoManager.getTestVersion().trim();
        Integer versionId=serverJacocoManager.getVersionId();
        String res= serverJacocoManagerServer.triggerVersionCoverage(tmp,versionId);
        ResModel resModel= new ResModel();


        resModel.setCode(20000);
        resModel.setData(res);

        String response = JSON.toJSONString(resModel);
        JSONObject responses = (JSONObject) JSONObject.parseObject(response);

        return responses;

    }
    @RequestMapping(value="api/delete",method = RequestMethod.POST)
    public JSONObject delete(@RequestParam("id")  Integer id){



        int insertRes= serverJacocoManagerDAO.deleteByPrimaryKey(id);
        ResModel resModel= new ResModel();


        resModel.setCode(20000);
        resModel.setData(insertRes);

        String response = JSON.toJSONString(resModel);
        JSONObject responses = (JSONObject) JSONObject.parseObject(response);

        return responses;

    }

    @RequestMapping(value="server/configs",method = RequestMethod.GET)
    public ResModel getConfigs(@RequestParam("moduleName")  String moduleName){
        ResModel resModel= new ResModel();
        resModel.setCode(20000);

        List<ServerJacocoManager> list=serverJacocoManagerDAO.selectConfigs(moduleName);
        JSONArray response= (JSONArray) JSONArray.toJSON(list);
        resModel.setData(response);
        return resModel;

    }


    private void deleteSpace(ServerJacocoManager var) {
        if (var.getBaseVersion() != null) {
            var.setBaseVersion(var.getBaseVersion().trim());
        }
        if (var.getGitUrl() != null) {
            var.setGitUrl(var.getGitUrl().trim());
        }
        if (var.getModuleName() != null) {
            var.setModuleName(var.getModuleName().trim());
        }
        if (var.getNowVersion() != null) {
            var.setNowVersion(var.getNowVersion().trim());
        }
        if (var.getPortJacoco() != null) {
            var.setPortJacoco(var.getPortJacoco().trim());
        }

        if (var.getSubModule() != null) {
            var.setSubModule(var.getSubModule().trim());
        }
        if (var.getServerAddress() != null) {
            var.setServerAddress(var.getServerAddress().trim());
        }
        if (var.getServerName() != null) {
            var.setServerName(var.getServerName().trim());
        }
    }
}
