package com.shebei.api.equipment_api_test.vueController;


import cn.hutool.http.HttpUtil;

import com.shebei.api.equipment_api_test.service.vueservice.CallBackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("callBack")
public class CallBackController {

    @Autowired
    CallBackService callBackService;
//停车场回调接口

    @RequestMapping(value="carpak/back/maneger",method = RequestMethod.POST)
    public void userCaseCopy(HttpServletRequest request, HttpServletResponse response){
         String body="";
         try{
             body =request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
         }catch (IOException e){
             e.printStackTrace();
         }
        System.out.println("收到回调："+body);
        Map<String, String> map = HttpUtil.decodeParamMap(body, "UTF-8");

       // System.out.println("map:"+ map.toString());
        callBackService.saveCallBack(map,"回调");



    }



}
