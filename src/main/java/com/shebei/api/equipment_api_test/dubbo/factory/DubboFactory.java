//package com.shebei.api.equipment_api_test.dubbo.factory;
//
//import com.shebei.api.equipment_api_test.dubbo.dubboClient.BusServiceInvokeFacadeDubbo;
//import com.shebei.api.equipment_api_test.dubbo.dubboClient.DubboBase;
//import com.shebei.api.equipment_api_test.dubbo.dubboClient.EventDubbo;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//@Component
//public class DubboFactory {
//
//    @Autowired
//    EventDubbo eventDubbo;
//    @Autowired
//    BusServiceInvokeFacadeDubbo busServiceInvokeFacadeDubbo;
//
//    public DubboBase getDubboApi(String dubboType){
//
//       if(dubboType.equals("EventRecordFacade")){
//           return eventDubbo;
//       }
//
//
//        if(dubboType.equals("ServiceInvokeFacade")){
//            return busServiceInvokeFacadeDubbo;
//        }
//
//        return null;
//    }
//
//}
