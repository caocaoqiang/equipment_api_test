//package com.shebei.api.equipment_api_test.dubbo;
//
//import com.uniubi.common.model.storage.StorageObjectModel;
//
//public class EventResponse extends BaseResponse {
//    private long eventId;
//    private long startTime;
//    private StorageObjectModel objectModel;
//
//    public long getEventId() {
//        return eventId;
//    }
//
//    public void setEventId(long eventId) {
//        this.eventId = eventId;
//    }
//
//    public long getStartTime() {
//        return startTime;
//    }
//
//    public void setStartTime(long startTime) {
//        this.startTime = startTime;
//    }
//
//    public StorageObjectModel getObjectModel() {
//        return objectModel;
//    }
//
//    public void setObjectModel(StorageObjectModel objectModel) {
//        this.objectModel = objectModel;
//    }
//}