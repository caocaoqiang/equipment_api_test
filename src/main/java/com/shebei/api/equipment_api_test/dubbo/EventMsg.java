//package com.shebei.api.equipment_api_test.dubbo;
//
//import com.uniubi.cloud.record.core.model.internal.util.GuidUtil;
//import com.uniubi.cloud.record.facade.api.event.EventRecordFacade;
//import com.uniubi.cloud.record.facade.request.event.EventRecordRequest;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//
//public class EventMsg {
//
//    private static final ApplicationContext context = new ClassPathXmlApplicationContext("spring-consumer.xml");
//    private static EventRecordFacade eventRecordFacade = (EventRecordFacade) context.getBean("eventRecordFacade");
//
//    public void runTest() {
//
//        long now = System.currentTimeMillis();
//
//        /**
//         * 模拟数据时需修改以下参数
//         * test03
//         */
//        Long userId = 1856L;
//        Long appId = 18560158L;
//      //  Long deviceId = 18560158000062L;
//        Long deviceId = 18560158000061L;
//     //   String unifiedId = "183681026261593653259301";
//        String unifiedId = "183681026261593653259302";
//        String deviceNo = "4130136";
//        Integer channelId = 0;
//        Integer source = 510004;
//
//
//        /**
//         * test02
//         */
//        //Long userId = 2005L;
//        //Long appId = 20050001L;
//        //Long deviceId = 20050001000016L;
//        //String unifiedId = "159900778961614846754484";
//        //String deviceNo = "4190257";
//        //Integer channelId = 0;
//        //Integer source = 520004;
//
//
//        Integer eventType = 51;
//        Integer type = 32;
//      //  String dataMsg = "P/AxARUFGgoNBhFnASIBWgiYAAAAAAYQAAAAAxA="; //003
//        String dataMsg="PwVYFQUbEycQAAAAAAAQAAAHICAAAJcAAAIAAAA=";
//        /**
//        String attributes = "{\n" +
//                "  \"version\": 1,\n" +
//                "  \"type\":" + type + ",\n" +
//                "  \"consumer\": false,\n" +
//                "  \"createTime\": " + now + ",\n" +
//                "  \"dataMsg\": " + dataMsg + ",\n" +
//                "  \"deviceModel\": {\n" +
//                "    \"appId\": "+appId+",\n" +
//                "    \"channelId\": "+channelId+",\n" +
//                "    \"deviceId\": "+deviceId+",\n" +
//                "    \"deviceNo\": "+deviceNo+",\n" +
//                "    \"did\": "+deviceId+",\n" +
//                "    \"emptyAppId\": false,\n" +
//                "    \"emptyDevcieNo\": false,\n" +
//                "    \"emptyDeviceId\": false,\n" +
//                "    \"emptySceneId\": true,\n" +
//                "    \"emptyUserId\": false,\n" +
//                "    \"productKey\": \"\",\n" +
//                "    \"serverIp\": 0,\n" +
//                "    \"source\": "+source+",\n" +
//                "    \"status\": 0,\n" +
//                "    \"uid\": "+userId+",\n" +
//                "    \"unifiedId\": "+unifiedId+",\n" +
//                "    \"userId\": "+userId+",\n" +
//                "    \"version\": \"\"\n" +
//                "  },\n" +
//                "  \"deviceNo\": "+deviceNo+",\n" +
//                "  \"endCode\": -869022916,\n" +
//                "  \"headCode\": -23206,\n" +
//                "  \"length\": 33,\n" +
//                "  \"linkTime\": " + now + ",\n" +
//                "  \"sendTime\": " + now + ",\n" +
//                "  \"sourceType\": \"CLOUD_DATA\",\n" +
//                "  \"spenId\": 0,\n" +
//                "  \"valid\": 20\n" +
//                "}";
//
//         **/
//
//        String attributes = "{\"consumer\":false,\"createTime\":"+now+",\"dataMsg\":\""+dataMsg+"\",\"deviceModel\":{\"appId\":"+appId+",\"channelId\":"+channelId+",\"deviceId\":"+deviceId+",\"deviceNo\":\""+deviceNo+"\",\"did\":"+deviceId+",\"emptyAppId\":false,\"emptyDevcieNo\":false,\"emptyDeviceId\":false,\"emptySceneId\":true,\"emptyUserId\":false,\"productKey\":\"\",\"serverIp\":0,\"source\":"+source+",\"status\":0,\"uid\":"+userId+",\"unifiedId\":\""+unifiedId+"\",\"userId\":"+userId+",\"version\":\"\"},\"deviceNo\":\""+deviceNo+"\",\"endCode\":-869022916,\"headCode\":-23206,\"length\":33,\"linkTime\":"+now+",\"sendTime\":"+now+",\"sourceType\":\"CLOUD_DATA\",\"spenId\":0,\"type\":"+type+",\"valid\":-105,\"version\":21}";
//
//
//        //attributes.trim();
//
//
//        //此处可以增加请求的label，也可以就这样
//        // sr.setSampleLabel(title);
//        EventRecordRequest resquest = new EventRecordRequest();
//        resquest.setAppId(appId);
//        resquest.setAttributes(attributes);
//        resquest.setDeviceId(deviceId);
//        resquest.setDeviceNo(deviceNo);
//        resquest.setEndTime(now);
//        resquest.setEventId(4755490165872742000L);
//        resquest.setEventType(eventType);
//        resquest.setProductKey("");
//        resquest.setUserId(userId);
//        resquest.setStartTime(now);
//        resquest.setRequestId(GuidUtil.newGuid());
//        resquest.setChannelId(channelId);
//        resquest.setStorageObject("");
//        resquest.setType(1);
//        resquest.setDeviceVersion("");
//
//
//        try {
//            System.out.println(eventRecordFacade.saveEventRecord(resquest));
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw e;
//        }
//
//    }
//
//    public  String  sendMsg(EventRecordRequest eventRecordRequest){
//       String res=eventRecordFacade.saveEventRecord(eventRecordRequest).toString();
//
//       return res;
//
//    }
//
//
//    public static void main(String[] args) throws InterruptedException {
//        EventMsg eventMsg = new EventMsg();
//
//        eventMsg.runTest();
//
//    }
//}
