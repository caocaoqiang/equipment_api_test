//package com.shebei.api.equipment_api_test.dubbo.dubboClient;
//
//
//import com.alibaba.fastjson.JSONObject;
//import com.uniubi.cloud.bus.facade.sis.api.ServiceInvokeFacade;
//import com.uniubi.cloud.bus.facade.sis.request.GetCompleteSyncMsgResultServiceRequest;
//import com.uniubi.cloud.bus.facade.sis.request.InvokeThingServiceRequest;
//import com.uniubi.cloud.bus.facade.sis.response.GetCompleteSyncMsgResultServiceResponse;
//import com.uniubi.cloud.bus.facade.sis.response.InvokeThingServiceResponse;
//import com.uniubi.cloud.record.core.model.internal.util.GuidUtil;
//import com.uniubi.cloud.record.facade.api.event.EventRecordFacade;
//import com.uniubi.cloud.record.facade.request.event.EventRecordRequest;
//import com.uniubi.common.response.ResponseLinkModel;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.springframework.stereotype.Component;
//
//
//@Component
//public class BusServiceInvokeFacadeDubbo implements DubboBase {
//
//    private static final ApplicationContext context = new ClassPathXmlApplicationContext("spring-consumer.xml");
//    private static ServiceInvokeFacade serviceInvokeFacade = (ServiceInvokeFacade) context.getBean("serviceInvokeFacade");
//
//
//    @Override
//    public String sendMsg(String jsonString,String method) {
//        if(method.equals("invokeThingService")){
//            return send1(jsonString);
//        }
//
//        if(method.equals("getCompleteSyncMsgResultService")){
//            return send2(jsonString);
//        }
//
//        return "没有获取到dubbo接口的method";
//    }
//
//
//
//    private String send1(String jsonString){  //invokeThingService
//        try {
//
//
//            // List<EventRecordRequest> dataArr = JSONArray.parseArray(jsonString,EventRecordRequest.class);
//            InvokeThingServiceRequest recordRequest = JSONObject.parseObject(jsonString, InvokeThingServiceRequest.class);
//            recordRequest.setRequestId(GuidUtil.newGuid());
//
//            ResponseLinkModel<InvokeThingServiceResponse> res = serviceInvokeFacade.invokeThingService(recordRequest);
//            if(res ==null){
//                return "返回null";
//            }
//            String response=res.toString();
//            return response;
//
//        } catch (Exception e) {
//            return e.getMessage();
//
//        }
//    }
//
//    private String send2(String jsonString){  //getCompleteSyncMsgResultService
//        try {
//
//
//            // List<EventRecordRequest> dataArr = JSONArray.parseArray(jsonString,EventRecordRequest.class);
//            GetCompleteSyncMsgResultServiceRequest recordRequest = JSONObject.parseObject(jsonString, GetCompleteSyncMsgResultServiceRequest.class);
//            recordRequest.setRequestId(GuidUtil.newGuid());
//
//            ResponseLinkModel<GetCompleteSyncMsgResultServiceResponse> res = serviceInvokeFacade.getCompleteSyncMsgResultService(recordRequest);
//            if(res ==null){
//                return "返回null";
//            }
//            String response=res.toString();
//            return response;
//
//        } catch (Exception e) {
//            return e.getMessage();
//
//        }
//    }
//
//
//}
