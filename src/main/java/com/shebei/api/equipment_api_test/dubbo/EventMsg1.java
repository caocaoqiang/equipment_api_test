//package com.shebei.api.equipment_api_test.dubbo;
//
//import com.uniubi.cloud.record.core.model.event.EventRecognition;
//import com.uniubi.cloud.record.core.model.internal.util.GuidUtil;
//import com.uniubi.cloud.record.facade.api.event.EventRecordFacade;
//import com.uniubi.cloud.record.facade.request.event.EventRecordRequest;
//import com.uniubi.common.response.ResponseLinkModel;
//
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//
//public class EventMsg1 {
//
//    private static final ApplicationContext context = new ClassPathXmlApplicationContext("spring-consumer.xml");
//    private static EventRecordFacade eventRecordFacade =(EventRecordFacade)context.getBean("eventRecordFacade");
//
//    public void runTest() {
//        String attributes="{\n" +
//                "  \"version\": 1,\n" +
//                "  \"type\": 16,\n" +
//                "  \"consumer\": false,\n" +
//                "  \"createTime\": 1622209651309,\n" +
//                "  \"dataMsg\": \"PwVYFQUcFS8vAAAAAAbLYwAFrQQFxQIBAAAIAAA\",\n" +
//                "  \"deviceModel\": {\n" +
//                "    \"appId\": 18560002,\n" +
//                "    \"channelId\": 0,\n" +
//                "    \"deviceId\": 18560002001662,\n" +
//                "    \"deviceNo\": \"4130136\",\n" +
//                "    \"did\": 18560002001662,\n" +
//                "    \"emptyAppId\": false,\n" +
//                "    \"emptyDevcieNo\": false,\n" +
//                "    \"emptyDeviceId\": false,\n" +
//                "    \"emptySceneId\": true,\n" +
//                "    \"emptyUserId\": false,\n" +
//                "    \"productKey\": \"\",\n" +
//                "    \"serverIp\": 0,\n" +
//                "    \"source\": 510004,\n" +
//                "    \"status\": 0,\n" +
//                "    \"uid\": 1856,\n" +
//                "    \"unifiedId\": \"183681026261593653259301\",\n" +
//                "    \"userId\": 1856,\n" +
//                "    \"version\": \"\"\n" +
//                "  },\n" +
//                "  \"deviceNo\": \"4130136\",\n" +
//                "  \"endCode\": -869022916,\n" +
//                "  \"headCode\": -23206,\n" +
//                "  \"length\": 33,\n" +
//                "  \"linkTime\": 1622209651309,\n" +
//                "  \"sendTime\": 1622209651309,\n" +
//                "  \"sourceType\": \"CLOUD_DATA\",\n" +
//                "  \"spenId\": 0,\n" +
//                "  \"valid\": 20\n" +
//                "}";
//        String requestId = GuidUtil.newGuid();
//        Long timeSlotId=20000l;
//
//
//            //此处可以增加请求的label，也可以就这样
//            // sr.setSampleLabel(title);
//            EventRecordRequest resquest = new EventRecordRequest();
//            resquest.setAppId(18560158L);
//            resquest.setAttributes(attributes);
//            resquest.setDeviceId(18560002001662L);
//            resquest.setDeviceNo("4130136");
//        resquest.setEndTime(1622209651309L);
//        resquest.setEventId(4755490165872742000L);
//        resquest.setEventType(52);
//            resquest.setProductKey("");
//            resquest.setUserId(1856L);
//        resquest.setStartTime(1622209651309L);
//            resquest.setRequestId(requestId);
//            resquest.setChannelId(0);
//        resquest.setStorageObject("");
//            resquest.setType(1);
//        resquest.setDeviceVersion("");
//
//
////            EventRecognition eventRecognition=getEventRe();
////
////
////            resquest.setAttributes(JSONObject.toJSONString(eventRecognition));
//
//
//            ResponseLinkModel responseData=eventRecordFacade.saveEventRecord(resquest);
//
//            String code=responseData.getCode();
//            String message=responseData.getMsg();
//            if(message!=null&&message.equals("success")){
//                System.out.println("requestId: "+requestId);
//
//                System.out.println("code: "+code);
//
//            }else {
//                System.out.println("***requestId*** = :"+ requestId +"error nessage:"+message);
//
//            }
//
//
//    }
//
//
//
//
//    public static EventRecognition eventRecognition=null;
//
//   public static EventRecognition getEventRe(){
//        if (eventRecognition==null){
//
//            EventRecognition eventRecognition = new EventRecognition();
//            eventRecognition.setAdminId("18560000573963");
//            eventRecognition.setDeviceId(18560002000009L);
//            eventRecognition.setDeviceNo("84E0F4226236607A");
//            eventRecognition.setProductKey("WO");
//            eventRecognition.setAdminName("jmeter测试");
//            eventRecognition.setAppId(18560002L);
//            eventRecognition.setDeviceName("jmeter_5c");
//            eventRecognition.setDeviceIp("192.168.72.124");
//            eventRecognition.setRecMode(1);
//            eventRecognition.setShowTime(1599036656543L);
//            eventRecognition.setFilePath("https://aiot-images-test.oss-cn-hangzhou.aliyuncs.com/device/84E0F4225070527A/20200902165057_969_rgb.jpg");
//            eventRecognition.setAliveType(1);
//            eventRecognition.setRecScore("0");
//            eventRecognition.setDeviceVersion("GD-V5.1108");
//            eventRecognition.setRecType(1);
//            eventRecognition.setPermissionTimeType(3);
//            eventRecognition.setPassTimeType(3);
//            eventRecognition.setRecModeType(3);
//
//                return   eventRecognition;
//        }else {
//            return eventRecognition;
//        }
//   }
//
//
//    public static void main(String[] args) {
//        EventMsg1 eventMsg=new EventMsg1();
//
//        eventMsg.runTest();
//    }
//}
