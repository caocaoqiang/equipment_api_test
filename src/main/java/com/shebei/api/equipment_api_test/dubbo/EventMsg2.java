//package com.shebei.api.equipment_api_test.dubbo;
//
//import com.uniubi.cloud.record.core.model.internal.util.GuidUtil;
//import com.uniubi.cloud.record.facade.api.event.EventRecordFacade;
//import com.uniubi.cloud.record.facade.request.event.EventRecordRequest;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//
//public class EventMsg2 {
//
//    private static final ApplicationContext context = new ClassPathXmlApplicationContext("spring-consumer.xml");
//    private static EventRecordFacade eventRecordFacade = (EventRecordFacade) context.getBean("eventRecordFacade");
//
//    public void runTest() {
//
//        long createTime = System.currentTimeMillis();
//
//        Integer eventType = 52;
//        Integer type = 16;
//        String dataMsg = "2222222222";
//
//        String attributes = "{\n" +
//                "  \"version\": 1,\n" +
//                "  \"type\": " + type + ",\n" +
//                "  \"consumer\": false,\n" +
//                "  \"createTime\": " + createTime + ",\n" +
//                "  \"dataMsg\": " + dataMsg + ",\n" +
//                "  \"deviceModel\": {\n" +
//                "    \"appId\": 18560002,\n" +
//                "    \"channelId\": 0,\n" +
//                "    \"deviceId\": 18560002001662,\n" +
//                "    \"deviceNo\": \"4130136\",\n" +
//                "    \"did\": 18560002001662,\n" +
//                "    \"emptyAppId\": false,\n" +
//                "    \"emptyDevcieNo\": false,\n" +
//                "    \"emptyDeviceId\": false,\n" +
//                "    \"emptySceneId\": true,\n" +
//                "    \"emptyUserId\": false,\n" +
//                "    \"productKey\": \"\",\n" +
//                "    \"serverIp\": 0,\n" +
//                "    \"source\": 510004,\n" +
//                "    \"status\": 0,\n" +
//                "    \"uid\": 1856,\n" +
//                "    \"unifiedId\": \"183681026261593653259301\",\n" +
//                "    \"userId\": 1856,\n" +
//                "    \"version\": \"\"\n" +
//                "  },\n" +
//                "  \"deviceNo\": \"4130136\",\n" +
//                "  \"endCode\": -869022916,\n" +
//                "  \"headCode\": -23206,\n" +
//                "  \"length\": 33,\n" +
//                "  \"linkTime\": " + createTime + ",\n" +
//                "  \"sendTime\": " + createTime + ",\n" +
//                "  \"sourceType\": \"CLOUD_DATA\",\n" +
//                "  \"spenId\": 0,\n" +
//                "  \"valid\": 20\n" +
//                "}";
//
//
//        //此处可以增加请求的label，也可以就这样
//        // sr.setSampleLabel(title);
//        EventRecordRequest resquest = new EventRecordRequest();
//        resquest.setAppId(18560002L);
//        resquest.setAttributes(attributes);
//        resquest.setDeviceId(18560002001662L);
//        resquest.setDeviceNo("4130136");
//        resquest.setEndTime(1622209651309L);
//        resquest.setEventId(4755490165872742000L);
//        resquest.setEventType(eventType);
//        resquest.setProductKey("");
//        resquest.setUserId(1856L);
//        resquest.setStartTime(1622209651309L);
//        resquest.setRequestId(GuidUtil.newGuid());
//        resquest.setChannelId(0);
//        resquest.setStorageObject("");
//        resquest.setType(1);
//        resquest.setDeviceVersion("");
//
//
//        try {
//            System.out.println(eventRecordFacade.saveEventRecord(resquest));
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw e;
//        }
//
//    }
//
//
//    public static void main(String[] args) throws InterruptedException {
//        EventMsg2 eventMsg = new EventMsg2();
//
//        eventMsg.runTest();
//
//    }
//}
