package com.shebei.api.equipment_api_test.dao;

import com.shebei.api.equipment_api_test.model.CallbackReceipt;

import javax.annotation.Resource;
import java.util.List;

@Resource
public interface CallbackReceiptDAO {
    int deleteByPrimaryKey(Integer id);

    int insert(CallbackReceipt record);

    int insertSelective(CallbackReceipt record);

    CallbackReceipt selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CallbackReceipt record);

    int updateByPrimaryKeyWithBLOBs(CallbackReceipt record);

    int updateByPrimaryKey(CallbackReceipt record);

     List<CallbackReceipt> selectByScene(CallbackReceipt record);

    int deleteByScene(CallbackReceipt record);
}