package com.shebei.api.equipment_api_test.dao;

import com.shebei.api.equipment_api_test.model.JacocoReport;
import com.shebei.api.equipment_api_test.model.JacocoReportExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface JacocoReportDAO {
    long countByExample(JacocoReportExample example);

    int deleteByExample(JacocoReportExample example);

    int deleteByPrimaryKey(String recordUuid);

    int insert(JacocoReport record);

    int insertSelective(JacocoReport record);

    List<JacocoReport> selectByExample(JacocoReportExample example);

    JacocoReport selectByPrimaryKey(String recordUuid);

    int updateByExampleSelective(@Param("record") JacocoReport record, @Param("example") JacocoReportExample example);

    int updateByExample(@Param("record") JacocoReport record, @Param("example") JacocoReportExample example);

    int updateByPrimaryKeySelective(JacocoReport record);

    int updateByPrimaryKey(JacocoReport record);
    List<JacocoReport> selectList(JacocoReport record);

    List<JacocoReport> selectLastUuid(JacocoReport record);
}