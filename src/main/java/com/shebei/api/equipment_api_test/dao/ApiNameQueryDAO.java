package com.shebei.api.equipment_api_test.dao;

import com.shebei.api.equipment_api_test.model.ApiNameQuery;
import com.shebei.api.equipment_api_test.model.ApiNameQueryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ApiNameQueryDAO {
    long countByExample(ApiNameQueryExample example);

    int deleteByExample(ApiNameQueryExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ApiNameQuery record);

    int insertSelective(ApiNameQuery record);

    List<ApiNameQuery> selectByExample(ApiNameQueryExample example);

    ApiNameQuery selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ApiNameQuery record, @Param("example") ApiNameQueryExample example);

    int updateByExample(@Param("record") ApiNameQuery record, @Param("example") ApiNameQueryExample example);

    int updateByPrimaryKeySelective(ApiNameQuery record);

    int updateByPrimaryKey(ApiNameQuery record);

    List<ApiNameQuery> selectApi(ApiNameQuery record);

    ApiNameQuery selectByName(String apiName);


    List<String> selectApiGroup(String apiCategory);
    int selectApiExist(String apiNavigateValue);
}