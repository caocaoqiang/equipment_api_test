package com.shebei.api.equipment_api_test.dao;

import com.shebei.api.equipment_api_test.model.SuperJacoco;

import javax.annotation.Resource;
import java.util.List;
@Resource
public interface SuperJacocoDAO {
    int deleteByPrimaryKey(Integer id);

    int insert(SuperJacoco record);

    int insertSelective(SuperJacoco record);

    SuperJacoco selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SuperJacoco record);

    int updateByPrimaryKey(SuperJacoco record);

    List<SuperJacoco> selectByModuleName(SuperJacoco record);
    List<String>  selectModuleName();

    int  selectIdByModuleName(String moduleName);
}