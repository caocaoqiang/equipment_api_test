package com.shebei.api.equipment_api_test.dao;

import com.shebei.api.equipment_api_test.model.EnviromentParameter;

import javax.annotation.Resource;
import java.util.List;

@Resource
public interface EnviromentParameterDAO {
    int deleteByPrimaryKey(Integer id);

    int insert(EnviromentParameter record);

    int insertSelective(EnviromentParameter record);

    EnviromentParameter selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(EnviromentParameter record);

    int updateByPrimaryKeyWithBLOBs(EnviromentParameter record);

    int updateByPrimaryKey(EnviromentParameter record);

    List<EnviromentParameter> selectPara(EnviromentParameter record);
    EnviromentParameter  selectByName(EnviromentParameter record);
    int updateByName(EnviromentParameter record);
    List<String> selectScene(EnviromentParameter record);
    List<String>  selectEnvList(EnviromentParameter record);

     String selectParamValue(Integer id);

}