package com.shebei.api.equipment_api_test.dao;

import com.shebei.api.equipment_api_test.model.DubboEnvConfig;

import javax.annotation.Resource;
import java.util.List;

@Resource
public interface DubboEnvConfigDAO {
    int deleteByPrimaryKey(Integer id);

    int insert(DubboEnvConfig record);

    int insertSelective(DubboEnvConfig record);

    DubboEnvConfig selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DubboEnvConfig record);

    int updateByPrimaryKey(DubboEnvConfig record);

    List<DubboEnvConfig> selectList(DubboEnvConfig record);
    List<DubboEnvConfig> selectSeverList(DubboEnvConfig record);
    List<DubboEnvConfig>  selectDubboList(DubboEnvConfig record);
    List<DubboEnvConfig> selectMethodList(DubboEnvConfig record);

    List<DubboEnvConfig> selectModuleList();
}