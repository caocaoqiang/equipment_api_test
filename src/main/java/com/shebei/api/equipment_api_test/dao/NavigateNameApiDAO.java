package com.shebei.api.equipment_api_test.dao;

import com.shebei.api.equipment_api_test.model.NavigateName;
import com.shebei.api.equipment_api_test.model.NavigateNameApi;
import com.shebei.api.equipment_api_test.model.NavigateNameApiExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface NavigateNameApiDAO {
    long countByExample(NavigateNameApiExample example);

    int deleteByExample(NavigateNameApiExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(NavigateNameApi record);

    int insertSelective(NavigateNameApi record);

    List<NavigateNameApi> selectByExample(NavigateNameApiExample example);

    NavigateNameApi selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") NavigateNameApi record, @Param("example") NavigateNameApiExample example);

    int updateByExample(@Param("record") NavigateNameApi record, @Param("example") NavigateNameApiExample example);

    int updateByPrimaryKeySelective(NavigateNameApi record);

    int updateByPrimaryKey(NavigateNameApi record);
    List<NavigateNameApi> selectGroups(@Param("module") String module, @Param("scene") String scene);

    int  deleteByUniqueKey(String uniqueValue);


    int updateByUniqueKey(NavigateNameApi record);

    List<NavigateNameApi> selectParent(@Param("module") String module, @Param("scene") String scene);

    List<NavigateNameApi> selectChild(Integer id);

    List<NavigateNameApi>  selectChildisExist(String uniqueValue);

}