package com.shebei.api.equipment_api_test.dao;

import com.shebei.api.equipment_api_test.model.ProjectModuleName;
import com.shebei.api.equipment_api_test.model.ProjectModuleNameExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import javax.annotation.Resource;

@Resource
public interface ProjectModuleNameDAO {
    long countByExample(ProjectModuleNameExample example);

    int deleteByExample(ProjectModuleNameExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ProjectModuleName record);

    int insertSelective(ProjectModuleName record);

    List<ProjectModuleName> selectByExample(ProjectModuleNameExample example);

    ProjectModuleName selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ProjectModuleName record, @Param("example") ProjectModuleNameExample example);

    int updateByExample(@Param("record") ProjectModuleName record, @Param("example") ProjectModuleNameExample example);

    int updateByPrimaryKeySelective(ProjectModuleName record);

    int updateByPrimaryKey(ProjectModuleName record);

    List<ProjectModuleName> selectLists(ProjectModuleName record);
}