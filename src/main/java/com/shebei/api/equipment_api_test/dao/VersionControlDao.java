package com.shebei.api.equipment_api_test.dao;

import com.shebei.api.equipment_api_test.model.VersionControl;
import com.shebei.api.equipment_api_test.model.VersionControlExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import javax.annotation.Resource;

@Resource
public interface VersionControlDao {
    long countByExample(VersionControlExample example);

    int deleteByExample(VersionControlExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(VersionControl record);

    int insertSelective(VersionControl record);

    List<VersionControl> selectByExample(VersionControlExample example);

    VersionControl selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") VersionControl record, @Param("example") VersionControlExample example);

    int updateByExample(@Param("record") VersionControl record, @Param("example") VersionControlExample example);

    int updateByPrimaryKeySelective(VersionControl record);

    int updateByPrimaryKey(VersionControl record);
    List<VersionControl>  selectVersions(VersionControl record);
}