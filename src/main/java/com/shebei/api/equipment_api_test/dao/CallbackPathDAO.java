package com.shebei.api.equipment_api_test.dao;

import com.shebei.api.equipment_api_test.model.CallbackPath;

import java.util.List;

public interface CallbackPathDAO {
    int deleteByPrimaryKey(Integer id);

    int insert(CallbackPath record);

    int insertSelective(CallbackPath record);

    CallbackPath selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CallbackPath record);

    int updateByPrimaryKey(CallbackPath record);

    List<CallbackPath> selectList(CallbackPath record);
}