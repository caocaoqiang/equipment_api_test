package com.shebei.api.equipment_api_test.dao;

import com.shebei.api.equipment_api_test.model.NavigateName;
import org.apache.ibatis.annotations.Param;

import javax.annotation.Resource;
import java.util.List;

@Resource
public interface NavigateNameDAO {
    int deleteByPrimaryKey(Integer id);

    int insert(NavigateName record);

    int insertSelective(NavigateName record);

    NavigateName selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(NavigateName record);

    int updateByPrimaryKey(NavigateName record);

    List<NavigateName> selectGroups(@Param("module") String module, @Param("scene") String scene);

    int  deleteByUniqueKey(String uniqueValue);

    int updateByUniqueKey(NavigateName record);

    List<NavigateName> selectParent(@Param("module") String module, @Param("scene") String scene);

    List<NavigateName> selectChild(Integer id);

    List<NavigateName>  selectChildisExist(String uniqueValue);
}