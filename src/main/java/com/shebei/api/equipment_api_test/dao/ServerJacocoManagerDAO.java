package com.shebei.api.equipment_api_test.dao;

import com.shebei.api.equipment_api_test.model.ServerJacocoManager;
import com.shebei.api.equipment_api_test.model.ServerJacocoManagerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import javax.annotation.Resource;

@Resource
public interface ServerJacocoManagerDAO {
    long countByExample(ServerJacocoManagerExample example);

    int deleteByExample(ServerJacocoManagerExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ServerJacocoManager record);

    int insertSelective(ServerJacocoManager record);

    List<ServerJacocoManager> selectByExample(ServerJacocoManagerExample example);

    ServerJacocoManager selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ServerJacocoManager record, @Param("example") ServerJacocoManagerExample example);

    int updateByExample(@Param("record") ServerJacocoManager record, @Param("example") ServerJacocoManagerExample example);

    int updateByPrimaryKeySelective(ServerJacocoManager record);

    int updateByPrimaryKey(ServerJacocoManager record);

    List<ServerJacocoManager> selectApi(ServerJacocoManager record);

    List<String>  selectModuleName();

    int selectJacocoSeverId(Integer id);
    List<ServerJacocoManager> selectConfigs(@Param("moduleName") String moduleName);
}