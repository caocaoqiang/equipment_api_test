package com.shebei.api.equipment_api_test.dao;

import com.shebei.api.equipment_api_test.model.CaseExcuteResult;
import com.shebei.api.equipment_api_test.model.CaseExcuteResultExample;
import com.shebei.api.equipment_api_test.model.CaseExcuteResultKey;
import com.shebei.api.equipment_api_test.model.CaseExcuteResultWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import javax.annotation.Resource;

@Resource
public interface CaseExcuteResultDAO {
    long countByExample(CaseExcuteResultExample example);

    int deleteByExample(CaseExcuteResultExample example);

    int deleteByPrimaryKey(CaseExcuteResultKey key);

    int insert(CaseExcuteResultWithBLOBs record);

    int insertSelective(CaseExcuteResultWithBLOBs record);

    List<CaseExcuteResultWithBLOBs> selectByExampleWithBLOBs(CaseExcuteResultExample example);

    List<CaseExcuteResult> selectByExample(CaseExcuteResultExample example);

    CaseExcuteResultWithBLOBs selectByPrimaryKey(CaseExcuteResultKey key);

    int updateByExampleSelective(@Param("record") CaseExcuteResultWithBLOBs record, @Param("example") CaseExcuteResultExample example);

    int updateByExampleWithBLOBs(@Param("record") CaseExcuteResultWithBLOBs record, @Param("example") CaseExcuteResultExample example);

    int updateByExample(@Param("record") CaseExcuteResult record, @Param("example") CaseExcuteResultExample example);

    int updateByPrimaryKeySelective(CaseExcuteResultWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(CaseExcuteResultWithBLOBs record);

    int updateByPrimaryKey(CaseExcuteResult record);

    int deleteByScene(CaseExcuteResultWithBLOBs record);

    List<CaseExcuteResultWithBLOBs> selectReportByScene(CaseExcuteResult record);

    int  selectResultCountByScene(CaseExcuteResult record);
}