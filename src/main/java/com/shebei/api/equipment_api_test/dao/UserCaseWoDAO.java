package com.shebei.api.equipment_api_test.dao;

import com.beust.jcommander.Parameter;
import com.shebei.api.equipment_api_test.model.UserCaseGroup;
import com.shebei.api.equipment_api_test.model.UserCaseWo;
import com.shebei.api.equipment_api_test.model.UserCaseWoWithBLOBs;
import org.apache.ibatis.annotations.Param;

import javax.annotation.Resource;
import java.util.List;

@Resource
public interface UserCaseWoDAO {
    int deleteByPrimaryKey(Integer id);

    int insert(UserCaseWoWithBLOBs record);

    int insertSelective(UserCaseWoWithBLOBs record);

    UserCaseWoWithBLOBs selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserCaseWoWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(UserCaseWoWithBLOBs record);

    int updateByPrimaryKey(UserCaseWo record);
    List<UserCaseWoWithBLOBs> selectCases(UserCaseWo record);

    UserCaseWoWithBLOBs  selectByUserName(String userCaseName);

    int selectCountByScene(UserCaseWo record);

    List<String> selectCasesByScene(UserCaseWo record);

    List<UserCaseGroup> selectCasesBySceneDes(UserCaseWo record);

    List<UserCaseGroup> selectCasesBySceneDesDeleted(UserCaseWo record);

    List<UserCaseWoWithBLOBs>  selectCasesVue(UserCaseWo record);

    int  updateApiGroup(UserCaseWo record);

    List<String>  selectCaseGroup(UserCaseWo record);

    List<String> selectExcutePerformance(String module);

    int  selectCaseGoup( @Param("adaptiveVersion") String adaptiveVersion);
    int updateDelete(UserCaseWo record);
}