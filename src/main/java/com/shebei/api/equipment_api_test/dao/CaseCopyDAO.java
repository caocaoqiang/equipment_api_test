package com.shebei.api.equipment_api_test.dao;

import com.shebei.api.equipment_api_test.model.CaseCopy;

import javax.annotation.Resource;
import java.util.List;

@Resource
public interface CaseCopyDAO {
    int deleteByPrimaryKey(Integer id);

    int insert(CaseCopy record);

    int insertSelective(CaseCopy record);

    CaseCopy selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CaseCopy record);

    int updateByPrimaryKeyWithBLOBs(CaseCopy record);

    int updateByPrimaryKey(CaseCopy record);

    List<CaseCopy> selectCaseCopyByScene(CaseCopy record);
}