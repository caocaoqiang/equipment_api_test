package com.shebei.api.equipment_api_test.dao;

import com.shebei.api.equipment_api_test.model.TestVersionRecord;
import com.shebei.api.equipment_api_test.model.TestVersionRecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TestVersionRecordDAO {
    long countByExample(TestVersionRecordExample example);

    int deleteByExample(TestVersionRecordExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TestVersionRecord record);

    int insertSelective(TestVersionRecord record);

    List<TestVersionRecord> selectByExample(TestVersionRecordExample example);

    TestVersionRecord selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TestVersionRecord record, @Param("example") TestVersionRecordExample example);

    int updateByExample(@Param("record") TestVersionRecord record, @Param("example") TestVersionRecordExample example);

    int updateByPrimaryKeySelective(TestVersionRecord record);

    int updateByPrimaryKey(TestVersionRecord record);


    List<TestVersionRecord> selectRecords(TestVersionRecord record);
    List<TestVersionRecord> selectLastUUid(TestVersionRecord record);
    TestVersionRecord selectByUUid( String uuidRecord);
    int updateByUuid(TestVersionRecord record);
}